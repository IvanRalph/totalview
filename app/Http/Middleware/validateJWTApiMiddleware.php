<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

class validateJWTApiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle ($request, Closure $next)
    {
        try {
            $parsedToken = JWTAuth::parseToken();
            $parsedToken->authenticate();
            $request['userData'] = $parsedToken->getPayload()->get('userData');
        } catch (TokenInvalidException $exception) {
            return response()->json(['error_message' => 'Invalid login token.']);
        } catch (TokenExpiredException $exception) {
            return response()->json(['error_message' => 'Login token has expired. Please re login your account.']);
        } catch (JWTException $exception) {
            return response()->json([
                'error_message' => 'Unknown error. Please contact administrator',
                'error'         => $exception->getMessage()
            ]);
        }

        return $next($request);
    }
}
