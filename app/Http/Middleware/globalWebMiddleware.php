<?php

namespace App\Http\Middleware;

use App\Models\Vanderbilt\Team;
use Closure;
use Modules\Cotiviti\Entities\User as User;
use Modules\Cotiviti\Entities\Schedule as Schedule;
use Modules\Main\Services\DatabaseService;

class globalWebMiddleware extends DatabaseService
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle ($request, Closure $next)
    {
        if (!session()->has('user_from_account')) {
            session()->flash('login_error', 'Session Expired. Please relogin your account.');

            return redirect('/');
        }

        if($this->checkIfTeamExists()){
            session()->flash('login_error', 'No Team Assigned. Please ask your team lead to assign you to a team.');

            return redirect('/');
        }

        $this->checkIfUserHasSchedule();

        if($this->checkIfUserIsKicked()){
            session()->flash('login_error', 'Your team\'s Shift has already ended. Please relogin your account.');

            return redirect('/');
        }

        return $next($request);
    }

    private function checkIfTeamExists(){
        $params = [
            'modelName'=>'User',
            'filter'=>[
                'main_user_id'=>session('user_from_account')->main_user_id
            ]
        ];

        return $this->fetch($params)->team == 0 ? true : false;
    }

    private function checkIfUserHasSchedule(){
        $params = [
            'modelName'=>'User',
            'join' => [
                'shift_schedule'=>[
                    'leftField'=>'user.main_user_id',
                    'operator'=>'=',
                    'rightField'=>'shift_schedule.user_id'
                ]
            ],
            'filter'=>[
                'main_user_id'=>session('user_from_account')->main_user_id
            ]
        ];

        if (!$this->fetch($params)) {
            $params = [
                'modelName'=>'User',
                'filter'=>[
                    'team'=>session('user_from_account')->team
                ]
            ];

            foreach ($this->fetchAll($params) as $user) {
                Schedule::on($this->initConnection())->insert([
                    'user_id'     => $user->main_user_id,
                    'shift_date'  => Team::on($this->initConnection())->find($user->team)->shift_date,
                    'start_shift' => '00:00:00',
                    'end_shift'   => '00:00:00',
                    'leave'       => '0'
                ]);
            }
        }
    }

    private function checkIfUserIsKicked(){
        $params = [
            'modelName'=>'User',
            'filter'=>[
                'main_user_id'=>session('user_from_account')->main_user_id
            ]
        ];

        return $this->fetch($params)->status == 0 || !session()->has('user_from_account') ? true : false;
    }
}
