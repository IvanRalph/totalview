<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class invalidateJWTMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle ($request, Closure $next)
    {
        try {
            $parsedToken         = JWTAuth::parseToken();
            $request['userData'] = $parsedToken->getPayload()->get('userData');
            $parsedToken->invalidate();

            return $next($request);
        } catch (TokenExpiredException $exception) {
            return redirect('/');
        }
    }
}