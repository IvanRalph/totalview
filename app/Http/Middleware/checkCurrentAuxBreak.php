<?php

namespace App\Http\Middleware;

use Closure;
use Modules\Main\Services\BaseAuxBreakService;

class checkCurrentAuxBreak
{
    protected $auxBreakService;

    public function __construct (BaseAuxBreakService $auxBreakService)
    {
        $this->auxBreakService = $auxBreakService;
    }

    /**
     * @param                     $request
     * @param Closure             $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userInfo = session('user_from_account');

        if (!session()->has('aux_break')) {
            $currentAuxBreak = $this->auxBreakService->getCurrent([
                'user_id'    => $userInfo->id,
                'shift_date' => $userInfo->shift_date
            ]);

            session([
                'aux_break' => $currentAuxBreak
            ]);

            return $currentAuxBreak ? redirect('/cotiviti/aux-break?token=' . $request->get('token')) : $next($request);
        }else{
            return session('aux_break') ? redirect('/cotiviti/aux-break?token=' . $request->get('token')) : $next($request);
        }
    }
}
