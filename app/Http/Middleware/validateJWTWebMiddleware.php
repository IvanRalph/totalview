<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

class validateJWTWebMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle ($request, Closure $next)
    {
        try {
            $parsedToken = JWTAuth::parseToken();
            $parsedToken->authenticate();
            $request['userData'] = $parsedToken->getPayload()->get('userData');

            view()->share('userModules', $request['userData']['modules']);

            return $next($request);
        } catch (TokenInvalidException $exception) {
            session()->flash('login_error', 'Invalid login token.');

            return redirect('/');
        } catch (TokenExpiredException $exception) {
            session()->flash('login_error', 'Login token has expired. Please re login your account.');

            return redirect('/');
        } catch (JWTException $exception) {
            session()->flash('login_error', 'Unknown error. Please contact administrator');

            return redirect('/');
        } catch (Exception $exception) {
            session()->flash('login_error', $exception->getMessage());

            return redirect('/');
        }

        return redirect('/');
    }
}
