<?php

namespace App\Http\Controllers\Web;

use App\Exceptions\Account\AccountIsLockedException;
use App\Exceptions\Account\InvalidCredentialsException;
use App\Exceptions\Account\PasswordIsExpiredException;
use App\Exceptions\Account\PasswordNeedsToResetException;
use App\Exceptions\Account\UserNotRegisteredInAccountException;
use App\Exceptions\Account\UserNotRegisteredInTotalViewException;
use App\Http\Controllers\Controller;
use App\Services\AccountService;
use App\Services\LoginService;
use App\Services\UserService;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    const STATUS_ONLINE = '1';
    const STATUS_OFFLINE = '0';

    /**
     * @var LoginService
     */
    private $loginService;

    /**
     * @var AccountService
     */
    private $accountService;

    /**
     * @var UserService
     */
    private $userService;

    public function __construct (LoginService $loginService, AccountService $accountService, UserService $userService)
    {
        $this->loginService   = $loginService;
        $this->accountService = $accountService;
        $this->userService    = $userService;
    }

    public function index ()
    {
        //$this->userService->updateFromAccount(session('user_from_account')->id, session('selected_account'), ['status' => '0']);
        $this->middleware('jwt.invalidate');

        return view('login');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function auth (Request $request)
    {
        try {
            $token = $this->loginService->handle($request->get('username'), $request->get('password'));

            return redirect()->route('accounts', ['token' => $token]);
        } catch (InvalidCredentialsException $e) {
            session()->flash('login_error', 'Invalid username or password.');

            return redirect('/');
        } catch (PasswordIsExpiredException $e) {
            session()->flash('login_error', 'Account password expired.');

            return redirect('/');
        } catch (AccountIsLockedException $e) {
            session()->flash('login_error', 'Account is locked out.');

            return redirect('/');
        } catch (PasswordNeedsToResetException $e) {
            session()->flash('login_error', 'Account password needs resetting.');

            return redirect('/');
        } catch (UserNotRegisteredInTotalViewException $e) {
            session()->flash('login_error', $e->getMessage());

            return redirect('/');
        } catch (UserNotRegisteredInAccountException $e) {
            session()->flash('login_error', $e->getMessage());

            return redirect('/');
        }
    }

    public function accounts (Request $request)
    {
        return view('accounts', ['userData' => $request->get('userData')]);
    }

    public function logout (Request $request)
    {
        $mainUser    = $request->get('userData');
        $accountInfo = session()->get('selected_account');

        session()->flush();

        $this->userService->updateFromAccount($mainUser['id'], $accountInfo, ['status' => self::STATUS_OFFLINE]);

        return redirect('/');
    }

    public function selectAccount (Request $request, $accountId)
    {
        $mainUser        = $request->get('userData');
        $accountInfo     = $this->accountService->getById($accountId);
        $userLobsInfo    = $this->userService->getLobs($mainUser['id'], $accountInfo);
        $userFromAccount = $this->userService->getFromAccount($mainUser['id'], $accountInfo);
        $this->userService->updateFromAccount($mainUser['id'], $accountInfo, ['status' => self::STATUS_ONLINE]);

        session([
            'selected_account'  => $accountInfo,
            'user_lobs_info'    => $userLobsInfo,
            'user_from_account' => $userFromAccount
        ]);

        return redirect($accountInfo->route . "?token=" . $request->get('token'));
    }
}