<?php

namespace App\Services;

class AccountService
{
    public function getById ($id)
    {
        $dbMain = \DB::connection('tv_main');

        return $dbMain->table('account')
            ->where('id', $id)
            ->first();
    }
}
