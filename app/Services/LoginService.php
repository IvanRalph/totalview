<?php

namespace App\Services;

use App\Exceptions\Account\UserNotRegisteredInAccountException;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginService
{
    /**
     * @var LdapService
     */
    private $ldapService;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var RoleService
     */
    private $roleService;

    /**
     * LoginService constructor.
     * @param LdapService $ldapService
     * @param UserService $userService
     * @param RoleService $roleService
     */
    public function __construct (LdapService $ldapService, UserService $userService, RoleService $roleService)
    {
        $this->ldapService = $ldapService;
        $this->userService = $userService;
        $this->roleService = $roleService;
    }

    public function handle ($username, $password)
    {
        $this->ldapService->login($username, $password);
        $userFromMain = $this->userService->getByUsername($username);
        $userAccounts = $this->userService->getAccounts($userFromMain->id);
        $userModules  = $this->roleService->getModules($userFromMain->role_id);

        if (!$userAccounts) {
            throw new UserNotRegisteredInAccountException("You're not registered in any account.");
        }

        $claims = $this->createCustomClaims($userFromMain, $userAccounts, $userModules);

        return JWTAuth::fromUser($userFromMain, $claims);
    }

    private function createCustomClaims ($userFromMain, $userAccounts, $userModules)
    {
        return [
            'userData' => [
                'id'         => $userFromMain->id,
                'username'   => $userFromMain->username,
                'role_id'    => $userFromMain->role_id,
                'modules'    => $userModules,
                'lastname'   => $userFromMain->lastname,
                'firstname'  => $userFromMain->firstname,
                'middlename' => $userFromMain->middlename,
                'account'    => $userAccounts
            ]
        ];
    }
}
