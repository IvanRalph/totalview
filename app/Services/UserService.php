<?php

namespace App\Services;

use App\Exceptions\Account\UserNotRegisteredInTotalViewException;
use App\Exceptions\Account\UserNotRegisteredInAccountException;

class UserService
{
    /**
     * @var AccountService
     */
    private $accountService;

    public function __construct (AccountService $accountService)
    {
        $this->accountService = $accountService;
    }

    /**
     * @param $username
     * @return array
     * @throws UserNotRegisteredInTotalViewException
     */
    public function getByUsername ($username)
    {
        $dbMain = \DB::connection('tv_main');
        $user   = $dbMain->table('user')
            ->where('username', $username)
            ->first();

        if (!$user) {
            throw new UserNotRegisteredInTotalViewException("You're not registered in Totalview");
        }

        return $user;
    }

    /**
     * @param $userId
     * @return array
     * @throws UserNotRegisteredInAccountException
     */
    public function getAccounts ($userId)
    {
        $dbMain       = \DB::connection('tv_main');
        $userAccounts = $dbMain->table('user_account')
            ->join('account', 'user_account.account_id', '=', 'account.id')
            ->select('*')
            ->where('user_account.user_id', $userId)
            ->get();

        if (!$userAccounts) {
            throw new UserNotRegisteredInAccountException("You're not registered in any account.");
        }

        return $userAccounts;
    }

    /**
     * @param $userId
     * @param $account
     * @return mixed
     */
    public function getLobs ($userId, $account)
    {
        $dbAccount = \DB::connection($account->db_name);
        $dbMain    = \DB::connection('tv_main');

        $userLobs = $dbAccount->table('user_lob')
            ->select('lob_id')
            ->where('user_id', $userId)
            ->get();

        $userLobIds = [];
        foreach ($userLobs as $userLob) {
            $userLobIds[] = $userLob->lob_id;
        }

        return $dbMain->table('lob')
            ->whereIn('id', $userLobIds)
            ->get();
    }

    public function getFromAccount ($mainUserId, $account)
    {
        $dbFromAccount   = \DB::connection($account->db_name);
        $userFromAccount = $dbFromAccount->table('user')
            ->where('main_user_id', $mainUserId)
            ->first();

        return $userFromAccount;
    }

    public function updateFromAccount ($mainUserId, $account, $updateData)
    {
        $dbFromAccount = \DB::connection($account->db_name);

        return $dbFromAccount->table('user')
            ->where('main_user_id', $mainUserId)
            ->update($updateData);
    }
}
