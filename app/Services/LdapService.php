<?php

namespace App\Services;

use App\Exceptions\Account\AccountIsLockedException;
use App\Exceptions\Account\InvalidCredentialsException;
use App\Exceptions\Account\InvalidLDAPGetEntriesParametersException;
use App\Exceptions\Account\PasswordIsExpiredException;
use App\Exceptions\Account\PasswordNeedsToResetException;
use App\Exceptions\Account\UnregisteredLdapErrorCodeException;

class LdapService
{
    const BASE_DN = 'DC=swh,DC=com';
    const LDAP_ERROR_CODES_EXCEPTIONS
        = [
            '52e' => InvalidCredentialsException::class,
            '775' => AccountIsLockedException::class,
            '532' => PasswordIsExpiredException::class,
            '773' => PasswordNeedsToResetException::class,
            '530' => 'Other Errors',
            '531' => 'Other Errors',
            '533' => 'Other Errors',
            '701' => 'Other Errors'
        ];

    /**
     * LdapService constructor.
     */
    public function __construct ()
    {
    }

    /**
     * @param $username
     * @param $password
     *
     * @return array
     */
    public function login ($username, $password)
    {
        $connection = $this->verifyLdapConnectionAndUserCredentialsOrFail(
            env('LDAP_HOSTNAME'),
            $username,
            $password
        );

        return $this->getUserInformationByUsernameOrFail($connection, $username);
    }

    /**
     * @param string $ldapHostName
     * @param string $username
     * @param string $password
     *
     * @throws InvalidCredentialsException
     * @throws UnregisteredLdapErrorCodeException
     * @return bool
     */
    protected function verifyLdapConnectionAndUserCredentialsOrFail ($ldapHostName, $username, $password)
    {
        if (!$username || !$password) {
            throw new InvalidCredentialsException('Invalid username or password.');
        }

        $connection = $this->setLdapConnection($ldapHostName);

        $result = $this->setLdapOptions($connection)
            ->bindLdap($connection, $this->buildLdapUsername($username), $password);

        if (!$result) {
            $exception = $this->getLdapError($connection);
            throw new $exception;
        }

        return $connection;
    }

    /**
     * @param string $ldapHostName
     *
     * @return resource
     */
    private function setLdapConnection ($ldapHostName)
    {
        return ldap_connect($ldapHostName);
    }

    /**
     * @param mixed $connection
     *
     * @return $this
     */
    private function setLdapOptions ($connection)
    {
        ldap_set_option($connection, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($connection, LDAP_OPT_REFERRALS, 0);

        return $this;
    }

    /**
     * @param string $connection
     * @param string $username
     * @param string $password
     *
     * @return bool
     */
    private function bindLdap ($connection, $username, $password)
    {
        return @ldap_bind($connection, $username, $password);
    }

    /**
     * @return InvalidCredentialsException|UnregisteredLdapErrorCodeException |AccountIsLockedException |
     *                                                                        PasswordIsExpiredException |
     *                                                                        PasswordNeedsToResetException
     */
    private function getLdapError ($connection)
    {
        ldap_get_option($connection, 0x0032, $extendedError);

        $errorCode = $this->getErrorCode($extendedError);

        if (array_key_exists($errorCode, self::LDAP_ERROR_CODES_EXCEPTIONS)) {
            return self::LDAP_ERROR_CODES_EXCEPTIONS[$errorCode];
        }

        return UnregisteredLdapErrorCodeException::class;
    }

    /**
     * @param string $extendedError
     *
     * @return string
     */
    private function getErrorCode ($extendedError)
    {
        $explodedError = explode(',', $extendedError);
        $explodedError = $explodedError[2];
        $explodedError = explode(' ', $explodedError);

        return $explodedError[2];
    }

    /**
     * @param string $username
     *
     * @return string
     */
    private function buildLdapUsername ($username)
    {
        return $username . env('LDAP_USERNAME_SUFFIX_URI');
    }

    /**
     * @param string $connection
     * @param string $username
     * @throws InvalidLDAPGetEntriesParametersException
     *
     * @return array
     */
    protected function getUserInformationByUsernameOrFail ($connection, $username)
    {
        $filter = '(samaccountname=' . $username . ')';

        $attributesToReturn = [
            'ou',
            'sn',
            'lastlogon',
            'givenname',
            'cn',
            'samaccountname',
            'description',
            'department',
            'mail'
        ];

        $ldapSearchResource = ldap_search(
            $connection,
            self::BASE_DN,
            $filter,
            $attributesToReturn
        );

        $userInformation = ldap_get_entries($connection, $ldapSearchResource);

        if (!$userInformation['count']) {
            throw new InvalidLDAPGetEntriesParametersException();
        }

        return $userInformation;
    }
}
