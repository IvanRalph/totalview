<?php

namespace App\Services;

class RoleService
{
    public function getModules ($roleId)
    {
        $db = \DB::connection('tv_main');

        return $db->table('role_access')
            ->join('module', 'role_access.module_id', '=', 'module.id')
            ->where('role_access.role_id', $roleId)
            ->orderBy('sequence', 'asc')
            ->get();
    }
}