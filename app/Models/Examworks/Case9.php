<?php

namespace App\Models\Examworks;

use Illuminate\Database\Eloquent\Model;

class Case8 extends Model
{
    protected $connection = 'tv_examworks';

    protected $table = 'case9';

    public $timestamps = false;

    protected $primaryKey = 'id';

}
