<?php

namespace App\Models\Examworks;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
	protected $fillable = ['shift_date'];

    protected $connection = 'tv_examworks';

    protected $table = 'user';

    public $timestamps = false;

    protected $primaryKey = 'main_user_id';

    public function cases(){
    	return $this->hasMany('Modules\Examworks\Entities\Case8', 'user_id');
    }

    public function schedules(){
    	return $this->hasMany('Modules\Examworks\Entities\Schedule', 'user_id');
    }
}
