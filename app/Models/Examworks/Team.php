<?php

namespace App\Models\Examworks;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $connection = 'tv_examworks';

    protected $table = 'team';

    public $timestamps = false;

    protected $primaryKey = 'ID';

    public function users(){
    	return $this->hasMany('App\Models\Examworks\User', 'team');
    }
}
