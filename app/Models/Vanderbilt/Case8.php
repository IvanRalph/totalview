<?php

namespace App\Models\Vanderbilt;

use Illuminate\Database\Eloquent\Model;

class Case8 extends Model
{
    protected $connection = 'tv_vanderbilt';

    protected $table = 'case8';

    public $timestamps = false;

    protected $primaryKey = 'id';

}
