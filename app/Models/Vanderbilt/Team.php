<?php

namespace App\Models\Vanderbilt;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $connection = 'tv_vanderbilt';

    protected $table = 'team';

    public $timestamps = false;

    protected $primaryKey = 'ID';

    public function users(){
    	return $this->hasMany('App\Models\Vanderbilt\User', 'team');
    }
}
