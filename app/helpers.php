<?php

function create_html_field ($field, $listValues = null)
{
    $readonly = $field->readonly == 1 ? "readonly" : "";
    $tabindex = isset($field->tabindex) && $field->tabindex != null ? "tabindex=" . $field->tabindex : "";
    switch ($field->type) {
        case 'number':
            return '<input type="number" data-lob-id="' . $field->lob_id . '" id="' . $field->html_id . '" name="' . $field->html_name . '" class="form-control" value="' . $field->default_value . '" ' . $readonly . ' ' . $tabindex . '>';
        case 'text':
            return '<input type="text" data-lob-id="' . $field->lob_id . '" id="' . $field->html_id . '" name="' . $field->html_name . '" class="form-control" value="' . $field->default_value . '" ' . $readonly . ' ' . $tabindex . '>';
        case 'textarea':
            return '<textarea id="' . $field->html_id . '" name="' . $field->html_name . '" class="form-control" value="' . $field->default_value . '" ' . $readonly . ' ' . $tabindex . '></textarea>';
        case 'date':
            return '<input type="text" id="' . $field->html_id . '" name="' . $field->html_name . '" class="form-control date-picker" value="' . $field->default_value . '" autocomplete="off" ' . $readonly . ' ' . $tabindex . '>';

        case 'radio':
            $options = '';

            foreach ($listValues as $value) {
                if ($field->id == $value->field_id) {
                    $options .= '<div class="radio-inline">';
                    $options .= '<label><input id="' . $field->html_id . '" type="radio" value="' . $value->value . '" name="' . $field->html_name . '" ' . $readonly . ' ' . $tabindex . '>' . $value->value . '</label>';
                    $options .= '</div>';
                }
            }
            $options .= '<input id="' . $field->html_id . '" type="radio" name="' . $field->html_name . '" value="" checked style="display:none;" ' . $readonly . '>';

            return $options;

        case 'dropdown':
            $class      = 'dropdown-' . $field->id . ' form-control';
            $attributes = 'data-lob-id="' . $field->lob_id . '" data-field-id="' . $field->id . '"';
            if ($field->child_field_id) {
                $attributes .= 'data-child-field-id="' . $field->child_field_id . '"';
                $class      = $class . ' parent-dropdown';
            }

            $dropdownStart = '<select id="' . $field->html_id . '" name="' . $field->html_name . '" class="' . $class . ' select2" ' . $attributes . ' ' . $readonly . '>';
            $dropdownEnd   = '</select>';
            $options       = '';
            $options       .= '<option value="" selected> </option>';

            foreach ($listValues as $value) {
                if ($field->id == $value->field_id) {
                    $selected = "";

                    if ($value->value == $field->default_value) {
                        $selected = $field->html_name != 'authority' ? "selected" : '';
                    }

                    $options .= '<option data-list-id="' . $value->id . '" value="' . $value->value . '" ' . $selected . '>' . $value->value . '</option>';
                }
            }

            return $dropdownStart . $options . $dropdownEnd;
    }
}

function duration_to_seconds ($duration)
{
    $seconds = 0;
    $timeArr = array_reverse(explode(":", $duration));

    foreach ($timeArr as $key => $value) {
        if ($key > 2) break;
        $seconds += pow(60, $key) * $value;
    }

    return $seconds;
}

function deduct_timestamp_hours ($interval)
{
    $currentTimestamp = date('Y-m-d h:i:s');
    $seconds          = $interval * 60 * 60;
    $date             = strtotime($currentTimestamp) - $seconds;

    return date("Y-m-d H:i:s", $date);
}

function generateStatusDisplay ($status)
{
    if (is_object($status)) {
        return '<span class="' . $status->class . '">' . $status->name . '</span>';
    }else{
        return '<span class="' . $status['class'] . '">' . $status['name'] . '</span>';
    }
}

