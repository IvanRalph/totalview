<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Modules\Examworks\Services\StatusService;

//Vanderbilt Models
//use Modules\Vanderbilt\Entities\Team as Vanderbilt_Team;
//use Modules\Vanderbilt\Entities\Schedule as Vanderbilt_Schedule;
//use Modules\Vanderbilt\Entities\Case8 as Vanderbilt_Case;
//use Modules\Vanderbilt\Entities\Report as Vanderbilt_Report;
//use Modules\Vanderbilt\Entities\BreakRaw as Vanderbilt_Break;
//use Modules\Vanderbilt\Entities\AuxBreak as Vanderbilt_Aux;

// Cotiviti Models
use Modules\Cotiviti\Entities\Team as Cotiviti_Team;
use Modules\Cotiviti\Entities\Schedule as Cotiviti_Schedule;
use Modules\Cotiviti\Entities\Case7 as Cotiviti_Case;
use Modules\Cotiviti\Entities\Report as Cotiviti_Report;
use Modules\Cotiviti\Entities\BreakRaw as Cotiviti_Break;
use Modules\Cotiviti\Entities\AuxBreak as Cotiviti_Aux;
use Modules\Cotiviti\Entities\Field as Cotiviti_Field;

// Examworks Models
use Modules\Examworks\Entities\Team as Examworks_Team;
use Modules\Examworks\Entities\Schedule as Examworks_Schedule;
use Modules\Examworks\Entities\Case9 as Examworks_Case;
use Modules\Examworks\Entities\Case10 as Examworks_Case2;
use Modules\Examworks\Entities\Report as Examworks_Report;
use Modules\Examworks\Entities\BreakRaw as Examworks_Break;
use Modules\Examworks\Entities\AuxBreak as Examworks_Aux;
use Modules\Examworks\Entities\Field as Examworks_Field;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands
        = [
            //
        ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule (Schedule $schedule)
    {
        $schedule->call(function () {
//            //Vanderbilt
//            foreach (Vanderbilt_Team::get() as $team) {
//                //Check if kickout
//                if (substr($team->end_time, 0, 5) == Carbon::parse(Carbon::now())->format('H:i')) {
//                    Log::info('SCHEDULE STARTED for ' . $team->description . '(VANDERBILT)');
//                    try {
//                        \DB::connection('tv_vanderbilt')->beginTransaction();
//                        $newShift = Carbon::parse($team->shift_date)->addDays(1)->format('Y-m-d');
//                        Log::info($newShift);
//
//                        Vanderbilt_Team::find($team->ID)->update(['shift_date' => $newShift]);
//
//                        //Get all user from team.
//                        foreach ($team->users as $user) {
//                            //Update all users' shift date
//                            $user->update([
//                                'shift_date'  => $newShift,
//                                'shift_start' => null,
//                                'shift_end'   => null,
//                                'status'      => '0'
//                            ]);
//
//                            //Insert shift schedule for each user
//                            Vanderbilt_Schedule::insert([
//                                'user_id'     => $user->main_user_id,
//                                'shift_date'  => $newShift,
//                                'start_shift' => $user->schedules->first() ? Vanderbilt_Schedule::where('user_id', $user->main_user_id)->orderBy('ID', 'DESC')->first()->start_shift : "00:00:00",
//                                'end_shift'   => $user->schedules->first() ? Vanderbilt_Schedule::where('user_id', $user->main_user_id)->orderBy('ID', 'DESC')->first()->end_shift : "00:00:00",
//                                'leave'       => 1
//                            ]);
//
//                            //Insert cases from previous shift to raw table
//                            foreach (Vanderbilt_Case::where('shift_date', $team->shift_date)->whereNotIn('status_id', [
//                                1,
//                                3,
//                                7
//                            ])->whereIn('user_id', $team->users->pluck('main_user_id'))->get() as $case) {
//                                Vanderbilt_Report::updateOrCreate(
//                                    [
//                                        'shift_date' => $case->shift_date,
//                                        'fin'        => $case->fin,
//                                        'case_id'    => $case->id
//                                    ],
//                                    [
//                                        'case_id'        => $case->id,
//                                        'shift_date'     => $case->shift_date,
//                                        'user_id'        => $case->user_id,
//                                        'team'           => $team->users->find($case->user_id)->team,
//                                        'start_time'     => $case->created_at,
//                                        'end_time'       => $case->updated_at,
//                                        'duration'       => $case->duration,
//                                        'hosp_serv'      => $case->hosp_serv,
//                                        'fin'            => $case->fin,
//                                        'mrn'            => $case->mrn,
//                                        'admit_date'     => $case->admit_date,
//                                        'discharge_date' => $case->discharge_date,
//                                        'price'          => $case->price,
//                                        'observation'    => $case->observation,
//                                        'hold_reason'    => $case->hold_reason,
//                                        'accountstat'    => $case->accountstat,
//                                        'status_id'      => $case->status_id
//                                    ]
//                                );
//                            }
//
//                            //Insert aux breaks from previous shift to breaks table
//                            foreach (Vanderbilt_Aux::where('shift_date', $team->shift_date)->where('user_id', $user->main_user_id)->get() as $aux) {
//                                Vanderbilt_Break::insert([
//                                    'user_id'     => $aux->user_id,
//                                    'shift_date'  => $aux->shift_date,
//                                    'aux_type_id' => $aux->aux_type_id,
//                                    'remarks'     => $aux->remarks,
//                                    'start_time'  => $aux->start_time,
//                                    'end_time'    => $aux->end_time,
//                                    'duration'    => gmdate('H:i:s', Carbon::parse($aux->start_time)->diffInSeconds(Carbon::parse($aux->end_time)))
//                                ]);
//                            }
//
//                            //Delete all cases with on going status
//                            Vanderbilt_Case::where('shift_date', $team->shift_date)->whereIn('status_id', [
//                                1,
//                                3,
//                                7
//                            ])->delete();
//
//                            //Get on hold cases
//                            $onHoldCases = Vanderbilt_Case::where('status_id', 6)->where('shift_date', $team->shift_date)->whereIn('user_id', $team->users->pluck('main_user_id'))->get();
//
//                            //Copy on hold cases to next shift
//                            foreach ($onHoldCases as $case) {
//                                $case->update(['shift_date' => $newShift]);
//                                Vanderbilt_Case::insert($case->first()->toArray());
//                            }
//                        }
//                        \DB::connection('tv_vanderbilt')->commit();
//                        Log::info('SCHEDULE RUN SUCCESS FOR ' . $team->description . '(VANDERBILT)');
//                    } catch (Exception $e) {
//                        \DB::connection('tv_vanderbilt')->rollback();
//                        Log::critical('KICKOUT FAILED FOR ' . $team->description);
//                        Log::error($e->getMessage());
//                    }
//                }
//            }
//            // <-- End of Vanderbilt -->

            //Cotiviti
            foreach (Cotiviti_Team::get() as $team) {
                //Check if kickout
                if (substr($team->end_time, 0, 5) == Carbon::parse(Carbon::now())->format('H:i')) {
                    Log::info('SCHEDULE STARTED for ' . $team->description . '(COTIVITI)');
                    if ($team->isKicked == 0) {
                        try {
                            \DB::connection('tv_cotiviti')->beginTransaction();
                            $newShift = Carbon::parse($team->shift_date)->addDays(1)->format('Y-m-d');
                            Log::info($newShift);

                            Cotiviti_Team::find($team->ID)->update(['shift_date' => $newShift]);

                            //Get all user from team.
                            foreach ($team->users as $user) {
                                //Update all users' shift date
                                $user->update([
                                    'shift_date'  => $newShift,
                                    'shift_start' => null,
                                    'shift_end'   => null,
                                    'status'      => '0'
                                ]);

                                //Insert shift schedule for each user
                                Cotiviti_Schedule::insert([
                                    'user_id'     => $user->main_user_id,
                                    'shift_date'  => $newShift,
                                    'start_shift' => $user->schedules->first() ? Cotiviti_Schedule::where('user_id', $user->main_user_id)->orderBy('ID', 'DESC')->first()->start_shift : "00:00:00",
                                    'end_shift'   => $user->schedules->first() ? Cotiviti_Schedule::where('user_id', $user->main_user_id)->orderBy('ID', 'DESC')->first()->end_shift : "00:00:00",
                                    'leave'       => 1
                                ]);

                                //Insert cases from previous shift to raw table
                                $fields = Cotiviti_Field::pluck('html_id');

                                foreach (Cotiviti_Case::where('shift_date', $team->shift_date)->whereNotIn('status_id', [
                                    1,
                                    3,
                                    7
                                ])->whereIn('user_id', $team->users->pluck('main_user_id'))->get() as $case) {
                                    $rawFields = [
                                        'caseid'          => $case->id,
                                        'shift_date'      => $case->shift_date,
                                        'userid'          => $case->user_id,
                                        'teamid'          => $team->users->find($case->user_id)->team,
                                        'timestamp_start' => $case->created_at,
                                        'timestamp_end'   => $case->updated_at,
                                        'duration'        => $case->duration
                                    ];

                                    foreach ($fields as $field) {
                                        $rawFields[$field] = $case->$field;
                                    }

                                    Cotiviti_Report::updateOrCreate(
                                        [
                                            'shift_date' => $case->shift_date,
                                            'icn_7'      => $case->icn,
                                            'caseid'     => $case->id
                                        ],
                                        $rawFields
                                    );
                                }

                                //Insert aux breaks from previous shift to breaks table
                                foreach (Cotiviti_Aux::where('shift_date', $team->shift_date)->where('user_id', $user->main_user_id)->get() as $aux) {
                                    Cotiviti_Break::insert([
                                        'user_id'     => $aux->user_id,
                                        'shift_date'  => $aux->shift_date,
                                        'aux_type_id' => $aux->aux_type_id,
                                        'remarks'     => $aux->remarks,
                                        'start_time'  => $aux->start_time,
                                        'end_time'    => $aux->end_time,
                                        'duration'    => gmdate('H:i:s', Carbon::parse($aux->start_time)->diffInSeconds(Carbon::parse($aux->end_time)))
                                    ]);
                                }

                                //Get on hold cases
                                $holdReasons = [
                                    'Scanned image corrupted (no scanned documents)',
                                    'Scanned image not readable (image not clear or half page)',
                                    'Incomplete template*',
                                    'Incomplete MR **'
                                ];
                                $onHoldCases = Cotiviti_Case::where('status_id', 6)->whereIn('hold_reason_7', $holdReasons)->where('shift_date', $team->shift_date)->whereIn('user_id', $team->users->pluck('main_user_id'))->get();

                                //Copy on hold cases to next shift
                                foreach ($onHoldCases as $case) {
                                    $case->update(['shift_date' => $newShift]);
                                }

                                //Delete all cases with on going status
                                Cotiviti_Case::where('shift_date', $team->shift_date)->whereIn('status_id', [
                                    1,
                                    3,
                                    7
                                ])->delete();
                            }

                            \DB::connection('tv_cotiviti')->commit();
                            Log::info('SCHEDULE RUN SUCCESS FOR ' . $team->description . '(COTIVITI)');
                        } catch (Exception $e) {
                            \DB::connection('tv_cotiviti')->rollback();
                            Log::error('ERROR ON LINE: ' . $e->getLine() . ' FILE: ' . $e->getFile() . ' ' . $e->getMessage());
                            Log::error('KICKOUT FAILED FOR ' . $team->description);
                        }
                    }else{
                        $team->update([
                            'isKicked'=>0
                        ]);
                        Log::error('KICKOUT FAILED FOR ' . $team->description . '(COTIVITI) - Manual Kick out');
                    }
                }
            }
            // <-- End of Cotiviti -->

            //Examworks
            foreach (Examworks_Team::get() as $team) {
                //Check if kickout
                if (substr($team->end_time, 0, 5) == Carbon::parse(Carbon::now())->format('H:i')) {
                    Log::info('SCHEDULE STARTED for ' . $team->description . '(EXAMWORKS)');
                    try {
                        \DB::connection('tv_examworks')->beginTransaction();
                        $newShift = Carbon::parse($team->shift_date)->addDays(1)->format('Y-m-d');
                        Log::info($newShift);

                        Examworks_Team::find($team->ID)->update(['shift_date' => $newShift]);

                        //Get all user from team.
                        foreach ($team->users as $user) {
                            //Update all users' shift date
                            $user->update([
                                'shift_date'  => $newShift,
                                'shift_start' => null,
                                'shift_end'   => null,
                                'status'      => '0'
                            ]);

                            //Insert shift schedule for each user
                            Examworks_Schedule::insert([
                                'user_id'     => $user->main_user_id,
                                'shift_date'  => $newShift,
                                'start_shift' => $user->schedules->first() ? Examworks_Schedule::where('user_id', $user->main_user_id)->orderBy('ID', 'DESC')->first()->start_shift : "00:00:00",
                                'end_shift'   => $user->schedules->first() ? Examworks_Schedule::where('user_id', $user->main_user_id)->orderBy('ID', 'DESC')->first()->end_shift : "00:00:00",
                                'leave'       => 1
                            ]);

                            //Insert cases from previous shift to raw table
                            $fields = Examworks_Field::pluck('html_id');

                            foreach (Examworks_Case::where('shift_date', $team->shift_date)->whereNotIn('status_id', StatusService::ONGOING_STATUS_ID)->whereIn('user_id', $team->users->pluck('main_user_id'))->get() as $case) {
                                $rawFields = [
                                    'case_id_9'  => $case->id,
                                    'shift_date' => $case->shift_date,
                                    'user_id'    => $case->user_id,
                                    'team_id'    => $team->users->find($case->user_id)->team,
                                    'status_id'  => $case->status_id,
                                    'start_time' => $case->created_at,
                                    'end_time'   => $case->updated_at,
                                    'duration'   => $case->duration,
                                    'lob_id'     => 9
                                ];

                                foreach ($fields as $field) {
                                    $rawFields[$field] = $case->$field;
                                }

                                Examworks_Report::updateOrCreate(
                                    [
                                        'shift_date' => $case->shift_date,
                                        'case_id_9'  => $case->id,
                                        'lob_id'     => 9
                                    ],
                                    $rawFields
                                );
                            }

                            foreach (Examworks_Case2::where('shift_date', $team->shift_date)->whereNotIn('status_id', StatusService::ONGOING_STATUS_ID)->whereIn('user_id', $team->users->pluck('main_user_id'))->get() as $case) {
                                $rawFields = [
                                    'case_id_10' => $case->id,
                                    'shift_date' => $case->shift_date,
                                    'user_id'    => $case->user_id,
                                    'team_id'    => $team->users->find($case->user_id)->team,
                                    'status_id'  => $case->status_id,
                                    'start_time' => $case->created_at,
                                    'end_time'   => $case->updated_at,
                                    'duration'   => $case->duration,
                                    'lob_id'     => 10
                                ];

                                foreach ($fields as $field) {
                                    $rawFields[$field] = $case->$field;
                                }

                                Examworks_Report::updateOrCreate(
                                    [
                                        'shift_date' => $case->shift_date,
                                        'case_id_10' => $case->id,
                                        'lob_id'     => 10
                                    ],
                                    $rawFields
                                );
                            }

                            //Insert aux breaks from previous shift to breaks table
                            foreach (Examworks_Aux::where('shift_date', $team->shift_date)->where('user_id', $user->main_user_id)->get() as $aux) {
                                Examworks_Break::insert([
                                    'user_id'     => $aux->user_id,
                                    'shift_date'  => $aux->shift_date,
                                    'aux_type_id' => $aux->aux_type_id,
                                    'remarks'     => $aux->remarks,
                                    'start_time'  => $aux->start_time,
                                    'end_time'    => $aux->end_time,
                                    'duration'    => gmdate('H:i:s', Carbon::parse($aux->start_time)->diffInSeconds(Carbon::parse($aux->end_time)))
                                ]);
                            }

                            //Delete all cases with on going status
                            Examworks_Case::where('shift_date', $team->shift_date)->whereIn('status_id', StatusService::ONGOING_STATUS_ID)->delete();

                            //Run volume reports stored proc
//                            DB::connection('tv_examworks')->raw('call tv_examworks.gen_Volume_Reports('. $team->shift_date .');');
                        }
                        \DB::connection('tv_examworks')->commit();
                        Log::info('SCHEDULE RUN SUCCESS FOR ' . $team->description . '(EXAMWORKS)');
                    } catch (\Exception $e) {
                        \DB::connection('tv_examworks')->rollback();
                        Log::error('ERROR ON LINE: ' . $e->getLine() . ' FILE: ' . $e->getFile() . ' ' . $e->getMessage());
                        Log::error('KICKOUT FAILED FOR ' . $team->description);
                    }
                }
            }
            // <-- End of Examworks -->
        });
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands ()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
