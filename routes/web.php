<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
    ['namespace' => 'Web'],
    function () {
        Route::get('/', 'LoginController@index');
        Route::get('/logout', 'LoginController@logout')->middleware('jwt.invalidate');
        Route::post('/auth', 'LoginController@auth');
    }
);

Route::group(
    ['namespace' => 'Web', 'middleware' => ['jwt.webauth', 'revalidate']],
    function() {
        Route::get('/accounts', 'LoginController@accounts')->name('accounts');
        Route::get('/select-account/{accountId}', 'LoginController@selectAccount');
    }
);
