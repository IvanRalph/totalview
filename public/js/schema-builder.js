var SCHEMA_BUILDER = {
    buildPost: function (type, attributes) {
        var payload = '{"data": {"type": "' + type + '","attributes": {';

        $.each(attributes, function(key, element) {
            payload+='"' + element.name + '":' + '"' + element.value + '", ';
        });

        payload = payload.slice(0, -2);

        return payload+='}}}';
    },
    buildPut: function (type, id, attributes) {
        var payload = '{"data": {"id":"' + id + '", "type": "' + type + '","attributes": {';

        $.each(attributes, function(key, element) {
            payload+='"' + element.name + '":' + '"' + element.value + '", ';
        });

        payload = payload.slice(0, -2);

        return payload+='}}}';
    }
};
