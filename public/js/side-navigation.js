var SIDE_NAVIGATION = {
    build: function() {
        this.bindSideNavigationClicker();
        this.bindSideNavigation();
        this.activateCurrentPage();
    },

    bindSideNavigationClicker: function() {
        $('.sidelinker').click(function(){
            STORAGE.set('currentNavPageId', $(this).attr('id'));
            STORAGE.set('currentPageTitle', $(this).attr('title'));
            STORAGE.set('currentNavPageIdHead', '');
        });

        $('.sidelinkerlv1').click(function(){
            STORAGE.set('currentNavPageId', $(this).attr('id'));
            STORAGE.set('currentNavPageIdHead', $(this).closest('.sidelinkerlvl1head').attr('id'));
            STORAGE.set('currentPageTitle', $(this).attr('title'));
        });
    },

    bindSideNavigation: function() {
        if (STORAGE.get('currentNavPageId') == false) {
            STORAGE.set('currentNavPageId', 'side-nav-case-tracker');
        }
    },
    activateCurrentPage: function() {
        $('#' + STORAGE.get('currentNavPageId')).addClass('active');

        if (STORAGE.get('currentNavPageIdHead') != '') {
            $('#' + STORAGE.get('currentNavPageIdHead')).addClass('active');
        } else {
            $('.sidelinkerlvl1head').removeClass('active');
        }
    },
  
};

SIDE_NAVIGATION.build();
