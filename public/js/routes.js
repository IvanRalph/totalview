var ROUTES = {

    accountRoute: '',

    setAccountRoute: function (url) {
        this.accountRoute = url;
    },

    team: function() {
        window.location.href = ROUTES.accountRoute + 'team';
    },
    caseTracker: function() {
        window.location.href = ROUTES.accountRoute + 'case-tracker';
    },
    break: function () {
        window.location.href = ROUTES.accountRoute + 'break';
    },
    logout: function() {
        window.location.href = '/auth/login';
    }
};
