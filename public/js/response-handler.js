var RESPONSE_HANDLER = {
    errorList: {
        '200': 'Success',
        '401': 'Unauthorized',
        '404': 'Not Found',
        '422': 'Validation Error',
        '500': 'Server Error'
    },
    defaultErrorMessage: 'Something went wrong :(',
    defaultSuccessMessage: 'Success! :)',
    defaultFieldValidationMessage: 'Invalid field values.',

    handleSuccess: function(responseData, message) {
        message = (!message) ? this.defaultSuccessMessage : message;

        toastr.options = {"closeButton": true, "progressBar": true};
        toastr.success(message);
    },

    handleError: function(responseData) {

        var message = '';
        var statusCode = responseData.status;
        if (this._isArrayOfErrors(responseData.responseJSON.errors)) {
            this._mapErrorsByFieldName(responseData.responseJSON.errors);
            toastr.error(this.defaultFieldValidationMessage, this.errorList[statusCode]);

            return true;
        }

        message = (!responseData.responseJSON.errors.detail) ? this.defaultErrorMessage : responseData.responseJSON.errors.detail;

        toastr.error(message, this.errorList[statusCode]);
    },

    _mapErrorsByFieldName: function(errors) {
        $.each(errors, function( key, value ) {
            var fieldName = value.detail.replace("data.attributes.", "");
            $('[name=' + fieldName + ']').parent('div').addClass('has-error');
        });
    },

    _isArrayOfErrors: function(errors) {
        return Array.isArray(errors);
    },
};
