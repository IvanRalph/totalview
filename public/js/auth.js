var AUTH = {
    doc: $(document),
    username: $('#username'),
    password: $('#password'),
    button: $('#login-button'),
    validateUri: '/sedgwick/api/validate-role',
    loginRoute: '/api/auth/login',
    logoutRoute: '/api/auth/logout',

    build: function() {
        this.bindElements();
    },

    bindElements: function() {
        this.doc.on('click', '#login-button', AUTH.login);
        this.doc.on('click', '#logout-button', AUTH.logout);
        this.doc.on('keypress', '#username', function(e) {
            AUTH._triggerLoginBtn(e);
        });
        this.doc.on('keypress', '#password', function(e) {
            AUTH._triggerLoginBtn(e);
        });
    },

    login: function() {
        $("#login-button").text("Logging in...");
    },

    logout: function() {
        var userData = {
            'username' :STORAGE.get('username'),
            'accountName': STORAGE.get('accountName'),
        };

        HTTP_REQUEST.logoutPost(AUTH.logoutRoute, userData).then(
            (response) => {
                AUTH.removeStorageData();
                ROUTES.logout();
            },
            (response) => {
                ROUTES.logout();
            }
        );
    },

    setStorageData: function(response, decodedJwt) {

        var firstLob = Object.keys(decodedJwt.userData.userLob).map(function(key) {
            return decodedJwt.userData.userLob[key];
        });

        STORAGE.set('jwtToken', response.token);
        STORAGE.set('userId', decodedJwt.userData.userId);
        STORAGE.set('shiftDate', decodedJwt.userData.shiftDate);
        STORAGE.set('userSchedule', decodedJwt.userData.userSchedule);
        STORAGE.set('username', decodedJwt.userData.username);
        STORAGE.set('roleId', decodedJwt.userData.roleId);
        STORAGE.set('accessRights', decodedJwt.userData.accessRights);
        STORAGE.set('userLob', decodedJwt.userData.userLob);
        STORAGE.set('currentUserFullName', decodedJwt.userData.fullName);
        STORAGE.set('department', decodedJwt.userData.department);
        ROUTES.setAccountRoute('/' + decodedJwt.userData.account + '/');
        STORAGE.set('teamId', decodedJwt.userData.teamId);
        STORAGE.set('accountName', decodedJwt.userData.account);
        STORAGE.set('firstLOB', firstLob[0]);
        STORAGE.set('timeIn', decodedJwt.userData.timeIn);
        STORAGE.set('timeOut', decodedJwt.userData.timeOut);
    },

    removeStorageData : function() {
        STORAGE.remove('jwtToken');
        STORAGE.remove('userId');
        STORAGE.remove('shiftDate');
        STORAGE.remove('userSchedule');
        STORAGE.remove('username');
        STORAGE.remove('roleId');
        STORAGE.remove('accessRights');
        STORAGE.remove('currentUserFullName');
        STORAGE.remove('userLob');
        STORAGE.remove('department');
        STORAGE.remove('accountUri');
        STORAGE.remove('teamId');
        STORAGE.remove('accountName');
        STORAGE.remove('currentCaseId');
        STORAGE.remove('currentNavPageId');
        STORAGE.remove('currentPageTitle');
        STORAGE.remove('currentNavPageIdHead');
        STORAGE.remove('firstLOB');
        STORAGE.remove('currentLOB');
        STORAGE.remove('timeIn');
        STORAGE.remove('timeOut');
    },

    _triggerLoginBtn: function(e) {
        var key = e.which;
        if (key == 13) {
            $("#login-button").click();
        }
    }
};

$(document).ready(function() {
    AUTH.build();
});
