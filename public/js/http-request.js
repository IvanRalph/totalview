var HTTP_REQUEST = {

    requestHeaders: {
        'Content-Type': 'application/vnd.api+json',
    },

    loginPost: function(url, formData) {
        return $.ajax({
            type: 'POST',
            url: url,
            data: formData
        });
    },

    logoutPost: function(url, formData) {
        return $.ajax({
            headers: {'Authorization': 'Bearer ' + localStorage.getItem('jwtToken')},
            type: 'POST',
            url: url,
            data: formData
        });
    },

    post: function(url, formData) {
        return $.ajax({
            headers: this.requestHeaders,
            type: 'POST',
            url: url,
            data: formData
        });
    },

    put: function(url, formData) {
        return $.ajax({
            headers: this.requestHeaders,
            type: 'PUT',
            url: url,
            data: formData
        });
    },

    patch: function(url, formData) {
        return $.ajax({
            headers: this.requestHeaders,
            type: 'PATCH',
            url: url,
            data: formData
        });
    },

    get: function(url) {
        return $.ajax({
            headers: this.requestHeaders,
            type: 'GET',
            url: url
        });
    },

    getWithParam: function(url, formData) {
        return $.ajax({
            headers: this.requestHeaders,
            type: 'GET',
            url: url,
            data: formData
        });
    },

    postUpload: function(url, formData) {
        return $.ajax({
            headers: {'Authorization': 'Bearer ' + localStorage.getItem('jwtToken')},
            type: 'POST',
            url: url,
            data: formData,
            async: true,
            cache: false,
            contentType: false,
            processData: false
        });
    }
};
