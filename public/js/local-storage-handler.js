var STORAGE = {
    set: function(key, value) {
        localStorage.setItem(key, value);
    },
    get: function(key) {
        if (localStorage.getItem(key) === null) {
            return false;
        }

        return localStorage.getItem(key);
    },
    remove: function(key) {
        localStorage.removeItem(key);
    }
};
