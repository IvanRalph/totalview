var SPINNERS = {
    bounces: function () {
        return '<div class="sk-spinner sk-spinner-three-bounce"> ' +
                '<div class="sk-bounce1"></div> ' +
                '<div class="sk-bounce2"></div> ' +
                '<div class="sk-bounce3"></div> </div>';
    },
    wave: function() {
        return '<div class="sk-spinner sk-spinner-wave">' +
            '<div class="sk-rect1"></div> ' +
            '<div class="sk-rect2"></div> ' +
            '<div class="sk-rect3"></div> ' +
            '<div class="sk-rect4"></div> ' +
            '<div class="sk-rect5"></div> ' +
            '</div>';
    },
    cube: function() {
        return '<div class="sk-spinner sk-spinner-cube-grid">' +
            '<div class="sk-cube"></div> ' +
            '<div class="sk-cube"></div> ' +
            '<div class="sk-cube"></div> ' +
            '<div class="sk-cube"></div> ' +
            '<div class="sk-cube"></div> ' +
            '<div class="sk-cube"></div> ' +
            '<div class="sk-cube"></div> ' +
            '<div class="sk-cube"></div> ' +
            '<div class="sk-cube"></div> ' +
        '</div>';
    }
};
