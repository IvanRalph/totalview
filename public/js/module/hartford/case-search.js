var CASE_SEARCH = {
    generateCaseSearchTableUri: CONSTANT.accountApiName + '/case-search',

    build: function () {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });

        CASE_SEARCH.initializeCaseSearchTable();

        $(document).on('click', '#case-search-btn', CASE_SEARCH.buildCaseSearchTable);
    },

    initializeCaseSearchTable: function () {
        CASE_SEARCH.searchCaseTable = $('#search-case-table').DataTable({
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'excel'},
                {extend: 'print'},
            ]
        });
    },

    buildCaseSearchTable: function () {
        if ($.fn.dataTable.isDataTable('#search-case-table')) {
            $('#search-case-table').DataTable().clear();
            $('#search-case-table').DataTable().destroy();
        }

        var lobId = $('#slct-lob').val();

        $.ajax({
            type: 'GET',
            url: CASE_SEARCH.generateCaseSearchTableUri + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            data: {
                'data': {
                    'start_date': $('#start-date').val(),
                    'end_date': $('#end-date').val()
                }
            },
            dataType: 'json'
        }).done(function (response) {
            var header = '';
            $.each(response.columns, function (key, val) {
                header += '<th>' + val.name + '</th>';
            });

            $('#search-case-table>thead>tr').html(header);

            $('#search-case-table').DataTable({
                "data": response.data,
                "columns": response.columns,
                "responsive": true
            });
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong');
        })
    }
};

$(document).ready(function () {
    CASE_SEARCH.build();
});