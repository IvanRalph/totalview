var CASE_TIMER = {
    build: function () {
        CASE_TIMER.initializeTimer();
    },

    initializeTimer: function () {
        $("#caseTrackerTimer").TimeCircles({
            "start": false,
            "animation": "smooth",
            "bg_width": 1,
            "fg_width": 0.08,
            "circle_bg_color": "#EEEEEE",
            "time": {
                "Days": {
                    "text": "Days",
                    "color": "#CCCCCC",
                    "show": false
                },
                "Hours": {
                    "text": "Hours",
                    "color": "#1AB394",
                    "show": true,

                },
                "Minutes": {
                    "text": "Minutes",
                    "color": "#1AB394",
                    "show": true
                },
                "Seconds": {
                    "text": "Seconds",
                    "color": "#1AB394",
                    "show": true
                }
            }
        });
    },

    startTimer: function() {
        $("#caseTrackerTimer").TimeCircles().start();
    },

    stopTimer: function() {
        $("#caseTrackerTimer").TimeCircles().stop();
    },

    restartTimer: function() {
        $("#caseTrackerTimer").data('date', '00:00:00');
        $("#caseTrackerTimer").TimeCircles().rebuild();
    },

    rebuildTimer: function() {
        $("#caseTrackerTimer").TimeCircles().rebuild();
    },

    getTimerDuration: function() {
        return TIME_HELPER.secToHHMMSS(CASE_TIMER._getTimeCircleTime());
    },

    _getTimeCircleTime: function () {
        return Math.abs($("#caseTrackerTimer").TimeCircles().getTime());
    },

    _calculateDataDateByDuration: function(duration) {
        durationMil = moment.duration(duration).asMilliseconds();
        dataDate =  moment().subtract(durationMil).format('YYYY-MM-DD HH:mm:ss')

        return dataDate;
    },

    _setTimerDataDate: function(dataDate) {
        $("#caseTrackerTimer").data('date', dataDate);
        CASE_TIMER.rebuildTimer();
    },
}

$(document).ready(function() {
    CASE_TIMER.build();
})