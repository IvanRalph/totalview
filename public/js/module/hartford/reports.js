var REPORTS = {
    doc: $(document),
    rawReportsUri: CONSTANT.accountApiName + '/reports/raw-reports-list/',
    hourlyReportsUri: CONSTANT.accountApiName + '/reports/hourly-reports-list/',
    teamList: {},
    bulkUserIds: [],

    build: function () {
        this.buildDatePicker();
        this.bind();
        this.refreshReportsTable();
    },

    bind: function () {
        $("#frmRawReports").submit(function (e) {
            var _self = $(this);
            e.stopPropagation();
            e.preventDefault();
            REPORTS.buildReportsTable(_self.serialize());
        });

        $("#frmHourlyReports").submit(function (e) {
            var _self = $(this);
            e.stopPropagation();
            e.preventDefault();
            REPORTS.buildReportsTableHourly(REPORTS._convertTimezone(_self.serialize()));
        })
    },


    buildDatePicker: function () {
        $('.input-daterange').datepicker({format: 'yyyy-mm-dd'})
    },


    buildReportsTable: function (formData) {
        var lobId = $("#slct-lob-id").val();
        REPORTS.buildReportsTableRaw(formData);
    },


    refreshReportsTable: function () {
        $('.table').DataTable({
            "width": "25%",
            'language': {
                'emptyTable': 'Please set your filter',
            },
            initComplete: function () {
            },
        })
    },

    buildReportsTableRaw: function (formData) { // reports Recon page
        $('#reports-table').DataTable().destroy();
        $('#reports-table').DataTable({
           scrollX: false,
        ajax: {
          url: this.rawReportsUri + "?" + formData + '&token=' + QUERY_STRING_HELPER.getByName('token'),
          type: 'GET',
          success: function (data) {
              if (data == "") {
                $('#reports-table').prepend('<h3>No data available .</h3>');
             }
            $('#reports-table').DataTable().destroy();
            var head = ''
            var cnt = 0
            var arrRes = new Array()
            $.each(data, function (key, value) {

              head = '<thead><tr>'
              $.each(data[key], function (col, data) {
                head += '<th>' + col + '</th>'
                arrRes[cnt++] = {'data': col}
            });
              head += '</tr></thead>';
              $('#reports-table').html(head) 
              $('#reports-table').DataTable({
                scrollX: true,
                processing: true,
                data: data,
                columns: arrRes,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                {extend: 'excel'},
                {extend: 'print'},
                ],
                columnDefs: [
                {
                  'targets': [0,2],
                  'searchable': false,
                  "width": "15%",
              },
              
              ],
               order: [[0, 'asc']],
          })
              return false;
          })
        },

    },
});
    },

    buildReportsTableHourly: function (formData) {
        $('#reports-table-hourly').DataTable().destroy();
        $('#reports-table-hourly').DataTable({
            scrollX: false,
            "language": {
                zeroRecords: "Nothing found - sorry",
                infoEmpty: "No records available",
                emptyTable: 'No Records Found!',
            },
            ajax: {
                url: REPORTS.hourlyReportsUri + "?" + formData + '&token=' + QUERY_STRING_HELPER.getByName('token'),
                type: 'GET',
                success: function (data) {
                    var hours = CONSTANT.hours;
                    var newData = [];
                    var colName = "";
                    $.each(data, function (key, value) {
                        $.each(hours, function (i, val) {
                            if (hours[i] != data[key][1] && colName != value[0]) {
                                newData.push([data[key][0], hours[i], 0]);
                            } else {
                                newData.push([data[key][0], data[key][1], data[key][2]]);
                            }
                        })
                        colName = value[0]
                    })

                    data = TABLE_PIVOT.getPivotArray(newData, 0, 1, 2);

                    $('#reports-table-hourly').DataTable().destroy();
                    var head = ''
                    var cnt = 0
                    var arrRes = new Array()
                    var newData = [];
                    $.each(data, function (key, value) {
                        head = '<thead><tr>'

                        $.each(data[key], function (col, data) {

                            if (key == 0) {
                                head += '<th>' + data + '</th>'
                            }

                            arrRes[cnt++] = {'data': col}
                        });
                        data.shift();

                        head += '</tr></thead>';
                        $('#reports-table-hourly').html(head)
                        $('#reports-table-hourly').DataTable({
                            scrollX: true,
                            processing: true,
                            data: data,
                            columns: arrRes,
                            dom: '<"html5buttons"B>lTfgitp',
                            buttons: [
                            {extend: 'excel'},
                            {extend: 'print'},
                            ],
                            columnDefs: [
                            {
                                'targets': 0,
                                'searchable': false,
                                'className': 'text-center',
                                'orderable': false,
                                "width": "15%",
                            },
                            ],
                            order: [[0, 'asc']],
                        })
                        return false
                    })
                },
            },
        });
    },

    _convertTimezone: function (formData) {

        var timezone = $('#slct-timezone-id').val();
        var start = $('#date-start').val() + CONSTANT.accountStartTime;
        var end = $('#date-end').val() + CONSTANT.accountEndTime;

        $('#date-start').val(TIMEZONE.convert(timezone, start));
        $('#date-end').val(TIMEZONE.convert(timezone, end));
        return $('#frmHourlyReports').serialize();
    },

}

$(document).ready(function () {
    REPORTS.build();
});
