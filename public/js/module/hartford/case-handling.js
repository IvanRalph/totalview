var CASE_HANDLING = {
    createCaseUri: CONSTANT.accountApiName + '/create-case',
    updateCaseUri: CONSTANT.accountApiName + '/update-case',
    trackerStatsUri: CONSTANT.accountApiName + '/tracker-stats',
    casesUri: CONSTANT.accountApiName + '/cases',
    caseUri: CONSTANT.accountApiName + '/case',
    currentBreakUri: CONSTANT.accountApiName + '/aux-break/current',
    currentCase: CONSTANT.accountApiName + '/case/current',
    auxBreakUri: CONSTANT.accountWebName + '/aux-break',
    childValuesUri: CONSTANT.accountApiName + '/child-values',
    syntaxCodesUri: CONSTANT.accountApiName + '/syntax-code',
    createSubMeasureURI : CONSTANT.accountApiName + '/create-submeasure',
    createSubMeasureAuditorURI : CONSTANT.accountApiName + '/create-submeasureAuditor',
    submeasureURI : CONSTANT.accountApiName + '/submeasure',
    syntaxReason : CONSTANT.accountApiName + '/reason',
    deleteSubmeasureURI : CONSTANT.accountApiName + '/delete-submeasure' ,
    updateSubmeasureURI : CONSTANT.accountApiName + '/update-submeasure' ,


    build: function () {
        CASE_HANDLING._setAccountColor();
        CASE_HANDLING.initializeSelect2();
        CASE_HANDLING.buildSubmeasureForm();
        CASE_HANDLING.checkCurrentAuxBreak();
        CASE_HANDLING.checkOngoingCase($('.lob-selector.active').attr('data-lob-id'));
        CASE_HANDLING.buildSyntaxCodes($('#measure').val());
        CASE_HANDLING.buildSyntaxReason();


        $('.parent-dropdown').each(function () {
            var selectedValueListId = $(this).find(":selected").attr('data-list-id');
            var lobId = $(this).attr('data-lob-id');
            var childFieldId = $(this).attr('data-child-field-id');

            CASE_HANDLING.buildChildValues(lobId, childFieldId, selectedValueListId);
        });

        $(document).on('click', '.btn-start-case', function () {
            CASE_HANDLING.startCase($(this).attr('lobid'));
        });

        $(document).on('click', '.btn-stop-case', function () {
            CASE_HANDLING.updateCase($(this).attr('lobid'), $(this).attr('actionid'));
        });

        $(document).on('click', '.btn-hold-case', function () {
            CASE_HANDLING.updateCase($(this).attr('lobid'), $(this).attr('actionid'));
        });

        $(document).on('click', '.btn-save-case', function () {
            CASE_HANDLING.saveCase($(this).attr('lobid'), $(this).attr('actionid'));
        });

        $(document).on('click', '.btn-escalate-case', function () {
            CASE_HANDLING.updateCase($(this).attr('lobid'), $(this).attr('actionid'));
        });

        $(document).on('click', '.btn-edit', function () {
            CASE_HANDLING.editCase($(this).attr('data-lob-id'), $(this).attr('data-case-id'), $(this).attr('data-action-id'));
        });

        $(document).on('click', '.btn-continue', function () {
            CASE_HANDLING.continueCase($(this).attr('data-lob-id'), $(this).attr('data-case-id'), $(this).attr('data-action-id'));
        });

        $(document).on('click', '.lob-selector', function () {
            CASE_HANDLING.buildCaseTable($(this).attr('data-lob-id'));
            CASE_HANDLING.buildSubMeasureResults($(this).attr('data-lob-id'), 0);
            CASE_HANDLING.checkOngoingCase($(this).attr('data-lob-id'));
        });

        $(document).on('click', '#btn-aux-break', function () {
            window.location.href = CASE_HANDLING.auxBreakUri + '?token=' + QUERY_STRING_HELPER.getByName('token');
        });

        $(document).on('click', '#close-validation-error-panel', function () {
            CASE_HANDLING._closeValidationErrorPanel($(this).attr('data-lob-id'));
        });

        $(document).on('change', '#measure', function () {

            CASE_HANDLING.buildSyntaxCodes($(this).val());
            CASE_HANDLING.buildSyntaxCodesDisplay($(this).find(":selected").text());

        });

        $(document).on('click', '.btn-submeasure' ,function() {
            CASE_HANDLING.saveSubMeasure($(this).attr('lobid'));
        });
        

        $(document).on('change', '.parent-dropdown', function () {
            var selectedValueListId = $(this).find(":selected").attr('data-list-id');
            var lobId = $(this).attr('data-lob-id');
            var childFieldId = $(this).attr('data-child-field-id');

            CASE_HANDLING.buildChildValues(lobId, childFieldId, selectedValueListId);

        });

        $(document).on('change', '.syntax-outcome', function () {
            $('#measure-syntax').val($(this).val());
        });

        $(document).on('click', '.btn-submeasure-remove', function () {
            var id = $(this).val();
            var lobId = $(this).attr('data-lob-id');
            CASE_HANDLING.deleteSubmeasure(id, lobId);
        });

        CASE_HANDLING.buildCaseTable($('#lob-tab li.active').attr('data-lob-id'));
    },

    startCase: function (lobId) {
        var formData = $('#frm-' + lobId).serializeArray();
        formData.push({name: 'duration', value: '00:00:00'});
        formData.push({name: 'created_at', value: TIME_HELPER.currentDateTime()});

        var payload = PAYLOAD.build(formData);

        $.ajax({
            type: 'POST',
            url: CASE_HANDLING.createCaseUri + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            data: payload,
            headers: {'Content-Type': 'application/vnd.api+json'},
            dataType: 'json'
        }).done(function (response) {
            if (typeof response.validation_errors !== 'undefined') {
                CASE_HANDLING._showValidationErrorPanel(lobId, response.validation_errors);
                return;
            }

            CASE_TIMER.startTimer();
            CASE_HANDLING._enableStopBtn();
            CASE_HANDLING._enableHoldBtn();
            CASE_HANDLING._enableEscalateBtn();
            CASE_HANDLING._enableSubmeasureBtn();
            CASE_HANDLING._disableContinueBtn();
            CASE_HANDLING._disableEditBtn();
            CASE_HANDLING._disableStartBtn();
            CASE_HANDLING._disableAuxBreakBtn();
            CASE_HANDLING._disableLobTab();
            CASE_HANDLING._closeValidationErrorPanel(lobId);

            setHiddenCaseIdValue(response.case_id);
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.');
        });
    },

    updateCase: function (lobId, actionId, _callback) {
        CASE_TIMER.stopTimer();
        CASE_HANDLING._disableStopBtn();
        CASE_HANDLING._disableHoldBtn();
        CASE_HANDLING._disableEscalateBtn();
        CASE_HANDLING._disableSubmeasureBtn();
        CASE_HANDLING._enableStartBtn();
        CASE_HANDLING._enableAuxBreakBtn();
        CASE_HANDLING._enableLobTab();


        if($('.btn-save-case').length == 0 ){ CASE_HANDLING.saveSubMeasure(lobId) };


        var formData = $('#frm-' + lobId).serializeArray();
        var duration = CASE_TIMER.getTimerDuration();
        var hiddenDuration = getHiddenDurationValue();

        if (hiddenDuration != '00:00:00' && hiddenDuration != '' && actionId != 6)
        {
            duration = hiddenDuration;
        }

        formData.push({name: 'duration', value: duration});
        formData.push({name: 'action_id', value: actionId});
        formData.push({name: 'updated_at', value: TIME_HELPER.currentDateTime()});

        var payload = PAYLOAD.build(formData);

        $.ajax({
            type: 'PUT',
            url: CASE_HANDLING.updateCaseUri + '/' + lobId + '/' + getHiddenCaseIdValue() + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            data: payload,
            headers: {'Content-Type': 'application/vnd.api+json'},
            dataType: 'json'
        }).done(function (response) {
            setHiddenCaseIdValue(response.is_success);
            CASE_HANDLING.updateTrackerStats(lobId);
            CASE_HANDLING.buildCaseTable(lobId);
            CASE_HANDLING._clearForm(lobId);
            CASE_HANDLING._clearSubmeasureResult(lobId);
            CASE_HANDLING.buildSubMeasureResults(lobId, 0);

            TOASTR_HELPER.success('Case done!');

            if (_callback) {
                _callback();
            }
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.')
        });


        CASE_TIMER.restartTimer();

    },

    continueCase: function (lobId, caseId, actionId) {
        CASE_HANDLING.populateCaseForm(lobId, caseId, actionId, function () {
            CASE_TIMER.startTimer();
        });

        CASE_HANDLING._enableStopBtn();
        CASE_HANDLING._enableHoldBtn();
        CASE_HANDLING._enableEscalateBtn();
        CASE_HANDLING._enableSubmeasureBtn();
        CASE_HANDLING._disableStartBtn();
        CASE_HANDLING._disableContinueBtn();
        CASE_HANDLING._disableEditBtn();
        CASE_HANDLING._disableAuxBreakBtn();
        CASE_HANDLING._disableLobTab();
        CASE_HANDLING.buildSubMeasureResults(lobId, caseId);
    },

    editCase: function (lobId, caseId, actionId) {
        CASE_HANDLING.populateCaseForm(lobId, caseId, actionId, function () {
            var stopBtn = $('.btn-stop-case');

            CASE_HANDLING._disableStartBtn();
            CASE_HANDLING._enableStopBtn();
            CASE_HANDLING._enableSubmeasureBtn();
            CASE_HANDLING._disableContinueBtn();
            CASE_HANDLING._disableEditBtn();
            CASE_HANDLING._disableAuxBreakBtn();
            CASE_HANDLING._disableLobTab();
            CASE_HANDLING.buildSubMeasureResults(lobId, caseId);

            stopBtn.html('SAVE');
            stopBtn.attr('actionid', 8);
            stopBtn.addClass('btn-save-case');
            stopBtn.removeClass('btn-stop-case');
        });
    },


    saveCase: function (lobId, caseId) {
        CASE_HANDLING.updateCase(lobId, caseId, function () {
            var saveBtn = $('#frm-' + lobId + ' .btn-save-case');
            saveBtn.html('STOP');
            saveBtn.attr('actionid', 4);
            saveBtn.addClass('btn-primary btn-stop-case');
            saveBtn.removeClass('btn-save-case');

            CASE_HANDLING._disableStopBtn();
            CASE_HANDLING._disableHoldBtn();
            CASE_HANDLING._disableSubmeasureBtn();
            CASE_HANDLING._enableContinueBtn();
            CASE_HANDLING._enableEditBtn();
            CASE_HANDLING._enableStartBtn();
            CASE_HANDLING._enableLobTab();
        });
    },

    updateTrackerStats: function (lobId) {
        $.ajax({
            type: 'GET',
            url: CASE_HANDLING.trackerStatsUri + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            headers: {'Content-Type': 'application/vnd.api+json'},
            dataType: 'json'
        }).done(function (response) {
            $('#completed-case-count-' + lobId).html(response.total_completed_case);
            $('#submeasure-count-' + lobId).html(response.total_submeasure_count);
            $('#aht-' + lobId).html(response.aht);
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong');
        })
    },

    deleteSubmeasure : function (id, lobId ) {
        $.ajax({
            type: 'PUT',
            url : CASE_HANDLING.deleteSubmeasureURI + '/' + id + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            headers: {'Content-Type': 'application/vnd.api+json'},
            dataType: 'json'
        }).done(function (response) {
            TOASTR_HELPER.success('Submeasure Deleted!');
            CASE_HANDLING.buildSubMeasureResults(lobId , getHiddenCaseIdValue());
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong');
        })
    },

    initializeSelect2 : function () {
        $('.select2').select2({
            width: '100%',
        });
    },

    buildSubmeasureForm : function () {
        $('.form-submeasure .form-group .inputs').children().addClass('submeasure-data');
        $('[name*=measure_code]').addClass('syntax-outcome');
    },

    buildCaseTable: function (lobId) {
        if ($.fn.dataTable.isDataTable('#processed-case-table')) {
            $('#processed-case-table').DataTable().clear();
            $('#processed-case-table').DataTable().destroy();
        }
        $('#case-table').children('.ibox-content').toggleClass('sk-loading');
        $.ajax({
            type: 'GET',
            url: CASE_HANDLING.casesUri + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            headers: {'Content-Type': 'application/vnd.api+json'},
            dataType: 'json'
        }).done(function (response) {
            var header = '';
            $.each(response.columns, function (key, val) {
                header += '<th>' + val.name + '</th>';
            });

            $('#processed-case-table>thead>tr').html(header);

            $('#processed-case-table').DataTable({
                "data": response.data,
                "columns": response.columns,
                "responsive": true
            });

            $('#case-table').children('.ibox-content').toggleClass('sk-loading');
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong');
        })
    },

    populateCaseForm: function (lobId, caseId, actionId, _callback) {
        var getCaseUri = CASE_HANDLING.caseUri + '/' + lobId + '/' + caseId + '?action=' + actionId + '&token=' + QUERY_STRING_HELPER.getByName('token');

        $.ajax({
            type: 'GET',
            url: getCaseUri,
            headers: {'Content-Type': 'application/vnd.api+json'},
            dataType: 'json'
        }).done(function (response) {
            var fields = response.fields;
            var caseInfo = response.case_info;

            setHiddenCaseIdValue(caseInfo.id);
            setHiddenDurationValue(caseInfo.duration);

            // set time circle
            var dataDate = CASE_TIMER._calculateDataDateByDuration(caseInfo.duration);
            CASE_TIMER._setTimerDataDate(dataDate);

            $(fields).each(function (key, value) {
                var fieldValue = caseInfo[value.html_name];


                if(value.type == "dropdown") {
                    $('[name="' + value.html_name + '"]').select2('val', fieldValue);
                } else {
                    $('[name="' + value.html_name + '"]').val(fieldValue);
                }
            });

            if (_callback) {
                _callback();
            }
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.');
        });
    },


    checkCurrentAuxBreak: function () {
        $.ajax({
            type: 'GET',
            url: CASE_HANDLING.currentBreakUri + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType: 'json'
        }).done(function (response) {
            if (response.current_aux_break) {
                window.location.href = CASE_HANDLING.auxBreakUri + '?token=' + QUERY_STRING_HELPER.getByName('token');
            }
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.');
        })
    },

    checkOngoingCase : function (lobId) {
        $.ajax ({
            type: 'GET',
            url: CASE_HANDLING.currentCase + '/' + lobId +'?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType: 'json'
        }).done(function (response) {
            if (response.current_case_info) {
                var case_info = response.current_case_info;
                CASE_HANDLING.populateCaseForm(lobId, case_info.id, 0, function () {
                    var dataDate = CASE_HANDLING._computeDatadate(case_info.duration, case_info.updated_at ,case_info.created_at ,case_info.status_id);
                    (case_info.status_id == 7) ? CASE_HANDLING._editTimer() : CASE_HANDLING._continueTimer(dataDate);
                    CASE_HANDLING.buildSubMeasureResults(lobId, case_info.id);
                });
            }
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.');
        })
    },

    buildChildValues: function (lobId, childFieldId, parentListId) {

        $.ajax({
            type: 'GET',
            url: CASE_HANDLING.childValuesUri + '/' + lobId + '/' + parentListId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType: 'json'
        }).done(function (response) {

            var options = '<option value="0"> </option>';
            $.each(response.child_values, function (key, value) {
                options += '<option value="'+value.value+'">'+value.value+'</option>';
            });

            $('.dropdown-' + childFieldId).html(options);

        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.');
        });

    },


    buildSyntaxCodes: function (measure) {
      $.ajax({
            type: 'GET',
            url: CASE_HANDLING.syntaxCodesUri + '/' + measure + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType: 'json'
        }).done(function (response) {
            var options = '<option value="0"> </option>';
            $.each(response.syntax_codes, function (key, syntax) {
                if (syntax.submeasure === null) {
                    options += '<option value="' + syntax.code + '">' + syntax.outcome + ' - <strong>' + syntax.code + '</strong></option>';
                } else {
                    options += '<option value="' + syntax.code + '">' + syntax.outcome + ' - <strong>' + syntax.code + '</strong></option>';
                }
            });

            $('.syntax-outcome').html(options);
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong');
      });
    },


    buildSyntaxReason: function () {
        $.ajax({
            type: 'GET',
            url: CASE_HANDLING.syntaxReason + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType: 'json'
        }).done(function (response) {
            var options = '<option value="0"> </option>';
            $.each(response.reason, function (id, value) {
                    options += '<option value="' + value.id + '">' + value.value + '</strong></option>';
            });

            $('.reason').html(options);
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong');
        });
    },



    buildSubMeasureResults : function(lobId, caseId) {
        $.ajax({
            type: 'GET',
            url : CASE_HANDLING.submeasureURI + '/' + lobId + '/' + caseId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType: 'json'
        }).done(function (response){

            var result = "";
            if(lobId == 3){ // Stars LOB
                $.each(response.submeasure , function (key, results) {
                    result += '<div class="form-group">';
                    result += '<div class="col-md-2 text-center">' + results.measure + '</div>';
                    result += '<div class="col-md-2">' + results.submeasure + '</div>';
                    result += '<div class="col-md-2">' + results.status + '</div>';
                    result += '<div class="col-md-2">' + results.reason + '</div>';
                    result += '<div class="col-md-2">' + results.remarks + '</div>';
                    result += '<div class="col-md-1">' + results.syntax_code + '</div>';
                    result += '<div class="col-md-1"><button data-lob-id ="' + lobId +'" class="btn btn-danger btn-circle btn-sm m-r btn-submeasure-remove" ' +
                        'type="button" value="'+ results.ID+'"><i class="fa fa-times"></i></button>' +
                        '</div>';
                    result += '</div>';
                });
            } else if(lobId == 4){ // Hedis LOB
              $.each(response.submeasure , function (key, results) {
                result += '<div class="form-group">';
                result += '<div class="col-md-2">' + results.submeasure + '</div>';
                result += '<div class="col-md-2">' + results.status + '</div>';
                result += '<div class="col-md-2">' + results.reason + '</div>';
                result += '<div class="col-md-2">' + results.remarks + '</div>';
                result += '<div class="col-md-2">' + results.syntax_code + '</div>';
                result += '<div class="col-md-2"><button data-lob-id ="' + lobId +'" class="btn btn-danger btn-circle btn-sm m-r btn-submeasure-remove" ' +
                    'type="button" value="'+ results.ID+'"><i class="fa fa-times"></i></button>' +
                    '</div>';
                result += '</div>';
              });
            } else if(lobId == 5){ // Auditor LOB
            $.each(response.submeasure , function (key, results) {
              result += '<div class="form-group">';
              result += '<div class="col-md-3">' + results.submeasure + '</div>';
              result += '<div class="col-md-3">' + results.status + '</div>';
              result += '<div class="col-md-4">' + ((results.remarks == null) ? " " : results.remarks) + '</div>';
              result += '<div class="col-md-2"><button data-lob-id ="' + lobId +'" class="btn btn-danger btn-circle btn-sm m-r btn-submeasure-remove" ' +
                    'type="button" value="'+ results.ID+'"><i class="fa fa-times"></i></button>' +
                    '</div>';
              result += '</div>';
            });
          }

            $('.submeasure-results').html(result);
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong');
        });

    },

    saveSubMeasure: function (lobId) {

      var formData = $('#frm-submeasure-' + lobId + ' .submeasure-data').
        serializeArray();
      formData.push({name: 'caseId', value: getHiddenCaseIdValue()});

      var payload = PAYLOAD.build(formData);

      if (lobId == 5) {

        $.ajax({
          type: 'POST',
          url: CASE_HANDLING.createSubMeasureAuditorURI + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
          data: payload,
          headers: {'Content-Type': 'application/vnd.api+json'},
          dataType: 'json'
        }).done(function () {
          TOASTR_HELPER.success('Submeasure saved successfully!');
          CASE_HANDLING.buildSubMeasureResults(lobId, getHiddenCaseIdValue());
          CASE_HANDLING._clearSubmeasureForm(lobId);
        }).fail(function () {
          TOASTR_HELPER.error('Something went wrong');
        });
      }  else {

          $.ajax({
              type: 'POST',
              url: CASE_HANDLING.createSubMeasureURI + '/' + lobId + '?token=' +  QUERY_STRING_HELPER.getByName('token'),
              data: payload,
              headers: {'Content-Type': 'application/vnd.api+json'},
              dataType: 'json'
          }).done(function () {
              TOASTR_HELPER.success('Submeasure saved successfully!');
              CASE_HANDLING.buildSubMeasureResults(lobId, getHiddenCaseIdValue());
              CASE_HANDLING._clearSubmeasureForm(lobId);
          }).fail(function () {
              TOASTR_HELPER.error('Something went wrong');
          });
      }
    },
    buildSyntaxCodesDisplay: function (measure) {
      $.ajax({
          type: 'GET',
          url: CASE_HANDLING.syntaxCodesUri + '/' + measure + '?token=' + QUERY_STRING_HELPER.getByName('token'),
          dataType: 'json'
        }).done(function (response) {
         var items = '';
          $.each(response.syntax_codes, function (key, syntax) {

            items += ' <li class="list-group-item">';
                if (syntax.submeasure === null) {
                  items += '     <span class="label pull-right">' + syntax.code + '</span>'+ syntax.measure +' | ' + syntax.outcome +'';
                } else {
                  items += '     <span class="label pull-right">' + syntax.code + '</span>'+ syntax.submeasure +' | ' + syntax.outcome +'';
                }
            items += '  </li>';
          });

          $('#syntax-code-display').html(items);
        }).fail(function () {
          TOASTR_HELPER.error('Something went wrong');
        });
    },

    _clearForm: function (lobId) {
        $('#frm-' + lobId)[0].reset();
        $('.select2').select2();
    },

    _clearSubmeasureForm : function (lobId) {
        $('#frm-submeasure-' + lobId ).find('textarea').val('');
        $('.select2').select2();
    },

    _clearSubmeasureResult : function (lobId) {
        $('#submeasure-results').empty();
    },

    _enableStopBtn: function () {
        $('.btn-stop-case').prop('disabled', '');
    },

    _enableContinueBtn: function () {
        $('.btn-continue').prop('disabled', '');
    },

    _enableEditBtn: function () {
        $('.btn-edit').prop('disabled', '');
    },

    _disableStopBtn: function () {
        $('.btn-stop-case').prop('disabled', 'disabled');
    },

    _enableStartBtn: function () {
        $('.btn-start-case').prop('disabled', '');
    },

    _disableStartBtn: function () {
        $('.btn-start-case').prop('disabled', 'disabled');
    },

    _enableHoldBtn: function () {
        $('.btn-hold-case').prop('disabled', '');
    },

    _disableHoldBtn: function () {
        $('.btn-hold-case').prop('disabled', 'disabled');
    },

    _disableContinueBtn: function () {
        $('.btn-continue').prop('disabled', 'disabled');
    },

    _disableEditBtn: function () {
        $('.btn-edit').prop('disabled', 'disabled');
    },

    _enableAuxBreakBtn: function () {
        $('#btn-aux-break').prop('disabled', '');
    },

    _disableAuxBreakBtn: function () {
        $('#btn-aux-break').prop('disabled', 'disabled');
    },

    _enableEscalateBtn: function () {
        $('.btn-escalate-case').prop('disabled', '');
    },

    _disableEscalateBtn: function () {
        $('.btn-escalate-case').prop('disabled', 'disabled');
    },

    _enableSubmeasureBtn : function () {
        $('.btn-submeasure').prop('disabled', '');
    },

    _disableSubmeasureBtn : function () {
        $('.btn-submeasure').prop('disabled', 'disabled');
    },

    _enableLobTab : function () {
        $('.lob-selector').removeClass('disabled');
    },

    _disableLobTab : function () {
      $('.lob-selector').addClass('disabled');
    },

    _showValidationErrorPanel: function (lobId, validationErrors) {
        var errorList = "";
        var validationPanel = $('#validation-error-panel-'+lobId);

        $('#validation-error-list-'+lobId).html("");

        // for re-animation
        if (validationPanel.is(':visible') === true) {
            validationPanel.hide("fast");
        }

        validationPanel.show("fast", function () {
            $.each(validationErrors, function (key, value) {
                errorList += '<li class="text-danger">' + value + '</li>';
            });

            $('#validation-error-list-'+lobId).html(errorList);
        });
    },

    _closeValidationErrorPanel: function (lobId) {
        $('#validation-error-panel-'+lobId).hide('fast');
    },

    _continueTimer: function ($datadate) {
        CASE_TIMER._setTimerDataDate($datadate);
        CASE_TIMER.startTimer();

        CASE_HANDLING._disableStartBtn();
        CASE_HANDLING._enableStopBtn();
        CASE_HANDLING._enableHoldBtn();
        CASE_HANDLING._enableSubmeasureBtn();
        CASE_HANDLING._disableAuxBreakBtn();
        CASE_HANDLING._disableLobTab();
    },

    _editTimer: function () {

        CASE_HANDLING._disableStartBtn();
        CASE_HANDLING._enableStopBtn();
        CASE_HANDLING._enableHoldBtn();
        CASE_HANDLING._enableSubmeasureBtn();
        CASE_HANDLING._disableAuxBreakBtn();
        CASE_HANDLING._enableLobTab();
    },

    _computeDatadate : function (duration , updatedAt ,createdAt ,statusId) {
        var elapsedTime = duration;
        var elapsedMil = moment.duration(elapsedTime).asMilliseconds()
        var currentTime =  moment(updatedAt).subtract(elapsedMil).format('YYYY-MM-DD HH:mm:ss');
        if(statusId == 1) {
            currentTime = createdAt;
        }
        return currentTime;
    },

    _setAccountColor : function () {
        $('#account-header').addClass(CONSTANT.accountColor['Royal Blue']);
    },

};

$(document).ready(function () {
    CASE_HANDLING.build();
});