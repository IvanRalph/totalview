var AUX_BREAK = {
    startUri: CONSTANT.accountApiName + '/aux-break/start',
    stopUri: CONSTANT.accountApiName + '/aux-break/stop',
    currentUri: CONSTANT.accountApiName + '/aux-break/current',
    
    build: function () {
        AUX_BREAK.initializeBreakTimer();
        AUX_BREAK._toggleStopBtn(true);
        AUX_BREAK.checkCurrent();

        $(document).on('click', '#btnStartAuxBreak', AUX_BREAK.start);
        $(document).on('click', '#btnStopAuxBreak', AUX_BREAK.stop);
        $(document).on('click', '#btnCancelAuxBreak', AUX_BREAK.cancel);
    },
    
    start: function () {
        var formData = [
            {name: 'aux_type_id', value: $('#auxBreakTypes').val()},
            {name: 'remarks', value: $('#remarks').val()},
            {name: 'start_time', value: TIME_HELPER.currentDateTime()}
        ];

        var payload = PAYLOAD.build(formData);

        AUX_BREAK._resetBreakTime();
        AUX_BREAK._startBreakTimer();

        $.ajax({
            type: 'POST',
            url: AUX_BREAK.startUri + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            headers: {'Content-Type': 'application/vnd.api+json'},
            data: payload,
            dataType: 'json'
        }).done(function (response) {

            AUX_BREAK._toggleStartBtn(true);
            AUX_BREAK._toggleCancelBtn(true);
            AUX_BREAK._toggleStopBtn(false);
            $('#aux-id').val(response.aux_id);
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.');
        })
    },
    
    stop: function () {
        AUX_BREAK._stopBreakTimer();

        var formData = [
            {name: 'end_time', value: TIME_HELPER.currentDateTime()},
            {name: 'aux_type_id', value: $('#auxBreakTypes').val()}
        ];

        var payload = PAYLOAD.build(formData);

        $.ajax({
            type: 'PUT',
            url: AUX_BREAK.stopUri + '/' + $('#aux-id').val() + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            headers: {'Content-Type': 'application/vnd.api+json'},
            data: payload,
            dataType: 'json'
        }).done(function () {
            window.location.href = CONSTANT.accountWebName + '?token=' + QUERY_STRING_HELPER.getByName('token');
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.');
        })
    },

    cancel: function () {
        window.location.href = CONSTANT.accountWebName + '?token=' + QUERY_STRING_HELPER.getByName('token');
    },

    checkCurrent: function () {
        $.ajax({
            type: 'GET',
            url: AUX_BREAK.currentUri + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType: 'json'
        }).done(function (response) {
            if (response.current_aux_break) {
                $('#aux-id').val(response.current_aux_break.id);
                $('#auxBreakTypes').val(response.current_aux_break.aux_type_id);
                $('#remarks').val(response.current_aux_break.remarks);
                $('#auxBreakTimer').attr('data-date', response.current_aux_break.start_time);
                AUX_BREAK._startBreakTimer();
                AUX_BREAK._toggleStopBtn(false);
                AUX_BREAK._toggleStartBtn(true);
            }
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.');
        })
    },

    initializeBreakTimer: function() {
        $("#auxBreakTimer").TimeCircles({
            "start": false,
            "animation": "smooth",
            "bg_width": 1,
            "fg_width": 0.08,
            "circle_bg_color": "#EEEEEE",
            "data-date": "00:00:00",
            "time": {
                "Days": {
                    "text": "Days",
                    "color": "#CCCCCC",
                    "show": false
                },
                "Hours": {
                    "text": "Hours",
                    "color": "#1AB394",
                    "show": true,

                },
                "Minutes": {
                    "text": "Minutes",
                    "color": "#1AB394",
                    "show": true
                },
                "Seconds": {
                    "text": "Seconds",
                    "color": "#1AB394",
                    "show": true
                }
            }
        });
    },

    _startBreakTimer: function() {
        $("#auxBreakTimer").TimeCircles().start();
    },

    _stopBreakTimer: function() {
        $("#auxBreakTimer").TimeCircles().stop();
    },

    _resetBreakTime: function() {
        $("#auxBreakTimer").attr("data-date", "00:00:00");
    },
    
    _toggleStartBtn: function (isDisabled) {
        $('#btnStartAuxBreak').attr('disabled', isDisabled);
    },

    _toggleStopBtn: function (isDisabled) {
        $('#btnStopAuxBreak').attr('disabled', isDisabled);
    },

    _toggleCancelBtn: function (isDisabled) {
        $('#btnCancelAuxBreak').attr('disabled', isDisabled);
    }
}

$(document).ready(function () {
    AUX_BREAK.build();
});