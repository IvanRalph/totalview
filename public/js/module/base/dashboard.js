var DASHBOARD = {
    metricsUri: CONSTANT.accountApiName + '/dashboard/metrics',
    userListUri: CONSTANT.accountApiName + '/dashboard/users',
    userActivityUri: CONSTANT.accountApiName + '/dashboard/user-activity',
    userListDataTable: {},
    userLoggedStatus : {
        '1': {
            'class': 'info',
            'text': 'Online'
        },
        '0': {
            'class': 'danger',
            'text': 'Offline'
        }
    },

    build: function () {
        var defaultSelectedLob = $('#slct-lob').val();
        DASHBOARD.buildMetrics(defaultSelectedLob);
        DASHBOARD.buildUserList(defaultSelectedLob);

        $(document).on('change', '#slct-lob', function () {
            DASHBOARD.buildMetrics($(this).val());
            DASHBOARD.buildUserList($(this).val());
        });

        $(document).on('click', '.showActivity', function () {
            DASHBOARD.buildUserActivity($(this));
        })
    },

    buildMetrics: function (lobId) {
        $('.metric-panel').children('.ibox-content').addClass('sk-loading');

        $.ajax({
            type: 'GET',
            url: DASHBOARD.metricsUri + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType: 'json'
        }).done(function (response) {
            $('.totalAvailableNurse').html(response.available_user_count);
            $('.totalNurse').html(response.all_user_count);
            $('.totalCompletedCase').html(response.completed_count);
            $('.totalPendingCase').html(response.pending_count);

            var userPercentage = (response.available_user_count / response.all_user_count) * 100;
            $('.totalAvailableNursePercentage').html(userPercentage);

            $('.metric-panel').children('.ibox-content').removeClass('sk-loading');
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.');
            $('.metric-panel').children('.ibox-content').removeClass('sk-loading');
        });
    },

    buildUserList: function (lobId) {
        if ($.fn.dataTable.isDataTable('#productivity-table')) {
            DASHBOARD.userListDataTable.destroy();
        }

        DASHBOARD.userListDataTable = $('#productivity-table').DataTable({
            processing: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
            {extend: 'excel'},
            ],
            ajax: {
                url: DASHBOARD.userListUri + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token')
            },
            columns: [
            {
                render: function(data, type, row) {
                    return '<a href="#" id="' + row.main_user_id + '" class="showActivity">' + row.name + '</a>';
                }
            },
            {data: 'shift_date'},
            {data: 'shift_schedule'},
            {data: 'total_completed_cases'},
            {data: 'total_duration'},
            {data: 'aht'},
            {
                render: function(data, type, row) {
                    var label = DASHBOARD.userLoggedStatus[row.status];
                    return '<div class="text-center"><span class="label label-'+label.class+'">'+label.text+'</span></div>';
                }
            },
            ],
        });
    },
    
    buildUserActivity: function (userLinkObj) {
        var userId = userLinkObj.attr('id');
        var nurseName = userLinkObj.text();

        $('.activity-panel').children('.ibox-content').toggleClass('sk-loading');

        $.ajax({
            type: 'GET',
            url: DASHBOARD.userActivityUri + '/' + userId +  '?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType: 'json'
        }).done(function (response) {
            DASHBOARD.buildActivityTimeline(response.activities)
            $('.nurseName').html(nurseName);
            $('.activity-panel').children('.ibox-content').toggleClass('sk-loading');
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.');
        });
    },

    buildActivityTimeline: function (activities) {
        var timelineBlock = '';

        for (i in activities) {
            var activity = activities[i];
            timelineBlock += '<div class="vertical-timeline-block">';
            timelineBlock += '<div class="vertical-timeline-icon ' + activity.icon_bg + '"><i class="fa ' + activity.icon + '"></i></div>';
            timelineBlock += '<div class="vertical-timeline-content">';
            timelineBlock += '<span id="activityDate" class="vertical-date pull-right"><small>' + activity.timestamp + '</small></span>';
            timelineBlock += '<h2>' + activity.type + '</h2>';
            timelineBlock += '<p>' + activity.description + '</p></div>';
            timelineBlock += '</div>';
        }

        if (timelineBlock == '') {
            timelineBlock += '<div class="vertical-timeline-block">';
            timelineBlock += '<div class="vertical-timeline-icon gray-bg"><i class="fa fa-times"></i></div>';
            timelineBlock += '<div class="vertical-timeline-content">';
            timelineBlock += '<span id="activityDate" class="vertical-date pull-right"><small></small></span>';
            timelineBlock += '<h2></h2>';
            timelineBlock += '<p>No activities available.</p></div>';
            timelineBlock += '</div>';
        }

        $("#vertical-timeline").html(timelineBlock);
    }
}

$(document).ready(function () {
    DASHBOARD.build();
});