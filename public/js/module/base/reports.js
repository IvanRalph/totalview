var REPORTS = {
  doc: $(document),
  rawReportsUri: CONSTANT.accountApiName + '/reports/raw-reports-list/',
  teamList: {},
  bulkUserIds: [],

  build: function() {
    this.buildDatePicker();
    this.bind();
    this.refreshReportsTable(); 
  },

  bind: function(){
   $("#frmRawReports").submit(function(e){
     var _self = $(this);
     e.stopPropagation();
     e.preventDefault();
     REPORTS.buildReportsTable(_self.serialize());
   });
 },

 buildDatePicker: function () {
  $('#datepicker,#datepickerForProductivity').
  datepicker({format: 'yyyy-mm-dd'})
},

buildReportsTable: function (formData) {
  var lobId = $("#slct-lob-id").val();
  REPORTS.buildReportsTableRaw(formData);
},

refreshReportsTable: function () {
  $('#reports-table').DataTable({
    "width": "25%", 
    'language': {
      'emptyTable': 'Please set your filter',
    },
    initComplete: function () {},
  })
},

       buildReportsTableRaw: function (formData) { // reports Recon page
        $('#reports-table').DataTable().destroy();
        $('#reports-table').DataTable({
         scrollX: false,
            emptyTable: 'No Records Found!',
         ajax: {
          url: this.rawReportsUri + "?" + formData + '&token=' + QUERY_STRING_HELPER.getByName('token'),
          type: 'GET',
          
          success: function (data) {
            $('#reports-table').DataTable().destroy();
            var head = ''
            var cnt = 0
            var arrRes = new Array()
            $.each(data, function (key, value) {
              head = '<thead><tr>'
              $.each(data[key], function (col, data) {
                head += '<th>' + col + '</th>'
                arrRes[cnt++] = {'data': col}
              });
              head += '</tr></thead>';
              $('#reports-table').html(head) 
              $('#reports-table').DataTable({
                scrollX: true,
                processing: true,
                data: data,
                columns: arrRes,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                {extend: 'excel'},
                {extend: 'print'},
                ],
                columnDefs: [
                {
                  'targets': [0,2],
                  'searchable': false,
                  'orderable': false, 
                   "width": "15%",
                },
                ],
                order: [[1, 'asc' ]],
              });
              return false;
            })
          },

        },
      });
      },

    }

    $(document).ready(function() {
      REPORTS.build();
    }); 