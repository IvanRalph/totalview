var DASHBOARD = {
    metricsUri: CONSTANT.accountApiName + '/dashboard/metrics',
    userListUri: CONSTANT.accountApiName + '/dashboard/users',
    userActivityUri: CONSTANT.accountApiName + '/dashboard/user-activity',
    userListDataTable: {},
    userLoggedStatus : {
        '1': {
            'class': 'info',
            'text': 'Online'
        },
        '0': {
            'class': 'danger',
            'text': 'Offline'
        }
    },
    errorTries : 0,

    build: function () {
        var defaultSelectedLob = $('#slct-lob').val();
        // var defaultSelectedShiftDate = $('#slct-shift-date').val();
        var defaultSelectedTeam = $('#slct-team').val();
        DASHBOARD.buildMetrics(defaultSelectedLob, defaultSelectedTeam);
        DASHBOARD.buildUserList(defaultSelectedLob, defaultSelectedTeam);

        $(document).on('change', '#slct-lob', function () {
            DASHBOARD.buildMetrics($(this).val(), defaultSelectedTeam);
            DASHBOARD.buildUserList($(this).val(), defaultSelectedTeam);
        });

        // $(document).on('change', '#slct-shift-date', function () {
        //     DASHBOARD.buildMetrics($('#slct-lob').val() ,$(this).val(), defaultSelectedTeam);
        //     DASHBOARD.buildUserList($('#slct-lob').val() ,$(this).val(), defaultSelectedTeam);
        // });

        $(document).on('change', '#slct-team', function () {
            DASHBOARD.buildMetrics($('#slct-lob').val(), $(this).val());
            DASHBOARD.buildUserList($('#slct-lob').val(), $(this).val());
        });

        $(document).on('click', '.showActivity', function () {
            DASHBOARD.buildUserActivity($(this));
        })

        $('.fullScreenBtn').on('click', function(e){
            e.preventDefault();

            var lob = $('#slct-lob').val();
            var shiftDate = $('#slct-shift-date').val();
            var team = $('#slct-team').val();

            window.open(CONSTANT.accountWebName +'/dashboard-fullscreen?lob='+ lob +'&team='+ team +'&token=' + QUERY_STRING_HELPER.getByName('token'), "_blank");
        })

        $(document).ajaxError(function(event, request, settings, thrownError){
            // console.log(settings);
            // console.log(request);
            // console.log(event);
            if(request.status == 500 && settings.type == "GET"){
                TOASTR_HELPER.warning('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Connection Error. Retrying, please wait... ', {"timeOut":0, "extendedTimeOut":999999, "preventDuplicates": true });
                if(DASHBOARD.errorTries < 3 ){
                    DASHBOARD.errorTries++;
                    setTimeout(function(){
                        $.ajax(settings.url).done(function(){
                            DASHBOARD.build();
                            DASHBOARD._removeSpinners();
                            DASHBOARD.errorTries = 0;
                            TOASTR_HELPER.remove();
                        });
                    }, 1000);
                }else{
                    DASHBOARD.errorTries = 0;
                    TOASTR_HELPER.remove();
                    TOASTR_HELPER.error('There was a problem loading a module. Please refresh the page. If the issue persists, give us a call. Thanks!', {"timeOut":9000, "preventDuplicates": true});
                }
            }else{
                TOASTR_HELPER.warning('Something went wrong. Please try again. If the issue persists, give us a call. Thanks!');
                DASHBOARD._removeSpinners();
            }
        });
    },

    buildMetrics: function (lobId, team) {
        $('.metric-panel').children('.ibox-content').addClass('sk-loading');

        $.ajax({
            type: 'GET',
            url: DASHBOARD.metricsUri + '/' + lobId + '/'+ team +'?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType: 'json'
        }).done(function (response) {
            $('.avgCph').html(parseFloat(response.avg_cph).toFixed(2));
            $('.avgCphTarget').html(response.avg_cph_target);
            $('.totalHandledCases').html(response.total_handled_cases);
            $('.totalProdCases').html(response.total_prod_cases);

            $('.metric-panel').children('.ibox-content').removeClass('sk-loading');
        })
    },

    buildUserList: function (lobId, team) {
        if ($.fn.dataTable.isDataTable('#productivity-table')) {
            DASHBOARD.userListDataTable.destroy();
        }

        DASHBOARD.userListDataTable = $('#productivity-table').DataTable({
            processing: true,
            'order': [],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'excel'},
            ],
            ajax: {
                url: DASHBOARD.userListUri + '/' + lobId + '/'+ team +'?token=' + QUERY_STRING_HELPER.getByName('token')
            },
            columns: [
                {
                    render: function(data, type, row) {
                        return '<a href="#" id="' + row.main_user_id + '" class="showActivity">' + row.name + '</a>';
                    }
                },
                {data: 'team'},
                {data: 'shift_date'},
                {data: 'shift_schedule'},
                {data: 'total_completed_cases'},
                {data: 'total_pending_cases'},
                {data: 'cph'},
                {data: 'cph_target'},
                {data: 'total_handled_cases'},
                {data: 'total_prod_cases'},
                {data: 'total_duration'},
                {data: 'aht'},
                {
                    render: function(data, type, row) {
                        var label = DASHBOARD.userLoggedStatus[row.status];
                        return '<div class="text-center"><span class="label label-'+label.class+'">'+label.text+'</span></div>';
                    }
                },
            ],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "footerCallback": function () {
                var api = this.api();

                var durationToSec = function (duration) {
                    return moment.duration(duration).asSeconds();
                };

                var completedCasesTotal = api.column(4, {search: 'applied'})
                    .data()
                    .reduce(function(a, b) {
                        return parseFloat(a) + parseFloat(b);
                    }, 0);

                var pendingCaseTotal = api.column(5, {search: 'applied'})
                    .data()
                    .reduce(function (a, b) {
                        return parseFloat(a) + parseFloat(b);
                    }, 0);

                 var escalatedCaseTotal = api.column(6, {search: 'applied'})
                    .data()
                    .reduce(function (a, b) {
                        return parseFloat(a) + parseFloat(b);
                    }, 0);    

                var casesDurationTotal = api.column(10, {search: 'applied'})
                    .data()
                    .reduce(function (a, b) {
                        return TIME_HELPER.secToHHMMSS(durationToSec(a) + durationToSec(b));
                    }, 0);

                var ahtTotal = api.column(11, {search: 'applied'})
                    .data()
                    .reduce(function (a, b) {
                        return TIME_HELPER.secToHHMMSS(durationToSec(a) + durationToSec(b));
                    }, 0);

                $(api.column(3).footer()).html('TOTAL');
                $(api.column(4).footer()).html(completedCasesTotal);
                $(api.column(5).footer()).html(pendingCaseTotal);
                $(api.column(10).footer()).html(casesDurationTotal);
                $(api.column(11).footer()).html(ahtTotal);
            },

            "scrollX": true,

            "fnInitComplete": function(){
                // Disable TBODY scoll bars
                $('.dataTables_scrollBody').css({
                    'overflow': 'hidden',
                    'border': '0'
                });

                // Enable TFOOT scoll bars
                $('.dataTables_scrollFoot').css('overflow', 'auto');

                // Sync TFOOT scrolling with TBODY
                $('.dataTables_scrollFoot').on('scroll', function () {
                    $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
                });                    
            },
        });
    },

    buildUserActivity: function (userLinkObj) {
        var userId = userLinkObj.attr('id');
        var nurseName = userLinkObj.text();

        $('.activity-panel').children('.ibox-content').toggleClass('sk-loading');

        $.ajax({
            type: 'GET',
            url: DASHBOARD.userActivityUri + '/' + userId +  '?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType: 'json'
        }).done(function (response) {
            DASHBOARD.buildActivityTimeline(response.activities)
            $('.nurseName').html(nurseName);
            $('.activity-panel').children('.ibox-content').toggleClass('sk-loading');
        })
    },

    buildActivityTimeline: function (activities) {
        var timelineBlock = '';

        for (i in activities) {
            var activity = activities[i];
            timelineBlock += '<div class="vertical-timeline-block">';
            timelineBlock += '<div class="vertical-timeline-icon ' + activity.icon_bg + '"><i class="fa ' + activity.icon + '"></i></div>';
            timelineBlock += '<div class="vertical-timeline-content">';
            timelineBlock += '<span id="activityDate" class="vertical-date pull-right"><small>' + activity.timestamp + '</small></span>';
            timelineBlock += '<h2>' + activity.type + '</h2>';
            timelineBlock += '<p>' + activity.description + '</p></div>';
            timelineBlock += '</div>';
        }

        if (timelineBlock == '') {
            timelineBlock += '<div class="vertical-timeline-block">';
            timelineBlock += '<div class="vertical-timeline-icon gray-bg"><i class="fa fa-times"></i></div>';
            timelineBlock += '<div class="vertical-timeline-content">';
            timelineBlock += '<span id="activityDate" class="vertical-date pull-right"><small></small></span>';
            timelineBlock += '<h2></h2>';
            timelineBlock += '<p>No activities available.</p></div>';
            timelineBlock += '</div>';
        }

        $("#vertical-timeline").html(timelineBlock);
    },

    _removeSpinners : function(){
        $('.metric-panel').children('.ibox-content').removeClass('sk-loading');
        $('.activity-panel').children('.ibox-content').removeClass('sk-loading');
    },

    _addSpinners : function(){
        $('.metric-panel').children('.ibox-content').addClass('sk-loading');
        $('.activity-panel').children('.ibox-content').addClass('sk-loading');
    },
}

$(document).ready(function () {
    DASHBOARD.build();

    setInterval(function(){
        DASHBOARD.build();
    }, 60000);
});