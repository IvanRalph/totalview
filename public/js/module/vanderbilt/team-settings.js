var TEAM_SETTINGS = {
    doc: $(document),
    userIdToEdit: 0,
    userScheduleIdToEdit: 0,
    teamIdToEdit: 0,
    allocatorCaseDatatable: {},
    userOnActive: {
        1: 'online',
        0: 'offline'
    },
    userOnLeaveStatus: {
        1: 'yes',
        0: 'no'
    },
    userLoggedStatus: {
        1: 'primary',
        0: 'danger'
    },
    teamUsersUri: CONSTANT.accountApiName + '/team/user-list',
    userTeamUpdateUri: CONSTANT.accountApiName + '/team-settings/update-team/user',
    teamScheduleUpdateUri: CONSTANT.accountApiName + '/team-settings/update-team/schedule',
    teamBulkUpdateUri: CONSTANT.accountApiName + '/team-settings/update-team/team',
    teamLobUpdateUri: CONSTANT.accountApiName + '/team-settings/update-team/lob',
    bulkUserIds: [],
    user_shift_schedule: [],
    teamListUri: CONSTANT.accountApiName + '/team',
    teamListUpdateUri: CONSTANT.accountApiName + '/teams-update',
    teamListAddUri: CONSTANT.accountApiName + '/teams-add',
    userLobUpdateUri: CONSTANT.accountApiName + '/team-settings/update-user/lob',


    build: function() {
        // this.bindHandlerErrorToDatatable();
        this.buildUsersTable();
        this.buildTeamsTable();
        this.bind();
    },

    bind: function() {

        this.doc.on('click', '#chk-all', function() {
            TEAM_SETTINGS._checkAll();
        });

        this.doc.on('click', '#btnSaveTeamSettings', function() {
            TEAM_SETTINGS.updateTeamInfo();
        });

        this.doc.on('click', '#btnSaveSchedule', function() {
            TEAM_SETTINGS.updateUserTeam();
        });

        this.doc.on('click', '#btnSaveScheduleBulk', function() {
            TEAM_SETTINGS.buildTeamBulkSchedule();
        });

        this.doc.on('click', '#btnSaveTeamBulk', function() {
            TEAM_SETTINGS.buildTeamBulkTeam();
        });

        this.doc.on('click', '#btnSaveLobBulk', function() {
            TEAM_SETTINGS.buildTeamBulkLob();
        });

        this.doc.on('click', '#btnSaveLob', function() {
            TEAM_SETTINGS.updateLob();
        });

        this.doc.on('click', '#btnAddTeam', function() {
            TEAM_SETTINGS.addTeam();
        });

        $("#buld-user-ramp, #per-user-ramp").TouchSpin({
            initval: 0
        });
    },


    bindHandlerErrorToDatatable: function() {
        $.fn.dataTable.ext.errMode = function(settings, helpPage, message) {
            TEAM_SETTINGS._showErrorNotif(settings.jqXHR);
        };
    },

    buildUsersTable: function() {
        $('#team-management-users-table').DataTable().destroy() //to initiate the dataTables
        var userTable = $('#team-management-users-table').DataTable({
            'language': {
                loadingRecords: SPINNERS.wave()
            },
            ajax: {
                url: this.teamUsersUri + '?token=' + QUERY_STRING_HELPER.getByName('token'),
                "dataSrc": "",
            },
            columns: [{
                    data: 'id'
                },
                {
                    data: 'full_name'
                },
                {
                    data: 'lob'
                },
                {
                    data: 'description'
                },
                {
                    data: 'ramp'
                },
                {
                    data: 'nurse_status'
                },
                {
                    data: 'shift_date'
                },
                {
                    data: 'start_shift'
                },
                {
                    data: 'end_shift'
                },
                {
                    render: function(data, type, row) {
                        var labelType = TEAM_SETTINGS.userOnLeaveStatus[row.leave];
                        var labelTypeColor = TEAM_SETTINGS.userLoggedStatus[row.leave];
                        return '<div class="text-center"><span class="label label-' + labelTypeColor + '">' + labelType + '</span></div>';
                    },
                    'orderable': false,
                },
                {
                    render: function(data, type, row) {
                        var labelType = TEAM_SETTINGS.userOnActive[row.status];
                        var labelTypeColor = TEAM_SETTINGS.userLoggedStatus[row.status];
                        return '<div class="text-center"><span class="label label-' + labelTypeColor + '">' + labelType + '</span></div>';
                    },
                    'orderable': false,
                },
                {
                    render: function(data, type, row) {
                        return TEAM_SETTINGS._buildEditUserButton();
                    },
                    'orderable': false,
                    'searchable': false,
                }
            ],

            dom: '<"html5buttons"B>lTfgitp',
            buttons: [{
                    text: 'Bulk Edit',
                    className: 'btn-bulk-edit'
                },
                {
                    extend: 'excel'
                },
                {
                    extend: 'print'
                },
            ],
            columnDefs: [{
                "width": "100px",
                'targets': 0,
                'searchable': false,
                'className': 'text-center',
                'orderable': false,
                render: function(data, type, jsonData, meta) {
                    return TEAM_SETTINGS._buildCheckBox(jsonData.id);
                },
            }, ],
            order: [
                [1, 'asc']
            ],
        });
        $('#team-management-users-table tbody').on('click', '.edit-user-button', function() {
            var data = userTable.row($(this).parents('tr')).data();
            TEAM_SETTINGS._bindUserTableToModal(data);
        });
        TEAM_SETTINGS._bindUserTableToBulkModal(userTable);
    },

    _bindUserTableToBulkModal: function(userTable) {
        this.doc.on('click', '.btn-bulk-edit', function() {
            bulksGranted = [];
            var idSelector = function() {
                return this.id;
            };
            var bulksGranted = $(":checkbox:checked").map(idSelector).get();
            var stringBulk = JSON.parse(JSON.stringify(bulksGranted));
            var userList = userTable.rows({
                search: 'applied'
            }).every(function(rowIdx, tableLoop, rowLoop) {});
            var chosenUser = userList.data();
            TEAM_SETTINGS.bulkUserIds = stringBulk;

            if ($('.bulk-checkbox:checkbox:checked').length == 0) {
                toastr.error("Please select a nurse.");
                return;
            }
            $('#bulkUserSettingModal').modal({
                show: true
            });
        });
    },

    buildTeamBulkSchedule: function() {
        var startShift = $('#bulk-user-start-shift').val();
        var endShift = $('#bulk-user-end-shift').val();
        var onLeaveBulk = $("#onLeaveBulk").is(":checked") ? '1' : '0';
        var ramp = $("#buld-user-ramp").val();

        var formsId = [
            TEAM_SETTINGS.bulkUserIds
        ];

        var formsData = [
            startShift,
            endShift,
            onLeaveBulk,
            ramp,
        ];



        if (startShift >= endShift) {
            TEAM_SETTINGS._showErrorNotif("End shift should be greater than first shift.");
            return false;
        }

        HTTP_REQUEST.put(TEAM_SETTINGS.teamScheduleUpdateUri + "/" + formsId + "/" + formsData + '?token=' + QUERY_STRING_HELPER.getByName('token')).then(
            (response) => {
                TEAM_SETTINGS._showSuccessNotif("Team information updated successfully!");
                TEAM_SETTINGS._reloadUsersTable();
                // $('#bulk-user-start-shift').val('');

            },
            (response) => {
                TEAM_SETTINGS._showErrorNotif("Team information schedule was not save.");
            }
        );
    },

    buildTeamBulkTeam: function() {
        var rdoTeam = $('input[name=rdoTeam]:checked').val();

        var formsId = [
            TEAM_SETTINGS.bulkUserIds
        ];

        HTTP_REQUEST.put(TEAM_SETTINGS.teamBulkUpdateUri + "/" + formsId + "/" + rdoTeam).then(
            (response) => {
                TEAM_SETTINGS._showSuccessNotif("Team information updated successfully!");
                TEAM_SETTINGS._reloadTeamsTable();
            },
            (response) => {
                TEAM_SETTINGS._showErrorNotif(response);
            }
        );
    },


    buildTeamBulkLob: function() {
        var formsId = [
            TEAM_SETTINGS.bulkUserIds
        ];
        var getCheckbox = $(".ads_Checkbox:checked").map(function() {
            return this.value;
        }).get();
        HTTP_REQUEST.put(TEAM_SETTINGS.teamLobUpdateUri + "/" + formsId + "/" + getCheckbox + '?token=' + QUERY_STRING_HELPER.getByName('token')).then(
            (response) => {
                TEAM_SETTINGS._showSuccessNotif("Team information updated successfully!");
                TEAM_SETTINGS._reloadUsersTable();
            },
            (response) => {
                TEAM_SETTINGS._showErrorNotif("Team information was not updated successfully!");
            }
        );
    },

    updateLob: function() {
        var user_id = $("#user-id").val();
        var getCheckbox = $(".ads_Checkbox:checked").map(function() {
            return this.value;
        }).get();

        HTTP_REQUEST.put(TEAM_SETTINGS.userLobUpdateUri + '/' + user_id + '/' + getCheckbox + '?token=' + QUERY_STRING_HELPER.getByName('token')).then(
            (response) => {
                TEAM_SETTINGS._showSuccessNotif("LOB updated successfully!");
                TEAM_SETTINGS._reloadUsersTable();
            },
            (response) => {
                TEAM_SETTINGS._showErrorNotif("Team information was not updated successfully!");
            }
        )
    },


    _buildTeamListonBulkModal: function() {
        var teamList = '';
        $(TEAM_SETTINGS.teamList).each(function(index, element) {
            teamList += TEAM_SETTINGS._buildTeamOption(element, false, "bulkRdoTeam");
        });

        $("#bulkTeamListPlaceholder").html(teamList);
    },

    _bindUserTableToModal: function(userTable) {


        $('input[type=checkbox]').prop('checked', false); //clear checkbox
        $('#user-id').val(userTable.id);
        $('#user-start-shift').val(userTable.start_shift);
        $('#user-end-shift').val(userTable.end_shift);
        $('#per-user-ramp').val(userTable.ramp);

        if (userTable.leave == 1) {
            $("#onLeave").prop("checked", true);
            $('#onLeave').val(1);
        } else {
            $("#onLeave").prop("checked", false);
            $('#onLeave').val(0);
        }

        if (userTable.shift_end == null) {
            $("#onShift").prop("checked", true);
            $('#onShift').val(1);
        } else {
            $("#onShift").prop("checked", false);
            $('#onShift').val(0);
        }

        $('#userSettingModal').modal({
            show: true
        });

        $('#userFullName').text(userTable.full_name);
        if (userTable.status == 1) {
            $('#isOnline').text('Online');
            $('#isOnline').addClass('label-info');
            $('#isOnline').removeClass('label-danger');
        } else {
            $('#isOnline').text('Offline');
            $('#isOnline').addClass('label-danger');
            $('#isOnline').removeClass('label-info');
        }
        $("#team_id_" + userTable.teamID).prop("checked", true);

        var arr = userTable.lob_id.split(',');
        for (i = 0; i < arr.length; i++) {
            $("#check_lob_" + arr[i]).prop("checked", true);
        }

        $('#slctRole').val(userTable.roleID);
    },

    buildTeamsTable: function(formData) {
        $(".select2_demo_2").select2({
            width: '100%'
        });
        var teamTable = TEAM_SETTINGS.allocatorCaseDatatable = $('#team-management-teams-table').DataTable({
            'language': {
                loadingRecords: SPINNERS.wave()
            },
            ajax: {
                url: this.teamListUri + '?token=' + QUERY_STRING_HELPER.getByName('token'),
                "dataSrc": "",
            },
            columns: [{
                    data: 'team'
                },
                {
                    data: 'fullname'
                },
                {
                    data: 'end_time'
                },

                {
                    render: function(data, type, row) {
                        return TEAM_SETTINGS._buildAllocatorEditUserButton(row.ID);
                    },
                    'orderable': false,
                    'searchable': false,
                },
            ],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [{
                    extend: 'excel'
                },
                {
                    extend: 'print'
                },
                {
                    text: 'Add Team',
                    action: function ( e, dt, node, config ) {
                        $('#addTeamModal').modal('show');
                    }
                }
            ],
            columnDefs: [{
                'targets': 0,
                'searchable': false,
                'className': 'text-center',
                'orderable': false,
            }, ],
            order: [1, 'asc'],
        });

        $('#team-management-teams-table tbody').on('click', '#edit-team-show-modal', function() {
            var data = teamTable.row($(this).parents('tr')).data();
            TEAM_SETTINGS._bindTeamTableToModal(data);
        });
    },

    _bindTeamTableToModal: function(table) { // manager team

        $('.team-info-leader').removeAttr('checked');
        $('#teamLeaderID').val(table.ID);
        $('#editTeamName').val(table.team);
        $('#team-kickout-time').val(table.end_time);
        $('#editTeamLeader').val(table.ID);
        var arr = table.lead.split(',');

        $("#team-info-leader").select2("val", arr);

        $('#teamSettingModal').modal({
            show: true
        });
    },

    updateTeamInfo: function() {

        var getCheckbox = $('#team-info-leader').val();

        var formsData = [
            $('#teamLeaderID').val(),
            $('#editTeamName').val(),
            $('#team-kickout-time').val(),
        ];

        HTTP_REQUEST.put(TEAM_SETTINGS.teamListUpdateUri + "/" + formsData + "/" + getCheckbox + "/" + '?token=' + QUERY_STRING_HELPER.getByName('token')).then(
            (response) => {
                TEAM_SETTINGS._showSuccessNotif("Team information updated successfully!");
                TEAM_SETTINGS._reloadTeamsTable();
                TEAM_SETTINGS._reloadUsersTable();
            },
            (response) => {
                TEAM_SETTINGS._showErrorNotif("Team information was not updated successfully!");
            }
        );
    },

    updateUserTeam: function() {
        var user_id = $('#user-id').val();
        var user_start_shift = $('#user-start-shift').val();
        var user_end_shift = $('#user-end-shift').val();
        var ramp = $('#per-user-ramp').val();
        var onLeave = $("#onLeave").is(":checked") ? '1' : '0';
        var onShift = $("#onShift").is(":checked") ? '1' : '0';
        var teamListPlaceholder = $('input[name=rdoTeam]:checked').val();
        var slctRole = $('#slctRole').val();
        var nurseStatus = $('#nurse-status').val();

        console.log(nurseStatus);

        var formsData = [
            user_id,
            user_start_shift,
            user_end_shift,
            onLeave,
            onShift,
            teamListPlaceholder,
            slctRole,
            ramp,
            nurseStatus
        ];


        HTTP_REQUEST.put(TEAM_SETTINGS.userTeamUpdateUri + '/' + formsData + '?token=' + QUERY_STRING_HELPER.getByName('token')).then(
            (response) => {
                TEAM_SETTINGS._showSuccessNotif("Team updated successfully!");
                $('#userSettingModal').modal('hide');
                TEAM_SETTINGS._reloadUsersTable();
            },
            (response) => {
                TEAM_SETTINGS._showErrorNotif(response);
            }
        );
    },

    addTeam: function(){
        var getCheckbox = $('#add-team-info-leader').val();

        var formsData = [
            $('#addTeamName').val(),
            $('#add-team-kickout-time').val(),
        ];

        HTTP_REQUEST.put(TEAM_SETTINGS.teamListAddUri + "/" + formsData + "/" + getCheckbox + "/" + '?token=' + QUERY_STRING_HELPER.getByName('token')).then(
            (response) => {
                TEAM_SETTINGS._showSuccessNotif("Team information added successfully!");
                TEAM_SETTINGS._reloadTeamsTable();
                TEAM_SETTINGS._reloadUsersTable();
                $('#addTeamModal').modal('hide');
                location.reload();
            },
            (response) => {
                TEAM_SETTINGS._showErrorNotif("Team information was not updated successfully!");
            }
        );
    },

    _buildAllocatorEditUserButton: function(caseId) {
        return '<div id="edit-single-show-modal" class="text-center">' +
            '<a id="' + caseId + ' " class="btn btn-xs btn-primary edit-case-allocator-button">' +
            '<i class="fa fa-pencil"></i> Edit</a></div>';
    },

    _reloadUsersTable: function() {
        $('#team-management-users-table').DataTable().ajax.reload();
    },

    _reloadTeamsTable: function() {
        $('#team-management-teams-table').DataTable().ajax.reload();
    },



    _buildTeamListonBulkModal: function() {
        var teamList = '';

        $(TEAM_SETTINGS.teamList).each(function(index, element) {
            teamList += TEAM_SETTINGS._buildTeamOption(element, false, "bulkRdoTeam");
        });

        $("#bulkTeamListPlaceholder").html(teamList);
    },


    _buildCheckBoxes: function(userId) {
        return '<div class="checkbox checkbox-primary">' +
            '<input id="' + userId + '"type="checkbox" class="bulk-checkbox"><label for="' + userId + '">&nbsp;</label>' +
            '</div>';
    },

    _showSuccessNotif: function(message) {
        toastr.options = {
            "closeButton": true,
            "progressBar": true,
            "positionClass": "toast-top-center"
        };
        toastr.success(message);
    },

    _showErrorNotif: function(message) {
        toastr.options = {
            "closeButton": true,
            "progressBar": false,
            "positionClass": "toast-top-center"
        };
        toastr.error(message);
    },

    _buildRoleOptions: function(roleElementId, _callback) {
        $(roleElementId).html("<option>Loading...</option>");
        HTTP_REQUEST.get(TEAM_SETTINGS.roleUri).then(
            (response) => {
                var roleOptions = '';
                var roles = response.data;

                for (i in roles) {
                    var role = roles[i];
                    roleOptions += '<option value="' + role.id + '">' + role.attributes.name + '</option>'
                }

                $(roleElementId).html(roleOptions);
                _callback();
            },
            (response) => {
                TEAM_SETTINGS._showErrorNotif(response);
            }
        )
    },

    _buildScheduleForm: function(user) {
        TEAM_SETTINGS._buildShiftScheduleOptions("#shiftSchedule", function() {
            $("#shiftSchedule").val(user.shift_start);
        });

        TEAM_SETTINGS.userScheduleIdToEdit = user.shift_start;
        $('#userFullName').text(user.fullname);
        $('#isOnline').text(user.log_status);
        $('#onLeave').val(user.on_leave);
        $('#userEmail').text(user.email);
        $('#shiftDate').html(user.shift_date);

        if (user.log_status == 'offline') {
            $('#isOnline').addClass('label-danger');
            $('#isOnline').removeClass('label-info');
        } else {
            $('#isOnline').addClass('label-info');
            $('#isOnline').removeClass('label-danger');
        }

        if (user.on_leave == 'no') {
            $('#onLeave').removeAttr('checked');
            $('#onLeave').prop('checked', false);
        } else {
            $('#onLeave').removeAttr('checked');
            $('#onLeave').prop('checked', true);
        }
    },


    _buildCaseAssignmentForm: function(user) {
        $("#chkOcr").prop('checked', user.is_ocr);
        $("#chkOcrTx").prop('checked', user.is_ocr_tx);
        $("#chkHos").prop('checked', user.is_hos);
        $("#chkHosTx").prop('checked', user.is_hos_tx);
        $("#caseAssignment").attr('case-assignment-id', user.case_assignment_id);
    },


    _buildTeamListonModal: function(user) {
        var teamList = '';

        $(TEAM_SETTINGS.teamList).each(function(index, element) {
            var isCurrentTeam = (element.id == user.team_id) ? 'checked=""' : '';
            teamList += TEAM_SETTINGS._buildTeamOption(element, isCurrentTeam, "rdoTeam");
        });

        $("#teamListPlaceholder").html(teamList);
    },

    _buildShiftScheduleOptions: function(shiftScheduleId, _callback) {
        $(shiftScheduleId).html("<option>Loading...</option>");
        HTTP_REQUEST.get(TEAM_SETTINGS.shiftScheduleUri).then(
            (response) => {
                var shiftScheduleOptions = '';
                var shiftSchedules = response.data;
                for (i in shiftSchedules) {
                    var shiftSchedule = shiftSchedules[i].attributes;
                    shiftScheduleOptions += '<option value="' + shiftSchedules[i].id + '">' + shiftSchedule.name + ' (' + shiftSchedule.start_time + ' - ' + shiftSchedule.end_time + ')</option>';
                }
                $(shiftScheduleId).html(shiftScheduleOptions);
                _callback();

            },
            (response) => {
                TEAM_SETTINGS._showErrorNotif(response);
            }
        )
    },

    _buildLobForm: function(user) {
        var _self = $("#lob");

        var lob = user.lob.split(', ');

        _self.attr('user-id', user.user_id);
        $("input[type='checkbox']", _self).each(function() {
            var _this = $(this);

            if (lob.indexOf($.trim(_this.attr('attr-name'))) >= 0) {
                _this.prop('checked', true);
            } else {
                _this.prop('checked', false);
            }
        })
    },

    _buildCheckBox: function(userId) {
        return '<div class="checkbox checkbox-primary">' +
            '<input id="' + userId + '"type="checkbox" class="bulk-checkbox"><label for="' + userId + '">&nbsp;</label>' +
            '</div>';
    },
    _buildEditUserButton: function() {
        return '<div class="text-center">' +
            '<a class="btn btn-xs btn-primary edit-user-button">' +
            '<i class="fa fa-pencil"></i> Edit</a></div>';
    },


    _buildAllocatorEditUserButton: function(caseId) {
        return '<div id="edit-team-show-modal" class="text-center">' +
            '<a id="' + caseId + ' " class="btn btn-xs btn-primary edit-case-allocator-button">' +
            '<i class="fa fa-pencil"></i> Edit</a></div>';
    },

    _checkAll: function() {
        if ($('.bulk-checkbox').prop('checked')) {

            $('.bulk-checkbox').prop('checked', false);
        } else {
            $('.bulk-checkbox').prop('checked', true);
        }
    },

    _testTeamURI: function() {
        $.ajax({
            type: 'GET',
            url: this.teamListUri + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            headers: {
                'Content-Type': 'application/vnd.api+json'
            },
            dataType: 'json'
        }).done(function(response) {

        });
    },
};

$(document).ready(function() {
    TEAM_SETTINGS.build();
});