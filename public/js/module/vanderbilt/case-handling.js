var CASE_HANDLING = {
    createCaseUri: CONSTANT.accountApiName + '/create-case',
    updateCaseUri: CONSTANT.accountApiName + '/update-case',
    updateCaseFinUri: CONSTANT.accountApiName + '/update-case-fin',
    trackerStatsUri: CONSTANT.accountApiName + '/tracker-stats',
    casesUri: CONSTANT.accountApiName + '/cases',
    caseUri: CONSTANT.accountApiName + '/case',
    caseByFinUri: CONSTANT.accountApiName + '/case-fin',
    currentBreakUri: CONSTANT.accountApiName + '/aux-break/current',
    currentCase: CONSTANT.accountApiName + '/case/current',
    auxBreakUri: CONSTANT.accountWebName + '/aux-break',
    childValuesUri: CONSTANT.accountApiName + '/child-values',
    deleteCaseUri : CONSTANT.accountApiName + '/delete-case',
    validationErrors : [],
    errorTries : 0,
    errorExists : false,

    build: function (_callback = null) {
        CASE_HANDLING._setAccountColor();
        CASE_HANDLING.initializeSelect2();
        CASE_HANDLING.initializeDatePicker();
        CASE_HANDLING._addSpinners();

        if($('#start-shift').is(':visible')) {
            $(document).on('click', '#start-shift', function () {
                CASE_HANDLING.checkCurrentAuxBreak();
                CASE_HANDLING.checkOngoingCase($('.lob-selector.active').attr('data-lob-id'));
            });
        } else {

            CASE_HANDLING.checkCurrentAuxBreak();
            CASE_HANDLING.checkOngoingCase($('.lob-selector.active').attr('data-lob-id'));

        }

        $(document).on('click', '.lob-selector.active', function (e) {
             e.preventDefault();
             $('.lob-selector.active').attr('style', 'pointer-events: none;');
        });

        $('.parent-dropdown').each(function () {
            var selectedValueListId = $(this).find(":selected").attr('data-list-id');
            var lobId = $(this).attr('data-lob-id');
            var childFieldId = $(this).attr('data-child-field-id');

            CASE_HANDLING.buildChildValues(lobId, childFieldId, selectedValueListId);
        });

        $(document).on('click', '.btn-start-case', function () {
            CASE_HANDLING.startCase($(this).attr('lobid'));
        });

        $(document).on('click', '.btn-stop-case', function () {
            CASE_HANDLING._disableFormButtons();
            CASE_HANDLING.updateCase($(this).attr('lobid'), $(this).attr('actionid'), function(){
                CASE_HANDLING._enableFormButtons();
            });
        });

        $(document).on('click', '.btn-hold-case', function () {
            CASE_HANDLING._disableFormButtons();
            CASE_HANDLING.updateCase($(this).attr('lobid'), $(this).attr('actionid'), function(){
                CASE_HANDLING._enableFormButtons();
            });
            
        });

        $(document).on('click', '.btn-save-case', function () {
            CASE_HANDLING.saveCase($(this).attr('lobid'), $(this).attr('actionid'));
        });

        $(document).on('click', '.btn-escalate-case', function () {
            CASE_HANDLING.updateCase($(this).attr('lobid'), $(this).attr('actionid'));
        });

        $(document).on('click', '.btn-edit', function () {
            CASE_HANDLING.editCase($(this).attr('data-lob-id'), $(this).attr('data-case-id'), $(this).attr('data-action-id'));
        });

        $(document).on('click', '.btn-continue', function () {
            CASE_HANDLING.continueCase($(this).attr('data-lob-id'), $(this).attr('data-case-id'), $(this).attr('data-action-id'));
        });

        $(document).on('click', '.lob-selector', function () {
            CASE_HANDLING.buildCaseTable($(this).attr('data-lob-id'));
            CASE_HANDLING.checkOngoingCase($(this).attr('data-lob-id'));
        });

        $(document).on('click', '#btn-aux-break', function () {
            CASE_TIMER.stopTimer();
            window.location.href = CASE_HANDLING.auxBreakUri + '?token=' + QUERY_STRING_HELPER.getByName('token');
        });

        $(document).on('click', '#close-validation-error-panel', function () {
            CASE_HANDLING._closeValidationErrorPanel($(this).attr('data-lob-id'));
        });

        $(document).on('change', '.parent-dropdown', function () {
            var selectedValueListId = $(this).find(":selected").attr('data-list-id');
            var lobId = $(this).attr('data-lob-id');
            var childFieldId = $(this).attr('data-child-field-id');

            CASE_HANDLING.buildChildValues(lobId, childFieldId, selectedValueListId);

        });

        CASE_HANDLING.buildCaseTable($('#lob-tab li.active').attr('data-lob-id'));

        $(document).ajaxError(function(event, request, settings, thrownError){
            CASE_HANDLING.errorExists = true;
            CASE_HANDLING._removeSpinners();
            CASE_HANDLING._addSpinners();
            // console.log(settings);
            // console.log(request);
            // console.log(event);
            if(request.status == 500 && settings.type == "GET"){
                TOASTR_HELPER.warning('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Connection Error. Retrying, please wait... ', {"timeOut":0, "extendedTimeOut":999999, "preventDuplicates": true });
                if(CASE_HANDLING.errorTries < 3 ){
                    CASE_HANDLING.errorTries++;
                    setTimeout(function(){
                        $.ajax(settings.url).done(function(){
                            CASE_HANDLING.build();
                            CASE_HANDLING._removeSpinners();
                            CASE_HANDLING.errorTries = 0;
                            TOASTR_HELPER.remove();
                        });
                    }, 1000);
                }else{
                    CASE_HANDLING.errorTries = 0;
                    TOASTR_HELPER.remove();
                    TOASTR_HELPER.error('There was a problem loading a module. Please refresh the page. If the issue persists, give us a call. Thanks!', {"timeOut":9000, "preventDuplicates": true});
                }
            }else{
                TOASTR_HELPER.warning('Something went wrong. Please try again. If the issue persists, give us a call. Thanks!');
                CASE_HANDLING._removeSpinners();
                CASE_HANDLING._enableFormButtons();
            }
        });

        $(document).ajaxStop(function() {
            if(!CASE_HANDLING.errorExists){
                CASE_HANDLING._removeSpinners();
            }
        });

        $(document).on('change', '#admit_date', function(){
            $('#discharge_date').val($(this).val());
        })

        $(document).on('blur', '#fin', function(){
            CASE_HANDLING.checkExistingCaseByFin($(this).data('lob-id'), $(this).val());
        })

        $(document).on('keyup', '#fin', function(event){
            CASE_HANDLING._validateNumeric($(this).attr('id'), 15);
        });

        $(document).on('keyup', '#mrn', function(event){
            CASE_HANDLING._validateNumeric($(this).attr('id'), 10);
        });

        $(document).on('keyup', '#price', function(event){
            CASE_HANDLING._validateNumeric($(this).attr('id'), 99999);
        });

        $(document).on('focus', '.select2', function (e) {
            if (e.originalEvent) {
                $(this).siblings('select').select2('open');    
            } 
        });

        $(".datepicker").datepicker({
            format: {
                toDisplay: function(date, format, language) {
                    return moment(date).format("MM/DD/YYYY");
                },
                toValue: function(date, format, language) {
                    return moment(date, ["DD.MM.YYYY", "DDMMYYYY"]).toDate();
                }
          }
        });

        if(_callback){
            _callback();
        }
    },

    startCase: function (lobId) {
        var formData = $('#frm-' + lobId).serializeArray();
        formData.push({name: 'duration', value: '00:00:00'});
        formData.push({name: 'created_at', value: TIME_HELPER.currentDateTime()});

        var payload = PAYLOAD.build(formData);

        $.ajax({
            type: 'POST',
            url: CASE_HANDLING.createCaseUri + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            data: payload,
            headers: {'Content-Type': 'application/vnd.api+json'},
            dataType: 'json'
        }).done(function (response) {
            if (typeof response.validation_errors !== 'undefined' ) {
                CASE_HANDLING._showValidationErrorPanel(lobId, response.validation_errors);
                return;
            }

            CASE_TIMER.startTimer();
            CASE_HANDLING._enableStopBtn();
            CASE_HANDLING._enableHoldBtn();
            CASE_HANDLING._enableEscalateBtn();
            CASE_HANDLING._enableContinueBtn();
            CASE_HANDLING._enableEditBtn();
            CASE_HANDLING._enableAuxBreakBtn();
            CASE_HANDLING._enableLobTab();
            CASE_HANDLING._closeValidationErrorPanel(lobId);

            setHiddenCaseIdValue(response.case_id);

            $('#hosp_serv').select2('open');
        })
    },

    updateCase: function (lobId, actionId, _callback) {
        
        var formData = $('#frm-' + lobId).serializeArray();
        var duration = CASE_TIMER.getTimerDuration();
        var hiddenDuration = getHiddenDurationValue();

        
        if (hiddenDuration != '00:00:00' && hiddenDuration != '' && actionId != 6)
        {
            duration = hiddenDuration;
        }

        formData.push({name: 'duration', value: duration});
        formData.push({name: 'action_id', value: actionId});
        formData.push({name: 'updated_at', value: TIME_HELPER.currentDateTime()});
        
        if(CASE_HANDLING._formValidation(actionId)){
            CASE_HANDLING._showValidationErrorPanel(lobId, CASE_HANDLING.validationErrors);
            CASE_HANDLING.validationErrors = [];
            CASE_HANDLING._enableFormButtons();
            return;
        }

        var payload = PAYLOAD.build(formData);
        
        $.ajax({
            type: 'PUT',
            url: CASE_HANDLING.updateCaseUri + '/' + lobId + '/' + getHiddenCaseIdValue() + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            data: payload,
            headers: {'Content-Type': 'application/vnd.api+json'},
            dataType: 'json'
        }).done(function (response) {

            if(typeof response.validation_errors !== 'undefined') {
                $.each(response.validation_errors, function(i, value ) {
                    CASE_HANDLING.validationErrors.push(response.validation_errors[i]);
                });
            }    
                  
            if (CASE_HANDLING.validationErrors.length > 0) {         
                CASE_HANDLING._showValidationErrorPanel(lobId, response.validation_errors);
                CASE_HANDLING._removeSpinners();
                return;
            } else {
                CASE_TIMER.stopTimer();

                setHiddenCaseIdValue(response.is_success);
                CASE_HANDLING.updateTrackerStats(lobId);
                CASE_HANDLING.buildCaseTable(lobId);
                CASE_HANDLING._clearForm(lobId);

                TOASTR_HELPER.success('Case done!');
                CASE_HANDLING._removeSpinners();
                CASE_TIMER.restartTimer();
                    if (_callback) {
                    _callback();
                }

                CASE_HANDLING.startCase(lobId);

                CASE_HANDLING.initializeSelect2();

            }    
          
        })
       
    },

    continueCase: function (lobId, caseId, actionId) {
        CASE_HANDLING._showConfirmationModal(lobId, caseId, actionId);
    },

    editCase: function (lobId, caseId, actionId) {
        CASE_HANDLING.populateCaseForm(lobId, caseId, actionId, function () {
            var stopBtn = $('.btn-stop-case');

            stopBtn.html('SAVE');
            stopBtn.attr('actionid', 8);
            stopBtn.addClass('btn-save-case');
            stopBtn.removeClass('btn-stop-case');
        });
    },

    saveCase: function (lobId, caseId) {
        CASE_HANDLING.updateCase(lobId, caseId, function () {
            var saveBtn = $('#frm-' + lobId + ' .btn-save-case');
            saveBtn.html('STOP');
            saveBtn.attr('actionid', 4);
            saveBtn.addClass('btn-primary btn-stop-case');
            saveBtn.removeClass('btn-save-case');

        });
    },

    deleteCase : function (caseId, lobId) {
        $.ajax({
            type: 'DELETE',
            url: CASE_HANDLING.deleteCaseUri + '/' + caseId + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            headers: {'Content-Type': 'application/vnd.api+json'},
            dataType: 'json'
        }).done(function(response) {

            return ;

        })
    },

    updateTrackerStats: function (lobId) {
        $.ajax({
            type: 'GET',
            url: CASE_HANDLING.trackerStatsUri + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            headers: {'Content-Type': 'application/vnd.api+json'},
            dataType: 'json'
        }).done(function (response) {
           
            $('#breaks-' + lobId).addClass(response.breakClass).removeClass('navy-bg red-bg lazur-bg');
            $('#breaks-' + lobId).html(response.totalBreaks);
            $('#cph-'+lobId).html(response.cph + " / " + response.target);
            $('#prod-' + lobId).html(response.total_prod_cases);
            $('#handled-' + lobId).html(response.total_completed_case);
        })
    },

    initializeSelect2 : function () {
        $('.select2').select2({
            width: '100%',
        });
        $('#accountstat').attr('tabindex','-1');
    },

    initializeDatePicker : function () {
        
         $('.date-picker').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            todayBtn: "linked",
            format: 'yyyy-mm-dd',
        });
    },

    buildCaseTable: function (lobId) {
        if ($.fn.dataTable.isDataTable('#processed-case-table')) {
            $('#processed-case-table').DataTable().clear();
            $('#processed-case-table').DataTable().destroy();
        }

        $.ajax({
            type: 'GET',
            url: CASE_HANDLING.casesUri + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            headers: {'Content-Type': 'application/vnd.api+json'},
            dataType: 'json'
        }).done(function (response) {
            var header = '';
            $.each(response.columns, function (key, val) {
                header += '<th>' + val.name + '</th>';
            });

            $('#processed-case-table>thead>tr').html(header);

            $('#processed-case-table').DataTable({
                "data": response.data,
                "columns": response.columns,
                "responsive": true
            });
        })
    },

    apopulateCaseForm: function (lobId, caseId, actionId, _callback) {
        var getCaseUri = CASE_HANDLING.caseUri + '/' + lobId + '/' + caseId + '?action=' + actionId + '&token=' + QUERY_STRING_HELPER.getByName('token');

        $.ajax({
            type: 'GET',
            url: getCaseUri,
            headers: {'Content-Type': 'application/vnd.api+json'},
            dataType: 'json'
        }).done(function (response) {
            var fields = response.fields;
            var caseInfo = response.case_info;
            var currentTimer = CASE_TIMER.getTimerDuration();
            var accumulatedTime = TIME_HELPER._addTimes(currentTimer,caseInfo.duration);
            console.log(caseInfo.id);
            setHiddenCaseIdValue(caseInfo.id);
            setHiddenDurationValue(accumulatedTime);

            // set time circle
            var dataDate =  CASE_TIMER._calculateDataDateByDuration(accumulatedTime);
            CASE_TIMER._setTimerDataDate(dataDate);

            $(fields).each(function (key, value) {
                var fieldValue = caseInfo[value.html_name];

                if(value.type == "dropdown") {
                    $('[name="' + value.html_name + '"]').select2('val', fieldValue);
                } else {
                    $('[name="' + value.html_name + '"]').val(fieldValue);
                }
            });

            if (_callback) {
                _callback();
            }
        })
    },

    checkCurrentAuxBreak: function () {
        $.ajax({
            type: 'GET',
            url: CASE_HANDLING.currentBreakUri + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType: 'json'
        }).done(function (response) {
            if (response.current_aux_break) {
                window.location.href = CASE_HANDLING.auxBreakUri + '?token=' + QUERY_STRING_HELPER.getByName('token');
            }
        })
    },

    checkOngoingCase : function (lobId) {
        $.ajax ({
            type: 'GET',
            url: CASE_HANDLING.currentCase + '/' + lobId +'?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType: 'json'
        }).done(function (response) {
         
            if (response.current_case_info) {
                var case_info = response.current_case_info;
                console.log(case_info.id);
                CASE_HANDLING.populateCaseForm(lobId, case_info.id, 0, function () {
                    var created_at = case_info.created_at
                    if(case_info.break_at != null) {
                        created_at = case_info.break_at ;
                    }
                    var dataDate = CASE_HANDLING._computeDatadate(case_info.duration, case_info.updated_at , created_at ,case_info.status_id);
                    (case_info.status_id == 7) ? CASE_HANDLING._editTimer() : CASE_HANDLING._continueTimer(dataDate);
                });
            } else {
                CASE_HANDLING.startCase(lobId);
            }
        })
    },

    checkExistingCaseByFin : function(lobId, fin){
        $.ajax ({
            type: 'GET',
            url: CASE_HANDLING.caseByFinUri + '/' + lobId + '/'+ fin +'?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType: 'json'
        }).done(function (response) {
            if(response.validationErrors){
                CASE_HANDLING._showValidationErrorPanel(lobId, [response.validationErrors]);
                CASE_HANDLING._removeSpinners();
                return;
            }

            var case_info = response.case_info;
            var currentCase = getHiddenCaseIdValue();
            if(case_info != null){
                var actionId;

                if(case_info.status_id == 6 && case_info.type == 0){
                    actionId = 3;
                }else{
                    actionId = case_info.status_id;
                }

                if(case_info.type == 0){
                    CASE_HANDLING.deleteCase(getHiddenCaseIdValue(), lobId);
                }

                CASE_HANDLING.populateCaseForm(lobId, case_info.id, actionId, function () {
                    var created_at = case_info.created_at
                    if(case_info.break_at != null) {
                        created_at = case_info.break_at ;
                    }

                    CASE_TIMER.startTimer();

                    if(case_info.type != 0){
                        CASE_TIMER.stopTimer();
                        CASE_TIMER.restartTimer();
                        CASE_TIMER.startTimer();
                        setHiddenDurationValue('00:00:00');
                        setHiddenCaseIdValue(currentCase);
                        CASE_HANDLING._updateCaseFin(lobId, getHiddenCaseIdValue());
                    }

                    CASE_HANDLING.buildCaseTable(lobId);

                    $('#accountstat').val('Old');
                });
            }else{
                $('#accountstat').val('New');
            }

            CASE_HANDLING._removeSpinners();
        })

    },

    _updateCaseFin : function(lobId, caseId){
        var formData = $('#frm-' + lobId).serializeArray();
        var currentTimer = CASE_TIMER.getTimerDuration();
        var accumulatedTime = TIME_HELPER._addTimes(currentTimer, '00:00:00');
        setHiddenDurationValue(accumulatedTime);

        console.log(getHiddenDurationValue());

        formData.push({name: 'duration', value: getHiddenDurationValue()});
        formData.push({name: 'action_id', value: 1});
        formData.push({name: 'updated_at', value: TIME_HELPER.currentDateTime()});

        var payload = PAYLOAD.build(formData);

        $.ajax({
            type: 'PUT',
            url: CASE_HANDLING.updateCaseFinUri + '/' + lobId + '/' + caseId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            data: payload,
            headers: {'Content-Type': 'application/vnd.api+json'},
            dataType: 'json'
        }).done(function (response) {
            CASE_TIMER.stopTimer();
            CASE_TIMER.restartTimer();
            CASE_TIMER.startTimer();
        });
    },

    _showConfirmationModal : function (lobId, caseId, actionId) {
        swal({
            title: "Ignore Current Case?",
            text: "Current time will add to the hold case, Continue?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "Cancel",
            confirmButtonText: "Continue",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function( confirmed ) {
            if( confirmed ) {
             CASE_HANDLING.deleteCase(getHiddenCaseIdValue(), lobId);
             CASE_HANDLING.populateCaseForm(lobId, caseId, actionId, function () {
                CASE_HANDLING.buildCaseTable(lobId);
                CASE_TIMER.startTimer();
             });
            
            CASE_HANDLING._removeSpinners();
            }
               
        })   

    },
  
    _clearForm: function (lobId) {
        $('#frm-' + lobId)[0].reset();
        $('.select2').select2();
    },

    _enableStopBtn: function () {
        $('.btn-stop-case').prop('disabled', '');
    },

    _enableContinueBtn: function () {
        $('.btn-continue').prop('disabled', '');
    },

    _enableEditBtn: function () {
        $('.btn-edit').prop('disabled', '');
    },

    _disableStopBtn: function () {
        $('.btn-stop-case').prop('disabled', 'disabled');
    },

    _enableStartBtn: function () {
        $('.btn-start-case').prop('disabled', '');
    },

    _disableStartBtn: function () {
        $('.btn-start-case').prop('disabled', 'disabled');
    },

    _enableHoldBtn: function () {
        $('.btn-hold-case').prop('disabled', '');
    },

    _disableHoldBtn: function () {
        $('.btn-hold-case').prop('disabled', 'disabled');
    },

    _disableContinueBtn: function () {
        $('.btn-continue').prop('disabled', 'disabled');
    },

    _disableEditBtn: function () {
        $('.btn-edit').prop('disabled', 'disabled');
    },

    _enableAuxBreakBtn: function () {
        $('#btn-aux-break').prop('disabled', '');
    },

    _disableAuxBreakBtn: function () {
        $('#btn-aux-break').prop('disabled', 'disabled');
    },

    _enableEscalateBtn: function () {
        $('.btn-escalate-case').prop('disabled', '');
    },

    _disableEscalateBtn: function () {
        $('.btn-escalate-case').prop('disabled', 'disabled');
    },

    _enableLobTab : function () {
        $('.lob-selector').removeClass('disabled');
    },

    _disableLobTab : function () {
      $('.lob-selector').addClass('disabled');
    },

    _showValidationErrorPanel: function (lobId, validationErrors) {
        var errorList = "";
        var validationPanel = $('#validation-error-panel-'+lobId);

        $('#validation-error-list-'+lobId).html("");

        // for re-animation
        if (validationPanel.is(':visible') === true) {
            validationPanel.hide("fast");
        }

        validationPanel.show("fast", function () {
            $.each(validationErrors, function (key, value) {
                errorList += '<li class="text-danger">' + value + '</li>';
            });

            $('#validation-error-list-'+lobId).html(errorList);
        });
    },

    _closeValidationErrorPanel: function (lobId) {
        $('#validation-error-panel-'+lobId).hide('fast');
    },

    _continueTimer: function ($datadate) {
        CASE_TIMER._setTimerDataDate($datadate);
        CASE_TIMER.startTimer();


        CASE_HANDLING._enableStopBtn();
        CASE_HANDLING._enableHoldBtn();
        CASE_HANDLING._enableEscalateBtn();

    },

    _editTimer: function () {

        CASE_HANDLING._enableStopBtn();
        CASE_HANDLING._enableHoldBtn();
        CASE_HANDLING._enableEscalateBtn();
    },

    _computeDatadate : function (duration , updatedAt ,createdAt ,statusId) {
        var elapsedTime = duration;
        var elapsedMil = moment.duration(elapsedTime).asMilliseconds()
        var currentTime =  moment(updatedAt).subtract(elapsedMil).format('YYYY-MM-DD HH:mm:ss');
        if(statusId == 1) {
            currentTime = createdAt;
        }
        return currentTime;
    },

    _setAccountColor : function () {
        $('#account-header').addClass(CONSTANT.accountColor['Royal Blue']);
    },

    _formValidation: function(actionId){
        var hospServ = $('#hosp_serv');
        var fin = $('#fin');
        var mrn = $('#mrn');
        var admitDate = $('#admit_date');
        var dischargeDate = $('#discharge_date');
        var price = $('#price');
        var observation = $('#observation');
        var holdReason = $('#hold_reason');
        var accountStat = $('#accountstat');
        var comments = $('#comments');

        $('.form-group').removeClass('has-error')

        if(hospServ.val() == 0){
            CASE_HANDLING.validationErrors.push("<b>Hospital Service<\/b> is required.")
            hospServ.parents('.form-group').addClass('has-error')
        }

        if(fin.val().replace(/\s/g,'') == ""){
            CASE_HANDLING.validationErrors.push("<b>FIN<\/b> is required.")
            fin.parents('.form-group').addClass('has-error')
        }

        if(mrn.val().replace(/\s/g,'') == ""){
            CASE_HANDLING.validationErrors.push("<b>MRN<\/b> is required.")
            mrn.parents('.form-group').addClass('has-error')
        }

        if(admitDate.val().replace(/\s/g,'') == ""){
            CASE_HANDLING.validationErrors.push("<b>Admit Date<\/b> is required.")
            admitDate.parents('.form-group').addClass('has-error')
        }

        if(dischargeDate.val().replace(/\s/g,'') == ""){
            CASE_HANDLING.validationErrors.push("<b>Discharge Date<\/b> is required.")
            dischargeDate.parents('.form-group').addClass('has-error')
        }

        if(price.val().replace(/\s/g,'') == ""){
            CASE_HANDLING.validationErrors.push("<b>Price<\/b> is required.")
            price.parents('.form-group').addClass('has-error')
        }

        if(observation.val() == 0 || observation.val() == null){
            CASE_HANDLING.validationErrors.push("<b>Observation<\/b> is required.")
            observation.parents('.form-group').addClass('has-error')
        }

        if((holdReason.val() == 0 || holdReason.val() == null) && actionId == 6){
            CASE_HANDLING.validationErrors.push("<b>Hold Reason<\/b> is required.")
            holdReason.parents('.form-group').addClass('has-error')
        }

        if (CASE_HANDLING.validationErrors.length > 0) {         
            return true;
        }else{
            return false;
        }

    },

    _removeSpinners : function(){
        $('#case-form').children('.ibox-content').removeClass('sk-loading');
        $('#case-table').children('.ibox-content').removeClass('sk-loading');
    },

    _addSpinners : function(){
        $('#case-form').children('.ibox-content').addClass('sk-loading');
        $('#case-table').children('.ibox-content').addClass('sk-loading');
    },

    _validateNumeric : function(id, maxLength){
        if($('#'+ id).val().length > maxLength){
            $('#'+ id).val($('#'+ id).val().substring(0, maxLength));
        }

        if(!$.isNumeric($('#'+ id).val().substr($('#'+ id).val().length - 1))){
            $('#'+ id).val($('#'+ id).val().substring(0,$('#'+ id).val().length-1))
        }
    },

    _disableFormButtons : function(){
        $('.btn-stop-case').button('loading');
        $('.btn-hold-case').button('loading');
    },

    _enableFormButtons : function(){
        $('.btn-hold-case').button('reset');
        $('.btn-stop-case').button('reset');
    }

};

$(document).ready(function () {
    CASE_HANDLING.build(function(){
        if($('#frm-8').is(':visible')){
            $('#hosp_serv').select2('open');
        }
    });
});