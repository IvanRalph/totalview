var CASE_HANDLING = {
    createCaseUri : CONSTANT.accountApiName + '/create-case',
    updateCaseUri : CONSTANT.accountApiName + '/update-case',
    trackerStatsUri : CONSTANT.accountApiName + '/tracker-stats',
    casesUri : CONSTANT.accountApiName + '/cases',
    caseUri : CONSTANT.accountApiName + '/case',
    currentBreakUri : CONSTANT.accountApiName + '/aux-break/current',
    currentCase : CONSTANT.accountApiName + '/case/current',
    auxBreakUri : CONSTANT.accountWebName + '/aux-break',
    reworkCase : CONSTANT.accountApiName + '/case-rework',
    getOngoingCaseLobUri : CONSTANT.accountApiName + '/case-currentLob',
    isStart:0,
    lobId:0,

    build : function () {

        CASE_HANDLING.initializeSelect2();
        CASE_HANDLING.checkCurrentAuxBreak();
        CASE_HANDLING._disableStartBtn();
        //CASE_HANDLING.checkAndDisableOtherTabs(9);
        // CASE_HANDLING.checkOngoingCase($('.lob-selector.active').attr('data-lob-id'));
        CASE_HANDLING._getLobOfOnGoingCase();
        

        $(document).on('click', '.btn-start-case', function () {
            CASE_HANDLING.startCase($(this).attr('lobid'));
        });

        $(document).on('click', '.btn-stop-case', function () {
            CASE_HANDLING.updateCase($(this).attr('lobid'), $(this).attr('actionid'));
        });

        $(document).on('click', '.btn-hold-case', function () {
            CASE_HANDLING.updateCase($(this).attr('lobid'), $(this).attr('actionid'));
        });

        $(document).on('click', '.btn-save-case', function () {
            CASE_HANDLING.saveCase($(this).attr('lobid'), $(this).attr('actionid'));
        });

        $(document).on('click', '.btn-escalate-case', function () {
            CASE_HANDLING.updateCase($(this).attr('lobid'), $(this).attr('actionid'));
        });

        $(document).on('click', '.btn-edit', function () {
            CASE_HANDLING.editCase($(this).attr('data-lob-id'), $(this).attr('data-case-id'), $(this).attr('data-action-id'));
        });

        $(document).on('click', '.btn-continue', function () {
            CASE_HANDLING.continueCase($(this).attr('data-lob-id'), $(this).attr('data-case-id'), $(this).attr('data-action-id'));
        });

        $(document).on('click', '.lob-selector', function () {

            if(CASE_HANDLING.isStart == 0){
                 CASE_HANDLING.buildCaseTable($(this).attr('data-lob-id'));
                //CASE_HANDLING._getLobOfOnGoingCase();
            }
         
        });

        $(document).on('click', '#btn-aux-break', function () {
            window.location.href = CASE_HANDLING.auxBreakUri + '?token=' + QUERY_STRING_HELPER.getByName('token');
        });

        $(document).on('click', '#close-validation-error-panel', function () {
            CASE_HANDLING._closeValidationErrorPanel($(this).attr('data-lob-id'));
        });

        $(document).on('keyup', '#pages', function () {
            $('#benchmark').val(CASE_HANDLING._computeBenchmark());
        });

        $(document).on('blur', '#service_id,#claimnant_2', function () {
            if(CASE_HANDLING.lobId == 0){
                var lobId = $('#lob-tab li.active').attr('data-lob-id');
            }else{
                var lobId = CASE_HANDLING.lobId;
            }
          

            if (lobId == 9 && $('#service_id').val() != "") {
                if(CASE_HANDLING.isStart != 1){
                    CASE_HANDLING._enableStartBtn();
                }
            } else if (lobId == 10 && $('#claimnant_2').val() != "") {
                if(CASE_HANDLING.isStart != 1){
                    CASE_HANDLING._enableStartBtn();
                }
             
            } else {
                TOASTR_HELPER.warning('Kindly Input ID first');
                CASE_HANDLING._disableStartBtn();
            }

            if ($(this).val().trim()) {
                CASE_HANDLING.checkReworkCase($('#lob-tab li.active').attr('data-lob-id'), $(this).val());
            }
        });

        CASE_HANDLING.buildCaseTable($('#lob-tab li.active').attr('data-lob-id'));
    },

    initializeSelect2 : function () {
        $('.select2').select2({
            width : '100%',
        });
    },


    checkAndDisableOtherTabs : function (lobid) {


        $('.nav-tabs li').removeClass('active');
        $('.tab-pane').removeClass('active');
        $('#selector' + lobid + '').addClass('active');
        $('#tab' + lobid + '').addClass('active');

        if(CASE_HANDLING.isStart == 1){
            $('.lob-selector a').removeAttr('data-toggle');
        }
        if(CASE_HANDLING.isStart == 0){

            $('.lob-selector a').attr('data-toggle','tab');
        }
        

    },


    startCase : function (lobId) {
        var formData = $('#frm-' + lobId).serializeArray();
        formData.push({name : 'duration', value : '00:00:00'});
        formData.push({name : 'created_at', value : TIME_HELPER.currentDateTime()});

        var payload = PAYLOAD.build(formData);

        $.ajax({
            type : 'POST',
            url : CASE_HANDLING.createCaseUri + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            data : payload,
            headers : {'Content-Type' : 'application/vnd.api+json'},
            dataType : 'json'
        }).done(function (response) {
            if (typeof response.validation_errors !== 'undefined') {
                CASE_HANDLING._showValidationErrorPanel(lobId, response.validation_errors);
                return;
            }
            CASE_HANDLING.isStart = 1;
            CASE_HANDLING.checkAndDisableOtherTabs(lobId);
            CASE_TIMER.startTimer();
            CASE_HANDLING._enableStopBtn();
            CASE_HANDLING._enableHoldBtn();
            CASE_HANDLING._enableEscalateBtn();
            CASE_HANDLING._disableContinueBtn();
            CASE_HANDLING._disableEditBtn();
            CASE_HANDLING._disableStartBtn();
            CASE_HANDLING._disableAuxBreakBtn();
            CASE_HANDLING._closeValidationErrorPanel();

       
            setHiddenCaseIdValue(response.case_id);
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.');
        });
    },

    updateCase : function (lobId, actionId, _callback) {
        var claimnant = (lobId == 9) ? 1 : 2;
        $('#claimnant_' + claimnant).val($('#claimnant_' + claimnant).val().toLowerCaseWords().toUpperCaseWords());
        var formData = $('#frm-' + lobId).serializeArray();
        var duration = CASE_TIMER.getTimerDuration();
        var hiddenDuration = getHiddenDurationValue();

        formData.push({name : 'duration', value : duration});
        formData.push({name : 'action_id', value : actionId});
        formData.push({name : 'updated_at', value : TIME_HELPER.currentDateTime()});

        var payload = PAYLOAD.build(formData);

        $.ajax({
            type : 'PUT',
            url : CASE_HANDLING.updateCaseUri + '/' + lobId + '/' + getHiddenCaseIdValue() + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            data : payload,
            headers : {'Content-Type' : 'application/vnd.api+json'},
            dataType : 'json'
        }).done(function (response) {
            if (typeof response.validation_errors !== 'undefined') {
                CASE_HANDLING._showValidationErrorPanel(lobId, response.validation_errors);
                return;
            } else {
                CASE_HANDLING.isStart = 0;
                CASE_HANDLING.checkAndDisableOtherTabs(lobId);
                CASE_TIMER.stopTimer();
                CASE_HANDLING._disableStopBtn();
                CASE_HANDLING._disableHoldBtn();
                CASE_HANDLING._disableEscalateBtn();
                CASE_HANDLING._disableStartBtn();
                CASE_HANDLING._enableAuxBreakBtn();
                CASE_HANDLING._closeValidationErrorPanel(lobId);
                setHiddenCaseIdValue(response.is_success);
                CASE_HANDLING.updateTrackerStats(lobId);
                CASE_HANDLING.buildCaseTable(lobId);
                CASE_HANDLING._clearForm(lobId);
                TOASTR_HELPER.success('Case done!');

                if (_callback) {
                    _callback();
                }
                CASE_TIMER.restartTimer();
            }
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.')
        });

    },

    continueCase : function (lobId, caseId, actionId) {
        CASE_HANDLING.populateCaseForm(lobId, caseId, actionId, function () {
             CASE_HANDLING.isStart = 1;
            CASE_HANDLING.checkAndDisableOtherTabs(lobId);
            CASE_TIMER.startTimer();
        });

        CASE_HANDLING._enableStopBtn();
        CASE_HANDLING._enableHoldBtn();
        CASE_HANDLING._enableEscalateBtn()
        CASE_HANDLING._disableStartBtn();
        CASE_HANDLING._disableContinueBtn();
        CASE_HANDLING._disableEditBtn();
        CASE_HANDLING._disableAuxBreakBtn();
    },

    editCase : function (lobId, caseId, actionId) {
        CASE_HANDLING.populateCaseForm(lobId, caseId, actionId, function () {
            var stopBtn = $('#frm-' + lobId + ' .btn-stop-case');

            CASE_HANDLING._disableStartBtn();
            CASE_HANDLING._enableStopBtn();
            CASE_HANDLING._disableContinueBtn();
            CASE_HANDLING._disableEditBtn();
            CASE_HANDLING._disableAuxBreakBtn();

            stopBtn.html('SAVE');
            stopBtn.attr('actionid', 8);
            stopBtn.addClass('btn-save-case');
            stopBtn.removeClass('btn-stop-case');
        });
    },

    saveCase : function (lobId, caseId) {
        CASE_HANDLING.updateCase(lobId, caseId, function () {
            var saveBtn = $('#frm-' + lobId + ' .btn-save-case');
            saveBtn.html('STOP');
            saveBtn.attr('actionid', 4);
            saveBtn.addClass('btn-primary btn-stop-case');
            saveBtn.removeClass('btn-save-case');

            CASE_HANDLING._disableStopBtn();
            CASE_HANDLING._disableHoldBtn();
            CASE_HANDLING._enableContinueBtn();
            CASE_HANDLING._enableEditBtn();
            CASE_HANDLING._disableStartBtn();
        });
    },

    updateTrackerStats : function (lobId) {
        $.ajax({
            type : 'GET',
            url : CASE_HANDLING.trackerStatsUri + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            headers : {'Content-Type' : 'application/vnd.api+json'},
            dataType : 'json'
        }).done(function (response) {
            $('#completed-case-count-' + lobId).html(response.total_completed_case);
            $('#aht-' + lobId).html(response.aht);
            $('#icd-9').html(response.total_icd);
            $('#claimnant-9').html(response.total_claimnant);
            $('#pages-10').html(response.total_pages);
            $('#benchmark-10').html(response.total_benchmark);

        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong');
        })
    },

    buildCaseTable : function (lobId) {
        if ($.fn.dataTable.isDataTable('#processed-case-table')) {
            $('#processed-case-table').DataTable().clear();
            $('#processed-case-table').DataTable().destroy();
        }

        $('#case-table').children('.ibox-content').toggleClass('sk-loading');
        $.ajax({
            type : 'GET',
            url : CASE_HANDLING.casesUri + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            headers : {'Content-Type' : 'application/vnd.api+json'},
            dataType : 'json'
        }).done(function (response) {

            var header = '';
            $.each(response.columns, function (key, val) {
                header += '<th>' + val.name + '</th>';
            });

            $('#processed-case-table>thead>tr').html(header);

            $('#processed-case-table').DataTable({
                "data" : response.data,
                "columns" : response.columns,
                "responsive" : true,
                "order" : []
            });

            $('#case-table').children('.ibox-content').toggleClass('sk-loading');
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong');
        })
    },

    populateCaseForm : function (lobId, caseId, actionId, _callback) {
        var getCaseUri = CASE_HANDLING.caseUri + '/' + lobId + '/' + caseId + '?action=' + actionId + '&token=' + QUERY_STRING_HELPER.getByName('token');

        $.ajax({
            type : 'GET',
            url : getCaseUri,
            headers : {'Content-Type' : 'application/vnd.api+json'},
            dataType : 'json'
        }).done(function (response) {

            var fields = response.fields;
            var caseInfo = response.case_info;

            setHiddenCaseIdValue(caseInfo.id);
            setHiddenDurationValue(caseInfo.duration);

            // set time circle
            var dataDate = CASE_TIMER._calculateDataDateByDuration(caseInfo.duration);
            CASE_TIMER._setTimerDataDate(dataDate);

            $(fields).each(function (key, value) {
                var fieldValue = caseInfo[value.html_name];
                $('[name="' + value.html_name + '"]').val(fieldValue);
            });

            if (_callback) {
                _callback();
            }
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.');
        });
    },

    checkCurrentAuxBreak : function () {
        $.ajax({
            type : 'GET',
            url : CASE_HANDLING.currentBreakUri + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType : 'json'
        }).done(function (response) {
            if (response.current_aux_break) {
                window.location.href = CASE_HANDLING.auxBreakUri + '?token=' + QUERY_STRING_HELPER.getByName('token');
            }
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.');
        })
    },

    checkOngoingCase : function (lobId) {

        $.ajax({
            type : 'GET',
            url : CASE_HANDLING.currentCase + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType : 'json'
        }).done(function (response) {
            if (response.current_case_info) {

                CASE_HANDLING.isStart = 1;
                var case_info = response.current_case_info;

                CASE_HANDLING.populateCaseForm(lobId, case_info.id, 0, function () {
                    var dataDate = CASE_HANDLING._computeDatadate(case_info.duration, case_info.updated_at, case_info.created_at, case_info.status_id);
                    (case_info.status_id == 7) ? CASE_HANDLING._editTimer() : CASE_HANDLING._continueTimer(dataDate);
                });

                CASE_HANDLING.checkAndDisableOtherTabs(lobId);
            }
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong. checkOngoingCase');
        })
    },

    
    checkReworkCase : function (lobId, reference) {
        $.ajax({
            type : 'GET',
            url : CASE_HANDLING.reworkCase + '/' + lobId + '/' + reference + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType : 'json'
        }).done(function (response) {
            if (response.case_info) {
                var case_info = response.case_info;
                var actionId = 1; //start

                CASE_HANDLING._showConfirmationModal(lobId, case_info.id, actionId);
            }
        }).fail(function () {
            TOASTR_HELPER.error('Unable to retrieve data, please refresh.');
        })

    },

    _showConfirmationModal : function (lobId, caseId, actionId) {
        swal({
            title : "Detected Rework Case",
            text : "Retrieve previous case details?",
            type : "warning",
            showCancelButton : true,
            confirmButtonColor : "#DD6B55",
            cancelButtonText : "No, don't do anything!",
            confirmButtonText : "Yes, Retrieve data.",
            closeOnConfirm : true,
            closeOnCancel : true
        }, function (confirmed) {
            if (confirmed) {
                CASE_HANDLING.isStart = 1;
                CASE_HANDLING.populateCaseForm(lobId, caseId, actionId, function () {
                    CASE_TIMER.startTimer();
                    CASE_HANDLING._disableStartBtn();
                    CASE_HANDLING._enableStopBtn();
                    CASE_HANDLING._enableHoldBtn();
                });

            }

        })

    },

    _getLobOfOnGoingCase : function () {
        $.ajax({
            type : 'GET',
            url : CASE_HANDLING.getOngoingCaseLobUri + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType : 'json'
        }).done(function (response) {
            console.log(response);

            if (response.current_case_lob) {
                if(response.current_case_lob.action_id == 1 || response.current_case_lob.action_id == 3 || response.current_case_lob.action_id == 6){
                   CASE_HANDLING.checkOngoingCase(response.current_case_lob.lob_id);
                }
            }else{
                   CASE_HANDLING.checkOngoingCase($('.lob-selector.active').attr('data-lob-id'));
            }
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong. _getLobOfOnGoingCase');
        })
     },

    _computeBenchmark : function () {
        return ($('#pages').val() / 400).toFixed(2);
    },

    _clearForm : function (lobId) {
        $('#frm-' + lobId)[0].reset();
    },

    _enableStopBtn : function () {
        $('.btn-stop-case').prop('disabled', '');
    },

    _enableContinueBtn : function () {
        $('.btn-continue').prop('disabled', '');
    },

    _enableEditBtn : function () {
        $('.btn-edit').prop('disabled', '');
    },

    _disableStopBtn : function () {
        $('.btn-stop-case').prop('disabled', 'disabled');
    },

    _enableStartBtn : function () {
        $('.btn-start-case').prop('disabled', '');
    },

    _disableStartBtn : function () {
        $('.btn-start-case').prop('disabled', 'disabled');
    },

    _enableHoldBtn : function () {
        $('.btn-hold-case').prop('disabled', '');
    },

    _disableHoldBtn : function () {
        $('.btn-hold-case').prop('disabled', 'disabled');
    },

    _disableContinueBtn : function () {
        $('.btn-continue').prop('disabled', 'disabled');
    },

    _disableEditBtn : function () {
        $('.btn-edit').prop('disabled', 'disabled');
    },

    _enableAuxBreakBtn : function () {
        $('#btn-aux-break').prop('disabled', '');
    },

    _disableAuxBreakBtn : function () {
        $('#btn-aux-break').prop('disabled', 'disabled');
    },

    _enableEscalateBtn : function () {
        $('.btn-escalate-case').prop('disabled', '');
    },

    _disableEscalateBtn : function () {
        $('.btn-escalate-case').prop('disabled', 'disabled');
    },

    _showValidationErrorPanel : function (lobId, validationErrors) {
        var errorList = "";
        var validationPanel = $('#validation-error-panel-' + lobId);

        $('#validation-error-list-' + lobId).html("");

        // for re-animation
        if (validationPanel.is(':visible') === true) {
            validationPanel.hide("fast");
        }

        validationPanel.show("fast", function () {
            $.each(validationErrors, function (key, value) {
                errorList += '<li class="text-danger">' + value + '</li>';
            });

            $('#validation-error-list-' + lobId).html(errorList);
        });
    },

    _closeValidationErrorPanel : function (lobId) {
        $('#validation-error-panel-' + lobId).hide('fast');
    },

    _continueTimer : function ($datadate) {
        CASE_TIMER._setTimerDataDate($datadate);
        CASE_TIMER.startTimer();

        CASE_HANDLING._disableStartBtn();
        CASE_HANDLING._enableStopBtn();
        CASE_HANDLING._enableHoldBtn();
        CASE_HANDLING._disableAuxBreakBtn();
    },

    _editTimer : function () {

        CASE_HANDLING._disableStartBtn();
        CASE_HANDLING._enableStopBtn();
        CASE_HANDLING._enableHoldBtn();
        CASE_HANDLING._disableAuxBreakBtn();
    },

    _computeDatadate : function (duration, updatedAt, createdAt, statusId) {
        var elapsedTime = duration;
        var elapsedMil = moment.duration(elapsedTime).asMilliseconds()
        var currentTime = moment(updatedAt).subtract(elapsedMil).format('YYYY-MM-DD HH:mm:ss');
        if (statusId == 1) {
            currentTime = createdAt;
        }
        return currentTime;
    }

};

$(document).ready(function () {
    CASE_HANDLING.build();
});

String.prototype.toUpperCaseWords = function () {
    return this.replace(/\w+/g, function (a) {
        return a.charAt(0).toUpperCase() + a.slice(1).toLowerCase()
    });
}

String.prototype.toLowerCaseWords = function () {
    return this.replace(/\w+/g, function (a) {
        return a.charAt(0).toLowerCase() + a.slice(1).toLowerCase()
    });
}