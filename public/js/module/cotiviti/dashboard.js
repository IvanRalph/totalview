var DASHBOARD = {
    metricsUri : CONSTANT.accountApiName + '/dashboard/metrics',
    userListUri : CONSTANT.accountApiName + '/dashboard/users',
    userActivityUri : CONSTANT.accountApiName + '/dashboard/user-activity',
    kickoutUri : CONSTANT.accountApiName + '/kickout',
    userListDataTable : {},
    userLoggedStatus : {
        '1' : {
            'class' : 'info',
            'text' : 'Online'
        },
        '0' : {
            'class' : 'danger',
            'text' : 'Offline'
        }
    },

    build : function () {
        var defaultSelectedLob = $('#slct-lob').val();
        var defaultSelectedTeam = $('#slct-team').val();
        DASHBOARD.buildMetrics(defaultSelectedLob, defaultSelectedTeam);
        DASHBOARD.buildUserList(defaultSelectedLob, defaultSelectedTeam);

        $(document).on('change', '#slct-lob', function () {
            DASHBOARD.buildMetrics($(this).val(), defaultSelectedTeam);
            DASHBOARD.buildUserList($(this).val(), defaultSelectedTeam);
        });

        $(document).on('change', '#slct-team', function () {
            DASHBOARD.buildMetrics($('#slct-lob').val(), $(this).val());
            DASHBOARD.buildUserList($('#slct-lob').val(), $(this).val());
        });

        $(document).on('click', '.showActivity', function () {
            DASHBOARD.buildUserActivity($(this));
        })

        $('.fullScreenBtn').on('click', function (e) {
            e.preventDefault();

            var lob = $('#slct-lob').val();
            var shiftDate = $('#slct-shift-date').val();
            var team = $('#slct-team').val();

            window.open('/cotiviti/dashboard-fullscreen?lob=' + lob + '&team=' + team + '&token=' + QUERY_STRING_HELPER.getByName('token'), "_blank");
        })

        $(document).on('click', '.manualKickOut', function (e) {
            e.preventDefault();
            DASHBOARD.manualKickOut($('#slct-lob').val(), $('#slct-team').val());
        });
    },

    buildMetrics : function (lobId, team) {
        $('.metric-panel').children('.ibox-content').addClass('sk-loading');

        $('.serviceLines').html('');

        $.ajax({
            type : 'GET',
            url : DASHBOARD.metricsUri + '/' + lobId + '/' + team + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType : 'json'
        }).done(function (response) {
            $('.avgCph').html(parseFloat(response.avg_cph).toFixed(2));
            $('.avgCphTarget').html(response.avg_cph_target);
            $('.totalNonProd').html(response.total_non_prod);
            $('.totalProdCases').html(response.total_prod_cases);

            var resultHeader = DASHBOARD.buildServiceLineHeader(response.new_list);
            var listResult = DASHBOARD.buildServiceLine(resultHeader['tbody']);
            var serviceLineHtml = '<table class="table table-condensed table-bordered text-center text-info font-bold">' +
                '<tbody>' +
                '<tr style="color: #676a6c;">' +
                '<td>Service Lines</td>' + resultHeader['thead'].replace(/undefined/i, "") +
                '</tr>' +
                '' +
                listResult +
                '</tbody></table>';
            $('#newServiceLines').html(serviceLineHtml);
            $('.metric-panel').children('.ibox-content').removeClass('sk-loading');
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.');
            $('.metric-panel').children('.ibox-content').removeClass('sk-loading');
        });
    },

    buildServiceLineHeader : function (result) {
        var serviceLineHeader = [];
        var serviceLineCounter = 0;
        var serviceLineHeader1 = [];
        serviceLineHeader['tbody'] = [];
        $.each(result, function (key, value) {
            serviceLineHeader['thead'] += '<td>' + key + '</td>';
            serviceLineCounter = serviceLineCounter + 1;
            serviceLineHeader['tbody'] = DASHBOARD.buildServiceLineBody(value, serviceLineHeader['tbody']);
            if (serviceLineCounter == 3) {
                return false;
            }
        });
        return serviceLineHeader;
    },

    buildServiceLineBody : function (result, tBody) {
        var serviceLineBody = [];
        serviceLineBody = $.merge(serviceLineBody, tBody);
        var serviceLineCounter = 0;
        $.each(result, function (key, value) {
            tBody[key] += '<td class="no-margins text-info text-center">' + value + '</td>';
            serviceLineCounter = serviceLineCounter + 1;
            serviceLineBody = tBody;
            if (serviceLineCounter == 3) {
                return false;
            }
        });
        return serviceLineBody;
    },

    buildServiceLine : function (tBody) {

        var serviceLineHtml = ''
        var serviceLineCounter = 0;

        serviceLineHtml += '<tr><td class="no-margins text-info text-center">CAT</td>' + tBody['CAT'].replace(/undefined/i, "") + '</tr>';
        serviceLineHtml += '<tr><td class="no-margins text-info text-center">Wellcare</td>' + tBody['Wellcare'].replace(/undefined/i, "") + '</tr>';
        serviceLineHtml += '<tr><td class="no-margins text-info text-center">Wellpoint</td>' + tBody['Wellpoint'].replace(/undefined/i, "") + '</tr>';

        /**
         $.each(tBody, function(index, value) {
            serviceLineHtml += '<tr><td></td>' + value.replace(/undefined/i, "") + '</tr>';
            serviceLineCounter = serviceLineCounter + 1;
            if (serviceLineCounter==3){
                return false;
            }
        });
         **/
        return serviceLineHtml;
    },

    buildUserList : function (lobId, team) {
        if ($.fn.dataTable.isDataTable('#productivity-table')) {
            DASHBOARD.userListDataTable.destroy();
        }

        DASHBOARD.userListDataTable = $('#productivity-table').DataTable({
            processing : true,
            'order' : [],
            dom : '<"html5buttons"B>lTfgitp',
            buttons : [
                {extend : 'excel'},
            ],
            ajax : {
                url : DASHBOARD.userListUri + '/' + lobId + '/' + team + '?token=' + QUERY_STRING_HELPER.getByName('token')
            },
            columns : [
                {
                    render : function (data, type, row) {
                        return '<a href="#" id="' + row.main_user_id + '" class="showActivity">' + row.name + '</a>';
                    }
                },
                {data : 'team'},
                {data : 'shift_date'},
                {data : 'shift_schedule'},
                {data : 'total_completed_cases'},
                {data : 'total_pending_cases'},
                {data : 'cph'},
                {data : 'cph_target'},
                {data : 'total_handled_cases'},
                {data : 'total_prod_cases'},
                {data : 'total_duration'},
                {data : 'aht'},
                {
                    render : function (data, type, row) {
                        var label = DASHBOARD.userLoggedStatus[row.status];
                        return '<div class="text-center"><span class="label label-' + label.class + '">' + label.text + '</span></div>';
                    }
                },
            ],
            "lengthMenu" : [
                [
                    10,
                    25,
                    50,
                    -1
                ],
                [
                    10,
                    25,
                    50,
                    "All"
                ]
            ],
            "footerCallback" : function () {
                var api = this.api();

                var durationToSec = function (duration) {
                    return moment.duration(duration).asSeconds();
                };

                var completedCasesTotal = api.column(4, {search : 'applied'})
                    .data()
                    .reduce(function (a, b) {
                        return parseFloat(a) + parseFloat(b);
                    }, 0);

                var pendingCaseTotal = api.column(5, {search : 'applied'})
                    .data()
                    .reduce(function (a, b) {
                        return parseFloat(a) + parseFloat(b);
                    }, 0);

                var handledCaseTotal = api.column(8, {search : 'applied'})
                    .data()
                    .reduce(function (a, b) {
                        return parseFloat(a) + parseFloat(b);
                    }, 0);

                var prodCaseTotal = api.column(9, {search : 'applied'})
                    .data()
                    .reduce(function (a, b) {
                        return parseFloat(a) + parseFloat(b);
                    }, 0);

                var escalatedCaseTotal = api.column(6, {search : 'applied'})
                    .data()
                    .reduce(function (a, b) {
                        return parseFloat(a) + parseFloat(b);
                    }, 0);

                var casesDurationTotal = api.column(10, {search : 'applied'})
                    .data()
                    .reduce(function (a, b) {
                        return TIME_HELPER.secToHHMMSS(durationToSec(a) + durationToSec(b));
                    }, 0);

                var ahtTotal = api.column(11, {search : 'applied'})
                    .data()
                    .reduce(function (a, b) {
                        return TIME_HELPER.secToHHMMSS(durationToSec(a) + durationToSec(b));
                    }, 0);

                $(api.column(3).footer()).html('TOTAL');
                $(api.column(4).footer()).html(completedCasesTotal);
                $(api.column(5).footer()).html(pendingCaseTotal);
                $(api.column(8).footer()).html(handledCaseTotal);
                $(api.column(9).footer()).html(prodCaseTotal);
                $(api.column(10).footer()).html(casesDurationTotal);
                $(api.column(11).footer()).html(ahtTotal);
            },

            "scrollX" : true,

            "fnInitComplete" : function () {
                // Disable TBODY scoll bars
                $('.dataTables_scrollBody').css({
                    'overflow' : 'hidden',
                    'border' : '0'
                });

                // Enable TFOOT scoll bars
                $('.dataTables_scrollFoot').css('overflow', 'auto');

                // Sync TFOOT scrolling with TBODY
                $('.dataTables_scrollFoot').on('scroll', function () {
                    $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
                });
            },
        });
    },

    buildUserActivity : function (userLinkObj) {
        var userId = userLinkObj.attr('id');
        var nurseName = userLinkObj.text();

        $('.activity-panel').children('.ibox-content').toggleClass('sk-loading');

        $.ajax({
            type : 'GET',
            url : DASHBOARD.userActivityUri + '/' + userId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType : 'json'
        }).done(function (response) {
            DASHBOARD.buildActivityTimeline(response.activities)
            $('.nurseName').html(nurseName);
            $('.activity-panel').children('.ibox-content').toggleClass('sk-loading');
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.');
        });
    },

    buildActivityTimeline : function (activities) {
        var timelineBlock = '';

        for (i in activities) {
            var activity = activities[i];
            timelineBlock += '<div class="vertical-timeline-block">';
            timelineBlock += '<div class="vertical-timeline-icon ' + activity.icon_bg + '"><i class="fa ' + activity.icon + '"></i></div>';
            timelineBlock += '<div class="vertical-timeline-content">';
            timelineBlock += '<span id="activityDate" class="vertical-date pull-right"><small>' + activity.timestamp + '</small></span>';
            timelineBlock += '<h2>' + activity.type + '</h2>';
            timelineBlock += '<p>' + activity.description + '</p></div>';
            timelineBlock += '</div>';
        }

        if (timelineBlock == '') {
            timelineBlock += '<div class="vertical-timeline-block">';
            timelineBlock += '<div class="vertical-timeline-icon gray-bg"><i class="fa fa-times"></i></div>';
            timelineBlock += '<div class="vertical-timeline-content">';
            timelineBlock += '<span id="activityDate" class="vertical-date pull-right"><small></small></span>';
            timelineBlock += '<h2></h2>';
            timelineBlock += '<p>No activities available.</p></div>';
            timelineBlock += '</div>';
        }

        $("#vertical-timeline").html(timelineBlock);
    },

    manualKickOut : function (lobId, team) {
        $.ajax({
            type : 'GET',
            url : DASHBOARD.userListUri + '/' + lobId + '/' + team + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType : 'json'
        }).done(function (response) {
            var onlineUsers = [];
            $.each(response.data, function (key, value) {
                if (value.status == 1) {
                    onlineUsers.push(value.name);
                }
            });

            if (onlineUsers.length > 0) {
                swal({
                    title : "Kick out online users?",
                    text : "There are " + onlineUsers.length + " online user(s) in TotalView, kick them out anyway?",
                    type : "warning",
                    showCancelButton : true,
                    confirmButtonColor : "#DD6B55",
                    cancelButtonText : "Cancel",
                    confirmButtonText : "Kick",
                    closeOnConfirm : true,
                    closeOnCancel : true
                }, function (confirmed) {
                    if (confirmed) {
                        $.ajax({
                            type : 'GET',
                            url : DASHBOARD.kickoutUri + '/' + lobId + '/' + team + '?token=' + QUERY_STRING_HELPER.getByName('token'),
                            dataType : 'json'
                        }).done(function (response) {
                            TOASTR_HELPER.success('Users have been kicked out.');
                        });
                    }
                })
            }
        });
    },
}

$(document).ready(function () {
    DASHBOARD.build();

    setInterval(function () {
        DASHBOARD.build();
    }, 600000);
});