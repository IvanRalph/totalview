var CASE_HANDLING = {
    createCaseUri : CONSTANT.accountApiName + '/create-case',
    updateCaseUri : CONSTANT.accountApiName + '/update-case',
    trackerStatsUri : CONSTANT.accountApiName + '/tracker-stats',
    updateCaseIcnUri : CONSTANT.accountApiName + '/update-case-icn',
    casesUri : CONSTANT.accountApiName + '/cases',
    caseUri : CONSTANT.accountApiName + '/case',
    caseByIcnUri : CONSTANT.accountApiName + '/case-icn',
    currentBreakUri : CONSTANT.accountApiName + '/aux-break/current',
    currentCase : CONSTANT.accountApiName + '/case/current',
    auxBreakUri : CONSTANT.accountWebName + '/aux-break',
    childValuesUri : CONSTANT.accountApiName + '/child-values',
    deleteCaseUri : CONSTANT.accountApiName + '/delete-case',
    buildCreditStatsUri : CONSTANT.accountApiName + '/credit-stats',
    validationErrors : [],
    errorTries : 0,
    errorExists : false,

    build : function () {
        CASE_HANDLING._setAccountColor();
        CASE_HANDLING.initializeSelect2();
        CASE_HANDLING._addSpinners();
        CASE_HANDLING._conceptDescriptionCheck();

        if ($('#start-shift').is(':visible')) {
            $(document).on('click', '#start-shift', function () {
                CASE_HANDLING.checkCurrentAuxBreak();
                CASE_HANDLING.checkOngoingCase($('.lob-selector.active').attr('data-lob-id'));
            });
        } else {

            CASE_HANDLING.checkCurrentAuxBreak();
            CASE_HANDLING.checkOngoingCase($('.lob-selector.active').attr('data-lob-id'));

        }

        $(document).on('click', '.lob-selector.active', function (e) {
            e.preventDefault();
            $('.lob-selector.active').attr('style', 'pointer-events: none;');
        });

        $('.parent-dropdown').each(function () {
            var selectedValueListId = $(this).find(":selected").attr('data-list-id');
            var lobId = $(this).attr('data-lob-id');
            var childFieldId = $(this).attr('data-child-field-id');

            CASE_HANDLING.buildChildValues(lobId, childFieldId, selectedValueListId);
        });

        $(document).on('click', '.btn-start-case', function () {
            CASE_HANDLING.startCase($(this).attr('lobid'));
        });

        $(document).on('click', '.btn-stop-case', function () {
            CASE_HANDLING.updateCase($(this).attr('lobid'), $(this).attr('actionid'));
        });

        $(document).on('click', '.btn-hold-case', function () {
            CASE_HANDLING.updateCase($(this).attr('lobid'), $(this).attr('actionid'));
        });

        $(document).on('click', '.btn-save-case', function () {
            CASE_HANDLING.saveCase($(this).attr('lobid'), $(this).attr('actionid'));
        });

        $(document).on('click', '.btn-escalate-case', function () {
            CASE_HANDLING.updateCase($(this).attr('lobid'), $(this).attr('actionid'));
        });

        $(document).on('click', '.btn-edit', function () {
            CASE_HANDLING.editCase($(this).attr('data-lob-id'), $(this).attr('data-case-id'), $(this).attr('data-action-id'));
        });

        $(document).on('click', '.btn-continue', function () {
            CASE_HANDLING.continueCase($(this).attr('data-lob-id'), $(this).attr('data-case-id'), $(this).attr('data-action-id'));
        });

        $(document).on('click', '.lob-selector', function () {
            CASE_HANDLING.buildCaseTable($(this).attr('data-lob-id'));
            CASE_HANDLING.checkOngoingCase($(this).attr('data-lob-id'));
        });

        $(document).on('click', '#btn-aux-break', function () {
            CASE_TIMER.stopTimer();
            window.location.href = CASE_HANDLING.auxBreakUri + '?token=' + QUERY_STRING_HELPER.getByName('token');
        });

        $(document).on('click', '#close-validation-error-panel', function () {
            CASE_HANDLING._closeValidationErrorPanel($(this).attr('data-lob-id'));
        });

        $(document).on('change', '.parent-dropdown', function () {
            var selectedValueListId = $(this).find(":selected").attr('data-list-id');
            var lobId = $(this).attr('data-lob-id');
            var childFieldId = $(this).attr('data-child-field-id');

            CASE_HANDLING.buildChildValues(lobId, childFieldId, selectedValueListId);
        });

        $(document).on('focus', '.select2', function (e) {
            if (e.originalEvent) {
                $(this).siblings('select').select2('open');
            }
        });

        CASE_HANDLING.buildCaseTable($('#lob-tab li.active').attr('data-lob-id'));

        $(document).ajaxStop(function () {
            if (!CASE_HANDLING.errorExists) {
                CASE_HANDLING._removeSpinners();
            }
        });

        $(document).ajaxError(function (event, request, settings, thrownError) {
            CASE_HANDLING.errorExists = true;
            CASE_HANDLING._removeSpinners();
            CASE_HANDLING._addSpinners();
            // console.log(settings);
            // console.log(request);
            // console.log(event);
            if (request.status == 500 && settings.type == "GET") {
                TOASTR_HELPER.warning('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Connection Error. Retrying, please wait... ', {
                    "timeOut" : 0,
                    "extendedTimeOut" : 999999,
                    "preventDuplicates" : true
                });
                if (CASE_HANDLING.errorTries < 3) {
                    CASE_HANDLING.errorTries++;
                    setTimeout(function () {
                        $.ajax(settings.url).done(function () {
                            CASE_HANDLING.build();
                            CASE_HANDLING._removeSpinners();
                            CASE_HANDLING.errorTries = 0;
                            TOASTR_HELPER.remove();
                        });
                    }, 1000);
                } else {
                    CASE_HANDLING.errorTries = 0;
                    TOASTR_HELPER.remove();
                    TOASTR_HELPER.error('There was a problem loading a module. Please refresh the page. If the issue persists, give us a call. Thanks!', {
                        "timeOut" : 9000,
                        "preventDuplicates" : true
                    });
                }
            } else {
                TOASTR_HELPER.warning('Something went wrong. Please try again. If the issue persists, give us a call. Thanks!');
                CASE_HANDLING._removeSpinners();
                CASE_HANDLING._enableFormButtons();
            }
        });

        $(document).on('change', '#drg_type_7', function () {
            console.log($(this).val());
            if ($(this).val() == "MS DRG") {
                $('#drg_7').attr('maxlength', 4);
                $('#adj_drg_7').attr('maxlength', 4);
            } else if ($(this).val() == "APR") {
                $('#drg_7').removeAttr('pattern');
                $('#drg_7').attr('maxlength', 5);
                $('#adj_drg_7').removeAttr('pattern');
                $('#adj_drg_7').attr('maxlength', 5);
            }
        })

        $(document).on('blur', '#icn_7', function () {
            if ($(this).val() != '') {
                CASE_HANDLING.checkExistingCase($(this).data('lob-id'), $(this).val(), $('#status_7').val());
            }
        });

        $(document).on('change', '#concept_desc_7', function () {
            CASE_HANDLING._conceptDescriptionCheck();
        });

        $(document).on('change', '#service_line_7', function () {
            if ($(this).val() == 'CAT') {
                $('#concept_desc_7').val('Other - comments');
                $('#concept_desc_7').trigger('change');
            }
        });

        $(document).on('click', '.credit-stats', function(){
            CASE_HANDLING.buildCreditStats($(this).html());
        })
    },

    startCase : function (lobId) {
        var formData = $('#frm-' + lobId).serializeArray();
        formData.push({name : 'duration', value : '00:00:00'});
        formData.push({name : 'created_at', value : TIME_HELPER.currentDateTime()});
        var payload = PAYLOAD.build(formData);

        $.ajax({
            type : 'POST',
            url : CASE_HANDLING.createCaseUri + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            data : payload,
            headers : {'Content-Type' : 'application/vnd.api+json'},
            dataType : 'json'
        }).done(function (response) {
            if (typeof response.validation_errors !== 'undefined') {
                CASE_HANDLING._showValidationErrorPanel(lobId, response.validation_errors);
                return;
            }

            CASE_TIMER.startTimer();
            CASE_HANDLING._enableStopBtn();
            CASE_HANDLING._enableHoldBtn();
            CASE_HANDLING._enableEscalateBtn();
            CASE_HANDLING._enableContinueBtn();
            CASE_HANDLING._enableEditBtn();
            CASE_HANDLING._enableAuxBreakBtn();
            CASE_HANDLING._enableLobTab();
            CASE_HANDLING._closeValidationErrorPanel(lobId);

            setHiddenCaseIdValue(response.case_id);
        })
    },

    updateCase : function (lobId, actionId, _callback) {

        var formData = $('#frm-' + lobId).serializeArray();
        var duration = CASE_TIMER.getTimerDuration();
        var hiddenDuration = getHiddenDurationValue();

        // if (hiddenDuration != '00:00:00' && hiddenDuration != '' && actionId != 6) {
        //     duration = hiddenDuration;
        // }

        formData.push({name : 'duration', value : duration});
        formData.push({name : 'action_id', value : actionId});
        formData.push({name : 'updated_at', value : TIME_HELPER.currentDateTime()});

        if (CASE_HANDLING._formValidation(actionId)) {
            CASE_HANDLING._showValidationErrorPanel(lobId, CASE_HANDLING.validationErrors);
            CASE_HANDLING.validationErrors = [];
            return;
        }

        var payload = PAYLOAD.build(formData);

        $.ajax({
            type : 'PUT',
            url : CASE_HANDLING.updateCaseUri + '/' + lobId + '/' + getHiddenCaseIdValue() + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            data : payload,
            headers : {'Content-Type' : 'application/vnd.api+json'},
            dataType : 'json'
        }).done(function (response) {
            if (typeof response.validation_errors !== 'undefined') {
                $.each(response.validation_errors, function (i, value) {
                    CASE_HANDLING.validationErrors.push(response.validation_errors[i]);
                });
            }

            if (CASE_HANDLING.validationErrors.length > 0) {
                CASE_HANDLING._showValidationErrorPanel(lobId, response.validation_errors);
                CASE_HANDLING.validationErrors = [];
                return;
            } else {
                CASE_TIMER.stopTimer();

                setHiddenCaseIdValue(response.is_success);
                CASE_HANDLING.updateTrackerStats(lobId);
                CASE_HANDLING.buildCaseTable(lobId);
                CASE_HANDLING.updateTrackerStats(lobId);
                CASE_HANDLING._clearForm(lobId);

                TOASTR_HELPER.success('Case done!', {"preventDuplicate" : true});
                CASE_TIMER.restartTimer();
                if (_callback) {
                    _callback();
                }

                CASE_HANDLING.startCase(lobId);

                $("#status_7").select2("val", response.is_success['status_7']);

                CASE_HANDLING.initializeSelect2();

                location.reload();
            }
        })
    },

    continueCase : function (lobId, caseId, actionId) {
        CASE_HANDLING._showConfirmationModal(lobId, caseId, actionId);

        CASE_HANDLING.buildCaseTable(lobId);
    },

    editCase : function (lobId, caseId, actionId) {
        CASE_HANDLING.populateCaseForm(lobId, caseId, actionId, function () {
            var stopBtn = $('.btn-stop-case');

            stopBtn.html('SAVE');
            stopBtn.attr('actionid', 8);
            stopBtn.addClass('btn-save-case');
            stopBtn.removeClass('btn-stop-case');
            CASE_TIMER.startTimer();
            CASE_HANDLING._removeSpinners();
        });
    },

    saveCase : function (lobId, caseId) {
        CASE_HANDLING.updateCase(lobId, caseId, function () {
            var saveBtn = $('#frm-' + lobId + ' .btn-save-case');
            saveBtn.html('COMPLETE');
            saveBtn.attr('actionid', 4);
            saveBtn.addClass('btn-primary btn-stop-case');
            saveBtn.removeClass('btn-save-case');
        });
    },

    deleteCase : function (caseId, lobId) {
        $.ajax({
            type : 'DELETE',
            url : CASE_HANDLING.deleteCaseUri + '/' + caseId + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            headers : {'Content-Type' : 'application/vnd.api+json'},
            dataType : 'json'
        }).done(function (response) {

            return;

        })
    },

    updateTrackerStats : function (lobId) {
        $.ajax({
            type : 'GET',
            url : CASE_HANDLING.trackerStatsUri + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            headers : {'Content-Type' : 'application/vnd.api+json'},
            dataType : 'json'
        }).done(function (response) {
            $.each(response.service_line_stats, function (serviceLine, cnt) {
                console.log(serviceLine);
                $('#' + response.serviceLine + '-' + lobId).html(response.serviceLine + " - " + cnt);
            });

            $('#completed-case-count-' + lobId).html(response.total_completed_case);
            $('#breaks-' + lobId).addClass(response.breakClass).removeClass('navy-bg red-bg lazur-bg');
            $('#breaks-' + lobId).html(response.totalBreaks);
            $('#cph-' + lobId).html(response.cph + " / " + response.target);
            $('#prod-' + lobId).html(response.total_prod_duration);
            $('#non-prod-' + lobId).html(response.total_non_prod_duration);
        })
    },

    initializeSelect2 : function () {
        $('.select2').select2({
            width : '100%',
        });
    },

    buildCaseTable : function (lobId) {
        if ($.fn.dataTable.isDataTable('#processed-case-table')) {
            $('#processed-case-table').DataTable().clear();
            $('#processed-case-table').DataTable().destroy();
        }

        $.ajax({
            type : 'GET',
            url : CASE_HANDLING.casesUri + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            headers : {'Content-Type' : 'application/vnd.api+json'},
            dataType : 'json'
        }).done(function (response) {
            var header = '';
            $.each(response.columns, function (key, val) {
                header += '<th>' + val.name + '</th>';
            });

            $('#processed-case-table>thead>tr').html(header);

            $('#processed-case-table').DataTable({
                "data" : response.data,
                "columns" : response.columns,
                "responsive" : true
            });
        })
    },

    populateCaseForm : function (lobId, caseId, actionId, _callback) {
        var getCaseUri = CASE_HANDLING.caseUri + '/' + lobId + '/' + caseId + '?action=' + actionId + '&token=' + QUERY_STRING_HELPER.getByName('token');

        $.ajax({
            type : 'GET',
            url : getCaseUri,
            headers : {'Content-Type' : 'application/vnd.api+json'},
            dataType : 'json'
        }).done(function (response) {
            var fields = response.fields;
            var caseInfo = response.case_info;
            var currentTimer = CASE_TIMER.getTimerDuration();
            var accumulatedTime = TIME_HELPER._addTimes(currentTimer, caseInfo.duration);

            setHiddenCaseIdValue(caseInfo.id);
            setHiddenDurationValue(accumulatedTime);

            if (response.role != 13) {
                $("option[value='Peer Review']").attr("disabled", "disabled");
                $("option[value='Peer Review']").html('Peer Review (Restricted to Reviewer Role)');
            }

            // set time circle
            var dataDate = CASE_TIMER._calculateDataDateByDuration(accumulatedTime);

            CASE_TIMER._setTimerDataDate(dataDate);

            $(fields).each(function (key, value) {
                var fieldValue = caseInfo[value.html_name];

                if (value.type == "dropdown") {
                    $('[name="' + value.html_name + '"]').select2('val', fieldValue);
                } else {
                    $('[name="' + value.html_name + '"]').val(fieldValue);
                }
            });

            if (_callback) {
                _callback();
            }
        })
    },

    checkCurrentAuxBreak : function () {
        $.ajax({
            type : 'GET',
            url : CASE_HANDLING.currentBreakUri + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType : 'json'
        }).done(function (response) {
            if (response.current_aux_break) {
                window.location.href = CASE_HANDLING.auxBreakUri + '?token=' + QUERY_STRING_HELPER.getByName('token');
            }
        })
    },

    checkOngoingCase : function (lobId) {
        $.ajax({
            type : 'GET',
            url : CASE_HANDLING.currentCase + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType : 'json'
        }).done(function (response) {
            if (response.current_case_info) {
                var case_info = response.current_case_info;
                CASE_HANDLING.populateCaseForm(lobId, case_info.id, 0, function () {
                    var created_at = case_info.created_at;
                    if (case_info.break_at != null) {
                        created_at = case_info.break_at;
                    }
                    console.log(case_info);
                    var dataDate = CASE_HANDLING._computeDatadate(case_info.duration, case_info.updated_at, created_at, case_info.status_id);
                    // (case_info.status_id == 7) ? CASE_HANDLING._editTimer() : CASE_HANDLING._continueTimer(dataDate);
                    CASE_HANDLING._continueTimer(dataDate);
                });
            } else {
                CASE_HANDLING.startCase(lobId);
            }
        })
    },

    checkExistingCase : function (lobId, icn, caseType) {
        CASE_HANDLING._addSpinners();
        $.ajax({
            type : 'GET',
            url : CASE_HANDLING.caseByIcnUri + '/' + lobId + '/' + icn + '?caseType=' + caseType + '&token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType : 'json'
        }).done(function (response) {
            if (response.toastr) {
                TOASTR_HELPER.warning(response.toastr, {"timeOut" : 10000, "extendedTimeOut" : 10000});
                $('#icn_7').val('');
                CASE_HANDLING._removeSpinners();
                return;
            }

            var case_info = response.case_info;
            var hiddenCase = getHiddenCaseIdValue();

            if (response.new_record) {
                CASE_HANDLING.populateCaseForm(lobId, case_info.id, case_info.status_id, function () {
                    setHiddenCaseIdValue(hiddenCase);
                    CASE_HANDLING._updateCaseIcn(lobId, getHiddenCaseIdValue());
                    CASE_TIMER.startTimer();
                });
                return;
            }

            if (case_info) {
                swal({
                    title : "Existing Case Detected",
                    text : "Retrieve Case details? This will disregard the current case.",
                    type : "warning",
                    showCancelButton : true,
                    confirmButtonColor : "#DD6B55",
                    cancelButtonText : "No, don't do anything.",
                    confirmButtonText : "Yes, Retrieve data.",
                    closeOnConfirm : true,
                    closeOnCancel : true
                }, function (confirmed) {
                    if (confirmed) {
                        CASE_HANDLING.deleteCase(getHiddenCaseIdValue(), lobId);
                        CASE_HANDLING.editCase(lobId, case_info.id, 7);
                    } else {
                        TOASTR_HELPER.warning('ICN# ' + case_info.icn_7 + ' already exist');
                        $('#icn_7').val('');
                    }
                })
            } else {
                CASE_HANDLING._removeSpinners();
            }

        })

    },

    buildCreditStats : function(serviceLine){
        CASE_HANDLING._addSpinners();
        $.ajax({
            type : 'GET',
            url : CASE_HANDLING.buildCreditStatsUri + '/' + serviceLine + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            headers : {'Content-Type' : 'application/vnd.api+json'},
            dataType : 'json'
        }).done(function (response) {
            $('.side-title').siblings().remove();
            var recovery = response.recovery;
            var nonRecovery = response.non_recovery;
            var total = response.total;

            console.log(response);

            $.each(recovery, function (key, value) {
                $('.stats-recovery').append('<td>'+value+'</td>');
            });

            $.each(nonRecovery, function (key, value) {
                $('.stats-non-recovery').append('<td>'+value+'</td>');
            });

            $.each(total, function (key, value) {
                $('.stats-total').append('<td>'+value+'</td>');
            });

            CASE_HANDLING._removeSpinners();
        });
    },

    _updateCaseIcn : function (lobId, caseId) {
        var formData = $('#frm-' + lobId).serializeArray();

        formData.push({name : 'duration', value : "00:00:00"});
        formData.push({name : 'action_id', value : 1});
        formData.push({name : 'updated_at', value : TIME_HELPER.currentDateTime()});

        var payload = PAYLOAD.build(formData);

        $.ajax({
            type : 'PUT',
            url : CASE_HANDLING.updateCaseIcnUri + '/' + lobId + '/' + caseId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            data : payload,
            headers : {'Content-Type' : 'application/vnd.api+json'},
            dataType : 'json'
        }).done(function (response) {

        });
    },

    _showConfirmationModal : function (lobId, caseId, actionId) {
        swal({
            title : "Ignore Current Case?",
            text : "Current time will add to the hold case, Continue?",
            type : "warning",
            showCancelButton : true,
            confirmButtonColor : "#DD6B55",
            cancelButtonText : "Cancel",
            confirmButtonText : "Continue",
            closeOnConfirm : true,
            closeOnCancel : true
        }, function (confirmed) {
            if (confirmed) {
                var latest = getHiddenCaseIdValue();
                CASE_HANDLING.populateCaseForm(lobId, caseId, actionId, function () {
                    CASE_TIMER.startTimer();
                    setHiddenCaseIdValue(latest);
                    setHiddenDurationValue("00:00:00");
                    CASE_HANDLING._updateCaseIcn(lobId, getHiddenCaseIdValue(), 1);
                });
            }

        })

    },

    _clearForm : function (lobId) {
        $('#frm-' + lobId)[0].reset();
        $('.select2').select2();
    },

    _enableStopBtn : function () {
        $('.btn-stop-case').prop('disabled', '');
    },

    _enableContinueBtn : function () {
        $('.btn-continue').prop('disabled', '');
    },

    _enableEditBtn : function () {
        $('.btn-edit').prop('disabled', '');
    },

    _disableStopBtn : function () {
        $('.btn-stop-case').prop('disabled', 'disabled');
    },

    _enableStartBtn : function () {
        $('.btn-start-case').prop('disabled', '');
    },

    _disableStartBtn : function () {
        $('.btn-start-case').prop('disabled', 'disabled');
    },

    _enableHoldBtn : function () {
        $('.btn-hold-case').prop('disabled', '');
    },

    _disableHoldBtn : function () {
        $('.btn-hold-case').prop('disabled', 'disabled');
    },

    _disableContinueBtn : function () {
        $('.btn-continue').prop('disabled', 'disabled');
    },

    _disableEditBtn : function () {
        $('.btn-edit').prop('disabled', 'disabled');
    },

    _enableAuxBreakBtn : function () {
        $('#btn-aux-break').prop('disabled', '');
    },

    _disableAuxBreakBtn : function () {
        $('#btn-aux-break').prop('disabled', 'disabled');
    },

    _enableEscalateBtn : function () {
        $('.btn-escalate-case').prop('disabled', '');
    },

    _disableEscalateBtn : function () {
        $('.btn-escalate-case').prop('disabled', 'disabled');
    },

    _enableLobTab : function () {
        $('.lob-selector').removeClass('disabled');
    },

    _disableLobTab : function () {
        $('.lob-selector').addClass('disabled');
    },

    _showValidationErrorPanel : function (lobId, validationErrors) {
        var errorList = "";
        var validationPanel = $('#validation-error-panel-' + lobId);

        $('#validation-error-list-' + lobId).html("");

        // for re-animation
        if (validationPanel.is(':visible') === true) {
            validationPanel.hide("fast");
        }

        validationPanel.show("fast", function () {
            $.each(validationErrors, function (key, value) {
                errorList += '<li class="text-danger">' + value + '</li>';
            });

            $('#validation-error-list-' + lobId).html(errorList);
        });
    },

    _closeValidationErrorPanel : function (lobId) {
        $('#validation-error-panel-' + lobId).hide('fast');
    },

    _continueTimer : function ($datadate) {
        CASE_TIMER._setTimerDataDate($datadate);
        CASE_TIMER.startTimer();

        CASE_HANDLING._enableStopBtn();
        CASE_HANDLING._enableHoldBtn();
        CASE_HANDLING._enableEscalateBtn();

    },

    _editTimer : function () {

        CASE_HANDLING._enableStopBtn();
        CASE_HANDLING._enableHoldBtn();
        CASE_HANDLING._enableEscalateBtn();
    },

    _computeDatadate : function (duration, updatedAt, createdAt, statusId) {
        var elapsedTime = duration;
        var elapsedMil = moment.duration(elapsedTime).asMilliseconds()
        var currentTime = moment(updatedAt).subtract(elapsedMil).format('YYYY-MM-DD HH:mm:ss');
        if (statusId == 1) {
            currentTime = createdAt;
        }
        return currentTime;
    },

    _setAccountColor : function () {
        $('#account-header').addClass(CONSTANT.accountColor['Royal Blue']);
    },

    _formValidation : function (action) {
        // var drgType = $('#drg_type_7').val();
        // var drg = $('#drg_7').val();
        // var adjDrg = $('#adj_drg_7').val();
        // var decision = $('#decision_7').val();
        // var serviceLine = $('#service_line_7').val();
        // var comment = $('#comments_7').val();
        // var status = $('#status_7').val();
        // var icn = $('#icn_7').val();
        // var conceptDesc = $('#concept_desc_7').val();
        // var holdReason = $('#hold_reason_7').val();
        //
        // $('.form-group').removeClass('has-error');
        //
        // if(drgType == 0){
        //     CASE_HANDLING.validationErrors.push("<b>DRG Type<\/b> cannot be empty.")
        //     $('#drg_type_7').parents('.form-group').addClass('has-error')
        // }
        //
        // if(drg == ""){
        //     CASE_HANDLING.validationErrors.push("<b>DRG<\/b> cannot be empty.")
        //     $('#drg_7').parents('.form-group').addClass('has-error')
        // }
        //
        // if((adjDrg == "" && action != 6) || decision != "Non-Recovery"){
        //     CASE_HANDLING.validationErrors.push("<b>Adjusted DRG<\/b> cannot be empty.")
        //     $('#adj_drg_7').parents('.form-group').addClass('has-error')
        // }
        //
        // if(decision == 0 && action != 6){
        //     CASE_HANDLING.validationErrors.push("<b>Decision<\/b> cannot be empty.")
        //     $('#decision_7').parents('.form-group').addClass('has-error')
        // }
        //
        // if(serviceLine == 0){
        //     CASE_HANDLING.validationErrors.push("<b>Service Line<\/b> cannot be empty.")
        //     $('#service_line_7').parents('.form-group').addClass('has-error')
        // }
        //
        // if(holdReason == 'Other - comments' && comment == ""){
        //     CASE_HANDLING.validationErrors.push("<b>Comment(s)<\/b> cannot be empty if hold reason is Others.")
        //     $('#comments_7').parents('.form-group').addClass('has-error')
        // }
        //
        // if(status == 0){
        //     CASE_HANDLING.validationErrors.push("<b>Status<\/b> cannot be empty.")
        //     $('#status_7').parents('.form-group').addClass('has-error')
        // }
        //
        // if(icn == ""){
        //     CASE_HANDLING.validationErrors.push("<b>ICN<\/b> cannot be empty.")
        //     $('#icn_7').parents('.form-group').addClass('has-error')
        // }
        //
        // if(conceptDesc == 0){
        //     CASE_HANDLING.validationErrors.push("<b>Concept Description<\/b> cannot be empty.")
        //     $('#concept_desc_7').parents('.form-group').addClass('has-error')
        // }
        //
        // if(status == "On Hold" && holdReason == 0){
        //     CASE_HANDLING.validationErrors.push("<b>Hold Reason<\/b> cannot be empty if Status is On Hold.")
        //     $('#hold_reason_7').parents('.form-group').addClass('has-error')
        // }

        // if (CASE_HANDLING.validationErrors.length > 0) {
        //     return true;
        // }else{
        //     return false;
        // }

        return false;
    },

    _removeSpinners : function () {
        $('#case-form').children('.ibox-content').removeClass('sk-loading');
        $('#case-table').children('.ibox-content').removeClass('sk-loading');
        $('.tracker-stats').children('.ibox-content').removeClass('sk-loading');
    },

    _addSpinners : function () {
        $('#case-form').children('.ibox-content').addClass('sk-loading');
        $('#case-table').children('.ibox-content').addClass('sk-loading');
        $('.tracker-stats').children('.ibox-content').addClass('sk-loading');
    },

    _conceptDescriptionCheck : function () {
        if ($('#concept_desc_7').val() != 'Other - comments') {
            $('#cd_comments').attr('disabled', 'disabled');
        } else {
            $('#cd_comments').removeAttr('disabled');
        }
    },

};

$(document).ready(function () {
    CASE_HANDLING.build();
});