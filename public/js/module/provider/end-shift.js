var END_SHIFT = {

	endShiftURI : CONSTANT.accountWebName + '/end-shift/',
	AccountListURI : '/accounts/',
	CaseHandlingURI :CONSTANT.accountWebName + '/',
	
	build : function (){
		
		this._showConfirmModal();		
	},	

	_showConfirmModal : function () {

		 swal({
                        title: "End your Shift?",
                        text: "You will not be able to track cases anymore for today, Continue?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        cancelButtonText: "Cancel",
                        confirmButtonText: "Continue",
                        closeOnConfirm: true,
                        closeOnCancel: true
        }, function( confirmed ) {
            if( confirmed ) {
                
		        $.ajax({
		            type: 'GET',
		            url: END_SHIFT.endShiftURI + '?token=' + QUERY_STRING_HELPER.getByName('token'),
		            headers: {'Content-Type': 'application/vnd.api+json'},
		            dataType: 'json'
		        }).done(function (result){
		     		window.location = END_SHIFT.AccountListURI + '?token=' + QUERY_STRING_HELPER.getByName('token') ;

		        }).fail(function () {
		        	window.location = END_SHIFT.AccountListURI + '?token=' + QUERY_STRING_HELPER.getByName('token') ;
		        });
                        
            } else {

            	window.location = END_SHIFT.CaseHandlingURI + '?token=' + QUERY_STRING_HELPER.getByName('token') ;
            }
               
        }) ;  
         
	},

};


$(document).ready(function () {
	END_SHIFT.build();
});