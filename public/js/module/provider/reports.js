var REPORTS = {
    doc: $(document),
    rawReportsUri: CONSTANT.accountApiName + '/reports/raw-reports-list/',
    breaksReportsUri: CONSTANT.accountApiName + '/breakreports/',
    teamList: {},
    bulkUserIds: [],

    build: function () {
        this.buildDatePicker();
        this.bind();
        this.refreshReportsTable();
    },

    bind: function () {
        $("#frmRawReports").submit(function (e) {
            var _self = $(this);
            e.stopPropagation();
            e.preventDefault();
            REPORTS.buildReportsTable(_self.serialize());
        });

        $("#frmBreaksReports").submit(function (e) {
            var _self = $(this);
            e.stopPropagation();
            e.preventDefault();
            REPORTS.buildReportsTableBreaks(_self.serialize());
        })
    },


    buildDatePicker: function () {
        $('.input-daterange').datepicker({format: 'yyyy-mm-dd'})
    },


    buildReportsTable: function (formData) {
        var lobId = $("#slct-lob-id").val();
        REPORTS.buildReportsTableRaw(formData);
    },


    refreshReportsTable: function () {
        $('.table').DataTable({
            "width": "25%",
            'language': {
                'emptyTable': 'Please set your filter',
            },
            initComplete: function () {
            },
        })
    },

        buildReportsTableRaw: function (formData) { 
            $('#reports-table').DataTable().destroy();
            $('#reports-table').DataTable({
               scrollX: false,
                ajax: {
                  url: this.rawReportsUri + "?" + formData + '&token=' + QUERY_STRING_HELPER.getByName('token'),
                  type: 'GET',
                  success: function (data) {
                    $('#reports-table').DataTable().destroy();
                     if (data == "") {
                        $('#reports-table').DataTable( {
                            "language": {
                              "emptyTable": "No Records Found"
                            }
                        } );
                    }
                    
                    var head = ''
                    var cnt = 0
                    var arrRes = new Array()
                    $.each(data, function (key, value) {

                      head = '<thead><tr role="row">'
                      $.each(data[key], function (col, data) {
                        head += '<th>' + col + '</th>'
                        arrRes[cnt++] = {'data': col}
                    });
                      head += '</tr></thead>';
                      $('#reports-table').html(head); 
                      $('#reports-table').DataTable({
                        scrollX: true,
                        processing: true,
                        data: data,
                        columns: arrRes,
                        dom: '<"html5buttons"B>lTfgitp',
                        buttons: [
                        {extend: 'excel'},
                        {extend: 'print'},
                        ],
                        columnDefs: [
                        {
                          'targets': [0,2],
                          'searchable': true,
                          "width": "15%",
                      },
                      
                      ],
                       order: [[0, 'asc']],
                  })
                      return false;
                  })
                },

            },
        });
    },

     buildReportsTableBreaks: function (formData) { 
            $('#reports-table-breaks').DataTable().destroy();
            $('#reports-table-breaks').DataTable({
               scrollX: false,
                ajax: {
                  url: this.breaksReportsUri + "?" + formData + '&token=' + QUERY_STRING_HELPER.getByName('token'),
                  type: 'GET',
                  success: function (data) {
                    $('#reports-table-breaks').DataTable().destroy();
                     if (data == "") {
                        $('#reports-table-breaks').DataTable( {
                            "language": {
                              "emptyTable": "No Records Found"
                            }
                        } );
                    }
                    
                    var head = ''
                    var cnt = 0
                    var arrRes = new Array()
                    $.each(data, function (key, value) {

                      head = '<thead><tr role="row">'
                      $.each(data[key], function (col, data) {
                        head += '<th>' + col + '</th>'
                        arrRes[cnt++] = {'data': col}
                    });
                      head += '</tr></thead>';
                      $('#reports-table-breaks').html(head); 
                      $('#reports-table-breaks').DataTable({
                        scrollX: true,
                        processing: true,
                        data: data,
                        columns: arrRes,
                        dom: '<"html5buttons"B>lTfgitp',
                        buttons: [
                        {extend: 'excel'},
                        {extend: 'print'},
                        ],
                        columnDefs: [
                        {
                          'targets': [0,1,2],
                          'searchable': true,
                          "width": "26%",
                      },
                      
                      ],
                       order: [[0, 'asc']],
                  })
                      return false;
                  })
                },

            },
        });
    },

    

    _convertTimezone: function (formData) {

        var timezone = $('#slct-timezone-id').val();
        var start = $('#date-start').val() + CONSTANT.accountStartTime;
        var end = $('#date-end').val() + CONSTANT.accountEndTime;

        $('#date-start').val(TIMEZONE.convert(timezone, start));
        $('#date-end').val(TIMEZONE.convert(timezone, end));
        return $('#frmHourlyReports').serialize();
    },

}

$(document).ready(function () {
    REPORTS.build();
});
