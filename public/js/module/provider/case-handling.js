var CASE_HANDLING = {
    createCaseUri: CONSTANT.accountApiName + '/create-case',
    updateCaseUri: CONSTANT.accountApiName + '/update-case',
    trackerStatsUri: CONSTANT.accountApiName + '/tracker-stats',
    casesUri: CONSTANT.accountApiName + '/cases',
    caseUri: CONSTANT.accountApiName + '/case',
    currentBreakUri: CONSTANT.accountApiName + '/aux-break/current',
    currentCase: CONSTANT.accountApiName + '/case/current',
    auxBreakUri: CONSTANT.accountWebName + '/aux-break',
    childValuesUri: CONSTANT.accountApiName + '/child-values',
    deleteCaseUri : CONSTANT.accountApiName + '/delete-case',
    validationErrors : [],



    build: function () {
        CASE_HANDLING._setAccountColor();
        CASE_HANDLING.initializeSelect2();
        CASE_HANDLING._hideDataTypeAndStatus();

        if($('#start-shift').is(':visible')) {
            $(document).on('click', '#start-shift', function () {
                CASE_HANDLING.checkCurrentAuxBreak();
                CASE_HANDLING.checkOngoingCase($('.lob-selector.active').attr('data-lob-id'));
            });
        } else {

            CASE_HANDLING.checkCurrentAuxBreak();
            CASE_HANDLING.checkOngoingCase($('.lob-selector.active').attr('data-lob-id'));

        }
  

        $(document).on('click', '.lob-selector.active', function (e) {
             e.preventDefault();
             $('.lob-selector.active').attr('style', 'pointer-events: none;');
        });


        $('.parent-dropdown').each(function () {
            var selectedValueListId = $(this).find(":selected").attr('data-list-id');
            var lobId = $(this).attr('data-lob-id');
            var childFieldId = $(this).attr('data-child-field-id');

            CASE_HANDLING.buildChildValues(lobId, childFieldId, selectedValueListId);
        });

        $(document).on('click', '.btn-start-case', function () {
            CASE_HANDLING.startCase($(this).attr('lobid'));
        });

        $(document).on('click', '.btn-stop-case', function () {
            CASE_HANDLING.updateCase($(this).attr('lobid'), $(this).attr('actionid'));
        });

        $(document).on('click', '.btn-hold-case', function () {
           CASE_HANDLING.updateCase($(this).attr('lobid'), $(this).attr('actionid'));
            
        });

        $(document).on('click', '.btn-save-case', function () {
            CASE_HANDLING.saveCase($(this).attr('lobid'), $(this).attr('actionid'));
        });

        $(document).on('click', '.btn-escalate-case', function () {
            CASE_HANDLING.updateCase($(this).attr('lobid'), $(this).attr('actionid'));
        });

        $(document).on('click', '.btn-edit', function () {
            CASE_HANDLING.editCase($(this).attr('data-lob-id'), $(this).attr('data-case-id'), $(this).attr('data-action-id'));
        });

        $(document).on('click', '.btn-continue', function () {
            CASE_HANDLING.continueCase($(this).attr('data-lob-id'), $(this).attr('data-case-id'), $(this).attr('data-action-id'));
        });

        $(document).on('click', '.lob-selector', function () {
            CASE_HANDLING.buildCaseTable($(this).attr('data-lob-id'));
            CASE_HANDLING.checkOngoingCase($(this).attr('data-lob-id'));
        });

        $(document).on('click', '#btn-aux-break', function () {
            CASE_TIMER.stopTimer();
            window.location.href = CASE_HANDLING.auxBreakUri + '?token=' + QUERY_STRING_HELPER.getByName('token');
        });

        $(document).on('click', '#close-validation-error-panel', function () {
            CASE_HANDLING._closeValidationErrorPanel($(this).attr('data-lob-id'));
        });

        $(document).on('change', '.parent-dropdown', function () {
            var selectedValueListId = $(this).find(":selected").attr('data-list-id');
            var lobId = $(this).attr('data-lob-id');
            var childFieldId = $(this).attr('data-child-field-id');

            CASE_HANDLING.buildChildValues(lobId, childFieldId, selectedValueListId);

        });

        CASE_HANDLING.buildCaseTable($('#lob-tab li.active').attr('data-lob-id'));

        $(document).on('change', '#type', function (){
            var val = $(this).find(':selected').data('list-id');

            if(val == 9){
                CASE_HANDLING._hideVerificationAndUtd();
                CASE_HANDLING._showDataTypeAndStatus();
                $("#datatypestatus").val(null).trigger("change");
            }else{
                CASE_HANDLING._showVerificationAndUtd();
                CASE_HANDLING._hideDataTypeAndStatus();
            }
        });
    },

    startCase: function (lobId) {
        var formData = $('#frm-' + lobId).serializeArray();
        formData.push({name: 'duration', value: '00:00:00'});
        formData.push({name: 'created_at', value: TIME_HELPER.currentDateTime()});

        var payload = PAYLOAD.build(formData);

        $.ajax({
            type: 'POST',
            url: CASE_HANDLING.createCaseUri + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            data: payload,
            headers: {'Content-Type': 'application/vnd.api+json'},
            dataType: 'json'
        }).done(function (response) {
            if (typeof response.validation_errors !== 'undefined' ) {
                CASE_HANDLING._showValidationErrorPanel(lobId, response.validation_errors);
                return;
            }

            CASE_TIMER.startTimer();
            CASE_HANDLING._enableStopBtn();
            CASE_HANDLING._enableHoldBtn();
            CASE_HANDLING._enableEscalateBtn();
            CASE_HANDLING._enableContinueBtn();
            CASE_HANDLING._enableEditBtn();
            CASE_HANDLING._enableAuxBreakBtn();
            CASE_HANDLING._enableLobTab();
            CASE_HANDLING._closeValidationErrorPanel(lobId);

            setHiddenCaseIdValue(response.case_id);
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.');
        });
    },

    updateCase: function (lobId, actionId, _callback) {
        
        var formData = $('#frm-' + lobId).serializeArray();
        var duration = CASE_TIMER.getTimerDuration();
        var hiddenDuration = getHiddenDurationValue();

        
        if (hiddenDuration != '00:00:00' && hiddenDuration != '' && actionId != 6)
        {
            duration = hiddenDuration;
        }

        formData.push({name: 'duration', value: duration});
        formData.push({name: 'action_id', value: actionId});
        formData.push({name: 'updated_at', value: TIME_HELPER.currentDateTime()});
        formData.push({name: 'datatypestatus', value: $('#datatypestatus').val().toString()});
        formData.push({name: 'authority', value: $('#authority').val().toString()});

          
        CASE_HANDLING.validationErrors = [] ;
        CASE_HANDLING._checkUTDField() 
        if(actionId == 6) { // Hold
                CASE_HANDLING._checkNotesField();
        }  
        else if (actionId == 4) { // Complete
                CASE_HANDLING._checkVerificationResultsField();
        } 
         
        if (CASE_HANDLING.validationErrors.length > 0) {
             formData.push({name: 'validation_error', value: CASE_HANDLING.validationErrors})
        }    
        
        var payload = PAYLOAD.build(formData);
        
            $.ajax({
                type: 'PUT',
                url: CASE_HANDLING.updateCaseUri + '/' + lobId + '/' + getHiddenCaseIdValue() + '?token=' + QUERY_STRING_HELPER.getByName('token'),
                data: payload,
                headers: {'Content-Type': 'application/vnd.api+json'},
                dataType: 'json'
            }).done(function (response) {

                if(typeof response.validation_errors !== 'undefined') {
                    $.each(response.validation_errors, function(i, value ) {
                        CASE_HANDLING.validationErrors.push(response.validation_errors[i]);
                    });
                }    
                      
                 
                if (CASE_HANDLING.validationErrors.length > 0) {         
                    CASE_HANDLING._showValidationErrorPanel(lobId, response.validation_errors);
                    return;
                } else {
                    CASE_TIMER.stopTimer();

                    setHiddenCaseIdValue(response.is_success);
                    CASE_HANDLING.updateTrackerStats(lobId);
                    CASE_HANDLING.buildCaseTable(lobId);
                    CASE_HANDLING._clearForm(lobId);

                    TOASTR_HELPER.success('Case done!');
                    CASE_TIMER.restartTimer();
                        if (_callback) {
                        _callback();
                    }

                    CASE_HANDLING.startCase(lobId);

                    $("#datatypestatus").val(null).trigger("change");
                    CASE_HANDLING.initializeSelect2();

                }    
              
            }).fail(function () {
                TOASTR_HELPER.error('Something went wrong.')
            });
       
    },

    continueCase: function (lobId, caseId, actionId) {
        CASE_HANDLING._showConfirmationModal(lobId, caseId, actionId);
        

    },

    editCase: function (lobId, caseId, actionId) {
        CASE_HANDLING.populateCaseForm(lobId, caseId, actionId, function () {
            var stopBtn = $('.btn-stop-case');

            stopBtn.html('SAVE');
            stopBtn.attr('actionid', 8);
            stopBtn.addClass('btn-save-case');
            stopBtn.removeClass('btn-stop-case');
        });
    },


    saveCase: function (lobId, caseId) {
        CASE_HANDLING.updateCase(lobId, caseId, function () {
            var saveBtn = $('#frm-' + lobId + ' .btn-save-case');
            saveBtn.html('STOP');
            saveBtn.attr('actionid', 4);
            saveBtn.addClass('btn-primary btn-stop-case');
            saveBtn.removeClass('btn-save-case');

        });
    },

    deleteCase : function (caseId, lobId) {
        $.ajax({
            type: 'DELETE',
            url: CASE_HANDLING.deleteCaseUri + '/' + caseId + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            headers: {'Content-Type': 'application/vnd.api+json'},
            dataType: 'json'
        }).done(function(response) {

            return ;

        }).fail(function() {
            TOASTR_HELPER.error('Something went wrong');
        });
    },

    updateTrackerStats: function (lobId) {
        $.ajax({
            type: 'GET',
            url: CASE_HANDLING.trackerStatsUri + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            headers: {'Content-Type': 'application/vnd.api+json'},
            dataType: 'json'
        }).done(function (response) {
            $('#completed-case-count-' + lobId).html(response.total_completed_case);
            $('#hold-case-count-' + lobId).html(response.total_hold_case);
            $('#escalated-case-count-' + lobId).html(response.total_escalated_case);
            $('#aht-' + lobId).html(response.total_duration);
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong');
        })
    },

    initializeSelect2 : function () {
        $('#datatypestatus').attr('multiple', 'multiple');
        $('#authority').attr('multiple', 'multiple');

        $("#authority").val(null).trigger("change");

        $('.select2').select2({
            width: '100%',
        });
    },

    buildCaseTable: function (lobId) {
        if ($.fn.dataTable.isDataTable('#processed-case-table')) {
            $('#processed-case-table').DataTable().clear();
            $('#processed-case-table').DataTable().destroy();
        }
        $('#case-table').children('.ibox-content').toggleClass('sk-loading');
        $.ajax({
            type: 'GET',
            url: CASE_HANDLING.casesUri + '/' + lobId + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            headers: {'Content-Type': 'application/vnd.api+json'},
            dataType: 'json'
        }).done(function (response) {
            var header = '';
            $.each(response.columns, function (key, val) {
                header += '<th>' + val.name + '</th>';
            });

            $('#processed-case-table>thead>tr').html(header);

            $('#processed-case-table').DataTable({
                "data": response.data,
                "columns": response.columns,
                "responsive": true
            });

            $('#case-table').children('.ibox-content').toggleClass('sk-loading');
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong');
        })
    },

    populateCaseForm: function (lobId, caseId, actionId, _callback) {
        var getCaseUri = CASE_HANDLING.caseUri + '/' + lobId + '/' + caseId + '?action=' + actionId + '&token=' + QUERY_STRING_HELPER.getByName('token');
        $('#case-form').children('.ibox-content').addClass('hidden');
        $('#case-form').children('.ibox-content').toggleClass('sk-loading');
        $.ajax({
            type: 'GET',
            url: getCaseUri,
            headers: {'Content-Type': 'application/vnd.api+json'},
            dataType: 'json'
        }).done(function (response) {
            var fields = response.fields;
            var caseInfo = response.case_info;
            var currentTimer = CASE_TIMER.getTimerDuration();
            var accumulatedTime = TIME_HELPER._addTimes(currentTimer,caseInfo.duration);

            setHiddenCaseIdValue(caseInfo.id);
            setHiddenDurationValue(accumulatedTime);

            // set time circle
            var dataDate =  CASE_TIMER._calculateDataDateByDuration(accumulatedTime);
            CASE_TIMER._setTimerDataDate(dataDate);

            $(fields).each(function (key, value) {
                var fieldValue = caseInfo[value.html_name];

                if(value.type == "dropdown") {
                    $('[name="' + value.html_name + '"]').select2('val', fieldValue);
                    if((value.html_name == 'datatypestatus' || value.html_name == 'authority') && caseInfo.datatypestatus != null && caseInfo.authority != null){
                        $("#result").val('1');
                        $('[name="' + value.html_name + '"]').select2('val', fieldValue.split(','));  
                    } else if(value.html_name == 'authority'  && caseInfo.authority != null) {
                        $('[name="' + value.html_name + '"]').select2('val', fieldValue.split(','));
                    }
                } else {
                    $('[name="' + value.html_name + '"]').val(fieldValue);
                }
            });

            if (_callback) {
                _callback();
            }
            $('#case-form').children('.ibox-content').removeClass('hidden');
            $('#case-form').children('.ibox-content').toggleClass('sk-loading');
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.');
        });
    },


    checkCurrentAuxBreak: function () {
        $.ajax({
            type: 'GET',
            url: CASE_HANDLING.currentBreakUri + '?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType: 'json'
        }).done(function (response) {
            if (response.current_aux_break) {
                window.location.href = CASE_HANDLING.auxBreakUri + '?token=' + QUERY_STRING_HELPER.getByName('token');
            }
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.');
        })
    },

    checkOngoingCase : function (lobId) {
        $.ajax ({
            type: 'GET',
            url: CASE_HANDLING.currentCase + '/' + lobId +'?token=' + QUERY_STRING_HELPER.getByName('token'),
            dataType: 'json'
        }).done(function (response) {
         
            if (response.current_case_info) {
                var case_info = response.current_case_info;
                CASE_HANDLING.populateCaseForm(lobId, case_info.id, 0, function () {
                    var created_at = case_info.created_at
                    if(case_info.break_at != null) {
                        created_at = case_info.break_at ;
                    }
                    var dataDate = CASE_HANDLING._computeDatadate(case_info.duration, case_info.updated_at , created_at ,case_info.status_id);
                    (case_info.status_id == 7) ? CASE_HANDLING._editTimer() : CASE_HANDLING._continueTimer(dataDate);
                });
            } else {
                CASE_HANDLING.startCase(lobId);
            }
        }).fail(function () {
            TOASTR_HELPER.error('Something went wrong.');
        })
    },

    _showConfirmationModal : function (lobId, caseId, actionId) {
        swal({
            title: "Ignore Current Case?",
            text: "Current time will add to the hold case, Continue?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "Cancel",
            confirmButtonText: "Continue",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function( confirmed ) {
            if( confirmed ) {
             CASE_HANDLING.deleteCase(getHiddenCaseIdValue(), lobId);  
             CASE_HANDLING.populateCaseForm(lobId, caseId, actionId, function () {
                CASE_TIMER.startTimer();
             });
            
                        
            }
               
        })   

    },
  
    _clearForm: function (lobId) {
        $('#frm-' + lobId)[0].reset();
        $('.select2').select2();
    },

    _enableStopBtn: function () {
        $('.btn-stop-case').prop('disabled', '');
    },

    _enableContinueBtn: function () {
        $('.btn-continue').prop('disabled', '');
    },

    _enableEditBtn: function () {
        $('.btn-edit').prop('disabled', '');
    },

    _disableStopBtn: function () {
        $('.btn-stop-case').prop('disabled', 'disabled');
    },

    _enableStartBtn: function () {
        $('.btn-start-case').prop('disabled', '');
    },

    _disableStartBtn: function () {
        $('.btn-start-case').prop('disabled', 'disabled');
    },

    _enableHoldBtn: function () {
        $('.btn-hold-case').prop('disabled', '');
    },

    _disableHoldBtn: function () {
        $('.btn-hold-case').prop('disabled', 'disabled');
    },

    _disableContinueBtn: function () {
        $('.btn-continue').prop('disabled', 'disabled');
    },

    _disableEditBtn: function () {
        $('.btn-edit').prop('disabled', 'disabled');
    },

    _enableAuxBreakBtn: function () {
        $('#btn-aux-break').prop('disabled', '');
    },

    _disableAuxBreakBtn: function () {
        $('#btn-aux-break').prop('disabled', 'disabled');
    },

    _enableEscalateBtn: function () {
        $('.btn-escalate-case').prop('disabled', '');
    },

    _disableEscalateBtn: function () {
        $('.btn-escalate-case').prop('disabled', 'disabled');
    },

    _enableLobTab : function () {
        $('.lob-selector').removeClass('disabled');
    },

    _disableLobTab : function () {
      $('.lob-selector').addClass('disabled');
    },

    _hideDataTypeAndStatus : function () {
        $('#datatypestatus').closest('.form-group').addClass('hidden');
        $('#status').closest('.form-group').addClass('hidden');
    },

    _showDataTypeAndStatus : function () {
        $('#datatypestatus').closest('.form-group').removeClass('hidden');
        $('#status').closest('.form-group').removeClass('hidden');
    },

    _hideVerificationAndUtd : function () {
        $('#result').closest('.form-group').addClass('hidden');
        $('#utd').closest('.form-group').addClass('hidden');
        $('#result').val('1');
    },

    _showVerificationAndUtd : function () {
        $('#result').closest('.form-group').removeClass('hidden');
        $('#utd').closest('.form-group').removeClass('hidden');
        // $('#result').val(0);
    },

    _showValidationErrorPanel: function (lobId, validationErrors) {
        var errorList = "";
        var validationPanel = $('#validation-error-panel-'+lobId);

        $('#validation-error-list-'+lobId).html("");

        // for re-animation
        if (validationPanel.is(':visible') === true) {
            validationPanel.hide("fast");
        }

        validationPanel.show("fast", function () {
            $.each(validationErrors, function (key, value) {
                errorList += '<li class="text-danger">' + value + '</li>';
            });

            $('#validation-error-list-'+lobId).html(errorList);
        });
    },

    _closeValidationErrorPanel: function (lobId) {
        $('#validation-error-panel-'+lobId).hide('fast');
    },

    _continueTimer: function ($datadate) {
        CASE_TIMER._setTimerDataDate($datadate);
        CASE_TIMER.startTimer();


        CASE_HANDLING._enableStopBtn();
        CASE_HANDLING._enableHoldBtn();
        CASE_HANDLING._enableEscalateBtn();

    },

    _editTimer: function () {

        CASE_HANDLING._enableStopBtn();
        CASE_HANDLING._enableHoldBtn();
        CASE_HANDLING._enableEscalateBtn();
    },

    _computeDatadate : function (duration , updatedAt ,createdAt ,statusId) {
        var elapsedTime = duration;
        var elapsedMil = moment.duration(elapsedTime).asMilliseconds()
        var currentTime =  moment(updatedAt).subtract(elapsedMil).format('YYYY-MM-DD HH:mm:ss');
        if(statusId == 1) {
            currentTime = createdAt;
        }
        return currentTime;
    },

    _setAccountColor : function () {
        $('#account-header').addClass(CONSTANT.accountColor['Royal Blue']);
    },

    
    _checkVerificationResultsField : function () {
        if ($('#result').val() == 0) {
           return  this.validationErrors.push("<b>Verification Result<\/b> cannot be empty."); 
        } 
        return false;
    },

    _checkUTDField : function () {
        if ($('#result').select2('val') == 'UTD' && $('#utd').val() == 0) {
             return  this.validationErrors.push("<b>UTD<\/b> cannot be empty."); 
        }
        return false;
    },

    _checkNotesField : function () {
        if ($('#notes').val() == "") {
             return  this.validationErrors.push("<b>Notes<\/b> cannot be empty."); 
        }
        return false;
    },


};

$(document).ready(function () {
    CASE_HANDLING.build();
});