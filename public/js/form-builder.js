var FORM_BUILDER = {
    create: function(type, id, name, classes, value, attributes) {
        return eval('this.' + type + '("' + id + '","' + name + '","' + classes + '","' + value + '","' + attributes + '")');
    },
    TEXT: function(id, name, classes, value, attributes) {
        return '<input id="' + id + '" name="'+name+'" class="' + classes + '" + type="text" ' + this._attachAttributes(attributes) + '>';
    },
    TEXTAREA: function(id, name, classes, value, attributes) {
        return '<textarea id="' + id + '" name="'+name+'" class="' + classes + '"' + this._attachAttributes(attributes) + '></textarea>';
    },
    SELECT: function(id, name, classes, value, attributes) {
        var fieldValue = this._convertToJsonObject(value);
        var dropdown = '<select id= ' + id + ' name="' + name + '" class="' + classes + '" ' + this._attachAttributes(attributes) + '>';

        $.each(fieldValue, function(key, value) {
            dropdown+='<option value=' + key + '>' + value + '</option>';
        });

        return dropdown+='</select>';
    },
    RADIO: function(id, name, classes, value, attributes) {
        var fieldValue = this._convertToJsonObject(value);
        var radioDivFront = '<div class="radio">';
        var radioDivEnd = '</div>';
        var radionButtons = '';
        var counter=1;

        $.each(fieldValue, function(key, value) {
            var isChecked = (counter==1) ? 'checked="checked"': '';
            var radioId = name+counter;
            var radioBody = '<input id="'+radioId+'" name="' + name + '" value="' + key + '" '+ isChecked +' type="radio" ' +  FORM_BUILDER._attachAttributes(attributes) + '> <label for="'+radioId+'">' + value + '</label>';

            radionButtons+=radioDivFront+radioBody+radioDivEnd;
            counter++;
        });

        return radionButtons;
    },

    _attachAttributes: function(attributes) {
        if (attributes != null || attributes != '' || attributes != 'null') {
            var mergedAttributes = '';
            attributes = this._convertToJsonObject(attributes);
            $.each(attributes, function(key, value) {
                var attribute =  key + '=' + '"' + value + '" ';
                mergedAttributes+=attribute;
            });

            return mergedAttributes;
        }
    },
    _convertToJsonObject: function(value) {
        var value = value.replace(/'/g, '"');
        return jQuery.parseJSON(value);
    }
};
