var ICHECK_STARTER = {
    start: function() {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    }
};

var CLOCKPICKER_STARTER = {
    start: function() {
        $('.clockpicker').clockpicker();
    }
};

var LADDA_BINDER = {
    bind: function() {
        Ladda.bind('.ladda-button', { timeout: 700 });
    }
};

var DATE_HELPER = {
  formatDate: function(date, format) {
      return moment(new Date(date)).format(format);
  }
};

var IS_ON_BREAK = {
    check: function() {
        var moduleWhiteList = ['case-tracker'];
        var currentUrlPathArray = window.location.pathname.split("/");
        var currentModule = currentUrlPathArray[2];

        if (STORAGE.get('isOnBreak') && !$.inArray(currentModule, moduleWhiteList)) {
            ROUTES.break();
        }
    }
};

var ERROR_FIELDS_HANDLER = {
    cleanFormErrorFields: function(formId) {
        $('#' + formId + ' .has-error').removeClass('has-error');
    }
};

var TIME_HELPER = {
    secToHHMMSS: function(sec) {
        var hours = Math.floor(sec / 3600);
        var minutes = Math.floor((sec - (hours * 3600)) / 60);
        var seconds = Math.floor(sec - (hours * 3600) - (minutes * 60));

        hours = TIME_HELPER._tensZeroPad(hours);
        minutes = TIME_HELPER._tensZeroPad(minutes);
        seconds = TIME_HELPER._tensZeroPad(seconds);

        return hours + ':' + minutes + ':' + seconds;
    },

    unixToYYMMDDHHIISS: function (unixTimestamp) {
        unixTimestamp = Math.floor(unixTimestamp);

        var dateFromUnix = new Date(unixTimestamp);
        var year = dateFromUnix.getFullYear();
        var month = TIME_HELPER._tensZeroPad(dateFromUnix.getMonth() + 1);
        var day = TIME_HELPER._tensZeroPad(dateFromUnix.getDate());
        var hour = TIME_HELPER._tensZeroPad(dateFromUnix.getHours());
        var minute = TIME_HELPER._tensZeroPad(dateFromUnix.getMinutes());
        var second = TIME_HELPER._tensZeroPad(dateFromUnix.getSeconds());

        return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
    },

    currentDateTime: function () {
        var unixTime = + new Date();
        return TIME_HELPER.unixToYYMMDDHHIISS(unixTime);
    },

    _tensZeroPad: function (val) {
        if (val < 10) {
            return "0" + val;
        }

        return val;
    },

    _addTimes : function(startTime, endTime) {
      var times = [ 0, 0, 0 ]
      var max = times.length

      var a = (startTime || '').split(':')
      var b = (endTime || '').split(':')

      // normalize time values
      for (var i = 0; i < max; i++) {
        a[i] = isNaN(parseInt(a[i])) ? 0 : parseInt(a[i])
        b[i] = isNaN(parseInt(b[i])) ? 0 : parseInt(b[i])
      }

      // store time values
      for (var i = 0; i < max; i++) {
        times[i] = a[i] + b[i]
      }

      var hours = times[0]
      var minutes = times[1]
      var seconds = times[2]

      if (seconds >= 60) {
        var m = (seconds / 60) << 0
        minutes += m
        seconds -= 60 * m
      }

      if (minutes >= 60) {
        var h = (minutes / 60) << 0
        hours += h
        minutes -= 60 * h
      }

      return ('0' + hours).slice(-2) + ':' + ('0' + minutes).slice(-2) + ':' + ('0' + seconds).slice(-2)
    },



};

var MODAL = {
    bind: function() {
    },
    Alert: function(params) {
        var c_alert         = $(".c_alert");
        $(".modal-dialog", c_alert).addClass(params.class);
        $(".modal-title", c_alert).html(params.title);
        $(".modal-body", c_alert).html(params.msg);
       
        $(".modal-footer",c_alert).removeClass('hide');
        if(!params.footer)
        {
            $(".modal-footer",c_alert).addClass('hide');
        }
        c_alert.modal({backdrop: 'static', keyboard: false});
    }
};

var QUERY_STRING_HELPER = {
    getByName: function (name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    }
}

var PAYLOAD = {
    build: function (serializedFormData) {
        var payload = '{"data": {';

        $.each(serializedFormData, function(key, element) {
            payload+='"' + element.name + '":' + '"' + element.value + '", ';
        });

        payload = payload.slice(0, -2);

        return payload += '}}';
    }
}

var TABLE_PIVOT = {
    getPivotArray : function (dataArray, rowIndex, colIndex, dataIndex ) {
        var result = {}, ret =[];
        var newCols = [];
        var firstHeader = ' Name';
        for(var i = 0; i <dataArray.length; i++) {
            if(!result[dataArray[i][rowIndex]]) {
                result[dataArray[i][rowIndex]] = {};
            }
            result[dataArray[i][rowIndex]][dataArray[i][colIndex]] = dataArray[i][dataIndex];

            // get oolumn names
            if(newCols.indexOf(dataArray[i][colIndex]) == -1) {
                newCols.push(dataArray[i][colIndex]);
            }
        }
        newCols.sort();
        var item = [];

        // for Header Row
        item.push(firstHeader);
        item.push.apply(item, newCols);
        ret.push(item)

        // content
        for(var key in result) {
            item = [];
            item.push(key);
            for(var i = 0; i < newCols.length; i++){
                item.push(result[key][newCols[i]] || "0");
            }
            ret.push(item);
        }
        return ret;
    },


}

var TOASTR_HELPER = {
    success: function (message, options = []) {
        var initOptions = {"closeButton": true, "progressBar": true};
        toastr.options = $.extend(initOptions, options);
        toastr.success(message);
    },

    error: function (message, options = []) {
        var initOptions = {"closeButton": true, "progressBar": true};
        toastr.options = $.extend(initOptions, options);
        toastr.error(message);
    },

    warning: function (message, options = []){
        var initOptions = {"closeButton": true, "progressBar": true};
        toastr.options = $.extend(initOptions, options);
        toastr.warning(message);
    },

    clear: function(){
        toastr.clear();
    },

    remove: function(){
        toastr.remove();
    }
}

var TIMEZONE = {
    Code : {'CST' : 'America/Chicago' , 'MNL' : 'Asia/Manila'},
    DAYLIGHT : 1,

    convert : function (timezone, dateTime) {
        if(isInObjectKey(timezone,this.Code)) {
            return moment(dateTime).tz(this.Code[timezone]).format('YYYY-MM-DD');
        } else {
            return dateTime;
        }

    }
}

function isInObjectKey(value, object) {
    return Object.hasOwnProperty(value) > -1;
}

function setHiddenCaseIdValue(val)
{
    $('#case-id').val(val);
}

function getHiddenCaseIdValue() {
    return $('#case-id').val();
}

function setHiddenDurationValue(val)
{
    $('#duration').val(val);
}

function getHiddenDurationValue()
{
    return $('#duration').val();
}

function precisionRound(number, precision) {
    var factor = Math.pow(10, precision);
    return Math.round(number * factor) / factor;
}

$(document).ready(function() {
    ICHECK_STARTER.start();
    CLOCKPICKER_STARTER.start();
    LADDA_BINDER.bind();
    IS_ON_BREAK.check();
    MODAL;
});
