<?php

namespace Modules\Cotiviti\Entities;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable
        = [
            'caseid',
            'shift_date',
            'userid',
            'teamid',
            'timestamp_start',
            'timestamp_end',
            'duration',
            'icn_7',
            'concept_desc_7',
            'hold_reason_7',
            'drg_type_7',
            'drg_7',
            'drg_dollar_7',
            'adj_drg_7',
            'adj_drg_dollar_7',
            'decision_7',
            'account_status_7',
            'service_line_7',
            'comments_7',
            'status_7'
        ];

    protected $connection = 'tv_cotiviti';

    protected $table = 'raw';

    public $timestamps = false;
}
