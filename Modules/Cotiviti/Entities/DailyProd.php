<?php

namespace Modules\Cotiviti\Entities;

use Illuminate\Database\Eloquent\Model;

class DailyProd extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_cotiviti';

    protected $table = 'daily_prod_reports';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
