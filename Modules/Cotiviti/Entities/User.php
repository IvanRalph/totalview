<?php

namespace Modules\Cotiviti\Entities;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable
        = [
            'shift_date',
            'status',
            'shift_start',
            'shift_end'
        ];

    protected $connection = 'tv_cotiviti';

    protected $table = 'user';

    public $timestamps = false;

    protected $primaryKey = 'main_user_id';

    public function cases ()
    {
        return $this->hasMany('Modules\Cotiviti\Entities\Case7', 'user_id');
    }

    public function schedules ()
    {
        return $this->hasMany('Modules\Cotiviti\Entities\Schedule', 'user_id');
    }
}
