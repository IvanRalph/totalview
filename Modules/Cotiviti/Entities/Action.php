<?php

namespace Modules\Cotiviti\Entities;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_cotiviti';

    protected $table = 'action';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
