<?php

namespace Modules\Cotiviti\Entities;

use Illuminate\Database\Eloquent\Model;

class ListValue extends Model
{
    protected $fillable = ['status'];

    protected $connection = 'tv_cotiviti';

    protected $table = 'list_value';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
