<?php

namespace Modules\Cotiviti\Entities;

use Illuminate\Database\Eloquent\Model;

class UserAudit extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_cotiviti';

    protected $table = 'user_audit_trail';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
