<?php

namespace Modules\Cotiviti\Entities;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_cotiviti';

    protected $table = 'status';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
