<?php

namespace Modules\Cotiviti\Entities;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = ['shift_date', 'isKicked'];

    protected $connection = 'tv_cotiviti';

    protected $table = 'team';

    public $timestamps = false;

    protected $primaryKey = 'ID';

    public function users ()
    {
        return $this->hasMany('Modules\Cotiviti\Entities\User', 'team');
    }
}
