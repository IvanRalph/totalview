<?php

namespace Modules\Cotiviti\Entities;

use Illuminate\Database\Eloquent\Model;

class Case7 extends Model
{
    protected $fillable
        = [
            'shift_date',
            'user_id',
            'created_at',
            'updated_at',
            'duration',
            'hosp_serv',
            'fin',
            'mrn',
            'admit_date',
            'discharge_date',
            'price',
            'observation',
            'hold_reason',
            'accountstat',
            'comment',
            'status_id'
        ];

    protected $connection = 'tv_cotiviti';

    protected $table = 'case7';

    public $timestamps = false;

    protected $primaryKey = 'id';

}
