<?php

namespace Modules\Cotiviti\Entities;

use Illuminate\Database\Eloquent\Model;

class BreakRaw extends Model
{
    protected $fillable
        = [
            'user_id',
            'shift_date',
            'aux_type_id',
            'remarks',
            'start_time',
            'end_time',
            'duration'
        ];

    protected $connection = 'tv_cotiviti';

    protected $table = 'breaks';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
