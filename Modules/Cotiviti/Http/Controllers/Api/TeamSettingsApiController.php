<?php

namespace Modules\Cotiviti\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Cotiviti\Services\TeamSettingService;
use Modules\Cotiviti\Services\LobService;
use Modules\Main\Http\Controllers\Api\BaseTeamSettingsApiController;

class TeamSettingsApiController extends BaseTeamSettingsApiController
{
    public function __construct (TeamSettingService $teamSettingService, LobService $lobService)
    {
        $this->teamSettingService = $teamSettingService;
        $this->lobService         = $lobService;
    }
}
