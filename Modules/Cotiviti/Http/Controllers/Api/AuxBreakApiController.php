<?php

namespace Modules\Cotiviti\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Cotiviti\Services\AuxBreakService;
use Modules\Cotiviti\Services\UserAuditTrailService;
use Modules\Cotiviti\Services\UserService;
use Modules\Cotiviti\Services\CaseService;
use Modules\Main\Http\Controllers\Api\BaseAuxBreakApiController;

class AuxBreakApiController extends BaseAuxBreakApiController
{
    public function __construct (AuxBreakService $auxBreakService, UserService $userService, UserAuditTrailService $userAuditTrailService, CaseService $caseService)
    {
        $this->auxBreakService       = $auxBreakService;
        $this->userService           = $userService;
        $this->userAuditTrailService = $userAuditTrailService;
        $this->caseService           = $caseService;
    }
}