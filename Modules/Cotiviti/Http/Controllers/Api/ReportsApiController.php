<?php

namespace Modules\Cotiviti\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Modules\Cotiviti\Services\LobService;
use Modules\Cotiviti\Services\CaseService;
use Modules\Cotiviti\Services\FieldService;
use Modules\Cotiviti\Services\ReportService;
use Modules\Main\Http\Controllers\Api\BaseReportsApiController;

class ReportsApiController extends BaseReportsApiController
{
    public function __construct (LobService $lobService, CaseService $caseService, FieldService $fieldService, ReportService $reportService)
    {
        $this->lobService    = $lobService;
        $this->caseService   = $caseService;
        $this->fieldService  = $fieldService;
        $this->reportService = $reportService;
    }

    public function getDailyReports (Request $request)
    {
        $dateFrom = $request->get('start');
        $dateTo   = $request->get('end');
        $data     = $this->reportService->getDaily($dateFrom, $dateTo);

        return json_encode($data);
    }

    public function getRawReports (Request $request)
    {
        $rawNumber = $request->get('rawNumber');
        $start     = $request->get('start');
        $end       = $request->get('end');
        $timezone  = $request->get('timezone');
        $data      = $this->reportService->getRaw([
            'start' => $start,
            'end'   => $end,
            'rows'  => "raw.shift_date as Date, concat(user.lastname, ', ', user.firstname) as Name, team.description as Team, raw.timestamp_start as `Start Time`, raw.timestamp_end as `End Time`, raw.duration as Duration, raw.icn_7 AS ICN, raw.concept_desc_7 AS Concept, raw.hold_reason_7 AS `Hold Reason`, raw.drg_type_7 AS Type, raw.drg_7 AS DRG, raw.drg_dollar_7 AS `Dollar Value`, raw.adj_drg_7 AS `Adjusted DRG`, raw.adj_drg_dollar_7 AS `Adjusted Dollar Value`, raw.decision_7 AS Decision, raw.service_line_7 AS `Service Line`, raw.comments_7 AS Comments, raw.status_7 AS Status",
        ]);

        return json_encode($data);
    }
}