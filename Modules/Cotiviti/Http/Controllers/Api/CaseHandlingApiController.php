<?php

namespace Modules\Cotiviti\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Cotiviti\Services\CaseActivityLogService;
use Modules\Cotiviti\Services\CaseService;
use Modules\Cotiviti\Services\FieldService;
use Modules\Cotiviti\Services\ListValueService;
use Modules\Cotiviti\Services\LobService;
use Modules\Cotiviti\Services\StatusService;
use Modules\Cotiviti\Services\UserAuditTrailService;
use Modules\Main\Http\Controllers\Api\BaseCaseHandlingApiController;
use Validator;

class CaseHandlingApiController extends BaseCaseHandlingApiController
{
    public function __construct (CaseService $caseService, FieldService $fieldService, StatusService $statusService, CaseActivityLogService $caseActivityLogService, UserAuditTrailService $userAuditTrailService, LobService $lobService, ListValueService $listValueService)
    {
        $this->caseService            = $caseService;
        $this->fieldService           = $fieldService;
        $this->statusService          = $statusService;
        $this->caseActivityLogService = $caseActivityLogService;
        $this->userAuditTrailService  = $userAuditTrailService;
        $this->lobService             = $lobService;
        $this->listValueService       = $listValueService;
    }

    public function updateIcn (Request $request, $lobId, $caseId)
    {
        $data        = $request->get('data');
        $isContinued = array_key_exists('isContinued', $data) ? $data['isContinued'] : 0;
        $fields      = $this->fieldService->getByLobId($lobId);
        $isSuccess   = $this->caseService->update([
            'lob_id'       => $lobId,
            'case_id'      => $caseId,
            'data'         => $data,
            'fields'       => $fields,
            'is_continued' => $isContinued
        ]);

        return response()->json(['is_success' => $isSuccess]);
    }

    public function getByIcn (Request $request, $lobId, $icn = null)
    {
        if ($icn == null) {
            return response()->json(['case_info' => null]);
        }

        $user        = session('user_from_account');
        $caseType    = $request->get('caseType');
        $icnCallBack = $this->caseService->getByIcn($lobId, $icn);

        if (in_array($caseType, [
            'Case Processing',
            'Second Pass'
        ])) {
            if (!$icnCallBack) {
                return response()->json(['case_info' => $icnCallBack]);
            }

            //Different user, same shift date and status is on hold
            if ($icnCallBack->user_id != $user->id && $icnCallBack->shift_date == $user->shift_date && $icnCallBack->status_id == $this->statusService::HOLD_STATUS_ID) {
                return response()->json(['toastr' => 'ICN #' . $icn . ' already exists. Please enter a new one.']);
            }

            //Different shift date and status is on hold
            if ($icnCallBack->shift_date != $user->shift_date && $icnCallBack->status_id == $this->statusService::HOLD_STATUS_ID) {
                return response()->json([
                    'case_info'  => $icnCallBack,
                    'new_record' => true
                ]);
            }

            return response()->json([
                'case_info'      => $icnCallBack,
                'new_record'     => true,
                'prev_case_type' => $icnCallBack->status_7
            ]);
        } else {
            if (!$icnCallBack) {
                return response()->json(['toastr' => 'ICN #' . $icn . ' does not exist. Please enter an existing ICN for review.']);
            }

            return response()->json([
                'case_info'      => $icnCallBack,
                'new_record'     => true,
                'prev_case_type' => $icnCallBack->status_7
            ]);
        }
    }

    public function creditStats ($serviceLine)
    {
        $user = session('user_from_account');

        $recovery = $this->caseService->getUserCreditStats([
            'service_line' => $serviceLine,
            'user_id'      => $user->id,
            'decision'     => 'Recovery'
        ]);

        $nonRecovery = $this->caseService->getUserCreditStats([
            'service_line' => $serviceLine,
            'user_id'      => $user->id,
            'decision'     => 'Non-Recovery'
        ]);

        $total = [];
        if ($serviceLine == "CAT") {
            $total[0] = $recovery->CV_APR + $nonRecovery->CV_APR;
            $total[1] = $recovery->CV_MS + $nonRecovery->CV_MS;
            $total[2] = $recovery->Second_Pass + $nonRecovery->Second_Pass;
        } else {
            $total[0] = $recovery->CV_APR + $nonRecovery->CV_APR;
            $total[1] = $recovery->CV_MS + $nonRecovery->CV_MS;
            $total[2] = $recovery->Coding_APR + $nonRecovery->Coding_APR;
            $total[3] = $recovery->Coding_MS + $nonRecovery->Coding_MS;
            $total[4] = $recovery->Second_Pass + $nonRecovery->Second_Pass;
        }

        return response()->json([
            'recovery'     => $recovery,
            'non_recovery' => $nonRecovery,
            'total'        => $total
        ]);
    }
}