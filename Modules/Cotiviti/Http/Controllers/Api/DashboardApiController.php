<?php

namespace Modules\Cotiviti\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Artisan;
use Modules\Cotiviti\Services\CaseService;
use Modules\Cotiviti\Services\StatusService;
use Modules\Cotiviti\Services\TeamService;
use Modules\Cotiviti\Services\TeamSettingService;
use Modules\Cotiviti\Services\UserAuditTrailService;
use Modules\Cotiviti\Services\UserService;
use Modules\Main\Http\Controllers\Api\BaseDashboardApiController;

class DashboardApiController extends BaseDashboardApiController
{
    /**
     * DashboardApiController constructor.
     * @param CaseService           $caseService
     * @param UserService           $userService
     * @param UserAuditTrailService $userAuditTrailService
     * @param TeamService           $teamService
     * @param TeamSettingService    $teamSettingService
     */
    public function __construct (
        CaseService $caseService,
        UserService $userService,
        UserAuditTrailService $userAuditTrailService,
        TeamService $teamService,
        TeamSettingService $teamSettingService
    )
    {
        $this->caseService           = $caseService;
        $this->userService           = $userService;
        $this->userAuditTrailService = $userAuditTrailService;
        $this->teamService           = $teamService;
        $this->teamSettingService    = $teamSettingService;
    }

    public function getMetrics (Request $request, $lobId, $teamId)
    {
        $mainUser        = $request->get('userData');
        $userFromAccount = $this->userService->getByMainUserId($mainUser['id']);
        $avgCph          = $this->caseService->getAvgCphPerTeam($lobId, $teamId);
        $avgCphTarget    = $this->caseService->getAvgCphTargetPerTeam($lobId, $teamId);
        $totalNonProd    = $this->caseService->getTotalNonProd($lobId, $teamId);
        $totalProdCases  = $this->caseService->getTotalProdCases($lobId, $teamId);
        $field_id        = '9';

        if ($teamId == 'all') {
            $sched = $this->caseService->getShiftSchedule();
        } else {
            $sched = $this->caseService->getTeamSchedule($teamId);
        }

        $shift_date         = $sched[0]->shift_date;
        $listValue          = $this->caseService->getListServiceLine($field_id);
        $newServiceLineView = [];

        //list
        $perServiceLine   = $this->caseService->getPerServiceLine($lobId, $teamId, $shift_date);
        $fieldid          = '11';
        $perServiceStatus = $this->caseService->getListStatus($fieldid);

        foreach ($perServiceStatus as $serviceStatus) {
            foreach ($listValue as $listValues) {
                $newServiceLineView += [
                    $listValues->value => 0,
                ];
            }
            $newList[$serviceStatus->value] = $newServiceLineView;
        }

        //listresults
        foreach ($perServiceLine as $key => $value) {
            $newList[$value->status_7][$value->service_line_7] = $value->prod;
        }

        //Total Production Cases
        $teamTPCOthers    = $this->caseService->getTeamTPCOthers($lobId, $teamId, $shift_date);
        $teamTPCSeconPass = $this->caseService->getTeamTPCSecondPass($lobId, $teamId, $shift_date);
        $teamTPCases      = $teamTPCOthers[0]->prod_count + ($teamTPCSeconPass[0]->prod_count * 0.5);
        $teamRamp         = $this->caseService->getTeamRamp($teamId);
        $teamNonProd      = $this->caseService->getTeamNonProd($teamId, $shift_date);

        //CPH
        $teamProdDuration = $this->caseService->getTeamProdDuration($teamId, $shift_date);

        if (!isset($teamProdDuration[0]->duration)) {
            $teamCPH = '0.00';
        } else {
            $teamCPH = round($teamTPCases / $teamProdDuration[0]->duration, 2);
        }

        return response()->json([
            'avg_cph'          => $teamCPH,
            'avg_cph_target'   => $avgCphTarget,
            'total_non_prod'   => $teamNonProd[0]->nonProd,
            'total_prod_cases' => $teamTPCases,
            //'service_line' => $ServiceLineView,
            'new_list'         => $newList
        ]);
    }

    public function manualKickout ($lobId, $teamId)
    {
        $oldKickOut = $this->teamService->getKickOut($teamId)->end_time;
        $this->teamSettingService->updateTeamKickout([
            'team_id'=>$teamId,
            'end_time' => Carbon::now()->format('H:i')
        ]);

        Artisan::call('schedule:run');

        $this->teamSettingService->updateTeamKickout([
            'team_id'=>$teamId,
            'end_time' => $oldKickOut,
            'is_kicked'=>1
        ]);

        return response()->json(['kickoutSuccess' => true], 200);
    }
}
