<?php

namespace Modules\Cotiviti\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Cotiviti\Services\AuxBreakService;
use Modules\Main\Http\Controllers\Web\BaseAuxBreakWebController;

class AuxBreakWebController extends BaseAuxBreakWebController
{
    /**
     * @param $auxBreakService AuxBreakService
     */
    public function __construct (AuxBreakService $auxBreakService)
    {
        $this->auxBreakService = $auxBreakService;
    }
}