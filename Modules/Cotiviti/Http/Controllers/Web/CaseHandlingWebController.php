<?php

namespace Modules\Cotiviti\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Cotiviti\Services\CaseService;
use Modules\Cotiviti\Services\FieldService;
use Modules\Cotiviti\Services\ListValueService;
use Modules\Cotiviti\Services\LobService;
use Modules\Cotiviti\Services\UserAuditTrailService;
use Modules\Cotiviti\Services\UserService;
use Modules\Main\Http\Controllers\Web\BaseCaseHandlingWebController;

class CaseHandlingWebController extends BaseCaseHandlingWebController
{
    /**
     * CaseHandlingWebController constructor.
     * @param FieldService          $fieldService
     * @param ListValueService      $listValueService
     * @param CaseService           $caseService
     * @param UserService           $userService
     * @param UserAuditTrailService $userAuditTrailService
     * @param LobService            $lobService
     */
    public function __construct (
        FieldService $fieldService,
        ListValueService $listValueService,
        CaseService $caseService,
        UserService $userService,
        UserAuditTrailService $userAuditTrailService,
        LobService $lobService
    )
    {
        $this->fieldService          = $fieldService;
        $this->listValueService      = $listValueService;
        $this->caseService           = $caseService;
        $this->userService           = $userService;
        $this->userAuditTrailService = $userAuditTrailService;
        $this->lobService            = $lobService;
    }
}