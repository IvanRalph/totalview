<?php

namespace Modules\Cotiviti\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Cotiviti\Services\TeamSettingService;
use Modules\Cotiviti\Services\LobService;
use Modules\Cotiviti\Services\RoleService;
use Modules\Cotiviti\Services\UserService;

class TeamSettingsWebController extends Controller
{
    private $teamSettingService;

    private $lobService;

    public function __construct (TeamSettingService $teamSettingService, LobService $lobService, RoleService $roleService, UserService $userService)
    {
        $this->teamSettingService = $teamSettingService;
        $this->lobService         = $lobService;
        $this->roleService        = $roleService;
        $this->userService        = $userService;
    }

    public function index ()
    {
        $data['data']['getTeamLeader']  = $this->teamSettingService->getTeamLeader();
        $data['data']['getTeamList']    = $this->teamSettingService->getTeamList();
        $data['data']['getLobList']     = $this->lobService->getLobList();
        $data['data']['getRoleList']    = $this->roleService->getRoleList();
        $data['data']['getNurseStatus'] = $this->userService->getNurseStatus();

        return view('cotiviti::team-settings.index', $data);
    }
}