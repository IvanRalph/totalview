<?php

namespace Modules\Cotiviti\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Cotiviti\Services\DashboardService;
use Modules\Cotiviti\Services\LobService;
use Modules\Cotiviti\Services\UserService;
use Modules\Cotiviti\Services\TeamService;

class DashboardWebController extends Controller
{
    private $dashboardService;

    public function __construct (DashboardService $dashboardService, LobService $lobService, UserService $userService, TeamService $teamService)
    {
        $this->dashboardService = $dashboardService;
        $this->lobService       = $lobService;
        $this->userService      = $userService;
        $this->teamService      = $teamService;
    }

    public function index ()
    {
        $data['data']['getLobList']   = $this->lobService->getLobList();
        $data['data']['getShiftDate'] = $this->userService->getShiftDateForDashboard();
        $data['data']['getTeamList']  = $this->teamService->getTeamList();

        return view('cotiviti::dashboard.index', $data);
    }

    public function fullscreen ()
    {
        return view('cotiviti::dashboard.partials.dashboard-fullscreen');
    }
}