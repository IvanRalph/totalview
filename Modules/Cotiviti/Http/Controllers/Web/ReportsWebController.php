<?php

namespace Modules\Cotiviti\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Cotiviti\Services\LobService;

class ReportsWebController extends Controller
{
    private $lobService;

    public function __construct (LobService $lobService)
    {
        $this->lobService = $lobService;
    }

    public function index ()
    {
        $data['getLobList'] = $this->lobService->getLobList();

        return view('cotiviti::reports.index', $data);
    }
}




