<style type="text/css">
    .select2-container {
        z-index: 2050;
    }
</style>
<div class="modal inmodal" id="valuelistEditModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="text-left">Manage Item </h2>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="text-center">Item Name</label>
                    <input type="text" id="edit-Item-Name" name="edit_Item_Name" class="form-control">

                </div>

                <div class="form-group">
                    <div class="checkbox checkbox-primary">
                        <input id="edit-Item-Status" type="checkbox">
                        <label for="status">
                            Status
                        </label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSaveValuelistSettings" class="btn btn-info"> Save</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div class="modal inmodal" id="addValuelistModal" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
              
                <form id = 'addformid'>
                                                    
            <div class="modal-header">
                  <button id = "add-More" class="btn btn-primary btn-sm pull-right" type="button"><i class="fa fa-plus"></i> Add More..</button>
                <h2 class="text-left">Add Item</h2>
            </div>
            <div class="modal-body" id = "itemlist">
                <div class="form-group">
                        <span class="badge badge-info">1</span>
                    <label class="text-center">Item Name</label>
                    <input type="text"  id = "itemupdate-name" name="additem_name[]" class="form-control">
                </div>
      
                
               
             
            </div>
            <div class="modal-footer">
                <button type="button" id="btnAddBulkItem" class="btn btn-info">Save
                </button>
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </form>
        </div>
    </div>
</div>