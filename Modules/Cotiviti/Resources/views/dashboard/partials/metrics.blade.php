<style>
    .metric-panel .ibox-content span {
        font-size: 1.3vw;
    }
</style>

<!--
<div class="col-lg-6">
    <div class="ibox float-e-margins metric-panel">
        <div class="ibox-title">
            <h5>Service Line</h5>
        </div>
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
            <h1 class="no-margins"><span class="serviceLines"></span></h1>
            <div class="stat-percent font-bold text-info"></div>
        </div>
    </div>
</div>
-->
<div class="col-lg-6">
    <div class="ibox float-e-margins metric-panel">
        <div id='newServiceLines' class="ibox-content">

        </div>
    </div>
</div>

<div class="col-lg-2">
    <div class="ibox float-e-margins metric-panel">
        <div class="ibox-title">
            <h4 class=" text-center">CPH</h4>
        </div>
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
            <h1 class="no-margins no-margins text-info text-center"><span class="avgCph">0.00</span><span>/</span><span
                        class="avgCphTarget">0</span></h1>
        </div>
    </div>
</div>

<div class="col-lg-2">
    <div class="ibox float-e-margins metric-panel">
        <div class="ibox-title">
            <h4 class=" text-center">Non Prod</h4>
        </div>
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
            <h1 class="no-margins no-margins text-info text-center"><span class="totalNonProd">0</span></h1>
        </div>
    </div>
</div>

<div class="col-lg-2">
    <div class="ibox float-e-margins metric-panel">
        <div class="ibox-title">
            <h4 class=" text-center">Prod</h4>
        </div>
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
            <h1 class="no-margins no-margins text-info text-center"><span class="totalProdCases">0.00</span></h1>
        </div>
    </div>
</div>
