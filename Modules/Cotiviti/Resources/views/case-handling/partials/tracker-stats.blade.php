<div class="ibox float-e-margins tracker-stats">
    <div class="ibox-title text-center">
        <h5>Tracker Stats</h5>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-12">
                <div id="caseStats" data-timer="0">
                    @foreach($userLobs as $key => $lob)
                        <div class="col-lg-7">
                            <div class="col-lg-4  no-margins">
                                <div class="ibox float-e-margins border-left-right border-bottom">
                                    <div class="ibox-title">
                                        <h4 class="text-center">CPH</h4>
                                    </div>
                                    <div class="ibox-content">
                                        <h3 id="cph-{{ $lob->id }}"
                                            class="no-margins text-info text-center">{{ $trackerStats[$lob->id]['cph'] }}
                                            / {{ $trackerStats[$lob->id]['target'] }}</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4  no-margins">
                                <div class="ibox float-e-margins border-left-right border-bottom">
                                    <div class="ibox-title">
                                        <h4 class="text-center">Non Prod</h4>
                                    </div>
                                    <div class="ibox-content">

                                        <h3 id="non-prod-{{ $lob->id }}"
                                            class="no-margins text-info text-center">{{ $trackerStats[$lob->id]['total_non_prod_duration'] }}</h3>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4  no-margins">
                                <div class="ibox float-e-margins border-left-right border-bottom">
                                    <div class="ibox-title">
                                        <h4 class="text-center">Prod</h4>
                                    </div>
                                    <div class="ibox-content">

                                        <h3 id="prod-{{ $lob->id }}"
                                            class="no-margins text-info text-center">{{ $trackerStats[$lob->id]['total_prod_duration'] }}</h3>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-5">
                            <div class="widget style1 {{$trackerStats[$lob->id]['break_class'] }}-bg">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <i class="fa fa-coffee fa-3x"></i>
                                    </div>
                                    <div class="col-sm-8 text-right">
                                        <span> Total Breaks </span>
                                        <h2 class="font-bold"
                                            id="break-{{ $lob->id }}">{{ $trackerStats[$lob->id]['breaks'] }}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="service-line-stat-{{$lob->id}}">
                                <table class="table table-condensed table-bordered text-center text-info font-bold"
                                       id="tracker-stats-table">
                                    <tr style="color: #676a6c;">
                                        <td>Service Lines</td>
                                        <td>Case Processing</td>
                                        <td>Second Pass</td>
                                        <td>Peer Review</td>
                                    </tr>
                                    <tr>
                                        <td>Wellcare</td>
                                        <td>{{ $trackerStats[$lob->id]['service_line_stats'][0]->wellcare_case_processing }}</td>
                                        <td>{{ $trackerStats[$lob->id]['service_line_stats'][0]->wellcare_second_pass }}</td>
                                        <td>{{ $trackerStats[$lob->id]['service_line_stats'][0]->wellcare_peer_review }}</td>
                                    </tr>
                                    <tr>
                                        <td>Wellpoint</td>
                                        <td>{{ $trackerStats[$lob->id]['service_line_stats'][0]->wellpoint_case_processing }}</td>
                                        <td>{{ $trackerStats[$lob->id]['service_line_stats'][0]->wellpoint_second_pass }}</td>
                                        <td>{{ $trackerStats[$lob->id]['service_line_stats'][0]->wellpoint_peer_review }}</td>
                                    </tr>
                                    <tr>
                                        <td>CAT</td>
                                        <td>{{ $trackerStats[$lob->id]['service_line_stats'][0]->cat_case_processing }}</td>
                                        <td>{{ $trackerStats[$lob->id]['service_line_stats'][0]->cat_second_pass }}</td>
                                        <td>{{ $trackerStats[$lob->id]['service_line_stats'][0]->cat_peer_review }}</td>
                                    </tr>
                                    <tr>
                                        <td>UHC</td>
                                        <td>{{ $trackerStats[$lob->id]['service_line_stats'][0]->uhc_case_processing }}</td>
                                        <td>{{ $trackerStats[$lob->id]['service_line_stats'][0]->uhc_second_pass }}</td>
                                        <td>{{ $trackerStats[$lob->id]['service_line_stats'][0]->uhc_peer_review }}</td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="panel blank-panel">
                                <div class="panel-heading" style="padding-left: 0px; padding-right: 0px;">
                                    <div class="panel-options">
                                        <ul class="nav nav-tabs">
                                            <li><a class="nav-link credit-stats" href="#tab-1" data-toggle="tab">Wellcare</a></li>
                                            <li><a class="nav-link credit-stats" href="#tab-2" data-toggle="tab">Wellpoint</a></li>
                                            <li><a class="nav-link credit-stats" href="#tab-3" data-toggle="tab">CAT</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel-body" style="padding-left: 0px; padding-right: 0px;">
                                    <div class="tab-content">
                                        <div class="tab-pane" id="tab-1">
                                            <table class="table table-condensed table-bordered text-center text-info font-bold">
                                                <thead>
                                                <tr style="color: #676a6c;">
                                                    <td style="color: #23c6c8">Credits</td>
                                                    <td>CV APR</td>
                                                    <td>CV MS</td>
                                                    <td>Coding APR</td>
                                                    <td>Coding MS</td>
                                                    <td>Second Pass</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr class="stats-recovery">
                                                    <td class="side-title" style="color: #676a6c;">R</td>
                                                </tr>
                                                <tr class="stats-non-recovery">
                                                    <td class="side-title" style="color: #676a6c;">NR</td>
                                                </tr>
                                                <tr class="stats-total">
                                                    <td class="side-title" style="color: #676a6c;">TOTAL</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="tab-2">
                                            <table class="table table-condensed table-bordered text-center text-info font-bold">
                                                <thead>
                                                <tr style="color: #676a6c;">
                                                    <td style="color: #23c6c8">Credits</td>
                                                    <td>CV APR</td>
                                                    <td>CV MS</td>
                                                    <td>Coding APR</td>
                                                    <td>Coding MS</td>
                                                    <td>Second Pass</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr class="stats-recovery">
                                                    <td class="side-title" style="color: #676a6c;">R</td>
                                                </tr>
                                                <tr class="stats-non-recovery">
                                                    <td class="side-title" style="color: #676a6c;">NR</td>
                                                </tr>
                                                <tr class="stats-total">
                                                    <td class="side-title" style="color: #676a6c;">TOTAL</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="tab-3">
                                            <table class="table table-condensed table-bordered text-center text-info font-bold">
                                                <thead>
                                                <tr style="color: #676a6c;">
                                                    <td style="color: #23c6c8">Credits</td>
                                                    <td>CV APR</td>
                                                    <td>CV MS</td>
                                                    <td>Second Pass</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr class="stats-recovery">
                                                    <td class="side-title" style="color: #676a6c;">R</td>
                                                </tr>
                                                <tr class="stats-non-recovery">
                                                    <td class="side-title" style="color: #676a6c;">NR</td>
                                                </tr>
                                                <tr class="stats-total">
                                                    <td class="side-title" style="color: #676a6c;">TOTAL</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>