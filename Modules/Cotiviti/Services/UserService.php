<?php

namespace Modules\Cotiviti\Services;

use Illuminate\Support\Facades\DB;
use Modules\Main\Services\BaseUserService;
use Modules\Main\Services\DatabaseService;

class UserService extends BaseUserService
{
    public function __construct (DatabaseService $databaseService)
    {
        parent::__construct($databaseService);

        $this->db = DB::connection('tv_cotiviti');
    }

    public function updateTeamSchedule ($id, $start_shift, $end_shift, $on_leave)
    {
        $myArrayId = explode(',', $id);

        $res = $this->db->table('attendance')
            ->whereIn('userid', $myArrayId)
            ->update([
                'start_shift' => $start_shift,
                'end_shift'   => $end_shift,
                'leave'       => $on_leave
            ]);
    }

    public function getNurseStatus ()
    {
        return $this->db->table('user_status')->get();
    }
}