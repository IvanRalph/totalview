<?php

namespace Modules\Cotiviti\Services;

use Illuminate\Support\Facades\DB;
use Modules\Main\Services\BaseTeamSettingService;
use Modules\Main\Services\DatabaseService;

class TeamSettingService extends BaseTeamSettingService
{
    public function __construct (DatabaseService $databaseService)
    {
        parent::__construct($databaseService);

        $this->db = DB::connection('tv_cotiviti');
    }

    public function getTeamList ()
    {
        $res = $this->db->table('team')
            ->leftJoin('user', function ($join) {
                $join->where(DB::raw('FIND_IN_SET(user.id, team.lead)'), '>', 0);
            })
            ->select('team.ID', 'team.lead', 'user.username', 'team.end_time', 'team.description as team',
                \DB::raw("GROUP_CONCAT(CONCAT(' ',user.firstname,' ', user.lastname, ' ' ) ORDER BY user.id ) as fullname"))
            ->groupBy('team.ID')
            ->get();

        return $res;
    }

    public function getTeamListForTable ()
    {
        $res = $this->db->table('team')
            ->leftJoin('user', function ($join) {
                $join->where(DB::raw('FIND_IN_SET(user.id, team.lead)'), '>', 0);
            })
            ->select('team.ID', 'team.lead', 'user.username', 'team.end_time',
                'team.description as team', \DB::raw("GROUP_CONCAT(CONCAT(' ',user.firstname,' ', user.lastname, ' ' ) ORDER BY user.id ) as fullname"))
            ->groupBy('team.ID')
            ->get();

        return $res;
    }
}