<?php

namespace Modules\Cotiviti\Services;

use Modules\Main\Services\BaseRoleService;
use Modules\Main\Services\DatabaseService;

class RoleService extends BaseRoleService
{
    public function __construct (DatabaseService $databaseService)
    {
        parent::__construct($databaseService);
    }
}