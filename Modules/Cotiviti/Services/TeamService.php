<?php

namespace Modules\Cotiviti\Services;

use Modules\Main\Services\BaseTeamService;
use Modules\Main\Services\DatabaseService;

class TeamService extends BaseTeamService
{
    public function __construct (DatabaseService $databaseService)
    {
        parent::__construct($databaseService);
    }
}