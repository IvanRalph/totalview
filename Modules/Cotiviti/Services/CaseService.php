<?php

namespace Modules\Cotiviti\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Modules\Cotiviti\Entities\User AS User;
use Modules\Cotiviti\Entities\Team AS Team;
use Modules\Cotiviti\Entities\Case7 AS Case7;
use Modules\Main\Services\BaseCaseService;
use Modules\Main\Services\DatabaseService;

class CaseService extends BaseCaseService
{
    public function __construct (CaseActivityLogService $caseActivityLogService, StatusService $statusService, FieldService $fieldService, DatabaseService $databaseService)
    {
        parent::__construct($databaseService);

        $this->caseActivityLogService = $caseActivityLogService;
        $this->statusService          = $statusService;
        $this->fieldService           = $fieldService;
        $this->databaseService        = $databaseService;
        $this->db                     = DB::connection('tv_cotiviti');
    }

    public function getCurrentCase ($lobId, $userId)
    {
        $caseTable = self::TABLE_NAME . $lobId;
        $statusIds = StatusService::ONGOING_STATUS_ID;

        $query = $this->db->table($caseTable)
            ->where('user_id', $userId)
            ->whereIn('status_id', $statusIds)
            ->where('shift_date', session('user_from_account')->shift_date)
            ->orderBy('id', 'desc');

        return $query->count() > 0 ? $query->first() : false;
    }

    public function getUserEscalated ($lobId, $userId, $shiftDate)
    {
        return $this->db->table(self::TABLE_NAME . $lobId)
            ->where('user_id', $userId)
            ->where('shift_date', $shiftDate)
            ->whereIn('status_id', [StatusService::ESCALATED_STATUS_ID])
            ->get();
    }

    public function getUserCompletedByServiceLine ($lobId, $userId, $shiftDate)
    {
        return $this->db->table(self::TABLE_NAME . $lobId)
            ->where('user_id', $userId)
            ->where('shift_date', $shiftDate)
            ->whereIn('status_id', [
                StatusService::COMPLETED_STATUS_ID,
                StatusService::EDIT_COMPLETED_STATUS_ID,
                StatusService::HOLD_STATUS_ID
            ])
            ->selectRaw('
                COALESCE(SUM(CASE WHEN status_7 = "Case Processing" AND service_line_7 = "Wellcare" AND isContinued = 0 THEN 1 ELSE 0 END), 0) AS wellcare_case_processing,
                COALESCE(SUM(CASE WHEN status_7 = "Second Pass" AND service_line_7 = "Wellcare" AND isContinued = 0 THEN 1 ELSE 0 END), 0) AS wellcare_second_pass,
                COALESCE(SUM(CASE WHEN status_7 = "Peer Review" AND service_line_7 = "Wellcare" AND isContinued = 0 THEN 1 ELSE 0 END), 0) AS wellcare_peer_review,
                COALESCE(SUM(CASE WHEN status_7 = "Case Processing" AND service_line_7 = "Wellpoint" AND isContinued = 0 THEN 1 ELSE 0 END), 0) AS wellpoint_case_processing,
                COALESCE(SUM(CASE WHEN status_7 = "Second Pass" AND service_line_7 = "Wellpoint" AND isContinued = 0 THEN 1 ELSE 0 END), 0) AS wellpoint_second_pass,
                COALESCE(SUM(CASE WHEN status_7 = "Peer Review" AND service_line_7 = "Wellpoint" AND isContinued = 0 THEN 1 ELSE 0 END), 0) AS wellpoint_peer_review,
                COALESCE(SUM(CASE WHEN status_7 = "Case Processing" AND service_line_7 = "CAT" AND isContinued = 0 THEN 1 ELSE 0 END), 0) AS cat_case_processing,
                COALESCE(SUM(CASE WHEN status_7 = "Second Pass" AND service_line_7 = "CAT" AND isContinued = 0 THEN 1 ELSE 0 END), 0) AS cat_second_pass,
                COALESCE(SUM(CASE WHEN status_7 = "Peer Review" AND service_line_7 = "CAT" AND isContinued = 0 THEN 1 ELSE 0 END), 0) AS cat_peer_review,
                COALESCE(SUM(CASE WHEN status_7 = "Case Processing" AND service_line_7 = "UHC" AND isContinued = 0 THEN 1 ELSE 0 END), 0) AS uhc_case_processing,
                COALESCE(SUM(CASE WHEN status_7 = "Second Pass" AND service_line_7 = "UHC" AND isContinued = 0 THEN 1 ELSE 0 END), 0) AS uhc_second_pass,
                COALESCE(SUM(CASE WHEN status_7 = "Peer Review" AND service_line_7 = "UHC" AND isContinued = 0 THEN 1 ELSE 0 END), 0) AS uhc_peer_review
            ')
            ->get();
    }

    public function getUserAuxBreakDuration ($userId, $shiftDate)
    {
        return $this->db->table('aux')
            ->where('user_id', $userId)
            ->where('shift_date', $shiftDate)
            ->whereNotIn('aux_type_id', [1])
            ->groupBy('user_id')
            ->selectRaw('sec_to_time(sum(time_to_sec(timediff(end_time,start_time)))) as duration')
            ->get();
    }

    public function getUserTrackerStats ($lobId, $userId, $shiftDate)
    {
        $totalProdDuration    = 0;
        $totalNonProdDuration = 0;
        $totalProdCount       = 0;
        $totalNonProdCount    = 0;
        $ahtSec               = 0;
        $cph                  = 0;

        $completedCases = $this->getUserCompleted([
            'lob_id'     => $lobId,
            'user_id'    => $userId,
            'shift_date' => $shiftDate
        ]);

        $totalCompletedCase = count($completedCases);

        $holdCases = $this->getUserPending([
            'lob_id'     => $lobId,
            'user_id'    => $userId,
            'shift_date' => $shiftDate
        ]);

        $totalHoldCase = count($holdCases);

        $totalBreaks = $this->getUserBreakDuration([
            'user_id'    => $userId,
            'shift_date' => $shiftDate
        ]);

        $totalAuxBreaks = $this->getUserAuxBreakDuration($userId, $shiftDate);

        $totalBreaks = duration_to_seconds($totalBreaks ? $totalBreaks->duration : '00:00:00') + duration_to_seconds($totalAuxBreaks->first() ? $totalAuxBreaks[0]->duration : '00:00:00');

        $breakClass = 'navy';
        if ($totalBreaks > duration_to_seconds('01:45:00')) {
            $breakClass = 'red';
        } else if ($totalBreaks < duration_to_seconds('01:10:00')) {
            $breakClass = 'lazur';
        }

        $target   = $this->getUserTarget($userId)->count();
        $Services = $this->getUserCompletedByServiceLine($lobId, $userId, $shiftDate);
        $cases    = $this->getUserCases([
            'lob_id'     => $lobId,
            'user_id'    => $userId,
            'shift_date' => $shiftDate
        ]);

        if ($totalCompletedCase > 0) {
            foreach ($cases as $case) {
                if (($case->status_id == 4 && in_array($case->status_7, [
                            "Case Processing",
                            "Peer Review",
                            "Second Pass"
                        ]) && $case->hold_reason_7 == null) || ($case->status_id == 4 && in_array($case->status_7, [
                            "Case Processing",
                            "Peer Review",
                            "Second Pass"
                        ]) && in_array($case->hold_reason_7, [
                            'Claim for query/clarification to onshore QA',
                            'Wrong format ICD 9/ICD 10',
                            'DRG mismatch'
                        ]))) {
                    $totalProdCount++;
                    $totalProdDuration += duration_to_seconds($case->duration);
                } else {
                    $totalNonProdCount++;
                    $totalNonProdDuration += duration_to_seconds($case->duration);
                }
            }

            $totalCases = $totalCompletedCase;
            $ahtSec     = ($totalProdDuration > 0 ? $totalProdDuration / $totalCases : 0);
            $cph        = ($totalProdDuration > 0 ? number_format($totalCompletedCase / $totalProdDuration, 2, '.', ',') : 0);
        }

        return [
            'total_completed_case'    => $totalCompletedCase,
            'total_hold_case'         => $totalHoldCase,
            'aht'                     => gmdate('H:i:s', $ahtSec),
            'cph'                     => $cph,
            'target'                  => $target,
            'breaks'                  => gmdate('H:i:s', $totalBreaks),
            'break_class'             => $breakClass,
            'service_line_stats'      => $Services,
            'total_prod_duration'     => $totalProdCount,
            'total_non_prod_duration' => $totalNonProdCount
        ];
    }

    public function getUserCaseTable ($lobId, $userId, $shiftDate)
    {
        return $this->db->table(self::TABLE_NAME . $lobId)
            ->where('user_id', $userId)
            ->where(function ($query) use ($shiftDate) {
                $query->where('shift_date', $shiftDate)
                    ->whereIn('status_id', [
                        StatusService::COMPLETED_STATUS_ID,
                        StatusService::EDIT_COMPLETED_STATUS_ID,
                        StatusService::ESCALATED_STATUS_ID
                    ])
                    ->orWhere('status_id', StatusService::HOLD_STATUS_ID);
            })
            ->orderBy('id', 'desc')
            ->orderBy('status_id', 'desc')
            ->get();
    }

    public function getByIcn ($lobId, $icn)
    {
        $user = session('user_from_account');

        $params = [
            'modelName' => 'Case' . $lobId,
            'filter'    => [
                'icn_7' => $icn
            ]
        ];

        return $this->databaseService->fetch($params);
    }

    public function getAvgCphPerTeam ($lobId, $teamId)
    {
        $totalProdCases = 0;
        $totalProdHours = 0;
        $cph            = 0;
        $caseTable      = self::TABLE_NAME . $lobId;

        if ($teamId == 'all') {
            $users = User::get();
        } else {
            $users = Team::find($teamId)->users;
        }

        foreach ($users as $user) {
            $totalProdCases += $user->cases->where('status_7', 'Case Processing')->where('shift_date', $user->shift_date)->count();

            foreach ($user->cases->where('status_7', 'Case Processing')->where('shift_date', $user->shift_date)->all() as $case) {
                $totalProdHours += duration_to_seconds($case->duration) / 3600;
            }

            if ($totalProdHours > 0) {
                $cph += $totalProdCases / $totalProdHours;
            }

            $totalProdCases = $totalProdHours = 0;
        }

        return round($cph, 2);
    }

    public function getAvgCphTargetPerTeam ($lobId, $teamId)
    {
        $avgCphTarget = 0;

        if ($teamId == 'all') {
            $users = User::get();
        } else {
            $users = Team::find($teamId)->users;
        }

        return $users->sum('ramp');
    }

    public function getTotalNonProd ($lobId, $teamId)
    {
        $totalHandledCases = 0;

        if ($teamId == 'all') {
            $users = User::get();
        } else {
            $users = Team::find($teamId)->users;
        }

        $totalHandledCases = Case7::whereIn('user_id', $users->pluck('main_user_id'))->whereIn('shift_date', $users->pluck('shift_date'))->where('status_7', 'Peer Review')->whereNotIn('status_id', [
            1,
            3,
            7
        ])->selectRaw('SEC_TO_TIME(sum(TIME_TO_SEC(duration))) AS secs')->first()->secs;

        return $totalHandledCases != null ? $totalHandledCases : '00:00:00';
    }

    public function getTotalProdCases ($lobId, $teamId)
    {
        $totalProdCases = 0;

        if ($teamId == 'all') {
            $users = User::get();
        } else {
            $users = Team::find($teamId)->users;
        }

        foreach ($users as $user) {
            $totalProdCases += $user->cases->where('status_7', 'Case Processing')->where('shift_date', $user->shift_date)->where('status_id', 4)->count();
        }

        return $totalProdCases;
    }

    public function getServiceLines ($lobId, $teamId)
    {
        return Case7::
        join('user', 'case' . $lobId . '.user_id', 'user.main_user_id')
            ->where('user.team', $teamId)
            ->where('case' . $lobId . '.shift_date', session('user_from_account')->shift_date)
            ->whereIn('case' . $lobId . '.status_id', [
                StatusService::COMPLETED_STATUS_ID,
                StatusService::EDIT_COMPLETED_STATUS_ID
            ])
            ->groupBy('case' . $lobId . '.service_line_' . $lobId)
            ->selectRaw('case' . $lobId . '.service_line_' . $lobId . ' as value ,count(case' . $lobId . '.id) as count')
            ->get();
    }

    public function getUserCases (array $post)
    {
        $params = [
            'modelName' => 'Case' . $post['lob_id'],
            'filter'    => [
                'user_id'    => $post['user_id'],
                'shift_date' => $post['shift_date'],
                'status_id'  => [
                    'not_in' => [$this->statusService::COMPLETED_STATUS_ID]
                ]
            ]
        ];

        return $this->databaseService->fetchAll($params);
    }

    public function getServiceLineDashboard ($lobId, $teamId, $shift_date)
    {
        $reports = $this->db->select("SELECT DISTINCT(service_line_7) as service_line,
                    (SELECT COUNT(service_line_7)
                    from case7 as ca
                    LEFT JOIN user as us ON ca.user_id=us.main_user_id
                    WHERE ca.service_line_7 = ser.service_line_7
                    and ca.shift_date = '" . $shift_date . "'
                    and ca.status_id in ('4', '6')
                    AND us.team = '" . $teamId . "')
                    as `service_count`
                    from case7 as ser
                    LEFT JOIN user as us2 ON ser.user_id=us2.id
                    WHERE ser.shift_date = '" . $shift_date . "'
                    and ser.status_id in ('4', '6')
                    AND us2.team = '" . $teamId . "';");

        return $reports;
    }

    public function getPerServiceLine ($lobId, $teamId, $shift_date)
    {
        if ($teamId == 'all') {
            $team = " ";
        } else {
            $team = " AND us.team = '" . $teamId . "' ";
        }

        $results = $this->db->select("SELECT count(ca.id) as prod, ca.service_line_7, ca.status_7
                            FROM case7 as ca
                            LEFT JOIN user as us ON ca.user_id=us.main_user_id
                            WHERE ca.shift_date = '" . $shift_date . "'
                            and ca.status_id in ('4', '6')
                            AND ca.isContinued = '0'
                            AND ca.status_7 != 'Audit Review'
                            " . $team . "
                            GROUP BY ca.service_line_7, ca.status_7;");

        return $results;
    }

    public function getTeamSchedule ($teamId)
    {
        $teamSchedule = $this->db->select("SELECT t.shift_date FROM team as t where t.ID = '" . $teamId . "';");

        return $teamSchedule;
    }

    public function getUserTeamSchedule ($userId)
    {
        $userSchedule = $this->db->select("SELECT t.shift_date FROM team as t
                                            LEFT JOIN user as us ON us.team = t.ID
                                            where us.main_user_id = '" . $userId . "';");

        return $userSchedule;
    }

    public function getShiftSchedule ()
    {
        $schedule = $this->db->select("SELECT shift_date FROM shift_schedule
                                        ORDER BY shift_date DESC
                                        limit 1");

        return $schedule;
    }

    public function getListStatus ($field_id)
    {
        $listStatus = $this->db->select("SELECT l.value FROM list_value as l where l.field_id = '" . $field_id . "' AND l.id != 159;");

        return $listStatus;
    }

    public function getListServiceLine ($field_id)
    {
        $listServiceLine = $this->db->select("SELECT l.value FROM list_value as l where l.field_id = '" . $field_id . "';");

        return $listServiceLine;
    }

    public function getTeamTPCSecondPass ($lobId, $teamId, $shift_date)
    {
        if ($teamId == 'all') {
            $team = " ";
        } else {
            $team = " AND us.team = '" . $teamId . "' ";
        }

        $results = $this->db->select("SELECT count(ca.id) AS prod_count
            FROM case7 as ca
            LEFT JOIN user as us ON ca.user_id=us.main_user_id
            WHERE ca.shift_date = '" . $shift_date . "'
            " . $team . "
            and ca.status_id in ('4', '6')
            AND ca.isContinued = '0'
            AND ca.status_7 = 'Second Pass';");

        return $results;
    }

    public function getTeamTPCOthers ($lobId, $teamId, $shift_date)
    {
        if ($teamId == 'all') {
            $team = " ";
        } else {
            $team = " AND us.team = '" . $teamId . "' ";
        }

        $results = $this->db->select("SELECT count(ca.id) AS prod_count
            FROM case7 as ca
            LEFT JOIN user as us ON ca.user_id=us.main_user_id
            WHERE ca.shift_date = '" . $shift_date . "'
            " . $team . "
            and ca.status_id in ('4', '6')
            AND ca.isContinued = '0'
            AND ca.status_7 in ('Peer Review', 'Case Processing');");

        return $results;
    }

    public function getTeamRamp ($teamId)
    {
        if ($teamId == 'all') {
            $users  = User::get();
            $result = $users->sum('ramp');
        } else {
            $result = Team::find($teamId)->users->sum('ramp');
        }

        return $result;
    }

    public function getTeamNonProd ($teamId, $shift_date)
    {
        if ($teamId == 'all') {
            $team = " ";
        } else {
            $team = " AND us.team = '" . $teamId . "' ";
        }

        $results = $this->db->select("SELECT count(*) as nonProd
            FROM case7 as ca
            LEFT JOIN user as us ON ca.user_id=us.main_user_id
            WHERE ca.shift_date = '" . $shift_date . "'
            " . $team . "
            AND ca.status_id in ('4', '6')
            AND (ca.isContinued = '1'
            OR ca.status_7 = 'Audit Review');");

        return $results;
    }

    public function getTeamProdDuration ($teamId, $shift_date)
    {
        if ($teamId == 'all') {
            $team = " ";
        } else {
            $team = " AND us.team = '" . $teamId . "' ";
        }

        $results = $this->db->select("SELECT SUM(TIME_TO_SEC(ca.duration))/3600 AS duration
            FROM case7 as ca
            LEFT JOIN user as us ON ca.user_id=us.main_user_id
            WHERE ca.shift_date = '" . $shift_date . "'
            " . $team . "
            and ca.status_id in ('4', '6')
            AND ca.isContinued = '0';");

        return $results;
    }

    public function getByConditions ($lobId, $userId, $conditions = [], $fetchSingleRow = false)
    {
        $query = $this->db->table(self::TABLE_NAME . $lobId);

        foreach ($conditions as $condition) {
            $query->where($condition['column'], $condition['comparison'], $condition['value'])
                ->where('status_id', 4)
                ->where('user_id', $userId);
        }

        if ($fetchSingleRow) {
            return $query->first();
        }

        return $query->get();
    }

    public function getUserCreditStats (array $post)
    {
        if ($post['service_line'] == "CAT") {
            $select
                = 'COALESCE(list_value.percentage * SUM(IF(drg_type_7 = "CV-APR", 1, 0)), 0.00) AS `CV_APR`,
                        COALESCE(list_value.percentage * SUM(IF(drg_type_7 = "CV-MS", 1, 0)), 0.00) AS `CV_MS`,
                        COALESCE(list_value.percentage * SUM(IF(status_7 = "Second Pass", 1, 0)), 0) AS `Second_Pass`';
        } else {
            $select
                = 'COALESCE(list_value.percentage * SUM(IF(drg_type_7 = "CV-APR", 1, 0)), 0) AS `CV_APR`,
                        COALESCE(list_value.percentage * SUM(IF(drg_type_7 = "CV-MS", 1, 0)), 0.00) AS `CV_MS`,
                        COALESCE(list_value.percentage * SUM(IF(drg_type_7 = "Coding-APR", 1, 0)), 0.00) AS `Coding_APR`,
                        COALESCE(list_value.percentage * SUM(IF(drg_type_7 = "Coding-MS", 1, 0)), 0.00) AS `Coding_MS`,
                        COALESCE(list_value.percentage * SUM(IF(status_7 = "Second Pass", 1, 0)), 0.00) AS `Second_Pass`';
        }

        $params = [
            'modelName' => 'ListValue',
            'filter'    => [
                'case7.user_id'        => $post['user_id'],
                'case7.decision_7'     => $post['decision'],
                'case7.service_line_7' => $post['service_line']
            ],
            'leftJoin'  => [
                'case7' => [
                    'leftField'  => 'list_value.value',
                    'operator'   => '=',
                    'rightField' => 'case7.decision_7'
                ]
            ],
            'select'    => $select
        ];

        return $this->databaseService->fetch($params);
    }

    public function dashboardQueryBuilder ($lobId, $teamId)
    {
        $shiftDate = $this->db->table('user')->where('user.id', session('user_from_account')->main_user_id)->first();

        if ($teamId == 'all') {
            $teamId = "(SELECT ID FROM team)";
        }

        // $result = User::
        //             selectRaw('CONCAT(user.lastname, ", ", user.firstname) AS name,
        //             team.description AS team,
        //             user.shift_date AS shift_date,
        //             COALESCE(CONCAT(shift_schedule.start_shift, " - ", shift_schedule.end_shift), "00:00:00 - 00:00:00") AS shift_schedule,
        //             SUM(IF(case'. $lobId .'.status_id IN (4,8), 1, 0)) AS total_completed_cases,
        //             SUM(IF(case'. $lobId .'.status_id = 6, 1, 0)) AS total_pending_cases,
        //             COALESCE((SELECT COUNT(*) FROM case'. $lobId .' WHERE account_status_7 = "New" AND user_id = user.main_user_id AND shift_date = "'. $shiftDate->shift_date .'") / (SELECT SUM(MINUTE(duration)) FROM case'. $lobId .' WHERE account_status_7 = "New" AND user_id = user.main_user_id AND shift_date = "'. $shiftDate->shift_date .'"), 0) AS cph,
        //             user.ramp AS cph_target,
        //             (SELECT count(*) FROM case'. $lobId .' WHERE user_id = user.main_user_id AND shift_date = "'. $shiftDate->shift_date .'" AND status_id NOT IN (1,3,7)) AS total_handled_cases,
        //             (SELECT count(*) FROM case'. $lobId .' WHERE user_id = user.main_user_id AND account_status_7 = "New" AND shift_date = "'. $shiftDate->shift_date .'" AND status_id = 4) AS total_prod_cases,
        //             SEC_TO_TIME(SUM(IF(case'. $lobId .'.status_id IN (4,8), TIME_TO_SEC(case'. $lobId .'.duration), 0))) AS total_duration,
        //             COALESCE(SEC_TO_TIME(CEILING(SUM(IF(case'. $lobId .'.status_id IN (4,8), TIME_TO_SEC(case'. $lobId .'.duration), 0)) / SUM(IF(case'. $lobId .'.status_id IN (4,8), 1, 0)))), "00:00:00") AS aht, user.main_user_id AS main_user_id, user.status AS status')
        //             ->leftJoin('case'.$lobId, function($join) use ($shiftDate, $lobId){
        //                 $case = 'case'.$lobId;
        //                 $join->on('user.main_user_id', '=', $case.'.user_id');
        //                 $join->on($case.'.shift_date', '=', \DB::raw('"'. $shiftDate->shift_date . '"'));
        //             })
        //             ->join('team', 'user.team', 'team.id')
        //             ->leftJoin('shift_schedule', function($join) {
        //                 $join->on('user.main_user_id', '=', 'shift_schedule.user_id');
        //                 $join->on('shift_schedule.shift_date', '=', 'user.shift_date');
        //             })
        //             ->whereIn('user.team', $teamId)
        //             ->whereIn('user.role', [5,11])
        //             ->where('user.active', '1')
        //             ->groupBy('user.main_user_id')
        //             ->orderBy('user.status', 'DESC');

        $result = $this->db->select('Select
            CONCAT(user.lastname, ", ", user.firstname) AS name,
            team.description AS team,
            user.shift_date AS shift_date,
            COALESCE(CONCAT(shift_schedule.start_shift, " - ", shift_schedule.end_shift), "00:00:00 - 00:00:00") AS shift_schedule,
            SUM(IF(case' . $lobId . '.status_id IN (4), 1, 0)) AS total_completed_cases,
            SUM(IF(case' . $lobId . '.status_id = 6, 1, 0)) AS total_pending_cases,
            COALESCE((SELECT COUNT(*) FROM case' . $lobId . ' WHERE account_status_7 = "New" AND user_id = user.main_user_id AND shift_date = "' . $shiftDate->shift_date . '") / (SELECT SUM(MINUTE(duration)) FROM case' . $lobId . ' WHERE account_status_7 = "New" AND user_id = user.main_user_id AND shift_date = "' . $shiftDate->shift_date . '"), 0) AS cph,
            user.ramp AS cph_target,
            (SELECT count(*) FROM case' . $lobId . ' WHERE user_id = user.main_user_id AND shift_date = "' . $shiftDate->shift_date . '" AND status_id NOT IN (1,3,7)) AS total_handled_cases,
            ((SELECT count(*) FROM case' . $lobId . ' WHERE user_id = user.main_user_id AND status_7 in ("Peer Review", "Case Processing") AND isContinued = 0 AND shift_date = "' . $shiftDate->shift_date . '" AND status_id IN (4,6)) + ((SELECT count(*) FROM case' . $lobId . ' WHERE user_id = user.main_user_id AND status_7 in ("Second Pass") AND isContinued = 0 AND shift_date = "' . $shiftDate->shift_date . '" AND status_id IN (4,6)) * .5)) AS total_prod_cases,
            SEC_TO_TIME(SUM(IF(case' . $lobId . '.status_id IN (4,6), TIME_TO_SEC(case' . $lobId . '.duration), 0))) AS total_duration,
            COALESCE(SEC_TO_TIME(CEILING(SUM(IF(case' . $lobId . '.status_id IN (4,6), TIME_TO_SEC(case' . $lobId . '.duration), 0)) / SUM(IF(case' . $lobId . '.status_id IN (4,6), 1, 0)))), "00:00:00") AS aht, user.main_user_id AS main_user_id, user.status AS status
            FROM user
            left join case' . $lobId . '  
            on user.main_user_id = case' . $lobId . '.user_id
            AND case' . $lobId . '.shift_date = "' . $shiftDate->shift_date . '"
            INNER JOIN team
            ON user.team = team.id
            LEFT JOIN shift_schedule
            ON user.main_user_id = shift_schedule.user_id
            AND shift_schedule.shift_date = user.shift_date
            WHERE user.team IN (' . $teamId . ')
            AND user.role IN (5,6,13)
            AND user.active = 2
            GROUP BY user.main_user_id
            ORDER BY user.status DESC');

        return $result;
    }
}