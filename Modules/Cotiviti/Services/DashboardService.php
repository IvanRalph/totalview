<?php

namespace Modules\Cotiviti\Services;

use Modules\Main\Services\BaseDashboardService;
use Modules\Main\Services\DatabaseService;

class DashboardService extends BaseDashboardService
{
    public function __construct (DatabaseService $databaseService)
    {
        parent::__construct($databaseService);
    }
}