<?php
/**
 * Created by PhpStorm.
 * User: OMLanuza
 * Date: 1/17/2018
 * Time: 4:11 PM
 */

namespace Modules\Cotiviti\Services;

use Illuminate\Support\Facades\DB;
use Modules\Main\Services\BaseReportService;
use Modules\Main\Services\DatabaseService;

class ReportService extends BaseReportService
{
    public function __construct (DatabaseService $databaseService)
    {
        parent::__construct($databaseService);

        $this->db = DB::connection('tv_cotiviti');
    }

    public function getHourly ($lobId, $dateFrom, $dateTo, $timezone)
    {
        if ($timezone == 'CST') {
            $hour = 'concat(hour(date_add(case_activity_log.created_at,INTERVAL 13 HOUR)), ":00") as hour';
        } else {
            $hour = 'concat(hour(case_activity_log.created_at), ":00") as hour';
        }

        return $this->db->table('case_activity_log')
            ->select(
                $this->db->raw('concat(user.lastname, \', \', user.firstname) as Name'),
                $this->db->raw($hour),
                $this->db->raw('count(case_activity_log.id) as count')
            )
            ->leftJoin('user', 'user.id', '=', 'case_activity_log.user_id')
            ->where('lob_id', $lobId)
            ->whereNotIn('status_id', StatusService::ONGOING_STATUS_ID)
            ->whereBetween('created_at', [
                $dateFrom,
                $dateTo
            ])
            ->groupBy('user.id', $this->db->raw('hour(case_activity_log.created_at)'))
            ->get()
            ->toArray();
    }

    public function getDaily ($start, $end)
    {
        $result = $this->db->table('daily_prod_reports')
            ->select('dpr_month as Month', 'dpr_week as Week', 'shift_date as Date', 'name as Coder', 'dpr_provider as Provider', 'icn', 'concept_desc as Concept_Description',
                'concept_id as Concept_ID', 'dpr_status as Status', 'hold_reason as Pend_Reason', 'drg as DRG', 'adj_drg as Adj_DRG', 'decision as Recovery',
                'account_status as Days_to_Review', 'service_line as Queue_Age', 'comments as Comments', 'start_time as Start_Time', 'end_time as Stop_Time',
                'duration as Total_Seconds', 'dpr_abstracted as Abstracted', 'dpr_pended as Pended', 'dpr_chart_count as Chart_Count', 'dpr_internal_count as Internal_Count'
            )
            ->where('shift_date', '>=', $start)
            ->where('shift_date', '<=', $end)
            ->get();

        return $result;
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function getRaw (array $post)
    {
        $params = [
            'modelName' => 'Raw',
            'leftJoin'  => [
                'user'   => [
                    'leftField'  => 'user.main_user_id',
                    'operator'   => '=',
                    'rightField' => 'raw.userid'
                ],
                'team'   => [
                    'leftField'  => 'team.ID',
                    'operator'   => '=',
                    'rightField' => 'raw.teamid'
                ]
            ],
            'between'   => [
                'raw.shift_date' => [
                    $post['start'],
                    $post['end']
                ]
            ],
            'select'    => isset($post['rows']) ? $post['rows'] : '*'
        ];

        return $this->databaseService->fetchAll($params);
    }
}