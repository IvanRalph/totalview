<?php

namespace Modules\Cotiviti\Services;

class BaseService
{
    protected $db;

    public function __construct ()
    {
        $this->db = \DB::connection(config('cotiviti.db_connection'));
    }
}