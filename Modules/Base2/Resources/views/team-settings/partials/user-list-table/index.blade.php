<table id="team-management-users-table"
       class="table table-striped table-bordered table-hover"
       cellspacing="0"
       width="100%">
    <thead>
    <tr role="row">
        <th class="text-center">
            <div class="checkbox checkbox-primary">
                <button id="chk-all" class="btn btn-white btn-xs" style="margin-right: 12px;"><i
                            class="fa fa-check-square-o"></i></button>
            </div>
        </th>
        <th>Name</th>
        <th>LOB</th>
        <th>Team</th>
        <th>Ramp</th>
        <th>Shift Date</th>
        <th>Shift Start</th>
        <th>Shift End</th>
        <th class="text-center" width="5%">On Leave</th>
        <th class="text-center" width="5%">Status</th>
        <th class="text-center" width="5%">Action</th>
    </tr>
    </thead>
</table>

@include('base2::team-settings.partials.user-list-table.user-setting-modal', $data)
@include('base2::team-settings.partials.user-list-table.bulk-user-setting-modal', $data)


@section('module-scripts')
    {!! Html::script('js/module/base2/constant.js') !!}
    {!! Html::script('js/module/base2/team-settings.js') !!}
@stop

