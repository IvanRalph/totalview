<div class="col-lg-12">
    <div class="ibox float-e-margins activity-panel">
        <div class="ibox-title">
            <h5>Nurse Activity - <span class="nurseName">(Nurse Name)</span></h5>
        </div>
        <div class="ibox-content" style="max-height: 700px; overflow-y: auto;">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>

            <div id="vertical-timeline" class="vertical-container dark-timeline no-margins">
                <div class="vertical-timeline-block">
                    <div class="vertical-timeline-icon gray-bg">
                        <i class="fa fa-times"></i>
                    </div>

                    <div class="vertical-timeline-content">
                        <h2 id="activityType"></h2>
                        <p id="activityDesc">No activities available.</p>
                        <span id="activityDate" class="vertical-date"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>