<div class="col-lg-3">
    <div class="ibox float-e-margins metric-panel">
        <div class="ibox-title">
            <span class="label label-success pull-right">Today</span>
            <h5>Available Nurses</h5>
        </div>
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
            <h1 class="no-margins"><span class="totalAvailableNurse">0</span>/<span class="totalNurse">0</span></h1>
            <div class="stat-percent font-bold text-success">
                <span class="totalAvailableNursePercentage"></span>% <i lass="fa fa-bolt"></i>
            </div>
            <small>Nurses</small>
        </div>
    </div>
</div>

<div class="col-lg-3">
    <div class="ibox float-e-margins metric-panel">
        <div class="ibox-title">
            <span class="label label-success pull-right">Today</span>
            <h5>Completed Cases</h5>
        </div>
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
            <h1 class="no-margins"><span class="totalCompletedCase">0</span></h1>
            <div class="stat-percent font-bold text-info"></div>
            <small>Cases</small>
        </div>
    </div>
</div>

<div class="col-lg-3">
    <div class="ibox float-e-margins metric-panel">
        <div class="ibox-title">
            <span class="label label-success pull-right">Today</span>
            <h5>On Hold Cases</h5>
        </div>
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
            <h1 class="no-margins"><span class="totalPendingCase">0</span></h1>
            <div class="stat-percent font-bold text-info"></div>
            <small>Cases</small>
        </div>
    </div>
</div>

<div class="col-lg-3">
    <div class="ibox float-e-margins metric-panel">
        <div class="ibox-title">
            <span class="label label-success pull-right">Today</span>
            <h5>Escalated Cases</h5>
        </div>
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
            <h1 class="no-margins"><span class="totalEscalatedCase">0</span></h1>
            <div class="stat-percent font-bold text-info"></div>
            <small>Cases</small>
        </div>
    </div>
</div>


