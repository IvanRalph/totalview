<!DOCTYPE html>
<html lang=en>
<head>

    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png/ico" href="/images/favicon.ico">

    <title>TotalView | Full Size Dashboard </title>

    {!! Html::style('css/lib/all.css') !!}
    {!! Html::style('css/lib/select2.min.css') !!}
    {!! Html::style('css/lib/sweetalert.min.css') !!}

    <style>
        [v-cloak] > * {
            display: none;
        }

        [v-cloak]::before {
            content: "loading...";
        }

        @media (min-width: 768px) {
            .center-div {
                position: relative;
                top: 50%;
                /*transform: translateY(3%);*/
            }

            .ibox-content h1 {
                font-weight: bold;
                font-size: 300%;
            }

            .ibox-content small {
                font-size: 200%;
            }

            .ibox-title h3 {
                font-size: 110%;
            }
        }

        @media (min-width: 992px) {
            .ibox-content h1 {
                font-weight: bold;
                font-size: 800%;
            }

            .ibox-content small {
                font-size: 200%;
            }

            .ibox-title h3 {
                font-size: 150%;
            }
        }

        @media (min-width: 1200px) {
            .ibox-content h1 {
                font-weight: bold;
                font-size: 1000%;
            }

            .ibox-content small {
                font-size: 200%;
            }

            .ibox-title h3 {
                font-size: 200%;
            }
        }

        .total-view-logo {
            width: 15%;
            height: auto;
            margin: 3% 0;
        }

    </style>
</head>

<body class="pace-done">

<div class="wrapper ">
    <div class="wrapper wrapper-content animated fadeInRight ">
        <div id="app" class="row text-center ">
            <input type="hidden" name="" id="slct-lob" value="{{ request()->get('lob') }}">
            <input type="hidden" name="" id="slct-team" value="{{ request()->get('team') }}">

            <div class="center-div">
                <img alt="image" class="img total-view-logo" src="/images/totalview_logo.png"/>
                <div class="row ">
                    <div class="col-sm-3">
                        <div class="ibox float-e-margins metric-panel">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">Today</span>
                                <h3>Available Nurses</h3>
                            </div>
                            <div class="ibox-content">
                                <br><br><br><br><br><br><br><br><br><br><br><br>
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <h1 class="no-margins"><span class="totalAvailableNurse">0</span>/<span
                                            class="totalNurse">0</span></h1>
                                <div class="stat-percent font-bold text-success hidden">
                                    <span class="totalAvailableNursePercentage"></span>% <i lass="fa fa-bolt"></i>
                                </div>
                                <small>Nurses</small>
                                <br><br><br><br><br><br><br><br><br><br><br><br>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="ibox float-e-margins metric-panel">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">Today</span>
                                <h3>Completed Cases</h3>
                            </div>
                            <div class="ibox-content">
                                <br><br><br><br><br><br><br><br><br><br><br><br>
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <h1 class="no-margins"><span class="totalCompletedCase">0</span></h1>
                                <div class="stat-percent font-bold text-info"></div>
                                <small>Cases</small>
                                <br><br><br><br><br><br><br><br><br><br><br><br>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="ibox float-e-margins metric-panel">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">Today</span>
                                <h3>On Hold Cases</h3>
                            </div>
                            <div class="ibox-content">
                                <br><br><br><br><br><br><br><br><br><br><br><br>
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <h1 class="no-margins"><span class="totalPendingCase">0</span></h1>
                                <div class="stat-percent font-bold text-info"></div>
                                <small>Cases</small>
                                <br><br><br><br><br><br><br><br><br><br><br><br>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="ibox float-e-margins metric-panel">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">Today</span>
                                <h3>Escalated Cases</h3>
                            </div>
                            <div class="ibox-content">
                                <br><br><br><br><br><br><br><br><br><br><br><br>
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <h1 class="no-margins"><span class="totalEscalatedCase">0</span></h1>
                                <div class="stat-percent font-bold text-info"></div>
                                <small>Cases</small>
                                <br><br><br><br><br><br><br><br><br><br><br><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>

<!-- Use this compiled js for official Implementation -->

{!! Html::script('js/lib/all.js') !!}
{!! Html::script('js/module/Base2/constant.js') !!}
{!! Html::script('js/module/Base2/dashboard.js') !!}

{!! Html::script('js/local-storage-handler.js') !!}
{!! Html::script('js/spinners.js') !!}
{!! Html::script('js/side-navigation.js') !!}
{!! Html::script('js/http-request.js') !!}
{!! Html::script('js/routes.js') !!}
{!! Html::script('js/response-handler.js') !!}
{!! Html::script('js/schema-builder.js') !!}
{!! Html::script('js/commons.js') !!}


<script src="https://cdn.jsdelivr.net/npm/vue"></script>
<!-- <script src="https://unpkg.com/vue"></script> -->
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>


    // var casesStatUri = CONSTANT.accountApiName + '/tracker-stats/ 6';
    // var vm = new Vue({
    // 	el : '#app',
    // 	data : {

    // 		Overall : 0,
    // 		RBM : 0,
    // 		MSK : 0,

    // 	},

    // 	methods : {
    // 		caseCount : function () {
    // 			var vm = this

    // 			axios.get(casesStatUri)
    // 			.then(function (response){
    // 				vm.Overall = response.data.overall
    // 				vm.RBM = response.data.rbm
    // 				vm.MSK = response.data.msk

    // 			})
    // 			.catch (function (error){
    // 				console.log(error)
    // 				vm.Overall = 0
    // 				vm.RBM = 0
    // 				vm.MSK = 0

    // 			})
    // 		}

    // 	},

    // })

    // vm.caseCount();
    // setInterval(function () {
    // 	vm.caseCount();
    // }, 60000);

</script>
