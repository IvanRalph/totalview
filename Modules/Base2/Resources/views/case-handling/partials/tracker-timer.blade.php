<div class="ibox float-e-margins">
    <div class="ibox-title text-center">
        <h5>Tracker Timer</h5>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-12">
                <div id="caseTrackerTimer" data-timer="0" style="width: 85%;" class="col-lg-offset-1"></div>
            </div>
        </div>
    </div>
</div>