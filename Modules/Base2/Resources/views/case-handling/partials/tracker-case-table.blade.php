<div id="case-table" class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Cases Table</h5>
    </div>

    <div class="ibox-content">
        <div class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
        <div class="table-responsive">
            <table id="processed-case-table" class="table table-bordered dataTable">
                <thead>
                <tr>

                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>