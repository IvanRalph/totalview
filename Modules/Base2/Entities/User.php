<?php

namespace Modules\Base2\Entities;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable
        = [
            'shift_date',
            'status',
            'shift_start',
            'shift_end'
        ];

    protected $connection = 'tv_base2';

    protected $table = 'user';

    public $timestamps = false;

    protected $primaryKey = 'main_user_id';

    public function cases ()
    {
        return $this->hasMany('Modules\Base2\Entities\Case99', 'user_id');
    }

    public function schedules ()
    {
        return $this->hasMany('Modules\Base2\Entities\Schedule', 'user_id');
    }
}
