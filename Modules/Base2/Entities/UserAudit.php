<?php

namespace Modules\Base2\Entities;

use Illuminate\Database\Eloquent\Model;

class UserAudit extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_base2';

    protected $table = 'user_audit_trail';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
