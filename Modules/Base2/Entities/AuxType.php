<?php

namespace Modules\Base2\Entities;

use Illuminate\Database\Eloquent\Model;

class AuxType extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_base2';

    protected $table = 'aux_type';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
