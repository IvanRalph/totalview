<?php

namespace Modules\Base2\Entities;

use Illuminate\Database\Eloquent\Model;

class ListValue extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_base2';

    protected $table = 'list_value';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
