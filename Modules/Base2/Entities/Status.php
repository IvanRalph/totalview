<?php

namespace Modules\Base2\Entities;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_base2';

    protected $table = 'status';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
