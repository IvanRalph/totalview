<?php

namespace Modules\Base2\Entities;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_base2';

    protected $table = 'role';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
