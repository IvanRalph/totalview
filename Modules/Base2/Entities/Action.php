<?php

namespace Modules\Base2\Entities;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_base2';

    protected $table = 'action';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
