<?php

namespace Modules\Base2\Entities;

use Illuminate\Database\Eloquent\Model;

class Lob extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_base2';

    protected $table = 'lob';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
