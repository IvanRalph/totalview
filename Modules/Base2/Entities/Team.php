<?php

namespace Modules\Base2\Entities;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = ['shift_date'];

    protected $connection = 'tv_base2';

    protected $table = 'team';

    public $timestamps = false;

    protected $primaryKey = 'ID';

    public function users ()
    {
        return $this->hasMany('Modules\Base2\Entities\User', 'team');
    }
}
