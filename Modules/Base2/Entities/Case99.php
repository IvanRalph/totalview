<?php

namespace Modules\Base2\Entities;

use Illuminate\Database\Eloquent\Model;

class Case99 extends Model
{
    protected $fillable
        = [
            'shift_date',
            'user_id',
            'created_at',
            'updated_at',
            'duration',
            'hosp_serv',
            'fin',
            'mrn',
            'admit_date',
            'discharge_date',
            'price',
            'observation',
            'hold_reason',
            'accountstat',
            'comment',
            'status_id'
        ];

    protected $connection = 'tv_base2';

    protected $table = 'Case99';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
