<?php

namespace Modules\Base2\Entities;

use Illuminate\Database\Eloquent\Model;

class Raw extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_base2';

    protected $table = 'raw';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
