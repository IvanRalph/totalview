<?php

namespace Modules\Base2\Entities;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_base2';

    protected $table = 'field';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
