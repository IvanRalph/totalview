<?php

namespace Modules\Base2\Entities;

use Illuminate\Database\Eloquent\Model;

class CaseActivity extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_base2';

    protected $table = 'case_activity_log';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
