<?php

namespace Modules\Base2\Entities;

use Illuminate\Database\Eloquent\Model;

class LobSetting extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_base2';

    protected $table = 'lob_setting';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
