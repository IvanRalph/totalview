<?php

namespace Modules\Base2\Entities;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable
        = [
            'case_id',
            'shift_date',
            'user_id',
            'team',
            'start_time',
            'end_time',
            'duration',
            'hosp_serv',
            'fin',
            'mrn',
            'admit_date',
            'discharge_date',
            'price',
            'observation',
            'hold_reason',
            'accountstat',
            'comment',
            'status_id'
        ];

    protected $connection = 'tv_base2';

    protected $table = 'raw';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
