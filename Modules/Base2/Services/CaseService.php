<?php

namespace Modules\Base2\Services;

use Illuminate\Support\Facades\DB;
use Modules\Main\Services\BaseCaseService;
use Modules\Main\Services\BaseStatusService;
use Modules\Main\Services\DatabaseService;

class CaseService extends BaseCaseService
{
    public function __construct (DatabaseService $databaseService, BaseStatusService $statusService,
                                 CaseActivityLogService $caseActivityLogService, FieldService $fieldService
    )
    {
        parent::__construct($databaseService);
        $this->db                     = DB::connection('tv_base2');
        $this->statusService          = $statusService;
        $this->caseActivityLogService = $caseActivityLogService;
        $this->fieldService           = $fieldService;
    }

    public function getUserTrackerStats ($lobId, $userId, $shiftDate)
    {
        $totalDuration = 0;
        $ahtSec        = 0;

        $post = [
            'lob_id'     => $lobId,
            'user_id'    => $userId,
            'shift_date' => $shiftDate,

        ];

        $cases              = $this->getUserCompleted($post);
        $totalCompletedCase = count($cases);

        $holdCases     = $this->getUserPending($post);
        $totalHoldCase = count($holdCases);

        $escalatedCases     = $this->getUserEscalated($post);
        $totalEscalatedCase = count($escalatedCases);

        if ($totalCompletedCase > 0) {
            foreach ($cases as $case) {
                $totalDuration += duration_to_seconds($case->duration);
            }

            $totalCases = $totalCompletedCase + $totalHoldCase + $totalEscalatedCase;

            //$ahtSec = $totalDuration / $totalCompletedCase;
            $ahtSec = $totalDuration / $totalCases;
        }

        return [
            'total_completed_case' => $totalCompletedCase,
            'total_hold_case'      => $totalHoldCase,
            'total_escalated_case' => $totalEscalatedCase,
            'aht'                  => gmdate('H:i:s', $ahtSec),
            'total_duration'       => gmdate('H:i:s', $totalDuration)
        ];
    }

    public function getUserEscalated (array $post)
    {
        $params = [
            'modelName' => 'Case' . $post['lob_id'],
            'filter'    => [
                'user_id'    => $post['user_id'],
                'shift_date' => $post['shift_date'],
                'status_id'  => [
                    BaseStatusService::ESCALATED_STATUS_ID,
                ]
            ]
        ];

        return $this->databaseService->fetchAll($params);
    }

    public function getUserCaseTable ($lobId, $userId, $shiftDate)
    {
        return $this->db->table(self::TABLE_NAME . $lobId)
            ->where('user_id', $userId)
            ->where(function ($query) use ($shiftDate) {
                $query->where('shift_date', $shiftDate)
                    ->whereIn('status_id', [
                        StatusService::COMPLETED_STATUS_ID,
                        StatusService::EDIT_COMPLETED_STATUS_ID,
                        StatusService::ESCALATED_STATUS_ID
                    ])
                    ->orWhere('status_id', StatusService::HOLD_STATUS_ID);
            })
            ->get();
    }

    public function getCurrentCase ($lobId, $userId)
    {
        $caseTable = self::TABLE_NAME . $lobId;
        $statusIds = StatusService::ONGOING_STATUS_ID;

        return $this->db->table($caseTable)
            ->where('user_id', $userId)
            ->whereIn('status_id', $statusIds)
            ->where(function ($query) {
                $query->where('created_at', '>', deduct_timestamp_hours(self::LAST_CASE_HOUR_INTERVAL))
                    ->orWhere('updated_at', '>', deduct_timestamp_hours(self::LAST_CASE_HOUR_INTERVAL));
            })
            ->orderBy('id', 'desc')
            ->first();
    }
}