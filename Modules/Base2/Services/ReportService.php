<?php
/**
 * Created by PhpStorm.
 * User: OMLanuza
 * Date: 1/17/2018
 * Time: 4:11 PM
 */

namespace Modules\Base2\Services;

use Modules\Main\Services\BaseReportService;
use Modules\Main\Services\DatabaseService;

class ReportService extends BaseReportService
{
    public function __construct (DatabaseService $databaseService)
    {
        parent::__construct($databaseService);
    }
}