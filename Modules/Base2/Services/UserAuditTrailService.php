<?php

namespace Modules\Base2\Services;

use Modules\Main\Services\BaseUserAuditTrailService;
use Modules\Main\Services\DatabaseService;

class UserAuditTrailService extends BaseUserAuditTrailService
{
    public function __construct (DatabaseService $databaseService)
    {
        parent::__construct($databaseService);
    }
}