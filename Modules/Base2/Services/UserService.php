<?php

namespace Modules\Base2\Services;

use Illuminate\Support\Facades\DB;
use Modules\Main\Services\BaseUserService;
use Modules\Main\Services\DatabaseService;

class UserService extends BaseUserService
{
    public function __construct (DatabaseService $databaseService)
    {
        parent::__construct($databaseService);
    }

    public function getNurseStatus ()
    {
        return DB::connection('tv_base2')->table('user_status')->get();
    }
}