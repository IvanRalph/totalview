<?php

namespace Modules\Base2\Services;

class BaseService
{
    protected $db;

    public function __construct ()
    {
        $this->db = \DB::connection(config('base2.db_connection'));
    }
}