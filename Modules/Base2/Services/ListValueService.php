<?php

namespace Modules\Base2\Services;

use Modules\Main\Services\BaseListValueService;
use Modules\Main\Services\DatabaseService;

class ListValueService extends BaseListValueService
{
    public function __construct (DatabaseService $databaseService)
    {
        parent::__construct($databaseService);
    }
}