<?php

namespace Modules\Base2\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Base2\Services\TeamSettingService;
use Modules\Base2\Services\LobService;
use Modules\Main\Http\Controllers\Api\BaseTeamSettingsApiController;

class TeamSettingsApiController extends BaseTeamSettingsApiController
{
    public function __construct (TeamSettingService $teamSettingService, LobService $lobService)
    {
        $this->teamSettingService = $teamSettingService;
        $this->lobService         = $lobService;
    }
}
