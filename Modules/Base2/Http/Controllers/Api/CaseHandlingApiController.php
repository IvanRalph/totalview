<?php

namespace Modules\Base2\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Base2\Services\CaseActivityLogService;
use Modules\Base2\Services\CaseService;
use Modules\Base2\Services\FieldService;
use Modules\Base2\Services\ListValueService;
use Modules\Base2\Services\LobService;
use Modules\Base2\Services\StatusService;
use Modules\Base2\Services\UserAuditTrailService;
use Modules\Main\Http\Controllers\Api\BaseCaseHandlingApiController;

class CaseHandlingApiController extends BaseCaseHandlingApiController
{
    public function __construct (CaseService $caseService, FieldService $fieldService, StatusService $statusService, CaseActivityLogService $caseActivityLogService, UserAuditTrailService $userAuditTrailService, LobService $lobService, ListValueService $listValueService)
    {
        $this->caseService            = $caseService;
        $this->fieldService           = $fieldService;
        $this->statusService          = $statusService;
        $this->caseActivityLogService = $caseActivityLogService;
        $this->userAuditTrailService  = $userAuditTrailService;
        $this->lobService             = $lobService;
        $this->listValueService       = $listValueService;
    }
}