<?php

namespace Modules\Base2\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Base2\Services\LobService;
use Modules\Base2\Services\CaseService;
use Modules\Base2\Services\FieldService;
use Modules\Base2\Services\ReportService;
use Modules\Main\Http\Controllers\Api\BaseReportsApiController;

class ReportsApiController extends BaseReportsApiController
{
    public function __construct (LobService $lobService, CaseService $caseService, FieldService $fieldService, ReportService $reportService)
    {
        $this->lobService    = $lobService;
        $this->caseService   = $caseService;
        $this->fieldService  = $fieldService;
        $this->reportService = $reportService;
    }

    public function getRawReports (Request $request)
    {
        $rawNumber = $request->get('rawNumber');
        $start     = $request->get('start');
        $end       = $request->get('end');
        $timezone  = $request->get('timezone');
        $data      = $this->reportService->getRaw([
            'start' => $start,
            'end'   => $end,
            'rows'  => "raw.shift_date as Shift_Date, concat(user.lastname, ', ', user.firstname) as Name, team.description as Team, raw.start_time as StartTime, raw.end_time as EndTime, raw.duration as Duration, raw.fin as FIN, raw.hosp_serv as Hospital_Service, raw.mrn as MRN, raw.admit_date as Admit_Date, raw.discharge_date as Discharge_Date, raw.price as Price, raw.observation as Observation , raw.hold_reason as Hold_Reason, raw.accountstat as Account_Status, raw.comment as Comments, status.name as Status",
        ]);

        return json_encode($data);
    }
}