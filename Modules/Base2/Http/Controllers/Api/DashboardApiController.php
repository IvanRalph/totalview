<?php

namespace Modules\Base2\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Base2\Services\CaseService;
use Modules\Base2\Services\StatusService;
use Modules\Base2\Services\TeamService;
use Modules\Base2\Services\UserAuditTrailService;
use Modules\Base2\Services\UserService;
use Modules\Main\Http\Controllers\Api\BaseDashboardApiController;

class DashboardApiController extends BaseDashboardApiController
{
    /**
     * BaseDashboardApiController constructor.
     * @param CaseService           $caseService
     * @param UserService           $userService
     * @param UserAuditTrailService $userAuditTrailService
     * @param TeamService           $teamService
     */
    public function __construct (
        CaseService $caseService,
        UserService $userService,
        UserAuditTrailService $userAuditTrailService,
        TeamService $teamService
    )
    {
        $this->caseService           = $caseService;
        $this->userService           = $userService;
        $this->userAuditTrailService = $userAuditTrailService;
        $this->teamService           = $teamService;
    }

    public function getMetrics (Request $request, $lobId, $teamId)
    {
        $mainUser        = $request->get('userData');
        $userFromAccount = $this->userService->getByMainUserId($mainUser['id']);

        $completedCount = $this->caseService->countByStatusId(
            [
                'lob_id'    => $lobId,
                'status_id' => [
                    StatusService::COMPLETED_STATUS_ID,
                    StatusService::EDIT_COMPLETED_STATUS_ID
                ],
                'team'      => $teamId
            ]
        );

        $pendingCount = $this->caseService->countByStatusId(
            [
                'lob_id'    => $lobId,
                'status_id' => [
                    StatusService::HOLD_STATUS_ID,
                ],
                'team'      => $teamId
            ]
        );

        $escalatedCount = $this->caseService->countByStatusId(
            [
                'lob_id'    => $lobId,
                'status_id' => [
                    StatusService::ESCALATED_STATUS_ID,
                ],
                'team'      => $teamId
            ]
        );

        $allUserCount = $this->userService->countAll([
            'lob_id'  => $lobId,
            'team_id' => $teamId
        ]);

        $availableUserCount = $this->userService->countAvailable([
            'lob_id'  => $lobId,
            'team_id' => $teamId
        ]);

        return response()->json([
            'completed_count'      => $completedCount,
            'escalated_count'      => $escalatedCount,
            'pending_count'        => $pendingCount,
            'all_user_count'       => $allUserCount,
            'available_user_count' => $availableUserCount
        ]);
    }
}
