<?php

namespace Modules\Base2\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Base2\Services\CaseService;
use Modules\Base2\Services\FieldService;
use Modules\Base2\Services\StatusService;
use Modules\Main\Http\Controllers\Api\BaseCaseSearchApiController;

class CaseSearchApiController extends BaseCaseSearchApiController
{
    /**
     * BaseCaseSearchApiController constructor.
     * @param CaseService   $caseService
     * @param FieldService  $fieldService
     * @param StatusService $statusService
     */
    public function __construct (CaseService $caseService, FieldService $fieldService, StatusService $statusService)
    {
        $this->caseService   = $caseService;
        $this->fieldService  = $fieldService;
        $this->statusService = $statusService;
    }
}