<?php

namespace Modules\Base2\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Base2\Services\AuxBreakService;
use Modules\Base2\Services\UserAuditTrailService;
use Modules\Base2\Services\UserService;
use Modules\Base2\Services\CaseService;
use Modules\Main\Http\Controllers\Api\BaseAuxBreakApiController;

class AuxBreakApiController extends BaseAuxBreakApiController
{
    public function __construct (AuxBreakService $auxBreakService, UserService $userService, UserAuditTrailService $userAuditTrailService, CaseService $caseService)
    {
        $this->auxBreakService       = $auxBreakService;
        $this->userService           = $userService;
        $this->userAuditTrailService = $userAuditTrailService;
        $this->caseService           = $caseService;
    }
}