<?php

namespace Modules\Base2\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Base2\Services\FieldService;
use Modules\Base2\Services\LobService;
use Modules\Main\Http\Controllers\Web\BaseCaseSearchWebController;

class CaseSearchWebController extends BaseCaseSearchWebController
{
    /**
     * CaseSearchWebController constructor.
     * @param LobService   $lobService
     * @param FieldService $fieldService
     */
    public function __construct (LobService $lobService, FieldService $fieldService)
    {
        $this->lobService   = $lobService;
        $this->fieldService = $fieldService;
    }
}