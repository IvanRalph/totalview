<?php

namespace Modules\Base2\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Base2\Services\TeamSettingService;
use Modules\Base2\Services\LobService;
use Modules\Base2\Services\RoleService;
use Modules\Base2\Services\UserService;
use Modules\Main\Http\Controllers\Web\BaseTeamSettingsWebController;

class TeamSettingsWebController extends BaseTeamSettingsWebController
{
    public function __construct (TeamSettingService $teamSettingService, LobService $lobService, RoleService $roleService, UserService $userService)
    {
        $this->teamSettingService = $teamSettingService;
        $this->lobService         = $lobService;
        $this->roleService        = $roleService;
        $this->userService        = $userService;
    }
}