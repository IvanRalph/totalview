<?php

namespace Modules\Base2\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Base2\Services\LobService;
use Modules\Main\Http\Controllers\Web\BaseReportsWebController;

class ReportsWebController extends BaseReportsWebController
{
    public function __construct (LobService $lobService)
    {
        $this->lobService = $lobService;
    }

    public function index ()
    {
        $data['getLobList'] = $this->lobService->getLobList();

        return view('base2::reports.index', $data);
    }
}