<?php

namespace Modules\Base2\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Base2\Services\CaseService;
use Modules\Base2\Services\FieldService;
use Modules\Base2\Services\ListValueService;
use Modules\Base2\Services\LobService;
use Modules\Base2\Services\UserAuditTrailService;
use Modules\Base2\Services\UserService;
use Modules\Main\Http\Controllers\Web\BaseCaseHandlingWebController;

class CaseHandlingWebController extends BaseCaseHandlingWebController
{
    /**
     * CaseHandlingWebController constructor.
     * @param FieldService          $fieldService
     * @param ListValueService      $listValueService
     * @param CaseService           $caseService
     * @param UserService           $userService
     * @param UserAuditTrailService $userAuditTrailService
     * @param LobService            $lobService
     */
    public function __construct (
        FieldService $fieldService,
        ListValueService $listValueService,
        CaseService $caseService,
        UserService $userService,
        UserAuditTrailService $userAuditTrailService,
        LobService $lobService
    )
    {
        $this->fieldService          = $fieldService;
        $this->listValueService      = $listValueService;
        $this->caseService           = $caseService;
        $this->userService           = $userService;
        $this->userAuditTrailService = $userAuditTrailService;
        $this->lobService            = $lobService;
    }
}