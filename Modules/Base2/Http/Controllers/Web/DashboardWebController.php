<?php

namespace Modules\Base2\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Base2\Services\DashboardService;
use Modules\Base2\Services\LobService;
use Modules\Base2\Services\UserService;
use Modules\Base2\Services\TeamService;
use Modules\Main\Http\Controllers\Web\BaseDashboardWebController;

class DashboardWebController extends BaseDashboardWebController
{
    public function __construct (DashboardService $dashboardService, LobService $lobService, UserService $userService, TeamService $teamService)
    {
        $this->dashboardService = $dashboardService;
        $this->lobService       = $lobService;
        $this->userService      = $userService;
        $this->teamService      = $teamService;
    }
}