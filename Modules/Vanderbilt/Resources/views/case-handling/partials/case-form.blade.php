<div id="case-form" class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Case Form</h5>
        <div class="ibox-tools">
            <button id="btn-aux-break" class="btn btn-primary"
                    href="/{{ session('selected_account')->route }}/aux-break?token={{ app('request')->get('token') }}">
                <i class="fa fa-clock-o"></i> Aux
            </button>

        </div>
    </div>
    <div class="ibox-content">
        <div class="tabs-container">
            <div class="tabs-left">
                <ul id="lob-tab" class="nav nav-tabs">
                    @foreach($userLobs as $key => $lob)
                        @if($key == 0)
                            <?php $active = "active" ?>
                        @else
                            <?php $active = "" ?>
                        @endif

                        <li data-lob-id="{{ $lob->id }}" class="lob-selector {{ $active }}"><a data-toggle="tab"
                                                                                               href="#tab{{ $lob->id }}">{{ $lob->name }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content gray-bg">
                    @foreach($userLobs as $key => $lob)
                        @if($key == 0)
                            <?php $active = "active" ?>
                        @else
                            <?php $active = "" ?>
                        @endif

                        <div id="tab{{ $lob->id }}" class="tab-pane {{ $active }}">


                            <div class="panel-body">
                                <div id="validation-error-panel-{{ $lob->id }}" class="col-lg-12"
                                     style="display: none;">
                                    <div class="ibox float-e-margins border-left-right border-bottom">
                                        <div class="ibox-title">
                                            <h5><span class="fa fa-exclamation-triangle"></span> Validation Errors </h5>
                                            <div class="ibox-tools">
                                                <a id="close-validation-error-panel" data-lob-id="{{ $lob->id }}">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            <ul id="validation-error-list-{{ $lob->id }}">
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <form id="frm-{{ $lob->id }}" class="form-horizontal">

                                        @foreach($fields[$lob->id] as $field)

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">{{ $field->name }}</label>
                                                <div class="col-sm-9">
                                                    {!! create_html_field($field, $listValues[$lob->id]) !!}
                                                </div>
                                            </div>

                                        @endforeach
                                        <div class="hr-line-dashed"></div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="pull-right">
                                                    <button type="button" actionid="4" lobid="{{ $lob->id }}"
                                                            id="btn-stop-{{ $lob->id }}"
                                                            class="btn btn-primary btn-stop-case"
                                                            data-loading-text="Loading..." disabled="">COMPLETE
                                                    </button>
                                                    <button type="button" actionid="6" lobid="{{ $lob->id }}"
                                                            id="btn-stop-{{ $lob->id }}"
                                                            class="btn btn-warning btn-hold-case"
                                                            data-loading-text="Loading..." disabled="">HOLD
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" id="case-id" name="case_id" value="">
                                        <input type="hidden" id="duration" name="duration" value="">
                                    </form>

                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
