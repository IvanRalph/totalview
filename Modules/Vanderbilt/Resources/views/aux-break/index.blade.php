<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png/ico" href="/images/favicon.ico">
    <title>Total View</title>
    {!! Html::style('css/lib/all.css') !!}

    @section('module-styles')@show
</head>
<body class="gray-bg">
<div id="wrapper">
    {!! csrf_field() !!}

    <div class="middle-box lockscreen animated fadeInDown" style="width: 600px;">
        <div class="row">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Aux Code</h5>
                </div>
                <div class="ibox-content">
                    <div id="auxBreakTimer" style="width: 100%;"></div>
                    <form id="breakForm" class="m-t" role="form">
                        <input type="hidden" id="aux-id" value="">
                        <div class="form-group">
                            <label>Type</label>
                            <select id="auxBreakTypes" class="form-control" name="type">
                                @foreach ($aux_types as $aux_type)
                                    <option value="{{ $aux_type->id }}">{{ $aux_type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Remarks</label>
                            <textarea id="remarks" class="form-control" name="remarks"></textarea>
                        </div>

                    </form>
                </div>
                <div class="ibox-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" id="btnCancelAuxBreak" class="btn btn-default col-sm-3 pull-right "
                                    style="margin-left: 5px;">C A N C E L
                            </button>
                            <button type="button" id="btnStopAuxBreak" class="btn btn-danger col-sm-3 pull-right"
                                    style="margin-left: 5px;">S T O P
                            </button>
                            <button type="button" id="btnStartAuxBreak" class="btn btn-primary col-sm-3 pull-right">S T
                                                                                                                    A R
                                                                                                                    T
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Html::script('js/lib/all.js') !!}
{!! Html::script('js/local-storage-handler.js') !!}

{!! Html::script('js/side-navigation.js') !!}
{!! Html::script('js/http-request.js') !!}
{!! Html::script('js/routes.js') !!}
{!! Html::script('js/response-handler.js') !!}
{!! Html::script('js/schema-builder.js') !!}
{!! Html::script('js/commons.js') !!}

{!! Html::script('js/auth.js') !!}
{!! Html::script('js/module/vanderbilt/constant.js') !!}
{!! Html::script('js/module/vanderbilt/break.js') !!}
</body>
</html>
