<div class="col-lg-3 col-md-6">
    <div class="ibox float-e-margins metric-panel">
        <div class="ibox-title">
            <span class="label label-success pull-right">Today</span>
            <h5>CPH</h5>
        </div>
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
            <h1 class="no-margins"><span class="avgCph">0</span></h1>
            <small>Case(s) Per Hour</small>
        </div>
    </div>
</div>

<div class="col-lg-3 col-md-6">
    <div class="ibox float-e-margins metric-panel">
        <div class="ibox-title">
            <span class="label label-success pull-right">Today</span>
            <h5>CPH Target</h5>
        </div>
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
            <h1 class="no-margins"><span class="avgCphTarget">0</span></h1>
            <small>Case(s) Per Hour</small>
        </div>
    </div>
</div>

<div class="col-lg-3 col-md-6">
    <div class="ibox float-e-margins metric-panel">
        <div class="ibox-title">
            <span class="label label-success pull-right">Today</span>
            <h5>Handled</h5>
        </div>
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
            <h1 class="no-margins"><span class="totalHandledCases">0</span></h1>
            <small>Total Cases</small>
        </div>
    </div>
</div>

<div class="col-lg-3 col-md-6">
    <div class="ibox float-e-margins metric-panel">
        <div class="ibox-title">
            <span class="label label-success pull-right">Today</span>
            <h5>Prod</h5>
        </div>
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
            <h1 class="no-margins"><span class="totalProdCases">0</span></h1>
            <small>Total Cases</small>
        </div>
    </div>
</div>

