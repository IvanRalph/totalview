<?php

namespace Modules\Vanderbilt\Entities;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable
        = [
            'user_id',
            'shift_date',
            'start_shift',
            'end_shift',
            'leave'
        ];

    protected $connection = 'tv_vanderbilt';

    protected $table = 'shift_schedule';

    public $timestamps = false;

    protected $primaryKey = 'ID';
}
