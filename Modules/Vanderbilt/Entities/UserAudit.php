<?php

namespace Modules\Vanderbilt\Entities;

use Illuminate\Database\Eloquent\Model;

class UserAudit extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_vanderbilt';

    protected $table = 'user_audit_trail';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
