<?php

namespace Modules\Vanderbilt\Entities;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_vanderbilt';

    protected $table = 'role';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
