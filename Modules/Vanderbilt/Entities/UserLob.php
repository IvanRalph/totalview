<?php

namespace Modules\Vanderbilt\Entities;

use Illuminate\Database\Eloquent\Model;

class UserLob extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_vanderbilt';

    protected $table = 'user_lob';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
