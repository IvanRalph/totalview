<?php

namespace Modules\Vanderbilt\Entities;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable
        = [
            'shift_date',
            'status',
            'shift_start',
            'shift_end'
        ];

    protected $connection = 'tv_vanderbilt';

    protected $table = 'user';

    public $timestamps = false;

    protected $primaryKey = 'main_user_id';

    public function cases ()
    {
        return $this->hasMany('Modules\Vanderbilt\Entities\Case8', 'user_id');
    }

    public function schedules ()
    {
        return $this->hasMany('Modules\Vanderbilt\Entities\Schedule', 'user_id');
    }
}
