<?php

namespace Modules\Vanderbilt\Entities;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_vanderbilt';

    protected $table = 'action';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
