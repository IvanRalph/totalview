<?php

namespace Modules\Vanderbilt\Entities;

use Illuminate\Database\Eloquent\Model;

class LobSetting extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_vanderbilt';

    protected $table = 'lob_setting';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
