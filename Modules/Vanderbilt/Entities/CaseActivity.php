<?php

namespace Modules\Vanderbilt\Entities;

use Illuminate\Database\Eloquent\Model;

class CaseActivity extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_vanderbilt';

    protected $table = 'case_activity_log';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
