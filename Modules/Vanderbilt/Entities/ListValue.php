<?php

namespace Modules\Vanderbilt\Entities;

use Illuminate\Database\Eloquent\Model;

class ListValue extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_vanderbilt';

    protected $table = 'list_value';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
