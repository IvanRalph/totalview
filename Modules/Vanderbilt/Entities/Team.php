<?php

namespace Modules\Vanderbilt\Entities;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = ['shift_date'];

    protected $connection = 'tv_vanderbilt';

    protected $table = 'team';

    public $timestamps = false;

    protected $primaryKey = 'ID';

    public function users ()
    {
        return $this->hasMany('Modules\Vanderbilt\Entities\User', 'team');
    }
}
