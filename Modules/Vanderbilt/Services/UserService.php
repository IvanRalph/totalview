<?php

namespace Modules\Vanderbilt\Services;

use Illuminate\Support\Facades\DB;
use Modules\Main\Services\BaseUserService;
use Modules\Main\Services\DatabaseService;

class UserService extends BaseUserService
{
    public function __construct (DatabaseService $databaseService)
    {
        parent::__construct($databaseService);
    }

    public function getNurseStatus ()
    {
        return DB::connection('tv_vanderbilt')->table('user_status')->get();
    }
}