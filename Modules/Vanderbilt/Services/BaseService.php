<?php

namespace Modules\Vanderbilt\Services;

class BaseService
{
    protected $db;

    public function __construct ()
    {
        $this->db = \DB::connection('tv_vanderbilt');
    }
}