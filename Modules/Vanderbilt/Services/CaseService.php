<?php

namespace Modules\Vanderbilt\Services;

use Illuminate\Support\Facades\DB;
use Modules\Main\Services\BaseCaseService;
use Modules\Main\Services\DatabaseService;
use Modules\Vanderbilt\Entities\Team;
use Modules\Vanderbilt\Entities\User;

class CaseService extends BaseCaseService
{
    public function __construct (CaseActivityLogService $caseActivityLogService, StatusService $statusService, FieldService $fieldService, DatabaseService $databaseService)
    {
        $this->caseActivityLogService = $caseActivityLogService;
        $this->statusService          = $statusService;
        $this->fieldService           = $fieldService;
        $this->databaseService        = $databaseService;
        $this->db                     = DB::connection('tv_vanderbilt');
    }

    public function getUserCaseTable ($lobId, $userId, $shiftDate)
    {
        return $this->db->table(self::TABLE_NAME . $lobId)
            ->where('user_id', $userId)
            ->where(function ($query) use ($shiftDate) {
                $query->where('shift_date', $shiftDate)
                    ->whereIn('status_id', [
                        StatusService::COMPLETED_STATUS_ID,
                        StatusService::EDIT_COMPLETED_STATUS_ID,
                        StatusService::ESCALATED_STATUS_ID
                    ])
                    ->orWhere('status_id', StatusService::HOLD_STATUS_ID);
            })
            ->get();
    }

    public function getUserAuxBreakDuration ($userId, $shiftDate)
    {
        return $this->db->table('aux')
            ->where('user_id', $userId)
            ->where('shift_date', $shiftDate)
            ->whereNotIn('aux_type_id', [1])
            ->groupBy('user_id')
            ->selectRaw('coalesce(sec_to_time(sum(time_to_sec(timediff(end_time,start_time)))),\'00:00:00\') as duration')
            ->get();
    }

    public function getByFin ($lobId, $fin)
    {
        $user = session('user_from_account');
        $fin  = $this->db->table(self::TABLE_NAME . $lobId)
            ->where('fin', $fin)
            ->orderBy('shift_date', 'DESC')
            ->orderBy('id', 'DESC')
            ->first();

        if (!$fin) {
            return null;
        }

        //if status is on hold and same shift date as user
        if ($fin->status_id == StatusService::HOLD_STATUS_ID && $fin->shift_date == $user->shift_date) {
            $data = $this->db->table(self::TABLE_NAME . $lobId)
                ->where('id', $fin->id)
                ->where('user_id', $user->main_user_id)
                ->first();
            if ($data) {
                $data->type = 0;
            }

            return $data;
        } else if ($fin->status_id == StatusService::HOLD_STATUS_ID && $fin->shift_date != $user->shift_date) {
            $data = $this->db->table(self::TABLE_NAME . $lobId)
                ->where('id', $fin->id)
                ->first();
            if ($data) {
                $data->type = 1;
            }

            return $data;
        } else {
            $data = $this->db->table(self::TABLE_NAME . $lobId)
                ->where('id', $fin->id)
                ->where('status_id', StatusService::COMPLETED_STATUS_ID)
                ->orderBy('shift_date', 'DESC')
                ->first();
            if ($data) {
                $data->type = 2;
            }

            if (in_array($fin->status_id, StatusService::ONGOING_STATUS_ID)) {
                return 'nurseOnGoing';
            }

            return $data;
        }
    }

    public function getUserTrackerStats ($lobId, $userId, $shiftDate)
    {
        //initialize stats
        $totalHandledDuration = 0;
        $totalProdDuration    = 0;
        $ahtSec               = 0;
        $cph                  = 0;
        $target               = 0;
        $totalProdCases       = 0;
        $breakClass           = 'navy';

        $postData = [
            'lob_id'     => $lobId,
            'user_id'    => $userId,
            'shift_date' => $shiftDate
        ];

        $cases              = $this->getUserCompleted($postData);
        $totalCompletedCase = $cases->count();

        $holdCases     = $this->getUserPending($postData);
        $totalHoldCase = $holdCases->count();

        $totalProdBreak = $this->getUserBreakDuration([
            'user_id'    => $userId,
            'shift_date' => $shiftDate
        ]);

        if (isset($totalProdBreak->duration)) {    // if has break
            $totalProdBreak = duration_to_seconds($totalProdBreak->duration);
            //For color code of breaks
            if ($totalProdBreak > duration_to_seconds('01:45:00')) {
                $breakClass = 'red';
            } else if ($totalProdBreak < duration_to_seconds('01:10:00')) {
                $breakClass = 'lazur';
            }
        } else { // break is null
            $totalProdBreak = 0;
        }

        $target = $this->getUserTarget($userId); // ramp
        $target = $target->ramp;

        if ($totalCompletedCase > 0) { // count completed cases only
            foreach ($cases as $case) {
                if ($case->accountstat == "New") {
                    $totalProdCases    += 1;
                    $totalProdDuration += duration_to_seconds($case->duration) / 3600;
                }

                $totalHandledDuration += duration_to_seconds($case->duration);
            }

            $totalCases = $totalCompletedCase;
            $ahtSec     = ($totalHandledDuration > 0 ? $totalHandledDuration / $totalCases : 0);
            // CPH formula(ProdCase/ Prod Hour)
            $cph = ($totalProdDuration > 0 ? round($totalProdCases / $totalProdDuration, 2) : 0);
        }

        return [
            'total_completed_case' => $totalCompletedCase + $totalHoldCase,
            'aht'                  => gmdate('H:i:s', $ahtSec),
            'cph'                  => $cph,
            'target'               => $target,
            'breaks'               => gmdate('H:i:s', $totalProdBreak),
            'break_class'          => $breakClass,
            'total_prod_cases'     => $totalProdCases,
            'total_duration'       => gmdate('H:i:s', $totalHandledDuration)
        ];
    }

    public function getCurrentCase ($lobId, $userId)
    {
        $caseTable = self::TABLE_NAME . $lobId;
        $statusIds = StatusService::ONGOING_STATUS_ID;

        return $this->db->table($caseTable)
            ->where('user_id', $userId)
            ->whereIn('status_id', $statusIds)
            ->where(function ($query) {
                $query->where('created_at', '>', deduct_timestamp_hours(self::LAST_CASE_HOUR_INTERVAL))
                    ->orWhere('updated_at', '>', deduct_timestamp_hours(self::LAST_CASE_HOUR_INTERVAL));
            })
            ->orderBy('id', 'desc')
            ->first();
    }

    public function getAvgCphPerTeam ($lobId, $teamId)
    {
        $totalProdCases = 0;
        $totalProdHours = 0;
        $cph            = 0;
        $caseTable      = self::TABLE_NAME . $lobId;

        if ($teamId == 'all') {
            $users = User::get();
        } else {
            $users = Team::find($teamId)->users;
        }

        foreach ($users as $user) {
            $totalProdCases += $user->cases->where('accountstat', 'New')->where('shift_date', $user->shift_date)->count();

            foreach ($user->cases->where('accountstat', 'New')->where('shift_date', $user->shift_date)->all() as $case) {
                $totalProdHours += duration_to_seconds($case->duration) / 3600;
            }

            if ($totalProdHours > 0) {
                $cph += $totalProdCases / $totalProdHours;
            }

            $totalProdCases = $totalProdHours = 0;
        }

        return round($cph, 2);
    }

    public function getAvgCphTargetPerTeam ($lobId, $teamId)
    {
        $avgCphTarget = 0;

        if ($teamId == 'all') {
            $users = User::get();
        } else {
            $users = Team::find($teamId)->users;
        }

        return $users->sum('ramp');
    }

    public function getTotalHandledCases ($lobId, $teamId)
    {
        $totalHandledCases = 0;

        if ($teamId == 'all') {
            $users = User::get();
        } else {
            $users = Team::find($teamId)->users;
        }

        foreach ($users as $user) {
            $totalHandledCases += $user->cases->where('shift_date', $user->shift_date)->whereNotIn('status_id', [
                1,
                3,
                7
            ])->count();
        }

        return $totalHandledCases;
    }

    public function getTotalProdCases ($lobId, $teamId)
    {
        $totalProdCases = 0;

        if ($teamId == 'all') {
            $users = User::get();
        } else {
            $users = Team::find($teamId)->users;
        }

        foreach ($users as $user) {
            $totalProdCases += $user->cases->where('accountstat', 'New')->where('shift_date', $user->shift_date)->where('status_id', 4)->count();
        }

        return $totalProdCases;
    }

    public function getByConditions ($lobId, $userId, $conditions = [], $fetchSingleRow = false)
    {
        $query = $this->db->table(self::TABLE_NAME . $lobId);

        foreach ($conditions as $condition) {
            $query->where($condition['column'], $condition['comparison'], $condition['value'])
                ->where('status_id', 4)
                ->where('user_id', $userId);
        }

        if ($fetchSingleRow) {
            return $query->first();
        }

        return $query->get();
    }

    public function dashboardQueryBuilder ($lobId, $teamId)
    {
        $shiftDate = $this->db->table('user')->where('user.id', session('user_from_account')->id)->first();

        if ($teamId == 'all') {
            $teamId = "(SELECT ID FROM team)";
        }

        $result = $this->db->select('Select
            CONCAT(user.lastname, ", ", user.firstname) AS name,
            team.description AS team,
            user.shift_date AS shift_date,
            COALESCE(CONCAT(shift_schedule.start_shift, " - ", shift_schedule.end_shift), "00:00:00 - 00:00:00") AS shift_schedule,
            SUM(IF(case' . $lobId . '.status_id IN (4,8), 1, 0)) AS total_completed_cases,
            SUM(IF(case' . $lobId . '.status_id = 6, 1, 0)) AS total_pending_cases,
            COALESCE((SELECT COUNT(*) FROM case' . $lobId . ' WHERE accountstat = "New" AND user_id = user.main_user_id AND shift_date = "' . $shiftDate->shift_date . '") / (SELECT SUM(MINUTE(duration)) FROM case' . $lobId . ' WHERE accountstat = "New" AND user_id = user.main_user_id AND shift_date = "' . $shiftDate->shift_date . '"), 0) AS cph,
            user.ramp AS cph_target,
            (SELECT count(*) FROM case' . $lobId . ' WHERE user_id = user.main_user_id AND shift_date = "' . $shiftDate->shift_date . '" AND status_id NOT IN (1,3,7)) AS total_handled_cases,
            (SELECT count(*) FROM case' . $lobId . ' WHERE user_id = user.main_user_id AND accountstat = "New" AND shift_date = "' . $shiftDate->shift_date . '" AND status_id = 4) AS total_prod_cases,
            SEC_TO_TIME(SUM(IF(case' . $lobId . '.status_id IN (4,8), TIME_TO_SEC(case' . $lobId . '.duration), 0))) AS total_duration,
            COALESCE(SEC_TO_TIME(CEILING(SUM(IF(case' . $lobId . '.status_id IN (4,8), TIME_TO_SEC(case' . $lobId . '.duration), 0)) / SUM(IF(case' . $lobId . '.status_id IN (4,8), 1, 0)))), "00:00:00") AS aht, user.main_user_id AS main_user_id, user.status AS status
            FROM user
            left join case' . $lobId . '
            on user.main_user_id = case' . $lobId . '.user_id
            AND case' . $lobId . '.shift_date = "' . $shiftDate->shift_date . '"
            INNER JOIN team
            ON user.team = team.id
            LEFT JOIN shift_schedule
            ON user.main_user_id = shift_schedule.user_id
            AND shift_schedule.shift_date = user.shift_date
            WHERE user.team IN (' . $teamId . ')
            AND user.role IN (5,11)
            AND user.active = 2
            GROUP BY user.main_user_id
            ORDER BY user.status DESC');

        return $result;
    }
}