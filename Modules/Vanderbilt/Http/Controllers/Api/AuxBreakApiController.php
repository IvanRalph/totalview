<?php

namespace Modules\Vanderbilt\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Main\Http\Controllers\Api\BaseAuxBreakApiController;
use Modules\Vanderbilt\Services\AuxBreakService;
use Modules\Vanderbilt\Services\CaseService;
use Modules\Vanderbilt\Services\UserAuditTrailService;
use Modules\Vanderbilt\Services\UserService;

class AuxBreakApiController extends BaseAuxBreakApiController
{
    public function __construct (AuxBreakService $auxBreakService, UserService $userService, UserAuditTrailService $userAuditTrailService, CaseService $caseService)
    {
        $this->auxBreakService       = $auxBreakService;
        $this->userService           = $userService;
        $this->userAuditTrailService = $userAuditTrailService;
        $this->caseService           = $caseService;
    }
}