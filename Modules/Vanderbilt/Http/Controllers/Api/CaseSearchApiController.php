<?php

namespace Modules\Vanderbilt\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Main\Http\Controllers\Api\BaseCaseSearchApiController;
use Modules\Vanderbilt\Services\CaseService;
use Modules\Vanderbilt\Services\FieldService;
use Modules\Vanderbilt\Services\StatusService;

class CaseSearchApiController extends BaseCaseSearchApiController
{
    /**
     * BaseCaseSearchApiController constructor.
     * @param CaseService   $caseService
     * @param FieldService  $fieldService
     * @param StatusService $statusService
     */
    public function __construct (CaseService $caseService, FieldService $fieldService, StatusService $statusService)
    {
        $this->caseService   = $caseService;
        $this->fieldService  = $fieldService;
        $this->statusService = $statusService;
    }
}