<?php

namespace Modules\Vanderbilt\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Modules\Main\Http\Controllers\Api\BaseReportsApiController;
use Modules\Vanderbilt\Services\LobService;
use Modules\Vanderbilt\Services\CaseService;
use Modules\Vanderbilt\Services\FieldService;
use Modules\Vanderbilt\Services\ReportService;

class ReportsApiController extends BaseReportsApiController
{
    public function __construct (LobService $lobService, CaseService $caseService, FieldService $fieldService, ReportService $reportService)
    {
        $this->lobService    = $lobService;
        $this->caseService   = $caseService;
        $this->fieldService  = $fieldService;
        $this->reportService = $reportService;
    }
}