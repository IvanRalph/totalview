<?php

namespace Modules\Vanderbilt\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Main\Http\Controllers\Api\BaseTeamSettingsApiController;
use Modules\Vanderbilt\Services\TeamSettingService;
use Modules\Vanderbilt\Services\LobService;

class TeamSettingsApiController extends BaseTeamSettingsApiController
{
    public function __construct (TeamSettingService $teamSettingService, LobService $lobService)
    {
        $this->teamSettingService = $teamSettingService;
        $this->lobService         = $lobService;
    }
}
