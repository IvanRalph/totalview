<?php

namespace Modules\Vanderbilt\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Main\Http\Controllers\Api\BaseDashboardApiController;
use Modules\Vanderbilt\Services\CaseService;
use Modules\Vanderbilt\Services\TeamService;
use Modules\Vanderbilt\Services\UserAuditTrailService;
use Modules\Vanderbilt\Services\UserService;

class DashboardApiController extends BaseDashboardApiController
{
    /**
     * BaseDashboardApiController constructor.
     * @param CaseService           $caseService
     * @param UserService           $userService
     * @param UserAuditTrailService $userAuditTrailService
     * @param TeamService           $teamService
     */
    public function __construct (
        CaseService $caseService,
        UserService $userService,
        UserAuditTrailService $userAuditTrailService,
        TeamService $teamService
    )
    {
        $this->caseService           = $caseService;
        $this->userService           = $userService;
        $this->userAuditTrailService = $userAuditTrailService;
        $this->teamService           = $teamService;
    }

    public function getMetrics (Request $request, $lobId, $teamId)
    {
        $mainUser        = $request->get('userData');
        $userFromAccount = $this->userService->getByMainUserId($mainUser['id']);

        $avgCph = $this->caseService->getAvgCphPerTeam($lobId, $teamId);

        $avgCphTarget = $this->caseService->getAvgCphTargetPerTeam($lobId, $teamId);

        $totalHandledCases = $this->caseService->getTotalHandledCases($lobId, $teamId);

        $totalProdCases = $this->caseService->getTotalProdCases($lobId, $teamId);

        $allUserCount       = $this->userService->countAll([
            'lob_id'  => $lobId,
            'team_id' => $teamId
        ]);
        $availableUserCount = $this->userService->countAvailable(['lob_id'  => $lobId,
                                                                  'team_id' => $teamId
        ]);

        return response()->json([
            'avg_cph'             => $avgCph,
            'avg_cph_target'      => $avgCphTarget,
            'total_handled_cases' => $totalHandledCases,
            'total_prod_cases'    => $totalProdCases
        ]);
    }
}
