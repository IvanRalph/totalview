<?php

namespace Modules\Vanderbilt\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Main\Http\Controllers\Api\BaseCaseHandlingApiController;
use Modules\Vanderbilt\Services\CaseActivityLogService;
use Modules\Vanderbilt\Services\CaseService;
use Modules\Vanderbilt\Services\FieldService;
use Modules\Vanderbilt\Services\ListValueService;
use Modules\Vanderbilt\Services\LobService;
use Modules\Vanderbilt\Services\StatusService;
use Modules\Vanderbilt\Services\UserAuditTrailService;

class CaseHandlingApiController extends BaseCaseHandlingApiController
{
    public function __construct (CaseService $caseService, FieldService $fieldService, StatusService $statusService, CaseActivityLogService $caseActivityLogService, UserAuditTrailService $userAuditTrailService, LobService $lobService, ListValueService $listValueService)
    {
        $this->caseService            = $caseService;
        $this->fieldService           = $fieldService;
        $this->statusService          = $statusService;
        $this->caseActivityLogService = $caseActivityLogService;
        $this->userAuditTrailService  = $userAuditTrailService;
        $this->lobService             = $lobService;
        $this->listValueService       = $listValueService;
    }

    public function getbyFin (Request $request, $lobId, $fin = null)
    {
        if ($fin == null) {
            return response()->json(['case_info' => null]);
        }
        $finCallBack = $this->caseService->getByFin($lobId, $fin);
        if ($finCallBack == 'nurseOnGoing') {
            return response()->json(['validationErrors' => 'FIN #' . $fin . ' is currently on going with another nurse.']);
        }

        return response()->json(['case_info' => $finCallBack]);
    }

    public function updateFin (Request $request, $lobId, $caseId)
    {
        $data   = $request->get('data');
        $fields = $this->fieldService->getByLobId($lobId);

        $post = [
            'lob_id'  => $lobId,
            'case_id' => $caseId,
            'data'    => $data,
            'fields'  => $fields
        ];

        $isSuccess = $this->caseService->update($post);

        return response()->json(['is_success' => $isSuccess]);
    }
}