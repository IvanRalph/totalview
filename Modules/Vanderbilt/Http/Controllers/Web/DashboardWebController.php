<?php

namespace Modules\Vanderbilt\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Main\Http\Controllers\Web\BaseDashboardWebController;
use Modules\Vanderbilt\Services\DashboardService;
use Modules\Vanderbilt\Services\LobService;
use Modules\Vanderbilt\Services\UserService;
use Modules\Vanderbilt\Services\TeamService;

class DashboardWebController extends BaseDashboardWebController
{
    public function __construct (DashboardService $dashboardService, LobService $lobService, UserService $userService, TeamService $teamService)
    {
        $this->dashboardService = $dashboardService;
        $this->lobService       = $lobService;
        $this->userService      = $userService;
        $this->teamService      = $teamService;
    }
}