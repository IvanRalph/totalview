<?php

namespace Modules\Vanderbilt\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Main\Http\Controllers\Web\BaseCaseSearchWebController;
use Modules\Vanderbilt\Services\FieldService;
use Modules\Vanderbilt\Services\LobService;

class CaseSearchWebController extends BaseCaseSearchWebController
{
    /**
     * CaseSearchWebController constructor.
     * @param LobService   $lobService
     * @param FieldService $fieldService
     */
    public function __construct (LobService $lobService, FieldService $fieldService)
    {
        $this->lobService   = $lobService;
        $this->fieldService = $fieldService;
    }
}