<?php

namespace Modules\Vanderbilt\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Main\Http\Controllers\Web\BaseReportsWebController;
use Modules\Vanderbilt\Services\LobService;

class ReportsWebController extends BaseReportsWebController
{
    public function __construct (LobService $lobService)
    {
        $this->lobService = $lobService;
    }

    public function index ()
    {
        $data['getLobList'] = $this->lobService->getLobList();

        return view('vanderbilt::reports.index', $data);
    }
}




