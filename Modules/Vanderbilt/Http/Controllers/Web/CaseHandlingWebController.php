<?php

namespace Modules\Vanderbilt\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Main\Http\Controllers\Web\BaseCaseHandlingWebController;
use Modules\Vanderbilt\Services\CaseService;
use Modules\Vanderbilt\Services\FieldService;
use Modules\Vanderbilt\Services\ListValueService;
use Modules\Vanderbilt\Services\LobService;
use Modules\Vanderbilt\Services\UserAuditTrailService;
use Modules\Vanderbilt\Services\UserService;

class CaseHandlingWebController extends BaseCaseHandlingWebController
{
    /**
     * CaseHandlingWebController constructor.
     * @param FieldService          $fieldService
     * @param ListValueService      $listValueService
     * @param CaseService           $caseService
     * @param UserService           $userService
     * @param UserAuditTrailService $userAuditTrailService
     * @param LobService            $lobService
     */
    public function __construct (
        FieldService $fieldService,
        ListValueService $listValueService,
        CaseService $caseService,
        UserService $userService,
        UserAuditTrailService $userAuditTrailService,
        LobService $lobService
    )
    {
        $this->fieldService          = $fieldService;
        $this->listValueService      = $listValueService;
        $this->caseService           = $caseService;
        $this->userService           = $userService;
        $this->userAuditTrailService = $userAuditTrailService;
        $this->lobService            = $lobService;
    }
}