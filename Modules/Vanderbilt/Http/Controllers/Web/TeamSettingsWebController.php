<?php

namespace Modules\Vanderbilt\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Main\Http\Controllers\Web\BaseTeamSettingsWebController;
use Modules\Vanderbilt\Services\TeamSettingService;
use Modules\Vanderbilt\Services\LobService;
use Modules\Vanderbilt\Services\RoleService;
use Modules\Vanderbilt\Services\UserService;

class TeamSettingsWebController extends BaseTeamSettingsWebController
{
    public function __construct (TeamSettingService $teamSettingService, LobService $lobService, RoleService $roleService, UserService $userService)
    {
        $this->teamSettingService = $teamSettingService;
        $this->lobService         = $lobService;
        $this->roleService        = $roleService;
        $this->userService        = $userService;
    }
}