<?php

namespace Modules\Vanderbilt\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Main\Http\Controllers\Web\BaseAuxBreakWebController;
use Modules\Vanderbilt\Services\AuxBreakService;

class AuxBreakWebController extends BaseAuxBreakWebController
{
    /**
     * @param $auxBreakService AuxBreakService
     */
    public function __construct (AuxBreakService $auxBreakService)
    {
        $this->auxBreakService = $auxBreakService;
    }
}