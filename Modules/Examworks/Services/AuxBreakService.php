<?php

namespace Modules\Examworks\Services;

use Modules\Main\Services\BaseAuxBreakService;
use Modules\Main\Services\DatabaseService;

class AuxBreakService extends BaseAuxBreakService
{
    public function __construct (DatabaseService $databaseService)
    {
        parent::__construct($databaseService);
    }
}