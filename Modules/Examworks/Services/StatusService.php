<?php

namespace Modules\Examworks\Services;

use Modules\Main\Services\BaseStatusService;
use Modules\Main\Services\DatabaseService;

class StatusService extends BaseStatusService
{
    public function __construct (DatabaseService $databaseService)
    {
        parent::__construct($databaseService);
    }
}