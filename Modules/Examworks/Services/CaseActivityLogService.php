<?php

namespace Modules\Examworks\Services;

use Modules\Main\Services\BaseCaseActivityLogService;
use Modules\Main\Services\DatabaseService;

class CaseActivityLogService extends BaseCaseActivityLogService
{
    public function __construct (DatabaseService $databaseService)
    {
        parent::__construct($databaseService);
    }
}