<?php

namespace Modules\Examworks\Services;

use Illuminate\Support\Facades\DB;
use Modules\Main\Services\BaseCaseService;
use Modules\Main\Services\DatabaseService;

class CaseService extends BaseCaseService
{
    public function __construct (CaseActivityLogService $caseActivityLogService, StatusService $statusService, FieldService $fieldService, DatabaseService $databaseService)
    {
        parent::__construct($databaseService);

        $this->caseActivityLogService = $caseActivityLogService;
        $this->statusService          = $statusService;
        $this->fieldService           = $fieldService;
        $this->databaseService        = $databaseService;
        $this->db                     = DB::connection('tv_examworks');
    }

    public function getUserCaseTable ($lobId, $userId, $shiftDate)
    {
        return $this->db->table(self::TABLE_NAME . $lobId)
            ->where('user_id', $userId)
            ->where(function ($query) use ($shiftDate) {
                $query->where('shift_date', $shiftDate)
                    ->whereIn('status_id', [
                        StatusService::COMPLETED_STATUS_ID,
                        StatusService::EDIT_COMPLETED_STATUS_ID,
                        StatusService::ESCALATED_STATUS_ID
                    ])
                    ->orWhere('status_id', StatusService::HOLD_STATUS_ID);
            })
            ->orderBy('id', 'desc')
            ->orderBy('status_id', 'desc')
            ->get();
    }

    public function getUserTotalICD (array $post)
    {
        $params = [
            'modelName' => 'Case' . $post['lob_id'],
            'filter'    => [
                'user_id'    => $post['user_id'],
                'shift_date' => $post['shift_date'],
                'status_id'  => $this->statusService::COMPLETED_STATUS_ID
            ],
            'sum'       => 'icd'
        ];

        return $this->databaseService->sum($params);
    }

    public function getTotalICD (array $post)
    {
        $caseTable = 'case' . $post['lob_id'];

        $params = [
            'modelName' => ucfirst($caseTable),
            'leftJoin'  => [
                'user' => [
                    'leftField'  => $caseTable . '.user_id',
                    'operator'   => '=',
                    'rightField' => 'user.main_user_id'
                ]
            ],
            'filter'    => [
                $caseTable . '.shift_date' => $post['shift_date'],
                'user.team'                => $post['team_id'],
            ],
            'sum'       => $caseTable . '.icd'
        ];

        return $this->databaseService->sum($params);
    }

    public function getUserTotalClaimnant (array $post)
    {
        $params = [
            'modelName' => 'Case' . $post['lob_id'],
            'filter'    => [
                'user_id'    => $post['user_id'],
                'shift_date' => $post['shift_date'],
                'status_id'  => $this->statusService::COMPLETED_STATUS_ID
            ],
        ];

        return $this->databaseService->count($params);
    }

    public function getTotalClaimnant (array $post)
    {
        $caseTable = 'case' . $post['lob_id'];

        $params = [
            'modelName' => ucfirst($caseTable),
            'leftJoin'  => [
                'user' => [
                    'leftField'  => $caseTable . '.user_id',
                    'operator'   => '=',
                    'rightField' => 'user.main_user_id'
                ]
            ],
            'filter'    => [
                $caseTable . '.shift_date' => $post['shift_date'],
                'user.team'                => $post['team_id'],
            ],
        ];

        return $this->databaseService->count($params);
    }

    public function getUserTotalPages (array $post)
    {
        $caseTable = 'case' . $post['lob_id'];

        $params = [
            'modelName' => ucfirst($caseTable),
            'filter'    => [
                'user_id'    => $post['user_id'],
                'shift_date' => $post['shift_date']
            ],
            'sum'       => 'pages'
        ];

        return $this->databaseService->sum($params);
    }

    //TODO: unknown column pages & benchmark on Case9
    public function getTotalPages (array $post)
    {
        $caseTable = 'case' . $post['lob_id'];

        $params = [
            'modelName' => ucfirst($caseTable),
            'leftJoin'  => [
                'user' => [
                    'leftField'  => $caseTable . '.user_id',
                    'operator'   => '=',
                    'rightField' => 'user.main_user_id'
                ]
            ],
            'filter'    => [
                $caseTable . '.shift_date' => $post['shift_date'],
                'user.team'                => $post['team_id'],
            ],
            'sum'       => $caseTable . '.pages'
        ];

        return $this->databaseService->sum($params);
    }

    public function getUserTotalBenchmark (array $post)
    {
        $caseTable = 'case' . $post['lob_id'];

        $params = [
            'modelName' => ucfirst($caseTable),
            'filter'    => [
                'user_id'    => $post['user_id'],
                'shift_date' => $post['shift_date']
            ],
            'sum'       => 'benchmark'
        ];

        return $this->databaseService->sum($params);
    }

    public function getTotalBenchmark (array $post)
    {
        $caseTable = 'case' . $post['lob_id'];

        $params = [
            'modelName' => ucfirst($caseTable),
            'leftJoin'  => [
                'user' => [
                    'leftField'  => $caseTable . '.user_id',
                    'operator'   => '=',
                    'rightField' => 'user.main_user_id'
                ]
            ],
            'filter'    => [
                $caseTable . '.shift_date' => $post['shift_date'],
                'user.team'                => $post['team_id'],
            ],
            'sum'       => 'benchmark'
        ];

        return $this->databaseService->sum($params);
    }

    public function getTotalAHT (array $post)
    {
        $caseTable = 'case' . $post['lob_id'];

        $params = [
            'modelName' => ucfirst($caseTable),
            'leftJoin'  => [
                'user' => [
                    'leftField'  => $caseTable . '.user_id',
                    'operator'   => '=',
                    'rightField' => 'user.main_user_id',
                ]
            ],
            'filter'    => [
                $caseTable . '.shift_date' => $post['shift_date'],
                'user.team'                => $post['team_id'],
            ],
            'sum'       => 'duration'
        ];

        return $this->databaseService->sum($params);
    }

    public function getReworkCase (array $post)
    {
        $caseTable = 'case' . $post['lob_id'];

        $params = [
            'modelName' => ucfirst($caseTable),
            'filter'    => [
                $post['key_field'] => $post['reference'],
                'status_id'        => $this->statusService::COMPLETED_STATUS_ID
            ]
        ];

        return $this->databaseService->fetch($params);
    }

    public function getUserTrackerStats ($lobId, $userId, $shiftDate)
    {
        //initialize stats
        $totalDuration = 0;
        $ahtSec        = 0;

        $icd = $this->getUserTotalICD([
            'user_id'    => $userId,
            'shift_date' => $shiftDate,
            'lob_id'     => 9
        ]);

        $claimnant = $this->getUserTotalClaimnant([
            'user_id'    => $userId,
            'shift_date' => $shiftDate,
            'lob_id'     => 9
        ]);

        $pages = $this->getUserTotalPages([
            'user_id'    => $userId,
            'shift_date' => $shiftDate,
            'lob_id'     => 10
        ]);

        $benchmark = $this->getUserTotalBenchmark([
            'user_id'    => $userId,
            'shift_date' => $shiftDate,
            'lob_id'     => 10
        ]);

        $cases              = $this->getUserCompleted([
            'user_id'    => $userId,
            'shift_date' => $shiftDate,
            'lob_id'     => $lobId
        ]);
        $totalCompletedCase = count($cases);

        if ($totalCompletedCase > 0) {
            foreach ($cases as $case) {
                $totalDuration += duration_to_seconds($case->duration);
            }

            $ahtSec = $totalDuration / $totalCompletedCase;
        }

        return [
            'total_completed_case' => $totalCompletedCase,
            'aht'                  => gmdate('H:i:s', $ahtSec),
            'total_duration'       => gmdate('H:i:s', $totalDuration),
            'total_icd'            => $icd,
            'total_claimnant'      => $claimnant,
            'total_pages'          => $pages,
            'total_benchmark'      => $benchmark,
        ];
    }

    public function getCurrentCase ($lobId, $userId)
    {
        $caseTable = self::TABLE_NAME . $lobId;
        $statusIds = StatusService::ONGOING_STATUS_ID;

        return $this->db->table($caseTable)
            ->where('user_id', $userId)
            ->whereIn('status_id', $statusIds)
            ->where(function ($query) {
                $query->where('created_at', '>', deduct_timestamp_hours(self::LAST_CASE_HOUR_INTERVAL))
                    ->orWhere('updated_at', '>', deduct_timestamp_hours(self::LAST_CASE_HOUR_INTERVAL));
            })
            ->orderBy('id', 'desc')
            ->first();
    }

    public function getByConditions ($lobId, $userId, $conditions = [], $fetchSingleRow = false)
    {
        $query = $this->db->table(self::TABLE_NAME . $lobId);

        foreach ($conditions as $condition) {
            $query->where($condition['column'], $condition['comparison'], $condition['value'])
                ->where('status_id', 4)
                ->where('user_id', $userId);
        }

        if ($fetchSingleRow) {
            return $query->first();
        }

        return $query->get();
    }

    /**
     * @param $lobId
     * @param $teamId
     * @return mixed
     */
    public function dashboardQueryBuilder ($lobId, $teamId)
    {
        $shiftDate = $this->db->table('user')->where('user.id', session('user_from_account')->id)->first();

        if ($teamId == 'all') {
            $teamId = "(SELECT ID FROM team)";
        }

        if ($lobId == 9) {
            $sum = 'SUM(coalesce(icd,0)) as total_icd';
            $cnt = 'SUM(IF(case' . $lobId . '.status_id IN (4,8), 1, 0)) as total_claimnant';
        } else {
            $sum = 'SUM(coalesce(pages,0)) as total_pages';
            $cnt = 'SUM(coalesce(benchmark,0)) as total_benchmark';
        }

        $result = $this->db->select('Select
            CONCAT(user.lastname, ", ", user.firstname) AS name,
            team.description AS team,
            user.shift_date AS shift_date,
            COALESCE(CONCAT(shift_schedule.start_shift, " - ", shift_schedule.end_shift), "00:00:00 - 00:00:00") AS shift_schedule,
            SUM(IF(case' . $lobId . '.status_id IN (4,8), 1, 0)) AS total_completed_cases,
            SUM(IF(case' . $lobId . '.status_id = 6, 1, 0)) AS total_pending_cases,
            ' . $sum . ',
            ' . $cnt . ',
            user.ramp AS cph_target,
            (SELECT count(*) FROM case' . $lobId . ' WHERE user_id = user.main_user_id AND shift_date = "' . $shiftDate->shift_date . '" AND status_id NOT IN (1,3,7)) AS total_handled_cases,
            SEC_TO_TIME(SUM(IF(case' . $lobId . '.status_id IN (4,8), TIME_TO_SEC(case' . $lobId . '.duration), 0))) AS total_duration,
            COALESCE(SEC_TO_TIME(CEILING(SUM(IF(case' . $lobId . '.status_id IN (4,8), TIME_TO_SEC(case' . $lobId . '.duration), 0)) / SUM(IF(case' . $lobId . '.status_id IN (4,8), 1, 0)))), "00:00:00") AS aht, user.main_user_id AS main_user_id, user.status AS status
            FROM user
            left join case' . $lobId . '
            on user.main_user_id = case' . $lobId . '.user_id
            AND case' . $lobId . '.shift_date = "' . $shiftDate->shift_date . '"
            INNER JOIN team
            ON user.team = team.id
            LEFT JOIN shift_schedule
            ON user.main_user_id = shift_schedule.user_id
            AND shift_schedule.shift_date = user.shift_date
            WHERE user.team IN (' . $teamId . ')
            AND user.role IN (6)
            AND user.active in (1,2)
            GROUP BY user.main_user_id
            ORDER BY user.status DESC');

        // dd($result);

        return $result;
    }
}