<?php

namespace Modules\Examworks\Services;

use Modules\Main\Services\BaseTeamService;
use Modules\Main\Services\DatabaseService;

class TeamService extends BaseTeamService
{
    public function __construct (DatabaseService $databaseService)
    {
        parent::__construct($databaseService);
    }

    public function getTeamShiftDate ($teamId)
    {
        $params = [
            'modelName' => 'Team',
            'filter'    => [
                'ID' => $teamId
            ],
            'select'    => 'shift_date'
        ];

        return $this->databaseService->fetch($params);
    }
}