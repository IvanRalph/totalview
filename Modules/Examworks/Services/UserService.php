<?php

namespace Modules\Examworks\Services;

use Illuminate\Support\Facades\DB;
use Modules\Main\Services\BaseUserService;
use Modules\Main\Services\DatabaseService;

class UserService extends BaseUserService
{
    public function __construct (DatabaseService $databaseService)
    {
        parent::__construct($databaseService);

        $this->db = DB::connection('tv_examworks');
    }

    public function getNurseStatus ()
    {
        return $this->db->table('user_status')->get();
    }
}