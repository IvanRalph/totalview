<?php
/**
 * Created by PhpStorm.
 * User: OMLanuza
 * Date: 1/17/2018
 * Time: 4:11 PM
 */

namespace Modules\Examworks\Services;

use Illuminate\Support\Facades\DB;
use Modules\Main\Services\BaseReportService;
use Modules\Main\Services\DatabaseService;

class ReportService extends BaseReportService
{
    public function __construct (DatabaseService $databaseService, StatusService $statusService)
    {
        parent::__construct($databaseService);

        $this->statusService = $statusService;

        $this->db = DB::connection('tv_examworks');
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function getRaw (array $post)
    {

        if($post['rawNumber'] == 9) {
            $select = 'raw.shift_date, concat(user.lastname," , ",user.firstname) as Name, team.description as Team, status.name as Status,
             raw.claimnant, raw.service_id, raw.icd, raw.remarks, raw.start_time, raw.end_time';
        } else {
            $select = 'raw.shift_date, concat(user.lastname," , ",user.firstname) as Name, team.description as Team, status.name as Status,
             raw.claimnant, raw.pages, raw.comorbidities, raw.case_type, raw.benchmark, raw.remarks, raw.start_time, raw.end_time';
        }
        $params = [
            'modelName' => 'Raw',
            'leftJoin'  => [
                'user'   => [
                    'leftField'  => 'user.main_user_id',
                    'operator'   => '=',
                    'rightField' => 'raw.user_id'
                ],
                'status' => [
                    'leftField'  => 'status.id',
                    'operator'   => '=',
                    'rightField' => 'raw.status_id'
                ],
                'team'   => [
                    'leftField'  => 'team.ID',
                    'operator'   => '=',
                    'rightField' => 'raw.team_id'
                ]
            ],
            'between'   => [
                'raw.shift_date' => [
                    $post['start'],
                    $post['end']
                ]
            ],
            'filter' => [
                'lob_id' => $post['rawNumber']

            ],

            'select' => $select,


            
        ];

        return $this->databaseService->fetchAll($params);
    }

    public function getHourly (array $post)
    {
        if ($post['timezone'] == 'CST') {
            $hour = 'concat(hour(date_add(case_activity_log.created_at,INTERVAL 13 HOUR)), ":00") as hour';
        } else {
            $hour = 'concat(hour(case_activity_log.created_at), ":00") as hour';
        }

        $params = [
            'modelName' => 'CaseActivity',
            'select'    => 'concat(user.lastname, \', \', user.firstname) as Name, ' . $hour . ', count(case_activity_log.id) as count',
            'leftJoin'  => [
                'user' => [
                    'leftField'  => 'user.id',
                    'operator'   => '=',
                    'rightField' => 'case_activity_log.user_id'
                ]
            ],
            'filter'    => [
                'lob_id'     => $post['lob_id'],
                'status_id'  => [
                    'notIn' => $this->statusService::ONGOING_STATUS_ID
                ],
                'created_at' => [
                    $post['date_from'],
                    $post['date_to']
                ]
            ],
            'group'     => [
                'user.id',
                $this->db->raw('hour(case_activity_log.created_at)')
            ]
        ];

        return $this->databaseService->fetchAll($params)->toArray();
    }

    public function getVolume ($lobNumber, $start, $end)
    {
       
        $result = $this->db->table('volume_reports')
            ->select('volume_reports.volume_month as Month', 'volume_reports.week_beginning as Week Beginning', 'volume_reports.account as Account',
                'volume_reports.shift_date as Date', 'lob.name as LOB', 'volume_reports.level as Level', 'volume_reports.volume_completed as Volume Completed',
                'volume_reports.volume_received as Volume Received', 'volume_reports.capacity as Capacity'
            )
            ->leftJoin('lob', 'lob.id', '=', 'volume_reports.lob_id')
            ->whereBetween('volume_reports.shift_date', [$start, $end])
            ->where('volume_reports.lob_id', '=', $lobNumber)
            ->get();

            return $result;
    }
}