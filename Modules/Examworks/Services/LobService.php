<?php

namespace Modules\Examworks\Services;

use Modules\Main\Services\BaseLobService;
use Modules\Main\Services\DatabaseService;

class LobService extends BaseLobService
{
    public function __construct (DatabaseService $databaseService)
    {
        parent::__construct($databaseService);
    }
}