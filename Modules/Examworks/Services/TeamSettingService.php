<?php

namespace Modules\Examworks\Services;

use DB;
use Modules\Examworks\Entities\User as User;
use Carbon\Carbon;
use Modules\Main\Services\BaseTeamSettingService;
use Modules\Main\Services\DatabaseService;

class TeamSettingService extends BaseTeamSettingService
{
    public function __construct (DatabaseService $databaseService)
    {
        parent::__construct($databaseService);

        $this->db = DB::connection('tv_examworks');
    }

    public function insertValuelistItem ($items,$lobId,$fieldid)
    {
        foreach ($items as $item) {
      
                $insert[] = ([
                    'value' => $item,
                    'lob_id' => $lobId,
                    'field_id' => $fieldid,
                ]);
       
        }
                 $params = [
                    'modelName' => 'ListValue',
                    'data'    => $insert
                    ];

                $res = $this->databaseService->createMany($params);

                return $res;
    }


    public function getTeamList ()
    {
        $res = $this->db->table('team')
            ->leftJoin('user', function ($join) {
                $join->where(DB::raw('FIND_IN_SET(user.id, team.lead)'), '>', 0);
            })
            ->select('team.ID', 'team.lead', 'user.username', 'team.end_time', 'team.description as team',
                \DB::raw("GROUP_CONCAT(CONCAT(' ',user.firstname,' ', user.lastname, ' ' ) ORDER BY user.id ) as fullname"))
            ->groupBy('team.ID')
            ->get();

        return $res;
    }


    public function getTeamListForTable ()
    {
        $res = $this->db->table('team')
            ->leftJoin('user', function ($join) {
                $join->where(DB::raw('FIND_IN_SET(user.id, team.lead)'), '>', 0);
            })
            ->select('team.ID', 'team.lead', 'user.username', 'team.end_time',
                'team.description as team', \DB::raw("GROUP_CONCAT(CONCAT(' ',user.firstname,' ', user.lastname, ' ' ) ORDER BY user.id ) as fullname"))
            ->groupBy('team.ID')
            ->get();

        return $res;
    }

    public function getLobDropdownList ($lobId)
    {
        $res = $this->db->table('field')
          
            ->select('field.id', 'field.name')
            ->where('field.lob_id', $lobId)
            ->where('field.type', "dropdown")
            ->get();

        return $res;
    }
    public function updateTeamUserScheduleShift (array $post)
    {
        $params = [
            'modelName' => 'User',
            'filter'    => [
                'main_user_id' => $post['user_id']
            ]
        ];

        $current_shift_date = $this->databaseService->fetch($params)->shift_date;

        $params = [
            'modelName' => 'Schedule',
            'leftJoin'  => [
                'user' => [
                    'leftField'  => 'shift_schedule.user_id',
                    'operator'   => '=',
                    'rightField' => 'user.main_user_id'
                ]
            ],
            'filter'    => [
                'user_id'                   => $post['user_id'],
                'shift_schedule.shift_date' => $current_shift_date
            ],
            'data'      => [
                'shift_schedule.start_shift'   => $post['start_shift'],
                'shift_schedule.end_shift'     => $post['end_shift'],
                'shift_schedule.leave'         => $post['on_leave'],
                'user.shift_end'               => $post['on_shift'],
                'user.role'                    => $post['slct_role'],
                'user.team'                    => $post['team_list'],
                'user.ramp'                    => $post['ramp'],
                'shift_schedule.user_statusid' => $post['nurse_status']
            ]
        ];

        $res = $this->databaseService->update($params);

        $mainRole = 1;

        if ($post['slct_role'] != 5) {
            $mainRole = 2;
        }

        $this->db->table('tv_main.user')
            ->where('id', $post['user_id'])
            ->update([
                'role_id' => $mainRole,
            ]);
    }
}