<?php

namespace Modules\Examworks\Services;

use Modules\Main\Services\BaseFieldService;
use Modules\Main\Services\DatabaseService;

class FieldService extends BaseFieldService
{
    public function __construct (DatabaseService $databaseService)
    {
        parent::__construct($databaseService);
    }
}