<?php

namespace Modules\Examworks\Services;

class BaseService
{
    protected $db;

    public function __construct ()
    {
        $this->db = \DB::connection(config('examworks.db_connection'));
    }
}