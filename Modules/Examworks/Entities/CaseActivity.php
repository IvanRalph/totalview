<?php

namespace Modules\Examworks\Entities;

use Illuminate\Database\Eloquent\Model;

class CaseActivity extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_examworks';

    protected $table = 'case_activity_log';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
