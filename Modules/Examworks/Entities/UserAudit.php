<?php

namespace Modules\Examworks\Entities;

use Illuminate\Database\Eloquent\Model;

class UserAudit extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_examworks';

    protected $table = 'user_audit_trail';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
