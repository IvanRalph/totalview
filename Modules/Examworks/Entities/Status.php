<?php

namespace Modules\Examworks\Entities;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_examworks';

    protected $table = 'status';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
