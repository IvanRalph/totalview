<?php

namespace Modules\Examworks\Entities;

use Illuminate\Database\Eloquent\Model;

class LobSetting extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_examworks';

    protected $table = 'lob_setting';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
