<?php

namespace Modules\Examworks\Entities;

use Illuminate\Database\Eloquent\Model;

class ListValue extends Model
{
    protected $fillable = ['status'];

    protected $connection = 'tv_examworks';

    protected $table = 'list_value';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
