<?php

namespace Modules\Examworks\Entities;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_examworks';

    protected $table = 'role';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
