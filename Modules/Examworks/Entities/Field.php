<?php

namespace Modules\Examworks\Entities;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_examworks';

    protected $table = 'field';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
