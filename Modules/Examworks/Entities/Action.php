<?php

namespace Modules\Examworks\Entities;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_examworks';

    protected $table = 'action';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
