<?php

namespace Modules\Examworks\Entities;

use Illuminate\Database\Eloquent\Model;

class AuxBreak extends Model
{
    protected $fillable
        = [
            'shift_date',
            'aux_type_id',
            'remarks',
            'start_time',
            'end_time'
        ];

    protected $connection = 'tv_examworks';

    protected $table = 'aux';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
