<?php

namespace Modules\Examworks\Entities;

use Illuminate\Database\Eloquent\Model;

class Lob extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_examworks';

    protected $table = 'lob';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
