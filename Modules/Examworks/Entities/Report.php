<?php

namespace Modules\Examworks\Entities;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable
        = [
            'case_id_9',
            'case_id_10',
            'shift_date',
            'parent_id',
            'user_id',
            'team_id',
            'status_id',
            'start_time',
            'end_time',
            'urgency',
            'duration',
            'claimnant',
            'service_id',
            'icd',
            'pages',
            'comorbidities',
            'case_type',
            'benchmark',
            'remarks',
            'lob_id',
            'timezone',
        ];

    protected $connection = 'tv_examworks';

    protected $table = 'raw';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
