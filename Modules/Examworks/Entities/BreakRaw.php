<?php

namespace Modules\Examworks\Entities;

use Illuminate\Database\Eloquent\Model;

class BreakRaw extends Model
{
    protected $fillable
        = [
            'user_id',
            'shift_date',
            'aux_type_id',
            'remarks',
            'start_time',
            'end_time',
            'duration'
        ];

    protected $connection = 'tv_examworks';

    protected $table = 'breaks';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
