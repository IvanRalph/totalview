<?php

namespace Modules\Examworks\Entities;

use Illuminate\Database\Eloquent\Model;

class Raw extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_examworks';

    protected $table = 'raw';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
