<?php

namespace Modules\Examworks\Entities;

use Illuminate\Database\Eloquent\Model;

class Case9 extends Model
{
    protected $fillable
        = [
            'shift_date',
            'user_id',
            'created_at',
            'updated_at',
            'break_at',
            'duration',
            'parent_id',
            'status_id',
            'urgency',
            'claimnant_1',
            'service_id',
            'icd',
            'remarks_1',
        ];

    protected $connection = 'tv_examworks';

    protected $table = 'case9';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
