<?php

namespace Modules\Examworks\Entities;

use Illuminate\Database\Eloquent\Model;

class AuxType extends Model
{
    protected $fillable = [];

    protected $connection = 'tv_examworks';

    protected $table = 'aux_type';

    public $timestamps = false;

    protected $primaryKey = 'id';
}
