<?php

namespace Modules\Examworks\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Examworks\Services\CaseService;
use Modules\Examworks\Services\StatusService;
use Modules\Examworks\Services\TeamService;
use Modules\Examworks\Services\UserAuditTrailService;
use Modules\Examworks\Services\UserService;
use Modules\Main\Http\Controllers\Api\BaseDashboardApiController;

class DashboardApiController extends BaseDashboardApiController
{
    /**
     * BaseDashboardApiController constructor.
     * @param CaseService           $caseService
     * @param UserService           $userService
     * @param UserAuditTrailService $userAuditTrailService
     * @param TeamService           $teamService
     */
    public function __construct (
        CaseService $caseService,
        UserService $userService,
        UserAuditTrailService $userAuditTrailService,
        TeamService $teamService
    )
    {
        $this->caseService           = $caseService;
        $this->userService           = $userService;
        $this->userAuditTrailService = $userAuditTrailService;
        $this->teamService           = $teamService;
    }

    public function getMetrics (Request $request, $lobId, $teamId)
    {
        $mainUser  = $request->get('userData');
        $shiftDate = $this->teamService->getTeamShiftDate($teamId)->shift_date;
        $totalICD  = $this->caseService->getTotalICD([
            'shift_date' => $shiftDate,
            'lob_id'     => 9,
            'team_id'    => $teamId
        ]);


        $totalClaimnant = $this->caseService->getTotalClaimnant([
            'shift_date' => $shiftDate,
            'lob_id'     => 9,
            'team_id'    => $teamId
        ]);

        $totalPages = $this->caseService->getTotalPages([
            'shift_date' => $shiftDate,
            'lob_id'     => 10,
            'team_id'    => $teamId
        ]);

        $totalBenchmark = $this->caseService->getTotalBenchmark([
            'shift_date' => $shiftDate,
            'lob_id'     => 10,
            'team_id'    => $teamId
        ]);

        $totalAHT = $this->caseService->getTotalAHT([
            'shift_date' => $shiftDate,
            'lob_id'     => $lobId,
            'team_id'    => $teamId
        ]);

        $AHTCode  = ($totalClaimnant > 0) ? duration_to_seconds($totalAHT) / $totalClaimnant : '00:00:00';
        $AHTIndex = ($totalPages > 0) ? duration_to_seconds($totalAHT) / $totalPages : '00:00:00';

        return response()->json([
            'total_ICD'       => $totalICD,
            'total_pages'     => $totalPages,
            'total_benchmark' => $totalBenchmark,
            'total_claimnant' => $totalClaimnant,
            'aht_code'        => $AHTCode,
            'aht_indexer'     => $AHTIndex
        ]);
    }
}
