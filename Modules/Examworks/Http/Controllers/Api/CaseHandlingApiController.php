<?php

namespace Modules\Examworks\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Examworks\Services\CaseActivityLogService;
use Modules\Examworks\Services\CaseService;
use Modules\Examworks\Services\FieldService;
use Modules\Examworks\Services\ListValueService;
use Modules\Examworks\Services\LobService;
use Modules\Examworks\Services\StatusService;
use Modules\Examworks\Services\UserAuditTrailService;
use Modules\Main\Http\Controllers\Api\BaseCaseHandlingApiController;

class CaseHandlingApiController extends BaseCaseHandlingApiController
{
    public function __construct (CaseService $caseService, FieldService $fieldService, StatusService $statusService, CaseActivityLogService $caseActivityLogService, UserAuditTrailService $userAuditTrailService, LobService $lobService, ListValueService $listValueService)
    {
        $this->caseService            = $caseService;
        $this->fieldService           = $fieldService;
        $this->statusService          = $statusService;
        $this->caseActivityLogService = $caseActivityLogService;
        $this->userAuditTrailService  = $userAuditTrailService;
        $this->lobService             = $lobService;
        $this->listValueService       = $listValueService;
    }

    public function getRework ($lobId, $reference)
    {
        $keyField       = $this->fieldService->getReference($lobId)->html_name;
        $reworkCallback = $this->caseService->getReworkCase([
            'lob_id'    => $lobId,
            'key_field' => $keyField,
            'reference' => $reference
        ]);

        return response()->json(['case_info' => $reworkCallback]);
    }


    public function getCurrentCaseLob (Request $request)
    {
        $mainUserShift   = session('user_from_account')->shift_date;
        $mainUserId      = $request->get('userData')['id'];

        $currentCase = $this->caseService->getCurrentCaseLob($mainUserId,$mainUserShift);

        return response()->json(['current_case_lob' => $currentCase]);
    }
}