<?php

namespace Modules\Examworks\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Examworks\Services\AuxBreakService;
use Modules\Examworks\Services\UserAuditTrailService;
use Modules\Examworks\Services\UserService;
use Modules\Examworks\Services\CaseService;
use Modules\Main\Http\Controllers\Api\BaseAuxBreakApiController;

class AuxBreakApiController extends BaseAuxBreakApiController
{
    public function __construct (AuxBreakService $auxBreakService, UserService $userService, UserAuditTrailService $userAuditTrailService, CaseService $caseService)
    {
        $this->auxBreakService       = $auxBreakService;
        $this->userService           = $userService;
        $this->userAuditTrailService = $userAuditTrailService;
        $this->caseService           = $caseService;
    }
}