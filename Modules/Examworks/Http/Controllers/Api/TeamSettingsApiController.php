<?php

namespace Modules\Examworks\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Modules\Examworks\Services\TeamSettingService;
use Modules\Examworks\Services\LobService;
use Modules\Main\Http\Controllers\Api\BaseTeamSettingsApiController;

class TeamSettingsApiController extends BaseTeamSettingsApiController
{
    public function __construct (TeamSettingService $teamSettingService, LobService $lobService)
    {
        $this->teamSettingService = $teamSettingService;
        $this->lobService         = $lobService;
    }
}