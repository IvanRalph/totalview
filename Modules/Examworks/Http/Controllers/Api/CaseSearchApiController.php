<?php

namespace Modules\Examworks\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Examworks\Services\CaseService;
use Modules\Examworks\Services\FieldService;
use Modules\Examworks\Services\StatusService;
use Modules\Main\Http\Controllers\Api\BaseCaseSearchApiController;

class CaseSearchApiController extends BaseCaseSearchApiController
{
    /**
     * BaseCaseSearchApiController constructor.
     * @param CaseService   $caseService
     * @param FieldService  $fieldService
     * @param StatusService $statusService
     */
    public function __construct (CaseService $caseService, FieldService $fieldService, StatusService $statusService)
    {
        $this->caseService   = $caseService;
        $this->fieldService  = $fieldService;
        $this->statusService = $statusService;
    }
}