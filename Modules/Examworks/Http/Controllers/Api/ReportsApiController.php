<?php

namespace Modules\Examworks\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Modules\Examworks\Services\LobService;
use Modules\Examworks\Services\CaseService;
use Modules\Examworks\Services\FieldService;
use Modules\Examworks\Services\ReportService;
use Modules\Main\Http\Controllers\Api\BaseReportsApiController;

class ReportsApiController extends BaseReportsApiController
{
    public function __construct (LobService $lobService, CaseService $caseService, FieldService $fieldService, ReportService $reportService)
    {
        $this->lobService    = $lobService;
        $this->caseService   = $caseService;
        $this->fieldService  = $fieldService;
        $this->reportService = $reportService;
    }

    public function getVolumeReports (Request $request)
    {
        $lobNumber = $request->get('holdNumber');
        $start     = $request->get('start');
        $end       = $request->get('end');
        $data      = $this->reportService->getVolume($lobNumber, $start, $end);

        return json_encode($data);
    }
}