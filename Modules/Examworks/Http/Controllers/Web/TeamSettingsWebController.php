<?php

namespace Modules\Examworks\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Examworks\Services\TeamSettingService;
use Modules\Examworks\Services\LobService;
use Modules\Examworks\Services\RoleService;
use Modules\Examworks\Services\UserService;
use Modules\Main\Http\Controllers\Web\BaseTeamSettingsWebController;

class TeamSettingsWebController extends BaseTeamSettingsWebController
{
    public function __construct (TeamSettingService $teamSettingService, LobService $lobService, RoleService $roleService, UserService $userService)
    {
        $this->teamSettingService = $teamSettingService;
        $this->lobService         = $lobService;
        $this->roleService        = $roleService;
        $this->userService        = $userService;
    }
}