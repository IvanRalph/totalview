<?php

namespace Modules\Examworks\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Examworks\Services\DashboardService;
use Modules\Examworks\Services\LobService;
use Modules\Examworks\Services\UserService;
use Modules\Examworks\Services\TeamService;
use Modules\Main\Http\Controllers\Web\BaseDashboardWebController;

class DashboardWebController extends BaseDashboardWebController
{
    public function __construct (DashboardService $dashboardService, LobService $lobService, UserService $userService, TeamService $teamService)
    {
        $this->dashboardService = $dashboardService;
        $this->lobService       = $lobService;
        $this->userService      = $userService;
        $this->teamService      = $teamService;
    }
}