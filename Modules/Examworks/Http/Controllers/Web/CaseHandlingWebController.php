<?php

namespace Modules\Examworks\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Examworks\Services\CaseService;
use Modules\Examworks\Services\FieldService;
use Modules\Examworks\Services\ListValueService;
use Modules\Examworks\Services\LobService;
use Modules\Examworks\Services\UserAuditTrailService;
use Modules\Examworks\Services\UserService;
use Modules\Main\Http\Controllers\Web\BaseCaseHandlingWebController;

class CaseHandlingWebController extends BaseCaseHandlingWebController
{
    /**
     * CaseHandlingWebController constructor.
     * @param FieldService          $fieldService
     * @param ListValueService      $listValueService
     * @param CaseService           $caseService
     * @param UserService           $userService
     * @param UserAuditTrailService $userAuditTrailService
     * @param LobService            $lobService
     */
    public function __construct (
        FieldService $fieldService,
        ListValueService $listValueService,
        CaseService $caseService,
        UserService $userService,
        UserAuditTrailService $userAuditTrailService,
        LobService $lobService
    )
    {
        $this->fieldService          = $fieldService;
        $this->listValueService      = $listValueService;
        $this->caseService           = $caseService;
        $this->userService           = $userService;
        $this->userAuditTrailService = $userAuditTrailService;
        $this->lobService            = $lobService;
    }
}