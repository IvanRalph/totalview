<?php

namespace Modules\Examworks\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Examworks\Services\LobService;
use Modules\Main\Http\Controllers\Web\BaseReportsWebController;

class ReportsWebController extends BaseReportsWebController
{
    public function __construct (LobService $lobService)
    {
        $this->lobService = $lobService;
    }
}