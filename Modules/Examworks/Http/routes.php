<?php

Route::group([
    'middleware' => [
        'jwt.webauth',
        'web',
        'revalidate',
        'global.webValidate'
    ],
    'prefix'     => 'examworks',
    'namespace'  => 'Modules\Examworks\Http\Controllers\Web'
], function () {
    Route::get('/', 'CaseHandlingWebController@index');
    Route::get('/team', 'TeamSettingsWebController@index');
    Route::get('/reports', 'ReportsWebController@index');
    Route::get('/dashboard', 'DashboardWebController@index');
    Route::get('/dashboard-fullscreen', 'DashboardWebController@fullscreen');
    Route::get('/start-shift', 'CaseHandlingWebController@startShift');
    Route::get('/end-shift', 'CaseHandlingWebController@endShift');
    Route::get('/confirm-end-shift', 'CaseHandlingWebController@confirmEndShift');
    Route::get('/aux-break', 'AuxBreakWebController@index');
    Route::get('/case-search', 'CaseSearchWebController@index');
});

Route::group(
    [
        'middleware' => [
            'jwt.apiauth',
            'api',
            'revalidate'
        ],
        'prefix'     => 'examworks/api',
        'namespace'  => 'Modules\Examworks\Http\Controllers\Api'
    ], function () {
    // case handling
    Route::get('/tracker-stats/{lobId}', 'CaseHandlingApiController@trackerStats');
    Route::get('/cases/{lobId}', 'CaseHandlingApiController@caseTable');
    Route::get('/case/current/{lobId}', 'CaseHandlingApiController@getCurrent');
    Route::get('/case/{lobId}/{caseId}', 'CaseHandlingApiController@get');
    Route::get('/case-rework/{lobId}/{rework}', 'CaseHandlingApiController@getRework');
    Route::get('/case-currentLob/', 'CaseHandlingApiController@getCurrentCaseLob');
    Route::post('/create-case/{lobId}', 'CaseHandlingApiController@create');
    Route::put('/update-case/{lobId}/{caseId}', 'CaseHandlingApiController@update');
    Route::put('/update-case-fin/{lobId}/{caseId}', 'CaseHandlingApiController@updateFin');
    Route::get('/child-values/{lobId}/{parentId}', 'CaseHandlingApiController@getChildValues');
    Route::delete('/delete-case/{caseId}/{lobId}', 'CaseHandlingApiController@deleteCase');

    // team settings
    Route::get('/team/user-list', 'TeamSettingsApiController@getUserList');
    Route::get('/team/dropdown-list/{lobId}', 'TeamSettingsApiController@getLobDropdownList');
    Route::get('/team/dropdown-items/{valulistID}', 'TeamSettingsApiController@getDropdownItems');
    Route::get('/team', 'TeamSettingsApiController@getTeamListForTable');
    Route::put('/teams-update/{teamLeaderID}/{checkBox}', 'TeamSettingsApiController@updateTeamList');
    Route::put('/teams-add/{teamLeaderID}/{checkBox}', 'TeamSettingsApiController@addTeamList');
    Route::get('/raw-reports/', 'ReportsApiController@getRawReports');
    Route::put('/team-settings/update-team/user/{teamListInfo}', 'TeamSettingsApiController@updateTeamUser');
    Route::put('/team-settings/update-team/schedule/{id}/{formData}', 'TeamSettingsApiController@updateTeamSchedule');
    Route::put('/team-settings/update-team/lob/{id}/{team}', 'TeamSettingsApiController@updateUserLob');
    Route::put('/team-settings/update-user/lob/{id}/{lob}', 'TeamSettingsApiController@updateUserLob');
    Route::put('/team-settings/valuelist-update', 'TeamSettingsApiController@updateValuelistItem');
    Route::post('/team-settings/valuelist-add/{lobId}/{fieldid}', 'TeamSettingsApiController@addValuelistItem');
    Route::put('/team-settings/valuelist-update/bulk', 'TeamSettingsApiController@updateValuelistItemBulk');


    // aux break
    Route::post('/aux-break/start', 'AuxBreakApiController@start');
    Route::put('/aux-break/stop/{auxId}', 'AuxBreakApiController@stop');
    Route::get('/aux-break/current', 'AuxBreakApiController@getCurrent');

    // dashboard
    Route::get('/dashboard/metrics/{lobId}/{team}', 'DashboardApiController@getMetrics');
    Route::get('/dashboard/users/{lobId}/{team}', 'DashboardApiController@userList');
    Route::get('/dashboard/user-activity/{userId}', 'DashboardApiController@userActivity');

    // case search
    Route::get('/case-search/{lobId}', 'CaseSearchApiController@get');

    //reports
    Route::get('/reports/hourly-list/', 'ReportsApiController@getHourlyReports');
    Route::get('/breakreports/', 'ReportsApiController@getBreaksReports');
    Route::get('/reports/raw-reports-list/', 'ReportsApiController@getRawReports');
    Route::get('/reports/volume-reports-list/', 'ReportsApiController@getVolumeReports');
    Route::get('/reports/consolidated-reports-list/', 'ReportsApiController@getConsolidatedReports');
});