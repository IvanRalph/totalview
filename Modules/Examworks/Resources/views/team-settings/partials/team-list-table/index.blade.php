<table id="team-management-teams-table"
       class="table table-striped table-bordered table-hover"
       cellspacing="0"
       width="100%">
    <thead>
    <tr role="row">
        <th>Name</th>
        <th>Team Leader</th>
        <th>Kickout Time</th>
        <th>Action</th>
    </tr>
    </thead>
</table>

@include('examworks::team-settings.partials.team-list-table.team-setting-modal', $data)

@section('module-scripts')
    {!! Html::script('js/module/examworks/team-settings.js') !!}

@stop
