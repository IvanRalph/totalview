<div class="row clearfix">
  <div class="col-md-3 col-xs-12">
      <div class="panel panel-default">
          <div class="panel-body">
              <form id="frmListSettings" name="frmListSettings"
                    class="form" method="get">
                  <div class="form-group">
                      <label class="control-label">LOB:</label>
                      <select id="ddl-lob" class="form-control" name="ddl_lob" required="required">
                        <option value="">-- Select Value list --</option>
                        @foreach($getLobList as $lob)
                           <option name="lob_name" id="ddl-lob" value="{{ $lob->id }}">{{ $lob->name }}</option>
                        @endforeach
                      </select>
                  </div>
                  <div class="form-group">
                      <label class="control-label">Value List Name</label>
                        <select id="ddl-valuelistName" class="form-control" name="ddl_valuelistName" required="required">
                          <option value="">
                            -- Select Value list --
                          </option>
                        </select>
                  </div>
                  <div class="form-group clearfix">
                     <button class=" text-center btn btn-sm btn-info pull-right" id="btnViewlist" name="btn_viewlist" type="button" value="1"><i class="fa fa-search"></i> Display List</button>
                  </div>

              </form>
          </div>
      </div>
  </div>
  <div class="col-md-9 col-xs-12">


      <div class="table-responsive">
         <div class="checkbox checkbox-danger">
            <input id="chk-all" type="checkbox"> <label for="checkbox6">Check All</label> 
                 <button class=" text-center btn btn-sm btn-primary pull-right" data-target="#addValuelistModal" data-toggle="modal" id="btnAddlist" name="btn_addlist" type="button"><i class="fa fa-plus"></i> Add New Item</button>
         </div>

        <table class="table table-striped table-bordered table-hover" id = "team-management-valuelist-table">
          <thead>
            <tr>
                <th class="text-center">
                    <div class="checkbox checkbox-primary">
                        <button id="chk-all" class="btn btn-white btn-xs" style="margin-right: 12px;"><i
                                    class="fa fa-check-square-o"></i></button>
                        <button id="switch-status" class="btn btn-white btn-xs" style="margin-right: 12px;"><i
                                    class="fa fa-exchange"></i></button>
                    </div>
                </th>
                  <th>
                    Item Name
                  </th>
                  <th>
                    Status
                  </th>
                   <th>
                    Action
                  </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
  </div>
</div>

@include('examworks::team-settings.partials.list-value-table.list-value-modal', $data)

@section('module-scripts')
    {!! Html::script('js/module/examworks/team-settings.js') !!}
@stop
