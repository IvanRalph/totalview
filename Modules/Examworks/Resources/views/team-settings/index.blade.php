@extends('examworks::layouts.master')

@section('content')

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class = "ibox-title">
                         <h2>Settings</h2>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                           
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a data-toggle="tab" href="#userSchedTab">
                                        <i class="fa fa-user"></i>User List
                                    </a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#userTeamTab">
                                        <i class="fa fa-group"></i>Team Info
                                    </a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#listValueTab">
                                        <i class="fa fa-list"></i>Value List Settings
                                    </a>
                                </li>
                            </ul>
                            <p></p>

                            <div class="tab-content">
                                <div id="userSchedTab" class="tab-pane active">
                                    <div class="full-height-scroll">
                                        @include('examworks::team-settings.partials.user-list-table.index')
                                    </div>
                                </div>

                                <div id="userTeamTab" class="tab-pane ">
                                    <div class="full-height-scroll">
                                        @include('examworks::team-settings.partials.team-list-table.index')
                                    </div>
                                </div>
                                <div id="listValueTab" class="tab-pane ">
                                    <div class="full-height-scroll">
                                        @include('examworks::team-settings.partials.list-value-table.index', $data)
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
