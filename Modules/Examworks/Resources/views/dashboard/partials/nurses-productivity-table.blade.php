<div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Nurse Productivity Table</h5>
            <a href="" class="btn btn-primary btn-xs pull-right fullScreenBtn">Full Screen</a>
        </div>
        <div class="ibox-content">
            <form role="form">
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label>LOB</label>
                        <select id="slct-lob" class="form-control">
                            @foreach($getLobList as $lobList)
                                <option value="{{ $lobList->id }}">{{ $lobList->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-sm-6">
                        <label>Team</label>
                        <select id="slct-team" class="form-control">
                            @foreach($getTeamList as $teamList)
                                <option value="{{ $teamList->ID }}" {{ $teamList->ID == session('user_from_account')->team ? 'selected' : '' }}>{{ $teamList->description }}</option>
                            @endforeach
                            <option value="all">All</option>
                        </select>
                    </div>
                </div>
                {{--<button id="btn-generate-user-list" class="btn btn-sm btn-primary btn-block" type="button">GENERATE</button>--}}
            </form>
        </div>
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
            <table id="productivity-table" class="table table-bordered table-hover" cellspacing="0" width="100%">
                <thead>
                <tr role="row">
                    <th>Name</th>
                    <th>Team</th>
                    <th>Current Shift</th>
                    <th>Shift Schedule</th>
                    <th>Completed Cases</th>
                    <th>On Hold Cases</th>
                    <th id="head-1">No. of ICD</th>
                    <th id="head-2">Claimnant</th>
                    <th>Avg Handling Time</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tfoot>
                <tr class="text-success">
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
