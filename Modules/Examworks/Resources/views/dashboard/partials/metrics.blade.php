<div class="col-lg-4 col-md-4">
    <div class="ibox float-e-margins metric-panel">
        <div class="ibox-title">
            <span class="label label-success pull-right">Today</span>
            <h5 id="label-1">No. of ICD</h5>
        </div>
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
            <h1 class="no-margins"><span class="totalICD">0</span></h1>
            <small>Page(s)</small>
        </div>
    </div>
</div>

<div class="col-lg-4 col-md-4">
    <div class="ibox float-e-margins metric-panel">
        <div class="ibox-title">
            <span class="label label-success pull-right">Today</span>
            <h5 id="label-2">Claimnant</h5>
        </div>
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
            <h1 class="no-margins"><span class="totalClaimnant">0</span></h1>
            <small>Claims</small>
        </div>
    </div>
</div>

<div class="col-lg-4 col-md-4">
    <div class="ibox float-e-margins metric-panel">
        <div class="ibox-title">
            <span class="label label-success pull-right">Today</span>
            <h5>AHT</h5>
        </div>
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
            <h1 class="no-margins"><span class="AHT">00:00:00</span></h1>
            <small>Handling Time</small>
        </div>
    </div>
</div>
