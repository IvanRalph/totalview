@extends('examworks::layouts.master')

@section('content')
    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="row">
                <div class="ibox float-e-margins">
                    <div class="ibox-title"><h5>Case Search</h5></div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-4 b-r">
                                <form role="form">
                                    <div class="form-group col-sm-12">
                                        <label>LOB</label>
                                        <select id="slct-lob" class="form-control">
                                            @foreach($lobs as $lob)
                                                <option value="{{ $lob->id }}">{{ $lob->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label>Start Date (MNL)</label>
                                        <input type="text" id="start-date" class="form-control datepicker">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label>End Date (MNL)</label>
                                        <input type="text" id="end-date" class="form-control datepicker">
                                    </div>
                                    <div class="form-group col-sm-12 ibox-content">
                                        <button id="case-search-btn" class="btn btn-primary btn-block" type="button">
                                            <strong>Generate</strong></button>
                                    </div>
                                </form>
                            </div>
                            <div id="search-case-table-div " class="col-sm-8 b-r table-responsive"
                                 style="overflow-x: auto;">
                                <table id="search-case-table" class="table table-responsive table-bordered dataTable">
                                    <thead>
                                    <tr>
                                        <th>Shift Date</th>
                                        <th>Data</th>
                                        <th>Status</th>
                                        <th>Created</th>
                                        <th>Update</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('module-scripts')
    {!! Html::script('js/module/examworks/constant.js') !!}
    {!! Html::script('js/module/examworks/case-search.js') !!}
@stop