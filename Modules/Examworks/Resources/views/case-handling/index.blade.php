@extends('examworks::layouts.master')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 id="page-header-title" class="bindPageTitleBreadCrumbs"></h2>
        </div>
        <div class="col-lg-2"></div>
    </div>

    <div class="wrapper wrapper-content">
        <?php
        $startShiftDisplayStatus = 'none';
        $shiftEndedDisplayStatus = 'none';
        $caseFormDisplayStatus = 'none';
        ?>

        @if(!$userInfo->shift_start)
            <?php $startShiftDisplayStatus = 'block'; ?>
        @elseif ($userInfo->shift_start && $userInfo->shift_end)
            <?php $shiftEndedDisplayStatus = 'block'; ?>
        @else
            <?php $caseFormDisplayStatus = 'block'; ?>
        @endif

        <div class="col-lg-offset-4 col-lg-4" style="display: {{ $startShiftDisplayStatus }}">
            <a class="btn btn-block btn-info btn-lg"
               href="/{{ session('selected_account')->route }}/start-shift{{ '?token='.app('request')->get('token') }}">Start
                                                                                                                        Shift</a>
        </div>

        <div class="row" style="display: {{ $caseFormDisplayStatus }}">
            <div class="col-lg-7">
                @include('examworks::case-handling.partials.case-form')
            </div>

            <div class="col-lg-5">
                @include('examworks::case-handling.partials.tracker-timer')
                @include('examworks::case-handling.partials.tracker-case-table')
            </div>
        </div>

        <div class="middle-box text-center animated fadeInRightBig" style="display: {{ $shiftEndedDisplayStatus }}">
            <h3 class="font-bold">Your shift already ended.</h3>
        </div>
    </div>

@stop

@section('module-scripts')
    {!! Html::script('js/module/examworks/constant.js') !!}
    {!! Html::script('js/module/examworks/case-handling.js') !!}
    {!! Html::script('js/module/examworks/case-timer.js') !!}
@stop