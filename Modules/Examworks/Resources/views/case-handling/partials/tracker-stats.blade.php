<div class="ibox float-e-margins">
    <div class="ibox-title text-center">
        <h5>Tracker Stats</h5>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-12">
                <div id="caseStats" data-timer="0">
                    @foreach($userLobs as $key => $lob)

                        <div class="form-group">
                            <div class="col-lg-3 col-md-6 col-sm-6 no-margins">
                                <div class="ibox float-e-margins border-left-right border-bottom">
                                    <div class="ibox-title">
                                        <h4 class="text-center">CPH / Target</h4>
                                    </div>
                                    <div class="ibox-content">
                                        <h2 id="cph-{{ $lob->id }}"
                                            class="no-margins text-info text-center">{{ $trackerStats[$lob->id]['cph'] }}
                                            / {{ $trackerStats[$lob->id]['target'] }}</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6  no-margins">
                                <div class="ibox float-e-margins border-left-right border-bottom">
                                    <div class="ibox-title">
                                        <h4 class="text-center">Handled Cases</h4>
                                    </div>
                                    <div class="ibox-content">

                                        <h2 id="handled-{{ $lob->id }}"
                                            class="no-margins text-info text-center">{{ $trackerStats[$lob->id]['total_completed_case'] }}</h2>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6  no-margins">
                                <div class="ibox float-e-margins border-left-right border-bottom">
                                    <div class="ibox-title">
                                        <h4 class="text-center">Prod Cases</h4>
                                    </div>
                                    <div class="ibox-content">

                                        <h2 id="prod-{{ $lob->id }}"
                                            class="no-margins text-info text-center">{{ $trackerStats[$lob->id]['total_prod_cases'] }}</h2>

                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 col-sm-6 m-t-n-xs">
                                <div class="widget style1 {{$trackerStats[$lob->id]['break_class'] }}-bg">
                                    <div class="row no-margin">
                                        <div class="col-3 ">
                                            <i class="fa fa-coffee fa-2x"></i>
                                        </div>
                                        <div class="col-9 text-right m-r ">
                                            <span> Total Breaks </span>
                                            <h2 class="font-bold"
                                                id="break-{{ $lob->id }}">{{ $trackerStats[$lob->id]['breaks'] }}</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>