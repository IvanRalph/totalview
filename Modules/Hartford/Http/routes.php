<?php

Route::group([
    'middleware' => [
        'jwt.webauth',
        'web'
    ],
    'prefix'     => 'hartford',
    'namespace'  => 'Modules\Hartford\Http\Controllers\Web'
], function () {
    Route::get('/', 'CaseHandlingWebController@index');
    Route::get('/team', 'TeamSettingsWebController@index');
    Route::get('/reports', 'ReportsWebController@index');
    Route::get('/dashboard', 'DashboardWebController@index');
    Route::get('/start-shift', 'CaseHandlingWebController@startShift');
    Route::get('/end-shift', 'CaseHandlingWebController@endShift');
    Route::get('/aux-break', 'AuxBreakWebController@index');
    Route::get('/case-search', 'CaseSearchWebController@index');
});

Route::group(
    [
        'middleware' => [
            'jwt.apiauth',
            'api'
        ],
        'prefix'     => 'hartford/api',
        'namespace'  => 'Modules\Hartford\Http\Controllers\Api'
    ], function () {
    // case handling
    Route::get('/tracker-stats/{lobId}', 'CaseHandlingApiController@trackerStats');
    Route::get('/cases/{lobId}', 'CaseHandlingApiController@caseTable');
    Route::get('/case/current/{lobId}', 'CaseHandlingApiController@getCurrent');
    Route::get('/case/{lobId}/{caseId}', 'CaseHandlingApiController@get');
    Route::post('/create-case/{lobId}', 'CaseHandlingApiController@create');
    Route::put('/update-case/{lobId}/{caseId}', 'CaseHandlingApiController@update');
    Route::get('/child-values/{lobId}/{parentId}', 'CaseHandlingApiController@getChildValues');
    Route::get('/syntax-code/{measure}', 'CaseHandlingApiController@getSyntaxCodes');
    Route::post('/create-submeasure/{lobId}', 'CaseHandlingApiController@createSubMeasure');
    Route::post('/create-submeasureAuditor/{lobId}', 'CaseHandlingApiController@createSubMeasureAuditor');
    Route::get('/submeasure/{lobId}/{caseId}', 'CaseHandlingApiController@getSubMeasure');
    Route::get('/reason/', 'CaseHandlingApiController@getReason');
    Route::put('/delete-submeasure/{id}/{lobId}', 'CaseHandlingApiController@deleteSubmeasure');

    // team settings
    Route::get('/team/user-list', 'TeamSettingsApiController@getUserList');
    Route::get('/team-settings/team', 'TeamSettingsApiController@getTeamList');
    Route::put('/team-settings/team-update/{teamLeaderID}/{checkBox}', 'TeamSettingsApiController@updateTeamList'); // team update
    Route::get('/raw-reports/', 'ReportsApiController@getLobList');
    Route::put('/team-settings/update-team/user/{teamListInfo}', 'TeamSettingsApiController@updateTeamUser');
    Route::put('/team-settings/update-team/schedule/{id}/{formData}', 'TeamSettingsApiController@updateTeamSchedule');
    Route::put('/team-settings/update-team/lob/{id}/{team}', 'TeamSettingsApiController@updateUserLob');
    Route::put('/team-settings/update-user/lob/{id}/{lob}', 'TeamSettingsApiController@updateUserLob');

    // aux break
    Route::post('/aux-break/start', 'AuxBreakApiController@start');
    Route::put('/aux-break/stop/{auxId}', 'AuxBreakApiController@stop');
    Route::get('/aux-break/current', 'AuxBreakApiController@getCurrent');

    // dashboard
    Route::get('/dashboard/metrics/{lobId}', 'DashboardApiController@getMetrics');
    Route::get('/dashboard/users/{lobId}', 'DashboardApiController@userList');
    Route::get('/dashboard/user-activity/{userId}', 'DashboardApiController@userActivity');

    // case search
    Route::get('/case-search/{lobId}', 'CaseSearchApiController@get');

    //reports
    Route::get('/reports/hourly-reports-list/', 'ReportsApiController@getHourlyReports');
    Route::get('/reports/raw-reports-list/', 'ReportsApiController@getRawReports');
});
