<?php

namespace Modules\Hartford\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Hartford\Services\AuxBreakService;

class AuxBreakWebController extends Controller
{
    private $auxBreakService;

    public function __construct (AuxBreakService $auxBreakService)
    {
        $this->auxBreakService = $auxBreakService;
    }

    public function index ()
    {
        $auxTypes = $this->auxBreakService->getAuxTypes();

        return view('hartford::aux-break.index', ['aux_types' => $auxTypes]);
    }
}