<?php

namespace Modules\Hartford\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Hartford\Services\LobService;

class ReportsWebController extends Controller
{
    private $lobService;

    public function __construct (LobService $lobService)
    {
        $this->lobService = $lobService;
    }

    public function index ()
    {
        $data['getLobList'] = $this->lobService->getLobList();

        return view('hartford::reports.index', $data);
    }
}




