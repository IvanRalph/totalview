<?php

namespace Modules\Hartford\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Hartford\Services\DashboardService;
use Modules\Hartford\Services\LobService;

class DashboardWebController extends Controller
{
    private $dashboardService;

    public function __construct (DashboardService $dashboardService, LobService $lobService)
    {
        $this->dashboardService = $dashboardService;
        $this->lobService       = $lobService;
    }

    public function index ()
    {
        $data['data']['getLobList'] = $this->lobService->getLobList();

        return view('hartford::dashboard.index', $data);
    }
}