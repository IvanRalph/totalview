<?php

namespace Modules\Hartford\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Hartford\Services\FieldService;
use Modules\Hartford\Services\LobService;

class CaseSearchWebController extends Controller
{
    /**
     * @var LobService
     */
    private $lobService;

    /**
     * @var FieldService
     */
    private $fieldService;

    public function __construct (LobService $lobService, FieldService $fieldService)
    {
        $this->lobService   = $lobService;
        $this->fieldService = $fieldService;
    }

    public function index ()
    {
        $lobs = $this->lobService->getLobList();

        return view('hartford::case-search.index', ['lobs' => $lobs]);
    }
}