<?php

namespace Modules\Hartford\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Hartford\Services\CaseService;
use Modules\Hartford\Services\FieldService;
use Modules\Hartford\Services\StatusService;

class CaseSearchApiController extends Controller
{
    /**
     * @var CaseService
     */
    private $caseService;

    /**
     * @var FieldService
     */
    private $fieldService;

    /**
     * @var StatusService
     */
    private $statusService;

    /**
     * CaseSearchApiController constructor.
     * @param CaseService   $caseService
     * @param FieldService  $fieldService
     * @param StatusService $statusService
     */
    public function __construct (CaseService $caseService, FieldService $fieldService, StatusService $statusService)
    {
        $this->caseService   = $caseService;
        $this->fieldService  = $fieldService;
        $this->statusService = $statusService;
    }

    public function get (Request $request, $lobId)
    {
        $mainUser = $request->get('userData');
        $data     = $request->get('data');
        $columns  = [
            [
                'data' => 'shift_date',
                'name' => 'Shift Date'
            ]
        ];

        $filter = [
            [
                'column'     => 'created_at',
                'comparison' => '>=',
                'value'      => $data['start_date'] . '00:00:00'
            ],
            [
                'column'     => 'created_at',
                'comparison' => '<=',
                'value'      => $data['end_date'] . '23:59:59'
            ]
        ];

        $fields    = $this->fieldService->getByLobId($lobId);
        $userCases = $this->caseService->getByUser($lobId, $mainUser['id'], $filter);
        $statuses  = $this->statusService->getAll()->toArray();
        $statusIds = array_column($statuses, 'id');

        foreach ($userCases as $key => $case) {
            $statusKey    = array_search($case->status_id, $statusIds);
            $status       = $statuses[$statusKey];
            $case->status = generateStatusDisplay($status);

            $userCases[$key] = $case;
        }

        foreach ($fields as $field) {
            $columns[] = [
                'data' => $field->html_name,
                'name' => $field->name
            ];
        }

        $columns[] = [
            'data' => 'duration',
            'name' => 'Duration'
        ];
        $columns[] = [
            'data' => 'status',
            'name' => 'Status'
        ];
        $columns[] = [
            'data' => 'created_at',
            'name' => 'Created'
        ];
        $columns[] = [
            'data' => 'updated_at',
            'name' => 'Updated'
        ];

        return response()->json([
            'data'    => $userCases,
            'columns' => $columns
        ]);
    }
}