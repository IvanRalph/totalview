<?php

namespace Modules\Hartford\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Hartford\Services\CaseService;
use Modules\Hartford\Services\StatusService;
use Modules\Hartford\Services\TeamService;
use Modules\Hartford\Services\UserAuditTrailService;
use Modules\Hartford\Services\UserService;

class DashboardApiController extends Controller
{
    /**
     * @var CaseService
     */
    private $caseService;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var UserAuditTrailService
     */
    private $userAuditTrailService;

    /**
     * @var TeamService
     */
    private $teamService;

    /**
     * DashboardApiController constructor.
     * @param CaseService           $caseService
     * @param UserService           $userService
     * @param UserAuditTrailService $userAuditTrailService
     * @param TeamService           $teamService
     */
    public function __construct (
        CaseService $caseService,
        UserService $userService,
        UserAuditTrailService $userAuditTrailService,
        TeamService $teamService
    )
    {
        $this->caseService           = $caseService;
        $this->userService           = $userService;
        $this->userAuditTrailService = $userAuditTrailService;
        $this->teamService           = $teamService;
    }

    public function getMetrics (Request $request, $lobId)
    {
        $mainUser        = $request->get('userData');
        $userFromAccount = $this->userService->getByMainUserId($mainUser['id']);

        $completedCount = $this->caseService->countByStatusId(
            $lobId,
            $userFromAccount->shift_date,
            [
                StatusService::COMPLETED_STATUS_ID,
                StatusService::EDIT_COMPLETED_STATUS_ID
            ]
        );

        $pendingCount = $this->caseService->countByStatusId(
            $lobId,
            $userFromAccount->shift_date,
            StatusService::HOLD_STATUS_ID
        );

        $allUserCount       = $this->userService->countAll($lobId);
        $availableUserCount = $this->userService->countAvailable($lobId);

        return response()->json([
            'completed_count'      => $completedCount,
            'pending_count'        => $pendingCount,
            'all_user_count'       => $allUserCount,
            'available_user_count' => $availableUserCount
        ]);
    }

    public function userList ($lobId)
    {
        $users = $this->userService->getAllByLobId($lobId);

        foreach ($users as $key => $user) {
            $trackerStats = $this->caseService->getUserTrackerStats($lobId, $user->main_user_id, $user->shift_date);

            $user->total_completed_cases = $trackerStats['total_completed_case'];
            $user->aht                   = $trackerStats['aht'];
            $user->total_duration        = $trackerStats['total_duration'];
            $user->name                  = $user->lastname . ', ' . $user->firstname;
            $user->total_pending_cases   = $this->caseService->getUserPending(
                $lobId,
                $user->main_user_id,
                $user->shift_date
            )->count();

            $userSchedule         = $this->userService->getShiftSchedule($user->main_user_id);
            $user->shift_schedule = 'Not Available';
            if ($userSchedule) {
                $user->shift_schedule = $userSchedule->start_shift . ' - ' . $userSchedule->end_shift;
            }

            $teamLeaders     = $this->teamService->getLeader($user->team);
            $teamLeaderNames = [];
            foreach ($teamLeaders as $teamLeader) {
                $teamLeaderNames[] = $teamLeader->firstname . ' ' . $teamLeader->lastname;
            }

            $user->team_leaders = implode(', ', $teamLeaderNames);

            $users[$key] = $user;
        }

        return response()->json(['data' => $users]);
    }

    public function userActivity ($userId)
    {
        $userActivityForDisplay = [];

        $user           = $this->userService->getByMainUserId($userId);
        $userActivities = $this->userAuditTrailService->getByUserId($userId, $user->shift_date);

        foreach ($userActivities as $activity) {
            $userActivityForDisplay[] = $this->userAuditTrailService->display($activity);
        }

        return response()->json(['activities' => $userActivityForDisplay]);
    }
}
