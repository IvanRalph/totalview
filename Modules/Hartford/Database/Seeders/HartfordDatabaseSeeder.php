<?php

namespace Modules\Hartford\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class HartfordDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        Model::unguard();
        // $this->call("OthersTableSeeder");
    }
}
