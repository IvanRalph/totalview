<?php

namespace Modules\Hartford\Services;

class FieldService extends BaseService
{
    private $fields = [];

    public function getByLobId ($lobId)
    {
        if (isset($this->fields[$lobId])) {
            return $this->fields[$lobId];
        }

        $this->fields[$lobId] = $this->db->table('field')
            ->where('lob_id', $lobId)
            ->orderBy('sequence', 'asc')
            ->get();

        return $this->fields[$lobId];
    }

    public function getReference ($lobId)
    {
        $fields = $this->getByLobId($lobId);

        foreach ($fields as $field) {
            if ($field->is_reference) {
                return $field;
            }
        }

        return false;
    }
}