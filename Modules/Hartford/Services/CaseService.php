<?php

namespace Modules\Hartford\Services;

class CaseService extends BaseService
{
    const TABLE_NAME = 'case';
    const TABLE_NAME_SUB = 'submeasure';
    const LAST_CASE_HOUR_INTERVAL = 5;

    /**
     * @var CaseActivityLogService
     */
    private $caseActivityLogService;

    /**
     * @var StatusService
     */
    private $statusService;

    /**
     * @var FieldService
     */
    private $fieldService;

    public function __construct (
        CaseActivityLogService $caseActivityLogService,
        StatusService $statusService,
        FieldService $fieldService
    )
    {
        parent::__construct();
        $this->caseActivityLogService = $caseActivityLogService;
        $this->statusService          = $statusService;
        $this->fieldService           = $fieldService;
    }

    public function getUserCompleted ($lobId, $userId, $shiftDate)
    {
        return $this->db->table(self::TABLE_NAME . $lobId)
            ->where('user_id', $userId)
            ->where('shift_date', $shiftDate)
            ->whereIn('status_id', [
                StatusService::COMPLETED_STATUS_ID,
                StatusService::EDIT_COMPLETED_STATUS_ID
            ])
            ->get();
    }

    public function getUserSubmeasure ($lobId, $userId, $shiftDate)
    {
        $submeasuretable = 'submeasure' . $lobId;
        $casetable       = 'case' . $lobId;

        $dbquery = $this->db->table($casetable)
            ->Join($submeasuretable, '' . $submeasuretable . '.case_id', '=', "$casetable.id")
            ->where('' . $casetable . '.user_id', $userId)
            ->where('' . $casetable . '.shift_date', $shiftDate)
            ->whereIn('' . $casetable . '.status_id', [
                StatusService::COMPLETED_STATUS_ID,
                StatusService::EDIT_COMPLETED_STATUS_ID
            ]);

        return $dbquery->count();
    }

    public function getUserGapsClosed ($lobId, $userId, $shiftDate)
    {
        return $this->db->table(self::TABLE_NAME . $lobId)
            ->where('user_id', $userId)
            ->where('shift_date', $shiftDate)
            ->sum('gaps_closed');
    }

    public function getUserPending ($lobId, $userId, $shiftDate)
    {
        return $this->db->query(self::TABLE_NAME . $lobId)
            ->where('user_id', $userId)
            ->where('shift_date', $shiftDate)
            ->where('status_id', StatusService::HOLD_STATUS_ID)
            ->get();
    }

    public function getUserCaseTable ($lobId, $userId, $shiftDate)
    {
        return $this->db->table(self::TABLE_NAME . $lobId)
            ->where('user_id', $userId)
            ->where(function ($query) use ($shiftDate) {
                $query->where('shift_date', $shiftDate)
                    ->orWhere('status_id', StatusService::HOLD_STATUS_ID);
            })
            ->get();
    }

    public function getById ($lobId, $id)
    {
        return $this->db->table(self::TABLE_NAME . $lobId)
            ->where('id', $id)
            ->first();
    }

    public function getUserTrackerStats ($lobId, $userId, $shiftDate)
    {
        $totalDuration = 0;
        $ahtSec        = 0;

        $cases              = $this->getUserCompleted($lobId, $userId, $shiftDate);
        $subcases           = (($lobId == 3) ? $this->getUserGapsClosed($lobId, $userId, $shiftDate) : $this->getUserSubmeasure($lobId, $userId, $shiftDate));
        $totalCompletedCase = count($cases);

        if ($totalCompletedCase > 0) {
            foreach ($cases as $case) {
                $totalDuration += duration_to_seconds($case->duration);
            }

            //$ahtSec = $totalDuration / $totalCompletedCase;
            $ahtSec = $totalDuration;
        }

        return [
            'total_completed_case'   => $totalCompletedCase,
            'total_submeasure_count' => $subcases,
            'aht'                    => gmdate('H:i:s', $ahtSec),
            'total_duration'         => gmdate('H:i:s', $totalDuration)
        ];
    }

    public function create ($lobId, $userId, $shiftDate, $data, $fields)
    {
        $caseTable        = self::TABLE_NAME . $lobId;
        $actionId         = 1;
        $customFieldsData = $this->establishCustomFieldsData($fields, $data);
        $statusId         = $this->statusService->getStatusIdByActionId($actionId);

        $mainFieldsData = [
            'shift_date' => $shiftDate,
            'user_id'    => $userId,
            'status_id'  => $statusId,
            'duration'   => $data['duration'],
            'created_at' => $data['created_at']
        ];

        $this->db->beginTransaction();

        try {
            $caseId = $this->db->table($caseTable)->insertGetId(array_merge($mainFieldsData, $customFieldsData));
            $this->caseActivityLogService->log($lobId, $caseId, $userId, $shiftDate, $actionId, $statusId, $data['created_at']);

            $this->db->commit();

            return $caseId;
        } catch (\Exception $e) {
            $this->db->rollBack();
        }
    }

    public function createSubMeasure ($lobId, $userId, $data)
    {
        $caseTable = self::TABLE_NAME_SUB . $lobId;

        if ($lobId == 4) { //Hedis

            $fieldData = [
                'case_id'        => $data['caseId'],
                'submeasure'     => $data['submeasure'],
                'syntax_outcome' => $data['measure_syntax'],
                'syntax_code'    => $data['measure_code'],
                'status'         => $data['status'],
                'remarks'        => $data['remarks'],
                'reason'         => $data['reason'],
                'started_at'     => date('Y-m-d H:i:s'),
                'ended_at'       => null,
                'created_by'     => $userId
            ];
        } else {
            $fieldData = [
                'case_id'        => $data['caseId'],
                'submeasure'     => $data['submeasure'],
                'syntax_outcome' => $data['measure_syntax'],
                'syntax_code'    => $data['measure_code'],
                'measure'        => $data['measure'],
                'status'         => $data['status'],
                'remarks'        => $data['remarks'],
                'reason'         => $data['reason'],
                'started_at'     => date('Y-m-d H:i:s'),
                'ended_at'       => null,
                'created_by'     => $userId
            ];
        }

        $this->db->beginTransaction();

        try {
            $caseId = $this->db->table($caseTable)->insertGetId($fieldData);

            $this->db->commit();

            return $caseId;
        } catch (\Exception $e) {
            $this->db->rollBack();
        }
    }

    public function createSubMeasureAuditor ($lobId, $userId, $data)
    {
        $caseTable = self::TABLE_NAME_SUB . $lobId;

        $fieldData = [
            'case_id'    => $data['caseId'],
            'submeasure' => $data['submeasure'],
            'status'     => $data['status'],
            'remarks'    => $data['remarks'],
            'started_at' => date('Y-m-d H:i:s'),
            'ended_at'   => null,
            'created_by' => $userId
        ];

        $this->db->beginTransaction();

        try {
            $caseId = $this->db->table($caseTable)->insertGetId($fieldData);

            $this->db->commit();

            return $caseId;
        } catch (\Exception $e) {
            $this->db->rollBack();
        }
    }

    public function update ($lobId, $caseId, $data, $fields)
    {
        $caseTable        = self::TABLE_NAME . $lobId;
        $statusId         = $this->statusService->getStatusIdByActionId($data['action_id']);
        $customFieldsData = $this->establishCustomFieldsData($fields, $data, true);
        $mainFieldsData   = [
            'status_id'  => $statusId,
            'duration'   => $data['duration'],
            'updated_at' => $data['updated_at']
        ];

        $this->db->beginTransaction();

        try {
            $caseInfo = $this->db->table($caseTable)
                ->where('id', $caseId)
                ->first();

            $updateSuccess = $this->db->table($caseTable)
                ->where('id', $caseId)
                ->update(array_merge($customFieldsData, $mainFieldsData));

            $this->caseActivityLogService->log(
                $lobId,
                $caseId,
                $caseInfo->user_id,
                $caseInfo->shift_date,
                $data['action_id'],
                $statusId,
                $data['updated_at']
            );

            $this->db->commit();

            return $updateSuccess;
        } catch (\Exception $e) {
            $this->db->rollBack();
        }
    }

    public function updateStatus ($lobId, $caseId, $statusId, $currentTime)
    {
        $caseTable = self::TABLE_NAME . $lobId;

        $mainFieldsData = [
            'status_id'  => $statusId,
            'updated_at' => $currentTime
        ];
        $updateSuccess  = $this->db->table($caseTable)
            ->where('id', $caseId)
            ->update($mainFieldsData);

        return $updateSuccess;
    }

    public function deleteSubmeasure ($Id, $lobId)
    {
        $table = self::TABLE_NAME_SUB . $lobId;

        $this->db->table($table)
            ->where('ID', $Id)
            ->delete();
    }

    public function getCases ($rawNumber, $start, $end, $timezone)
    {
        if ($timezone == '1') {
            $hour = '14';
        } else {
            $hour = '0';
        }
        $selectLob = $this->fieldService->getByLobId($rawNumber);
        $fields    = [];
        foreach ($selectLob as $key) {
            $fields[] = ([
                'html_name' => $key->html_name,
                'name'      => $key->name,
            ]);
        }

        for ($i = 0; $i <= count($selectLob) - 1; $i++) {
            $field[] = $fields[$i]['html_name'] . ' as ' . $fields[$i]['name'];
        }

        $table    = 'case' . $rawNumber;
        $column[] = $table . "." . 'shift_date AS Shift Date';
        $column[] = 'duration AS Duration';
        $column[] = \DB::raw("CONCAT(user.firstname,' ',user.lastname) AS Name");
        $column[] = 'status.name AS Status';
        $column[] = 'duration AS Duration';
        $date[]   = \DB::raw("DATE_FORMAT(created_at, DATE_SUB(created_at, INTERVAL '$hour' HOUR)) as Created");
        $date[]   = \DB::raw("DATE_FORMAT(updated_at, DATE_SUB(updated_at, INTERVAL '$hour' HOUR)) as Updated");
        $columns  = array_merge($column, $field, $date);

        $res = $this->db->table($table)
            ->leftJoin('user', 'user.main_user_id', '=', "$table.user_id")
            ->leftJoin('status', "$table.status_id", '=', 'status.id')
            ->whereBetween("$table.shift_date", [
                $start,
                $end
            ])
            ->distinct()
            ->select($columns)
            ->get();

        return $res;
    }

    public function getCurrentCase ($lobId, $userId)
    {
        $caseTable = self::TABLE_NAME . $lobId;
        $statusIds = StatusService::ONGOING_STATUS_ID;

        return $this->db->table($caseTable)
            ->where('user_id', $userId)
            ->whereIn('status_id', $statusIds)
            ->where(function ($query) {
                $query->where('created_at', '>', deduct_timestamp_hours(self::LAST_CASE_HOUR_INTERVAL))
                    ->orWhere('updated_at', '>', deduct_timestamp_hours(self::LAST_CASE_HOUR_INTERVAL));
            })
            ->orderBy('id', 'desc')
            ->first();
    }

    public function countByStatusId ($lobId, $shiftDate, $statusId)
    {
        $caseTable = self::TABLE_NAME . $lobId;

        $db = $this->db->table($caseTable)
            ->where('shift_date', $shiftDate);

        if (is_array($statusId)) {
            $db->whereIn('status_id', $statusId);
        } else {
            $db->where('status_id', $statusId);
        }

        return $db->count();
    }

    public function getByUser ($lobId, $userId, $filters = [])
    {
        $query = $this->db->table(self::TABLE_NAME . $lobId)
            ->where('user_id', $userId);

        foreach ($filters as $filter) {
            $query = $query->where($filter['column'], $filter['comparison'], $filter['value']);
        }

        return $query->get();
    }

    public function getByConditions ($lobId, $conditions = [], $fetchSingleRow = false)
    {
        $query = $this->db->table(self::TABLE_NAME . $lobId);

        foreach ($conditions as $condition) {
            $query->where($condition['column'], $condition['comparison'], $condition['value']);
        }

        if ($fetchSingleRow) {
            return $query->first();
        }

        return $query->get();
    }

    public function getSyntaxCodesByMeasure ($measure)
    {
        return $this->db->table('syntax_code')
            ->where('measure', $measure)
            ->get();
    }

    private function establishCustomFieldsData ($fields, $data, $isUpdate = false)
    {
        $customFieldsData = [];
        foreach ($fields as $field) {
            if ($field->is_submeasure == 0) {
                if (isset($data[$field->html_name])) {
                    $customFieldsData[$field->html_name] = $data[$field->html_name];
                } else if (!$isUpdate) {
                    $customFieldsData[$field->html_name] = $field->default_value;
                }
            }
        }

        return $customFieldsData;
    }

    public function getSubMeasure ($lobId, $caseId)
    {
        $table = self::TABLE_NAME_SUB . $lobId;

        If ($lobId == 3) {
            return $this->db->table($table)
                ->where('case_id', $caseId)
                ->orderBy('measure', 'asc')
                ->get();
        } else {
            return $this->db->table($table)
                ->where('case_id', $caseId)
                ->get();
        }
    }

    public function getReason ()
    {
        $table = 'list_value';

        return $this->db->table($table)
            ->where('field_id', 11)
            ->get();
    }
}