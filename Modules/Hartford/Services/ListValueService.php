<?php

namespace Modules\Hartford\Services;

class ListValueService extends BaseService
{
    public function getByLobId ($lobId)
    {
        return $this->db->table('list_value')
            ->where('lob_id', $lobId)
            ->get();
    }

    public function getByParentId ($lobId, $parentId)
    {
        return $this->db->table('list_value')
            ->where('lob_id', $lobId)
            ->where('parent_list_id', $parentId)
            ->get();
    }
}