<?php

namespace Modules\Hartford\Services;

use DB;

class TeamSettingService extends BaseService
{
    public function getUserList ()
    {
        $role = session()->get('user_from_account')->role;
        $idUser = session()->get('user_from_account')->main_user_id;

        if ($role == '5') {
            $ifTeam
                = "(CASE
            WHEN FIND_IN_SET('$idUser', team.lead)  THEN '1'
                else 0
          END) ";
        } else {
            $ifTeam = '1';
        }

        $res = $this->db->table('user')
            ->leftJoin('shift_schedule', 'user.main_user_id', '=', 'shift_schedule.user_id')
            ->leftJoin('user_lob', 'user.main_user_id', '=', 'user_lob.user_id')
            ->leftJoin('lob', 'lob.id', '=', 'user_lob.lob_id')
            ->leftJoin('team', 'team.id', '=', 'user.team')
            ->leftJoin('role', 'role.id', '=', 'user.role')
            ->leftJoin("tv_main.lob as main_lob", "user_lob.lob_id", '=', "main_lob.id")
            ->select('user.main_user_id as id', \DB::raw("CONCAT(user.firstname,' ',user.lastname) as full_name"),
                \DB::raw("GROUP_CONCAT(main_lob.name) as lob"),
                'team.description', 'user.shift_date',
                'team.lead as teamLeadID',
                'shift_schedule.start_shift', 'shift_schedule.end_shift', 'shift_schedule.leave', 'user.status',
                'team.id as teamID',
                \DB::raw("GROUP_CONCAT(user_lob.lob_id ) as lob_id"),
                'role.id as roleID', 'user.ramp',
                \DB::raw("$ifTeam as blocked")
            )
            ->where('shift_schedule.shift_date', '=', \DB::raw("(SELECT distinct user.shift_date FROM user)"))
            ->where('user.active', '=', '1')
            ->groupBy('user_lob.user_id')
            ->get();

        return $res;
    }

    public function getTeamList ()
    {
        $res = $this->db->table('team')
            ->leftJoin('user', function ($join) {
                $join->where(DB::raw('FIND_IN_SET(user.id, team.lead)'), '>', 0);
            })
            ->select('team.ID', 'team.lead', 'user.username', 'team.description', \DB::raw("GROUP_CONCAT(CONCAT(' ',user.firstname,' ', user.lastname, ' ' ) ORDER BY user.id ) as lob"))
            ->groupBy('team.ID')
            // ->toSql();
            ->get();

        return $res;
    }

    public function getTeamLeader ()
    {
        $res = $this->db->table('user')
            ->select(\DB::raw("CONCAT(user.firstname,' ',user.lastname) as full_name"), 'user.id')
            ->whereIn('role', [
                4,
                5
            ])
            ->get();

        return $res;
    }

    public function updateTeamList ($editID, $editTeamName, $checkbox)
    {
        $res = $this->db->table('team')
            ->where('ID', $editID)
            ->update([
                'description' => $editTeamName,
                'lead'        => $checkbox
            ]);
    }

    public function updateTeamUserScheduleShift ($user_id, $user_start_shift, $user_end_shift, $onLeave, $current_shift_date, $teamList, $slctRole, $ramp)
    {
        $res = $this->db->table('shift_schedule')
            ->where('user_id', $user_id)
            ->where('shift_schedule.shift_date', $current_shift_date)
            ->leftJoin('user', 'shift_schedule.user_id', '=', 'user.main_user_id')
            ->update([
                'shift_schedule.start_shift' => $user_start_shift,
                'shift_schedule.end_shift'   => $user_end_shift,
                'shift_schedule.leave'       => $onLeave,
                'user.role'                  => $slctRole,
                'user.team'                  => $teamList,
                'user.ramp'                  => $ramp
            ]);
    }

    public function updateTeamSchedule ($id, $start_shift, $end_shift, $on_leave, $ramp)
    {
        $myArrayId = explode(',', $id);

        $res = $this->db->table('shift_schedule')
            ->whereIn('user_id', $myArrayId)
            ->join('user', 'user.main_user_id', '=', 'shift_schedule.user_id')
            ->update([
                'start_shift' => $start_shift,
                'end_shift'   => $end_shift,
                'leave'       => $on_leave,
                'user.ramp'   => $ramp
            ]);
    }

    public function updateTeamBulk ($id, $team)
    {
        $myArrayId = explode(',', $id);

        $res = $this->db->table('user')
            ->whereIn('id', $myArrayId)
            ->update(['team' => $team]);
    }
}