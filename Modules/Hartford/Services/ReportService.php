<?php
/**
 * Created by PhpStorm.
 * User: OMLanuza
 * Date: 1/17/2018
 * Time: 4:11 PM
 */

namespace Modules\Hartford\Services;

class ReportService extends BaseService
{
    public function getHourly ($lobId, $dateFrom, $dateTo, $timezone)
    {
        if ($timezone == 'CST') {
            $hour = 'concat(hour(date_add(case_activity_log.created_at,INTERVAL 13 HOUR)), ":00") as hour';
        } else {
            $hour = 'concat(hour(case_activity_log.created_at), ":00") as hour';
        }

        return $this->db->table('case_activity_log')
            ->select(
                $this->db->raw('concat(user.lastname, \', \', user.firstname) as Name'),
                $this->db->raw($hour),
                $this->db->raw('count(case_activity_log.id) as count')
            )
            ->leftJoin('user', 'user.id', '=', 'case_activity_log.user_id')
            ->where('lob_id', $lobId)
            ->whereNotIn('status_id', StatusService::ONGOING_STATUS_ID)
            ->whereBetween('created_at', [
                $dateFrom,
                $dateTo
            ])
            ->groupBy('user.id', $this->db->raw('hour(case_activity_log.created_at)'))
            ->get()
            ->toArray();
    }
}