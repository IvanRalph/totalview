<?php

namespace Modules\Hartford\Services;

class BaseService
{
    protected $db;

    public function __construct ()
    {
        $this->db = \DB::connection(config('hartford.db_connection'));
    }
}