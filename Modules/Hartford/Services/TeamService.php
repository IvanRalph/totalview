<?php

namespace Modules\Hartford\Services;

class TeamService extends BaseService
{
    public function getLeader ($teamId)
    {
        $teamInfo = $this->db->table('team')
            ->where('id', $teamId)
            ->first();

        $teamLeadersId = explode(',', $teamInfo->lead);

        return $this->db->table('user')
            ->whereIn('main_user_id', $teamLeadersId)
            ->get();
    }
}