<?php

namespace Modules\Hartford\Services;

class RoleService extends BaseService
{
    public function getRoleList ()
    {
        $res = $this->db->table('role')
            ->whereIn('id', [
                3,
                4,
                5,
                6
            ])
            ->get();

        return $res;
    }
}