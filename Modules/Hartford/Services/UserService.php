<?php

namespace Modules\Hartford\Services;

class UserService extends BaseService
{
    const STATUS_ONLINE = '1';
    const STATUS_OFFLINE = '0';

    public function getByMainUserId ($mainUserId)
    {
        return $this->db->table('user')
            ->where('main_user_id', $mainUserId)
            ->first();
    }

    public function update ($mainUserId, $data)
    {
        return $this->db->table('user')
            ->where('main_user_id', $mainUserId)
            ->update($data);
    }

    public function updateUserList ($editID, $editTeamName, $editTeamLeader)
    {
        $res = $this->db->table('team')
            ->where('ID', $editID)
            ->update([
                'description' => $editTeamName,
                'createdby'   => $editTeamLeader
            ]);
    }

    public function updateTeamSchedule ($id, $start_shift, $end_shift, $on_leave)
    {
        $myArrayId = explode(',', $id);

        $res = $this->db->table('attendance')
            ->whereIn('userid', $myArrayId)
            ->update([
                'start_shift' => $start_shift,
                'end_shift'   => $end_shift,
                'leave'       => $on_leave
            ]);
    }

    public function countAll ($lobId)
    {
        return $this->db->table('user_lob')
            ->join('user', 'user_lob.user_id', '=', 'user.main_user_id')
            ->where('user_lob.lob_id', $lobId)
            ->count();
    }

    public function countAvailable ($lobId)
    {
        return $this->db->table('user_lob')
            ->join('user', 'user_lob.user_id', '=', 'user.main_user_id')
            ->where('user_lob.lob_id', $lobId)
            ->where('user.status', self::STATUS_ONLINE)
            ->count();
    }

    public function getAllByLobId ($lobId)
    {
        return $this->db->table('user_lob')
            ->join('user', 'user_lob.user_id', '=', 'user.main_user_id')
            ->where('user_lob.lob_id', $lobId)
            ->get();
    }

    public function getShiftSchedule ($userId)
    {
        return $this->db->table('shift_schedule')
            ->where('user_id', $userId)
            ->orderBy('shift_date', 'desc')
            ->first();
    }
}