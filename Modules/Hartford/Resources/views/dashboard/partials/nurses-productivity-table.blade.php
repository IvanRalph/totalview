<div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Nurse Productivity Table</h5>
        </div>
        <div class="ibox-content">
            <form role="form">
                <div class="form-group">
                    <label>LOB</label>
                    <select id="slct-lob" class="form-control">
                        @foreach($getLobList as $lobList)
                            <option value="{{ $lobList->id }}">{{ $lobList->name }}</option>
                        @endforeach
                    </select>
                </div>
                {{--<button id="btn-generate-user-list" class="btn btn-sm btn-primary btn-block" type="button">GENERATE</button>--}}
            </form>
        </div>
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-wave">
                <div class="sk-rect1"></div>
                <div class="sk-rect2"></div>
                <div class="sk-rect3"></div>
                <div class="sk-rect4"></div>
                <div class="sk-rect5"></div>
            </div>
            <table id="productivity-table" class="table table-bordered table-hover table-responsive" cellspacing="0"
                   width="100%">
                <thead>
                <tr role="row">
                    <th>Name</th>
                    <th>Team Leader</th>
                    <th>Shift Date</th>
                    <th>Shift Schedule</th>
                    <th>Completed Cases</th>
                    <th>Pending Cases</th>
                    <th>Cases Duration</th>
                    <th>Avg Handling Time</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tfoot>
                <tr class="text-success">
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
