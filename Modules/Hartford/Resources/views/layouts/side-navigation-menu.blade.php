@section('side-navigation-menu')
    @foreach($userModules as $module)
        <li id="{{ $module['html_id'] }}" class="{{ $module['html_class'] }}" title="{{ $module['name'] }}">
            <a class="sidelinks"
               href="/{{ session('selected_account')->route.'/'.$module['uri'].'?token='.app('request')->get('token') }}">
                <i class="fa {{ $module['html_icon'] }}"></i>
                <span class="nav-label">{{ $module['name'] }}</span>
            </a>
        </li>
    @endforeach

    <li id="side-nav-switch-accout" title="Switch Account">
        <a class="sidelinks" href="/accounts{{ '?token='.app('request')->get('token') }}">
            <i class="fa fa-exchange"></i>
            <span class="nav-label">Switch Account</span>
        </a>
    </li>

    <li id="side-nav-end-shift" title="End Shift">
        <a class="sidelinks"
           href="/{{ session('selected_account')->route }}/end-shift{{ '?token='.app('request')->get('token') }}">
            <i class="fa fa-sign-out"></i>
            <span class="nav-label">End Shift</span>
        </a>
    </li>

    <li id="side-nav-log-out" title="Logout">
        <a class="sidelinks" href="/logout{{ '?token='.app('request')->get('token') }}">
            <i class="fa fa-power-off"></i>
            <span class="nav-label">Logout</span>
        </a>
    </li>
@stop