<div id="frm-submeasure-{{ $lob->id }}" class="form-horizontal form-submeasure">
    <div class="row text-center m-b"><label class="control-label"> SUB MEASURE </label></div>
    @foreach($fields[$lob->id] as $field)
        @if($field->is_submeasure == 1)
            @if($field->html_name == 'measure_code')
                <div class="form-group">
                    <div class="col-sm-3"><label class=" control-label">{{ $field->name }}</label></div>
                    <div class="col-sm-4 inputs">
                        {!! create_html_field($field, $listValues[$lob->id]) !!}
                    </div>
                    @elseif($field->html_name == 'measure_syntax' )
                        <div class="col-sm-1"><label class=" control-label">{{ $field->name }}</label></div>
                        <div class="col-sm-4 inputs">
                            {!! create_html_field($field, $listValues[$lob->id]) !!}
                        </div>
                </div>
            @else
                <div class="form-group">
                    <div class="col-sm-3"><label class=" control-label">{{ $field->name }}</label></div>
                    <div class="col-sm-9 inputs">
                        {!! create_html_field($field, $listValues[$lob->id]) !!}
                    </div>
                </div>
            @endif
        @endif
    @endforeach
    {{--<div class="row text-center m-b"> <label class="control-label">  SUB MEASURE </label></div>--}}
    {{--<div class="form-group">--}}
    {{--<div class="col-md-3"><label class="control-label">Submeasure</label></div>--}}
    {{--<div class="col-md-9"><select class="dropdown-9 form-control input-md submeasure-data select2" name = "submeasure" id="submeasure"></select></div>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<div class="col-md-3"><label class="control-label">Status</label></div>--}}
    {{--<div class="col-md-9"><select class="dropdown-10 form-control input-md submeasure-data status select2" name = "status" ></select></div>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<div class="col-md-3"><label class="control-label">Reason</label></div>--}}
    {{--<div class="col-md-9"><select class="dropdown-11 form-control input-md submeasure-data reason select2" name = "reason" ></select></div>--}}
    {{--</div>--}}


    {{--<div class="form-group">--}}
    {{--<div class="col-md-3"><label class="control-label">Remarks</label></div>--}}
    {{--<div class="col-md-9"><textarea  class="form-control input-md submeasure-data" type="text" name="remarks" id="remarks"  rows="2" ></textarea></div>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<div class="col-md-3"><label class="control-label">Syntax Outcome</label></div>--}}
    {{--<div class="col-md-4"><select class="form-control input-md submeasure-data syntax-outcome select2" name="measure_code"></select></div>--}}
    {{--<div class="col-md-1"><label class="control-label">Code</label></div>--}}
    {{--<div class="col-md-4"><input  class="form-control input-md submeasure-data" type="text" name="measure_syntax" id="measure-syntax"  rows="1"></div>--}}
    {{--</div>--}}
</div>

<div class="hr-line-dashed"></div>


<div id="submeasure-results-label">
    <div class="form-group">
        @foreach($fields[$lob->id] as $field)
            @if($field->is_submeasure == 1 && $field->html_name != "measure_syntax")
                <div class="col-md-2"><label class="control-label">{{$field->description}}</label></div> @endif
        @endforeach
    </div>
    {{--<div class="form-group">
        <div class="col-md-2 "><label class="control-label">Submeasure</label></div>
        <div class="col-md-2"><label class="control-label"> Status </label></div>
        <div class="col-md-2"><label class="control-label"> Reason</label></div>
        <div class="col-md-2"><label class="control-label"> Remarks</label></div>
        <div class="col-md-2"><label class="control-label"> Syntax</label></div>
    </div>--}}
    <div class="row  m-t submeasure-results" id="submeasure-results">

    </div>

</div>
<div class="hr-line-dashed"></div>