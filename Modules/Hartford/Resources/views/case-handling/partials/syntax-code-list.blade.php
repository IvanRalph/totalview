<div class="hr-line-dashed"></div>
<div class="form-group">

    <div class="ibox float-e-margins collapsed">
        <div class="ibox-title collapse-link">
            <h5>View Syntax Outcome & Code</h5>
            <div class="ibox-tools">
                <a>
                    <i class="fa fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content no-padding">
            <ul class="list-group" id="syntax-code-display">
                <li class="list-group-item ">
                    <span class="label pull-right"></span> No Code to Display
                </li>
            </ul>
        </div>
    </div>

</div>