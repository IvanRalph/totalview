<div id="frm-submeasure-{{ $lob->id }}" class="form-horizontal ">
    <div class="row text-center m-b"><label class="control-label"> SUB MEASURE </label></div>
    <div class="form-group">
        <div class="col-md-3"><label class="control-label">SubMeasure</label></div>
        <div class="col-md-9"><select class="dropdown-23 form-control input-md submeasure-data select2"
                                      name="submeasure"
                                      id="submeasure"></select></div>
    </div>
    <div class="form-group">
        <div class="col-md-3"><label class="control-label">Status</label></div>
        <div class="col-md-9">

            <select class="dropdown-24 form-control input-md submeasure-data select2" id="status" name="status">
                <option>AA</option>
                <option>ME</option>
                <option>CE</option>
            </select>

        </div>
    </div>
    <div class="form-group">
        <div class="col-md-3"><label class="control-label">Remarks</label></div>
        <div class="col-md-9"><textarea class="form-control input-md submeasure-data" type="text" name="remarks"
                                        id="remarks" rows="2"></textarea></div>
    </div>

</div>

<div class="hr-line-dashed"></div>

<div id="submeasure-results-label">
    <div class="form-group">
        <div class="col-md-3"><label class="control-label">Sub Measure</label></div>
        <div class="col-md-3"><label class="control-label">Status</label></div>
        <div class="col-md-4"><label class="control-label">Remarks</label></div>
    </div>

    <div class="submeasure-results" id="submeasure-results">

    </div>

</div>

