<style type="text/css">
    .select2-container {
        z-index: 2050;
    }
</style>
<div class="modal inmodal" id="teamSettingModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h2 class="text-left">Manage Team</h2>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <label class="text-center">Team Name</label>
                    <div class="input-group col-lg-12" data-autoclose="true">
                        <input type="text" id="editTeamName" name="editTeamName" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="text-center">Team Leader</label>
                    <div class="input-group col-lg-12" data-autoclose="true">
                        <input type="hidden" id="teamLeaderID">
                    </div>
                    <select class="select2_demo_2 form-control select2-hidden-accessible" id="team-info-leader"
                            multiple="" tabindex="-1" aria-hidden="true">
                        @foreach($getTeamLeader as $leader)
                            <option name="check_lob[]" id="check_lob_{{ $leader->id }}"
                                    value="{{ $leader->id }}">{{ $leader->full_name }}</option>
                        @endforeach
                    </select>
                    <span class="select2 select2-container select2-container--default select2-container--above"
                          dir="ltr" style="width: 100%;"><span class="selection">
                 </span>
                 <span class="dropdown-wrapper" aria-hidden="true"></span></span>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" id="btnSaveTeamSettings" class="ladda-button btn btn-info" data-style="zoom-in">
                    Save
                </button>
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



