<div class="modal inmodal" id="userSettingModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h2 class="text-left">Manage User</h2>
            </div>

            <div class="modal-body">
                <div class="col-lg-12">
                    <div class="profile-info" style="margin-left: 0px; margin-bottom: 30px;">
                        <input type="hidden" name="_method" name="user-id" id="user-id">
                        <h2 id="userFullName" class="no-margins"></h2>
                        <h4><span id="isOnline" class="label"></span></h4>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="tabs-container">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#schedule">Schedule</a></li>
                                    <li><a data-toggle="tab" href="#team">Team</a></li>
                                    <li><a data-toggle="tab" href="#lob">LOB</a></li>
                                    <li><a data-toggle="tab" href="#role">Role</a></li>
                                </ul>

                                <div class="tab-content">
                                    <div id="schedule" class="tab-pane active">
                                        <form role="form" style="margin-top: 20px;" class="form" method="POST">
                                            <div class="form-group ">
                                                <div class="input-group col-lg-12" data-autoclose="true">
                                                    <div class="input-group clockpicker" data-autoclose="true">
                                                        <label>Start Shift</label>
                                                        <input type="text" class="form-control" id="user-start-shift">
                                                        <span class="input-group-addon">
                                                        <span class="fa fa-clock-o"></span>
                                                    </span>
                                                    </div>
                                                </div>

                                                <div class="input-group col-lg-12" data-autoclose="true">
                                                    <div class="input-group clockpicker" data-autoclose="true">
                                                        <label>End Shift</label>

                                                        <input type="text" class="form-control" id="user-end-shift">
                                                        <span class="input-group-addon">
                                                    <span class="fa fa-clock-o"></span>
                                                </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="input-group col-lg-12" data-autoclose="true">
                                                <div class="form-group">
                                                    <label for="forRamp">Ramp:</label>
                                                    <input id="per-user-ramp" type="text" name="per-user-ramp">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="checkbox checkbox-primary">
                                                    <input id="onLeave" type="checkbox">
                                                    <label for="onLeave">
                                                        On Leave
                                                    </label>
                                                </div>

                                            </div>
                                            <button type="button" id="btnSaveSchedule"
                                                    class="ladda-button btn btn-info pull-right" data-style="zoom-in">
                                                Save
                                            </button>
                                        </form>
                                    </div>

                                    <div id="team" class="tab-pane">
                                        @foreach($getTeamList as $teamList)
                                            <div id="teamListPlaceholder">
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="rdoTeam" id="team_id_{{ $teamList->ID }}"
                                                           value="{{ $teamList->ID }}" checked="">
                                                    <label for="team_id_{{ $teamList->ID }}">{{ $teamList->description }}</label>
                                                </div>
                                            </div>
                                        @endforeach

                                        <button type="button" id="btnSaveSchedule"
                                                class="ladda-button btn btn-info pull-right" data-style="zoom-in">
                                            Save
                                        </button>
                                    </div>

                                    <div id="lob" class="tab-pane">
                                        <div id="teamListPlaceholder">
                                            <form role="form" class="form-horizontal" style="margin-top: 13px;">
                                                @foreach($getLobList as $lobList)
                                                    <div class="checkbox m-r-xs">
                                                        <input type="checkbox" name="check_lob[]" class="ads_Checkbox"
                                                               id="check_lob_{{ $lobList->id }}"
                                                               value="{{ $lobList->id }}">
                                                        <label>{{ $lobList->name }}</label>

                                                    </div>
                                                @endforeach
                                                <div class="text-right">
                                                    <button type="button" id="btnSaveLob"
                                                            class="ladda-button btn btn-info" data-style="zoom-in">
                                                        Save
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>


                                    <div id="role" class="tab-pane">
                                        <form role="form" style="margin-top: 13px;">
                                            <div class="form-group">

                                                <div class="input-group col-lg-12" data-autoclose="true">
                                                    <select id="slctRole" class="form-control">
                                                        @foreach($getRoleList as $roleList)
                                                            <option value="{{ $roleList->id }}">{{ $roleList->name }}</option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                                <button type="button" id="btnSaveSchedule"
                                                        class="ladda-button btn btn-info pull-right"
                                                        data-style="zoom-in" style="margin-top: 13px;">
                                                    Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
