<div class="modal inmodal" id="bulkUserSettingModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h2 class="text-left">Manage User (Bulk)</h2>
            </div>

            <div class="modal-body">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="tabs-container">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#bulkSchedule">Schedule</a></li>
                                    <li><a data-toggle="tab" href="#bulkTeam">Team</a></li>
                                    <li><a data-toggle="tab" href="#bulkLob">LOB</a></li>
                                    <li><a data-toggle="tab" href="#bulkRole">Role</a></li>
                                </ul>

                                <input type="hidden" id="bulk-id-list" name="" value="">
                                <div class="tab-content">
                                    <div id="bulkSchedule" class="tab-pane active">
                                        <form role="form" style="margin-top: 20px;">
                                            <div class="form-group">

                                                <div class="input-group col-lg-12" data-autoclose="true">
                                                    <div class="input-group clockpicker" data-autoclose="true">
                                                        <label>Start Shift</label>
                                                        <input type="text" class="form-control"
                                                               name="bulk-user-start-shift" id="bulk-user-start-shift">
                                                        <span class="input-group-addon">
                            <span class="fa fa-clock-o"></span>
                          </span>
                                                    </div>
                                                </div>

                                                <div class="input-group col-lg-12" data-autoclose="true">
                                                    <div class="input-group clockpicker" data-autoclose="true">
                                                        <label>End Shift</label>

                                                        <input type="text" class="form-control"
                                                               name="bulk-user-end-shift" id="bulk-user-end-shift">
                                                        <span class="input-group-addon">
                            <span class="fa fa-clock-o"></span>
                          </span>
                                                    </div>
                                                </div>

                                                <div class="input-group col-lg-12" data-autoclose="true">
                                                    <div class="form-group">
                                                        <label for="forRamp">Ramp:</label>
                                                        <input id="buld-user-ramp" type="text" name="buld-user-ramp">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-group">
                                                <div class="checkbox checkbox-primary">
                                                    <input id="onLeaveBulk" type="checkbox">
                                                    <label for="onLeaveBulk">
                                                        On Leave
                                                    </label>
                                                </div>
                                            </div>
                                            <button type="button" id="btnSaveScheduleBulk"
                                                    class="ladda-button btn btn-info pull-right" data-style="zoom-in">
                                                Save
                                            </button>
                                        </form>
                                    </div>

                                    <div id="bulkTeam" class="tab-pane">
                                        @foreach($getTeamList as $teamList)

                                            <div id="bulkTeamListPlaceholder">
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="rdoTeam" id="rdoTeam"
                                                           value="{{ $teamList->ID }}">
                                                    <label for="team_id_{{ $teamList->ID }}">{{ $teamList->description }}</label>
                                                </div>

                                            </div>
                                        @endforeach

                                        <button type="button" id="btnSaveTeamBulk"
                                                class="ladda-button btn btn-info pull-right" data-style="zoom-in">
                                            Save
                                        </button>
                                    </div>

                                    <div id="bulkLob" class="tab-pane">
                                        <div id="teamListPlaceholder">
                                            <form role="form" class="form-horizontal" style="margin-top: 13px;">
                                                @foreach($getLobList as $lobList)
                                                    <div class="checkbox m-r-xs">

                                                        <input type="checkbox" name="check_lob[]" class="ads_Checkbox"
                                                               id="check_lob_{{ $lobList->id }}"
                                                               value="{{ $lobList->id }}">
                                                        <label>{{ $lobList->name }}</label>
                                                    </div>
                                                @endforeach

                                                <div class="text-right">
                                                    <button type="button" id="btnSaveLobBulk"
                                                            class="ladda-button btn btn-info" data-style="zoom-in">
                                                        Save
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <div id="bulkRole" class="tab-pane">
                                        <form role="form" style="margin-top: 13px;">
                                            <div class="form-group">
                                                <div class="input-group col-lg-12" data-autoclose="true">
                                                    <select id="bulkSlctRole" class="form-control">
                                                        @foreach($getRoleList as $roleList)
                                                            <option value="{{ $roleList->id }}">{{ $roleList->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <button type="button" id="btnSaveRoleBulk"
                                                        class="ladda-button btn btn-info pull-right"
                                                        data-style="zoom-in" style="margin-top: 13px;">
                                                    Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white btn-close-bulk-modal" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>