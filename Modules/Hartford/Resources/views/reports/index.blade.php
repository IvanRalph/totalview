@extends('hartford::layouts.master')

@section('content')

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="tabs-container">

                    <div class="tab-content">
                        <div class="tab-pane active">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Reports</h5>
                                </div>
                                <div class="ibox-content">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a data-toggle="tab" href="#rawReports">
                                                <i class="fa fa-group"></i>Raw Reports
                                            </a>
                                        </li>
                                        <li class="">
                                            <a data-toggle="tab" href="#hourlyReports">
                                                <i class="fa fa-user"></i>Hourly Reports
                                            </a>
                                        </li>
                                    </ul>
                                    <p></p>

                                    <div class="tab-content">
                                        <div id="rawReports" class="tab-pane active">
                                            <div class="full-height-scroll">
                                                <div class="row clearfix">
                                                    <div class="col-md-3 col-xs-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel-body">
                                                                <form id="frmRawReports" name="frmRawReports"
                                                                      class="form" method="get">
                                                                    <div class="form-group">
                                                                        <label class="control-label">LOB:</label>
                                                                        <select id="slct-lob-id" class="form-control"
                                                                                name="rawNumber" required="required">
                                                                            @foreach($getLobList as $lobList)
                                                                                <option value="{{ $lobList->id }}">{{ $lobList->name }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label">Select Time
                                                                                                     Zone:</label>
                                                                        <select id="slct-lob-id" class="form-control"
                                                                                name="timezone">
                                                                            <option value="0">MNL - Manila</option>
                                                                            <option value="1">CST - Central</option>
                                                                        </select>
                                                                    </div>


                                                                    <div class="form-group">
                                                                        <label class="control-label">Shift Date
                                                                                                     Range</label>
                                                                        <div class="input-daterange input-group"
                                                                             id="datepickerForProductivity">
                                                                            <input type="text"
                                                                                   class="input-sm form-control"
                                                                                   required="required" name="start"
                                                                                   value=""
                                                                                   placeholder="YYYY-MM-DD">
                                                                            <span class="input-group-addon">to</span>
                                                                            <input type="text"
                                                                                   class="input-sm form-control"
                                                                                   required="required" name="end"
                                                                                   value=""
                                                                                   placeholder="YYYY-MM-DD">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group clearfix">
                                                                        <button type="submit" id="submitBtn"
                                                                                class="btn btn-md btn-primary col-md-12 col-xs-12">
                                                                            Generate
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-9 col-xs-12">
                                                        <div class="table-responsive">
                                                            <table id="reports-table"
                                                                   class="table table-striped table-bordered table-hover"
                                                                   cellspacing="0">
                                                                <thead>

                                                                <tr role="row">
                                                                    <th>Shift Date</th>
                                                                    <th>User ID</th>
                                                                    <th>Status</th>
                                                                    <th>Duration</th>
                                                                    <th>Attachment</th>
                                                                    <th>Ref Num</th>
                                                                    <th>Text</th>
                                                                    <th>Dropdown</th>
                                                                    <th>Created</th>
                                                                    <th>Updated</th>
                                                                </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div id="hourlyReports" class="tab-pane">
                                            <div class="full-height-scroll">
                                                <div class="row clearfix">
                                                    <div class="col-md-3 col-xs-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel-body">
                                                                <form id="frmHourlyReports" name="frmHourlyyReports"
                                                                      class="form" method="get">
                                                                    <div class="form-group">
                                                                        <label class="control-label">LOB:</label>
                                                                        <select id="slct-lob-id" class="form-control"
                                                                                name="hourlyNumber" required="required">
                                                                            @foreach($getLobList as $lobList)
                                                                                <option value="{{ $lobList->id }}">{{ $lobList->name }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label">Timezone: </label>
                                                                        <select id="slct-timezone-id"
                                                                                class="form-control" name="timezone"
                                                                                required="required">
                                                                            <option value="CST"> CST - Central</option>
                                                                            <option value="MNL"> MNL - Manila</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label">Shift Date
                                                                                                     Range</label>
                                                                        <div class="input-daterange input-group"
                                                                             id="datepickerForHourly">
                                                                            <input type="text"
                                                                                   class="input-sm form-control"
                                                                                   id="date-start"
                                                                                   required="required" name="start"
                                                                                   value=""
                                                                                   placeholder="YYYY-MM-DD">
                                                                            <span class="input-group-addon">to</span>
                                                                            <input type="text"
                                                                                   class="input-sm form-control"
                                                                                   id="date-end"
                                                                                   required="required" name="end"
                                                                                   value=""
                                                                                   placeholder="YYYY-MM-DD">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group clearfix">
                                                                        <button type="submit" id="submitBtnHourly"
                                                                                class="btn btn-md btn-primary col-md-12 col-xs-12">
                                                                            Generate
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-9 col-xs-12">
                                                        <div class="table-responsive">
                                                            <table id="reports-table-hourly"
                                                                   class="table table-striped table-bordered table-hover"
                                                                   cellspacing="0">
                                                                <thead>
                                                                <tr role="row">
                                                                    <th>Name</th>
                                                                    <th> 00:00</th>
                                                                    <th> 01:00</th>
                                                                    <th> 02:00</th>
                                                                    <th> 03:00</th>
                                                                    <th> 04:00</th>
                                                                    <th> 05:00</th>
                                                                    <th> 06:00</th>
                                                                    <th> 07:00</th>
                                                                    <th> 08:00</th>
                                                                    <th> 09:00</th>
                                                                    <th> 10:00</th>
                                                                    <th> 11:00</th>
                                                                    <th> 12:00</th>
                                                                    <th> 13:00</th>
                                                                    <th> 14:00</th>
                                                                    <th> 15:00</th>
                                                                    <th> 16:00</th>
                                                                    <th> 17:00</th>
                                                                    <th> 18:00</th>
                                                                    <th> 19:00</th>
                                                                    <th> 20:00</th>
                                                                    <th> 21:00</th>
                                                                    <th> 22:00</th>
                                                                    <th> 23:00</th>

                                                                </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('module-scripts')
    {!! Html::script('js/module/hartford/constant.js') !!}
    {!! Html::script('js/module/hartford/reports.js') !!}
@stop