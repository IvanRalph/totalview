@extends('provider::layouts.master')

@section('content')
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-8">
                <div class="row">
                    @include('provider::dashboard.partials.metrics')
                </div>
                <div class="row">
                    @include('provider::dashboard.partials.nurses-productivity-table', $data)
                </div>
            </div>
            <div class="col-lg-4">
                <div class="row">
                    @include('provider::dashboard.partials.nurse-audit-trail')
                </div>
            </div>
        </div>

    </div>
@stop

@section('module-scripts')
    {!! Html::script('js/module/provider/constant.js') !!}
    {!! Html::script('js/module/provider/dashboard.js') !!}
@stop
