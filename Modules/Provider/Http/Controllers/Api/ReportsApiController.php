<?php

namespace Modules\Provider\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Modules\Provider\Services\LobService;
use Modules\Provider\Services\CaseService;
use Modules\Provider\Services\FieldService;
use Modules\Provider\Services\ReportService;

class ReportsApiController extends Controller
{
    private $lobService;

    public function __construct (LobService $lobService, CaseService $caseService, FieldService $fieldService, ReportService $reportService)
    {
        $this->lobService    = $lobService;
        $this->caseService   = $caseService;
        $this->fieldService  = $fieldService;
        $this->reportService = $reportService;
    }

    public function getLobList ()
    {
        $userList = $this->lobService->getLobList();
    }

    //  public function getRawReports(Request $request) {

    //      $rawNumber = $request->get('rawNumber');
    //      $start = $request->get('start');
    //      $end = $request->get('end');
    //      $timezone = $request->get('timezone');
    //      $data = $this->caseService->getCases($rawNumber, $start, $end, $timezone);
    //      return json_encode($data);
    // }

    public function getRawReports (Request $request)
    {
        $rawNumber = $request->get('rawNumber');
        $start     = $request->get('start');
        $end       = $request->get('end');
        $timezone  = $request->get('timezone');
        $data      = $this->reportService->getRaw($rawNumber, $start, $end, $timezone);

        return json_encode($data);
    }

    public function getHourlyReports (Request $request)
    {
        $lobId    = $request->get('hourlyNumber');
        $dateFrom = $request->get('start');
        $dateTo   = $request->get('end');
        $timezone = $request->get('timezone');
        $newArray = array();

        $hourlyData = $this->reportService->getHourly($lobId, $dateFrom, $dateTo, $timezone);

        foreach ($hourlyData as $value) {
            array_push($newArray, array_values((array)$value));
        }

        return json_encode($newArray);
    }

    public function getBreaksReports (Request $request)
    {
        $dateFrom = $request->get('start');
        $dateTo   = $request->get('end');
        $timezone = $request->get('timezone');
        $data     = $this->reportService->getBreaks($dateFrom, $dateTo, $timezone);

        return json_encode($data);
    }
}