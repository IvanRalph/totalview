<?php

namespace Modules\Provider\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Provider\Services\CaseService;
use Modules\Provider\Services\StatusService;
use Modules\Provider\Services\TeamService;
use Modules\Provider\Services\UserAuditTrailService;
use Modules\Provider\Services\UserService;

class DashboardApiController extends Controller
{
    /**
     * @var CaseService
     */
    private $caseService;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var UserAuditTrailService
     */
    private $userAuditTrailService;

    /**
     * @var TeamService
     */
    private $teamService;

    /**
     * DashboardApiController constructor.
     * @param CaseService           $caseService
     * @param UserService           $userService
     * @param UserAuditTrailService $userAuditTrailService
     * @param TeamService           $teamService
     */
    public function __construct (
        CaseService $caseService,
        UserService $userService,
        UserAuditTrailService $userAuditTrailService,
        TeamService $teamService
    )
    {
        $this->caseService           = $caseService;
        $this->userService           = $userService;
        $this->userAuditTrailService = $userAuditTrailService;
        $this->teamService           = $teamService;
    }

    public function getMetrics (Request $request, $lobId, $teamId)
    {
        $mainUser        = $request->get('userData');
        $userFromAccount = $this->userService->getByMainUserId($mainUser['id']);

        $completedCount = $this->caseService->countByStatusId(
            $lobId,
            [
                StatusService::COMPLETED_STATUS_ID,
                StatusService::EDIT_COMPLETED_STATUS_ID
            ],
            $teamId
        );

        $pendingCount = $this->caseService->countByStatusId(
            $lobId,
            StatusService::HOLD_STATUS_ID,
            $teamId
        );

        $escalatedCount = $this->caseService->countByStatusId(
            $lobId,
            StatusService::ESCALATED_STATUS_ID,
            $teamId
        );

        $allUserCount       = $this->userService->countAll($lobId, $teamId);
        $availableUserCount = $this->userService->countAvailable($lobId, $teamId);

        return response()->json([
            'completed_count'      => $completedCount,
            'escalated_count'      => $escalatedCount,
            'pending_count'        => $pendingCount,
            'all_user_count'       => $allUserCount,
            'available_user_count' => $availableUserCount
        ]);
    }

    public function userList ($lobId, $teamId)
    {
        $users = $this->caseService->dashboardQueryBuilder($lobId, $teamId);

        return response()->json(['data' => $users]);
    }

    public function userActivity ($userId)
    {
        $userActivityForDisplay = [];

        $user           = $this->userService->getByMainUserId($userId);
        $userActivities = $this->userAuditTrailService->getByUserId($userId, $user->shift_date);

        foreach ($userActivities as $activity) {
            $userActivityForDisplay[] = $this->userAuditTrailService->display($activity);
        }

        return response()->json(['activities' => $userActivityForDisplay]);
    }
}
