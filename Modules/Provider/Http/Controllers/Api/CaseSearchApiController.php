<?php

namespace Modules\Provider\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Provider\Services\CaseService;
use Modules\Provider\Services\FieldService;
use Modules\Provider\Services\StatusService;

class CaseSearchApiController extends Controller
{
    /**
     * @var CaseService
     */
    private $caseService;

    /**
     * @var FieldService
     */
    private $fieldService;

    /**
     * @var StatusService
     */
    private $statusService;

    /**
     * CaseSearchApiController constructor.
     * @param CaseService   $caseService
     * @param FieldService  $fieldService
     * @param StatusService $statusService
     */
    public function __construct (CaseService $caseService, FieldService $fieldService, StatusService $statusService)
    {
        $this->caseService   = $caseService;
        $this->fieldService  = $fieldService;
        $this->statusService = $statusService;
    }

    public function get (Request $request, $lobId)
    {
        $mainUser = $request->get('userData');
        $data     = $request->get('data');
        $columns  = [
            [
                'data' => 'shift_date',
                'name' => 'Shift Date'
            ]
        ];

        $filter = [
            [
                'column'     => 'shift_date',
                'comparison' => '>=',
                'value'      => $data['start_date']
            ],
            [
                'column'     => 'shift_date',
                'comparison' => '<=',
                'value'      => $data['end_date']
            ],
            [
                'column'     => 'status_id',
                'comparison' => '>',
                'value'      => '1'
            ]
        ];

        $fields    = $this->fieldService->getByLobId($lobId);
        $userCases = $this->caseService->getByUser($lobId, $mainUser['id'], $filter);
        $statuses  = $this->statusService->getAll()->toArray();
        $statusIds = array_column($statuses, 'id');

        foreach ($userCases as $key => $case) {
            $statusKey    = array_search($case->status_id, $statusIds);
            $status       = $statuses[$statusKey];
            $case->status = generateStatusDisplay($status);

            $userCases[$key] = $case;
        }

        foreach ($fields as $field) {
            $columns[] = [
                'data' => $field->html_name,
                'name' => $field->name
            ];
        }

        $columns[] = [
            'data' => 'duration',
            'name' => 'Duration'
        ];
        $columns[] = [
            'data' => 'status',
            'name' => 'Status'
        ];
        $columns[] = [
            'data' => 'created_at',
            'name' => 'Created'
        ];
        $columns[] = [
            'data' => 'updated_at',
            'name' => 'Updated'
        ];

        return response()->json([
            'data'    => $userCases,
            'columns' => $columns
        ]);
    }
}