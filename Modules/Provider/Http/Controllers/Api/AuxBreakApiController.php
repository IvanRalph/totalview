<?php

namespace Modules\Provider\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Provider\Services\AuxBreakService;
use Modules\Provider\Services\UserAuditTrailService;
use Modules\Provider\Services\UserService;
use Modules\Provider\Services\CaseService;

class AuxBreakApiController extends Controller
{
    /**
     * @var AuxBreakService
     */
    private $auxBreakService;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var UserAuditTrailService
     */

    private $userAuditTrailService;

    /**
     * @var CaseService
     */
    private $userCaseService;

    public function __construct (AuxBreakService $auxBreakService, UserService $userService, UserAuditTrailService $userAuditTrailService, CaseService $userCaseService)
    {
        $this->auxBreakService       = $auxBreakService;
        $this->userService           = $userService;
        $this->userAuditTrailService = $userAuditTrailService;
        $this->CaseService           = $userCaseService;
    }

    public function start (Request $request)
    {
        $data            = $request->get('data');
        $mainUser        = $request->get('userData');
        $userFromAccount = $this->userService->getByMainUserId($mainUser['id']);

        $auxId = $this->auxBreakService->create(
            $mainUser['id'],
            $userFromAccount->shift_date,
            $data
        );

        if ($auxId) {
            $this->userAuditTrailService->record(
                $mainUser['id'],
                $userFromAccount->shift_date,
                UserAuditTrailService::ACTION_START_BREAK,
                $this->auxBreakService->getAuxTypeNameById($data['aux_type_id'])
            );
        }

        return response()->json(['aux_id' => $auxId]);
    }

    public function stop (Request $request, $auxId)
    {
        $mainUser        = $request->get('userData');
        $data            = $request->get('data');
        $userFromAccount = $this->userService->getByMainUserId($mainUser['id']);
        $updateSuccess   = $this->auxBreakService->update($auxId, $data);

        if ($updateSuccess) {
            // $this->caseService->updateBreakAt ($mainUser['lob_id'], $mainUser['id']);

            $this->userAuditTrailService->record(
                $mainUser['id'],
                $userFromAccount->shift_date,
                UserAuditTrailService::ACTION_END_BREAK,
                $this->auxBreakService->getAuxTypeNameById($data['aux_type_id'])
            );
        }

        $breakDuration = $this->getBreaksDuration($mainUser['id']);

        $durationStringFormat = "+" . $breakDuration->h . " hour +" . $breakDuration->i . " minutes +" . $breakDuration->s . " seconds";

        $this->CaseService->updateBreakAt($mainUser['id'], $durationStringFormat);

        return response()->json(['update_success' => $updateSuccess]);
    }

    public function getCurrent (Request $request)
    {
        $mainUser        = $request->get('userData');
        $userFromAccount = $this->userService->getByMainUserId($mainUser['id']);
        $currentAuxBreak = $this->auxBreakService->getCurrent($mainUser['id'], $userFromAccount->shift_date);

        return response()->json(['current_aux_break' => $currentAuxBreak]);
    }

    public function getBreaksDuration ($userId)
    {
        $lastbreak = $this->auxBreakService->getLastBreak($userId);
        $start     = $lastbreak->start_time;
        $end       = $lastbreak->end_time;

        return date_diff(date_create($start), date_create($end));
    }
}