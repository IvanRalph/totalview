<?php

namespace Modules\Provider\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Provider\Services\TeamSettingService;
use Modules\Provider\Services\LobService;
use Modules\Provider\Services\RoleService;

class TeamSettingsWebController extends Controller
{
    private $teamSettingService;

    private $lobService;

    public function __construct (TeamSettingService $teamSettingService, LobService $lobService, RoleService $roleService)
    {
        $this->teamSettingService = $teamSettingService;
        $this->lobService         = $lobService;
        $this->roleService        = $roleService;
    }

    public function index ()
    {
        $data['data']['getTeamLeader'] = $this->teamSettingService->getTeamLeader();
        $data['data']['getTeamList']   = $this->teamSettingService->getTeamList();
        $data['data']['getLobList']    = $this->lobService->getLobList();
        $data['data']['getRoleList']   = $this->roleService->getRoleList();

        return view('provider::team-settings.index', $data);
    }
}