<?php

namespace Modules\Provider\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Provider\Services\FieldService;
use Modules\Provider\Services\LobService;

class CaseSearchWebController extends Controller
{
    /**
     * @var LobService
     */
    private $lobService;

    /**
     * @var FieldService
     */
    private $fieldService;

    public function __construct (LobService $lobService, FieldService $fieldService)
    {
        $this->lobService   = $lobService;
        $this->fieldService = $fieldService;
    }

    public function index ()
    {
        $lobs = $this->lobService->getLobList();

        return view('provider::case-search.index', ['lobs' => $lobs]);
    }
}