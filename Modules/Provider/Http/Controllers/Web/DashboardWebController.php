<?php

namespace Modules\Provider\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Provider\Services\DashboardService;
use Modules\Provider\Services\LobService;
use Modules\Provider\Services\UserService;
use Modules\Provider\Services\TeamService;

class DashboardWebController extends Controller
{
    private $dashboardService;

    public function __construct (DashboardService $dashboardService, LobService $lobService, UserService $userService, TeamService $teamService)
    {
        $this->dashboardService = $dashboardService;
        $this->lobService       = $lobService;
        $this->userService      = $userService;
        $this->teamService      = $teamService;
    }

    public function index ()
    {
        $data['data']['getLobList']   = $this->lobService->getLobList();
        $data['data']['getShiftDate'] = $this->userService->getShiftDateForDashboard();
        $data['data']['getTeamList']  = $this->teamService->getTeamList();

        return view('provider::dashboard.index', $data);
    }

    public function fullscreen ()
    {
        return view('provider::dashboard.partials.dashboard-fullscreen');
    }
}