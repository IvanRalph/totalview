<?php

Route::group([
    'middleware' => [
        'jwt.webauth',
        'web',
        'revalidate'
    ],
    'prefix'     => 'provider',
    'namespace'  => 'Modules\Provider\Http\Controllers\Web'
], function () {
    Route::get('/', 'CaseHandlingWebController@index');
    Route::get('/team', 'TeamSettingsWebController@index');
    Route::get('/reports', 'ReportsWebController@index');
    Route::get('/dashboard', 'DashboardWebController@index');
    Route::get('/dashboard-fullscreen', 'DashboardWebController@fullscreen');
    Route::get('/start-shift', 'CaseHandlingWebController@startShift');
    Route::get('/end-shift', 'CaseHandlingWebController@endShift');
    Route::get('/confirm-end-shift', 'CaseHandlingWebController@confirmEndShift');
    Route::get('/aux-break', 'AuxBreakWebController@index');
    Route::get('/case-search', 'CaseSearchWebController@index');
});

Route::group(
    [
        'middleware' => [
            'jwt.apiauth',
            'api',
            'revalidate'
        ],
        'prefix'     => 'provider/api',
        'namespace'  => 'Modules\Provider\Http\Controllers\Api'
    ], function () {
    // case handling
    Route::get('/tracker-stats/{lobId}', 'CaseHandlingApiController@trackerStats');
    Route::get('/cases/{lobId}', 'CaseHandlingApiController@caseTable');
    Route::get('/case/current/{lobId}', 'CaseHandlingApiController@getCurrent');
    Route::get('/case/{lobId}/{caseId}', 'CaseHandlingApiController@get');
    Route::post('/create-case/{lobId}', 'CaseHandlingApiController@create');
    Route::put('/update-case/{lobId}/{caseId}', 'CaseHandlingApiController@update');
    Route::get('/child-values/{lobId}/{parentId}', 'CaseHandlingApiController@getChildValues');
    Route::delete('/delete-case/{caseId}/{lobId}', 'CaseHandlingApiController@deleteCase');

    // team settings

    Route::get('/team/user-list', 'TeamSettingsApiController@getUserList');
    // Route::get('/team-settings/team', 'TeamSettingsApiController@getTeamList');
    Route::get('/team', 'TeamSettingsApiController@getTeamListForTable');
    Route::put('/teams-update/{teamLeaderID}/{checkBox}', 'TeamSettingsApiController@updateTeamList');
    Route::get('/raw-reports/', 'ReportsApiController@getRawReports');
    Route::put('/team-settings/update-team/user/{teamListInfo}', 'TeamSettingsApiController@updateTeamUser');
    Route::put('/team-settings/update-team/schedule/{id}/{formData}', 'TeamSettingsApiController@updateTeamSchedule');
    Route::put('/team-settings/update-team/lob/{id}/{team}', 'TeamSettingsApiController@updateUserLob');
    Route::put('/team-settings/update-user/lob/{id}/{lob}', 'TeamSettingsApiController@updateUserLob');

    // aux break
    Route::post('/aux-break/start', 'AuxBreakApiController@start');
    Route::put('/aux-break/stop/{auxId}', 'AuxBreakApiController@stop');
    Route::get('/aux-break/current', 'AuxBreakApiController@getCurrent');

    // dashboard
    Route::get('/dashboard/metrics/{lobId}/{team}', 'DashboardApiController@getMetrics');
    Route::get('/dashboard/users/{lobId}/{team}', 'DashboardApiController@userList');
    Route::get('/dashboard/user-activity/{userId}', 'DashboardApiController@userActivity');

    // case search
    Route::get('/case-search/{lobId}', 'CaseSearchApiController@get');

    //reports
    Route::get('/reports/hourly-list/', 'ReportsApiController@getHourlyReports');
    Route::get('/breakreports/', 'ReportsApiController@getBreaksReports');
    Route::get('/reports/raw-reports-list/', 'ReportsApiController@getRawReports');
});

