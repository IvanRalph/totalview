<?php

namespace Modules\Provider\Services;

class DashboardService extends BaseService
{
    public function getCases ($rawNumber, $start, $end)
    {
        $table = 'case' . $rawNumber;
        $res   = $this->db->table($table)
            ->leftJoin('user', 'user.id', '=', "$table.user_id")
            ->whereBetween('shift_date', [
                $start,
                $end
            ])
            ->get();

        return $res;
    }

    public function getDashboardUserList ($id)
    {
        $table = 'case' . $id;
        $res   = $this->db->table($table)
            ->leftJoin('user', "$table.user_id", '=', 'user.main_user_id')
            ->leftJoin('attendance', 'user.id', '=', 'attendance.userid')
            ->distinct()
            ->select('user.id', 'user.firstname', 'user.lastname', 'attendance.shift_date', 'user.status')
            ->get();

        return $res;
    }
}