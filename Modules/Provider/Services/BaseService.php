<?php

namespace Modules\Provider\Services;

class BaseService
{
    protected $db;

    public function __construct ()
    {
        $this->db = \DB::connection(config('provider.db_connection'));
    }
}