<?php

namespace Modules\Provider\Services;

class RoleService extends BaseService
{
    public function getRoleList ()
    {
        $res = $this->db->table('role')
            ->whereIn('id', [
                5,
                11,
                6
            ])
            ->get();

        return $res;
    }
}