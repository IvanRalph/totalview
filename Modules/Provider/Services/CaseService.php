<?php

namespace Modules\Provider\Services;

class CaseService extends BaseService
{
    const TABLE_NAME = 'case';
    const TABLE_NAME_SUB = 'submeasure';
    const LAST_CASE_HOUR_INTERVAL = 5;

    /**
     * @var CaseActivityLogService
     */
    private $caseActivityLogService;

    /**
     * @var StatusService
     */
    private $statusService;

    /**
     * @var FieldService
     */
    private $fieldService;

    public function __construct (
        CaseActivityLogService $caseActivityLogService,
        StatusService $statusService,
        FieldService $fieldService
    )
    {
        parent::__construct();
        $this->caseActivityLogService = $caseActivityLogService;
        $this->statusService          = $statusService;
        $this->fieldService           = $fieldService;
    }

    public function getUserCompleted ($lobId, $userId, $shiftDate)
    {
        return $this->db->table(self::TABLE_NAME . $lobId)
            ->where('user_id', $userId)
            ->where('shift_date', $shiftDate)
            ->whereIn('status_id', [
                StatusService::COMPLETED_STATUS_ID,
                StatusService::EDIT_COMPLETED_STATUS_ID
            ])
            ->get();
    }

    public function getUserPending ($lobId, $userId, $shiftDate)
    {
        return $this->db->table(self::TABLE_NAME . $lobId)
            ->where('user_id', $userId)
            ->where('shift_date', $shiftDate)
            ->whereIn('status_id', [StatusService::HOLD_STATUS_ID])
            ->get();
    }

    public function getUserEscalated ($lobId, $userId, $shiftDate)
    {
        return $this->db->table(self::TABLE_NAME . $lobId)
            ->where('user_id', $userId)
            ->where('shift_date', $shiftDate)
            ->whereIn('status_id', [StatusService::ESCALATED_STATUS_ID])
            ->get();
    }

    public function getUserCaseTable ($lobId, $userId, $shiftDate)
    {
        return $this->db->table(self::TABLE_NAME . $lobId)
            ->where('user_id', $userId)
            ->where('shift_date', $shiftDate)
            ->whereIn('status_id', [
                StatusService::COMPLETED_STATUS_ID,
                StatusService::EDIT_COMPLETED_STATUS_ID,
                StatusService::ESCALATED_STATUS_ID,
                StatusService::HOLD_STATUS_ID
            ])
            ->get();
    }

    public function getById ($lobId, $id)
    {
        return $this->db->table(self::TABLE_NAME . $lobId)
            ->where('id', $id)
            ->first();
    }

    public function getUserTrackerStats ($lobId, $userId, $shiftDate)
    {
        $totalDuration = 0;
        $ahtSec        = 0;

        $cases              = $this->getUserCompleted($lobId, $userId, $shiftDate);
        $totalCompletedCase = count($cases);

        $holdCases     = $this->getUserPending($lobId, $userId, $shiftDate);
        $totalHoldCase = count($holdCases);

        $escalatedCases     = $this->getUserEscalated($lobId, $userId, $shiftDate);
        $totalEscalatedCase = count($escalatedCases);

        if ($totalCompletedCase > 0) {
            foreach ($cases as $case) {
                $totalDuration += duration_to_seconds($case->duration);
            }

            $totalCases = $totalCompletedCase + $totalHoldCase + $totalEscalatedCase;

            //$ahtSec = $totalDuration / $totalCompletedCase;
            $ahtSec = $totalDuration / $totalCases;
        }

        return [
            'total_completed_case' => $totalCompletedCase,
            'total_hold_case'      => $totalHoldCase,
            'total_escalated_case' => $totalEscalatedCase,
            'aht'                  => gmdate('H:i:s', $ahtSec),
            'total_duration'       => gmdate('H:i:s', $totalDuration)
        ];
    }

    public function create ($lobId, $userId, $shiftDate, $data, $fields)
    {
        $caseTable        = self::TABLE_NAME . $lobId;
        $actionId         = 1;
        $customFieldsData = $this->establishCustomFieldsData($fields, $data);
        $statusId         = $this->statusService->getStatusIdByActionId($actionId);

        $mainFieldsData = [
            'shift_date' => $shiftDate,
            'user_id'    => $userId,
            'status_id'  => $statusId,
            'duration'   => $data['duration'],
            'created_at' => $data['created_at']
        ];

        $this->db->beginTransaction();

        try {
            $caseId = $this->db->table($caseTable)->insertGetId(array_merge($mainFieldsData, $customFieldsData));
            $this->caseActivityLogService->log($lobId, $caseId, $userId, $shiftDate, $actionId, $statusId, $data['created_at']);

            $this->db->commit();

            return $caseId;
        } catch (\Exception $e) {
            $this->db->rollBack();
        }
    }

    public function update ($lobId, $caseId, $data, $fields)
    {
        $caseTable        = self::TABLE_NAME . $lobId;
        $statusId         = $this->statusService->getStatusIdByActionId($data['action_id']);
        $customFieldsData = $this->establishCustomFieldsData($fields, $data, true);
        $mainFieldsData   = [
            'status_id'  => $statusId,
            'duration'   => $data['duration'],
            'updated_at' => $data['updated_at']
        ];

        $this->db->beginTransaction();

        try {
            $caseInfo = $this->db->table($caseTable)
                ->where('id', $caseId)
                ->first();

            $updateSuccess = $this->db->table($caseTable)
                ->where('id', $caseId)
                ->update(array_merge($customFieldsData, $mainFieldsData));

            $this->caseActivityLogService->log(
                $lobId,
                $caseId,
                $caseInfo->user_id,
                $caseInfo->shift_date,
                $data['action_id'],
                $statusId,
                $data['updated_at']
            );

            $this->db->commit();

            return $updateSuccess;
        } catch (\Exception $e) {
            $this->db->rollBack();
        }
    }

    public function deleteCase ($caseId, $lobId)
    {
        $this->db->beginTransaction();

        try {
            $caseTable = self::TABLE_NAME . $lobId;

            $deleteSuccess = $this->db->table($caseTable)
                ->join('case_activity_log', '.case_id', '=', $caseTable . '.id')
                ->where($caseTable . '.id', $caseId)
                ->delete();

            $this->db->commit();

            return $deleteSuccess;
        } catch (\Exception $e) {
            $this->db->rollBack();
        }
    }

    public function updateStatus ($lobId, $caseId, $statusId, $currentTime)
    {
        $caseTable = self::TABLE_NAME . $lobId;

        $mainFieldsData = [
            'status_id'  => $statusId,
            'updated_at' => $currentTime
        ];
        $updateSuccess  = $this->db->table($caseTable)
            ->where('id', $caseId)
            ->update($mainFieldsData);

        return $updateSuccess;
    }

    public function updateBreakAt ($userId, $breakDurationString)
    {
        $lobId     = 6;
        $caseTable = self::TABLE_NAME . $lobId;

        $currentCase = $this->getCurrentCase($lobId, $userId);

        $caseId = $currentCase->id;

        $startTime = $currentCase->created_at;

        if ($currentCase->break_at != null) {
            $startTime = $currentCase->break_at;
        }

        $computedTime = date('Y-m-d H:i:s', strtotime($breakDurationString, strtotime($startTime)));

        $FieldsData = [
            'break_at' => $computedTime
        ];

        $updateSuccess = $this->db->table($caseTable)
            ->where('id', $caseId)
            ->update($FieldsData);

        return $updateSuccess;
    }

    public function getCases ($rawNumber, $start, $end, $timezone)
    {
        if ($timezone == '1') {
            $hour = '14';
        } else {
            $hour = '0';
        }
        $selectLob = $this->fieldService->getByLobId($rawNumber);
        $fields    = [];
        foreach ($selectLob as $key) {
            $fields[] = ([
                'html_name' => $key->html_name,
                'name'      => $key->name,
            ]);
        }

        for ($i = 0; $i <= count($selectLob) - 1; $i++) {
            $field[] = $fields[$i]['html_name'] . ' as ' . $fields[$i]['name'];
        }

        $table    = 'case' . $rawNumber;
        $column[] = $table . "." . 'shift_date AS Shift Date';
        $column[] = 'duration AS Duration';
        $column[] = \DB::raw("CONCAT(user.firstname,' ',user.lastname) AS Name");
        $column[] = 'status.name AS Status';
        $column[] = 'duration AS Duration';
        $date[]   = \DB::raw("DATE_FORMAT(created_at, DATE_SUB(created_at, INTERVAL '$hour' HOUR)) as Created");
        $date[]   = \DB::raw("DATE_FORMAT(updated_at, DATE_SUB(updated_at, INTERVAL '$hour' HOUR)) as Updated");
        $columns  = array_merge($column, $field, $date);

        $res = $this->db->table($table)
            ->leftJoin('user', 'user.main_user_id', '=', "$table.user_id")
            ->leftJoin('status', "$table.status_id", '=', 'status.id')
            ->whereBetween("$table.shift_date", [
                $start,
                $end
            ])
            ->distinct()
            ->select($columns)
            ->get();

        return $res;
    }

    public function getCurrentCase ($lobId, $userId)
    {
        $caseTable = self::TABLE_NAME . $lobId;
        $statusIds = StatusService::ONGOING_STATUS_ID;

        return $this->db->table($caseTable)
            ->where('user_id', $userId)
            ->whereIn('status_id', $statusIds)
            ->where(function ($query) {
                $query->where('created_at', '>', deduct_timestamp_hours(self::LAST_CASE_HOUR_INTERVAL))
                    ->orWhere('updated_at', '>', deduct_timestamp_hours(self::LAST_CASE_HOUR_INTERVAL));
            })
            ->orderBy('id', 'desc')
            ->first();
    }

    public function countByStatusId ($lobId, $statusId, $teamId)
    {
        $caseTable = self::TABLE_NAME . $lobId;

        $shiftDate = $this->db->table('user')->where('user.id', session('user_from_account')->id)->first();

        $db = $this->db->table($caseTable)
            ->join('user', $caseTable . '.user_id', 'user.main_user_id')
            ->where('user.team', $teamId)
            ->where($caseTable . '.shift_date', $shiftDate->shift_date)
            ->whereIn('user.role', [
                6,
                7,
                11
            ]);

        if (is_array($statusId)) {
            $db->whereIn('status_id', $statusId);
        } else {
            $db->where('status_id', $statusId);
        }

        return $db->count();
    }

    public function getByUser ($lobId, $userId, $filters = [])
    {
        $query = $this->db->table(self::TABLE_NAME . $lobId)
            ->where('user_id', $userId);

        foreach ($filters as $filter) {
            $query = $query->where($filter['column'], $filter['comparison'], $filter['value']);
        }

        return $query->get();
    }

    public function getByConditions ($lobId, $userId, $conditions = [], $fetchSingleRow = false)
    {
        $query = $this->db->table(self::TABLE_NAME . $lobId);

        foreach ($conditions as $condition) {
            $query->where($condition['column'], $condition['comparison'], $condition['value'])
                ->where('status_id', 4)
                ->where('user_id', $userId);
        }

        if ($fetchSingleRow) {
            return $query->first();
        }

        return $query->get();
    }

    private function establishCustomFieldsData ($fields, $data, $isUpdate = false)
    {
        $customFieldsData = [];
        foreach ($fields as $field) {
            if (isset($data[$field->html_name])) {
                $customFieldsData[$field->html_name] = $data[$field->html_name];
            } else if (!$isUpdate) {
                $customFieldsData[$field->html_name] = $field->default_value;
            }
        }

        return $customFieldsData;
    }

    public function dashboardQueryBuilder ($lobId, $teamId)
    {
        $shiftDate = $this->db->table('user')->where('user.id', session('user_from_account')->id)->first();

        $result = $this->db->select('Select
            CONCAT(user.lastname, ", ", user.firstname) AS name,
            team.description AS team,
            user.shift_date AS shift_date,
            COALESCE(CONCAT(shift_schedule.start_shift, " - ", shift_schedule.end_shift), "00:00:00 - 00:00:00") AS shift_schedule,
            SUM(IF(case' . $lobId . '.status_id IN (4,8), 1, 0)) AS total_completed_cases,
            SUM(IF(case' . $lobId . '.status_id = 6, 1, 0)) AS total_pending_cases,
            SUM(IF(case' . $lobId . '.status_id = 5, 1, 0)) AS total_escalated_case,
            SEC_TO_TIME(SUM(IF(case' . $lobId . '.status_id IN (4,8), TIME_TO_SEC(case' . $lobId . '.duration), 0))) AS total_duration,
            COALESCE(SEC_TO_TIME(CEILING(SUM(IF(case' . $lobId . '.status_id IN (4,8), TIME_TO_SEC(case' . $lobId . '.duration), 0)) / SUM(IF(case' . $lobId . '.status_id IN (4,8), 1, 0)))), "00:00:00") AS aht, user.main_user_id AS main_user_id, user.status AS status
            FROM user
            left join case' . $lobId . '
            on user.main_user_id = case' . $lobId . '.user_id
            AND case' . $lobId . '.shift_date = "' . $shiftDate->shift_date . '"
            INNER JOIN team
            ON user.team = team.id
            LEFT JOIN shift_schedule
            ON user.main_user_id = shift_schedule.user_id
            AND shift_schedule.shift_date = user.shift_date
            WHERE user.team = ' . $teamId . '
            AND user.role IN (6,7,11)
            GROUP BY user.main_user_id
            ORDER BY user.status DESC');

        return $result;
    }
}