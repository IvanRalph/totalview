<?php
/**
 * Created by PhpStorm.
 * User: OMLanuza
 * Date: 1/17/2018
 * Time: 4:11 PM
 */

namespace Modules\Provider\Services;

class ReportService extends BaseService
{
    public function getHourly ($lobId, $dateFrom, $dateTo, $timezone)
    {
        if ($timezone == 'CST') {
            $hour = 'concat(hour(date_add(case_activity_log.created_at,INTERVAL 13 HOUR)), ":00") as hour';
        } else {
            $hour = 'concat(hour(case_activity_log.created_at), ":00") as hour';
        }

        return $this->db->table('case_activity_log')
            ->select(
                $this->db->raw('concat(user.lastname, \', \', user.firstname) as Name'),
                $this->db->raw($hour),
                $this->db->raw('count(case_activity_log.id) as count')
            )
            ->leftJoin('user', 'user.id', '=', 'case_activity_log.user_id')
            ->where('lob_id', $lobId)
            ->whereNotIn('status_id', StatusService::ONGOING_STATUS_ID)
            ->whereBetween('created_at', [
                $dateFrom,
                $dateTo
            ])
            ->groupBy('user.id', $this->db->raw('hour(case_activity_log.created_at)'))
            ->get()
            ->toArray();
    }

    public function getRaw ($rawNumber, $start, $end, $timezone)
    {
        // if ($timezone == 'CST') {
        //    $start = $start . " 00:00:00";
        //    $end = $end . " 23:59:59";
        // } else {
        //    $start = $start . " 00:00:00";
        //    $end = $end . " 23:59:59";

        // }

        return $this->db->table('raw')
            ->select('raw.shift_date as Shift Date', $this->db->raw('concat(user.lastname, \', \', user.firstname) as Name'), 'team.description as Team', 'raw.timestamp_start as Start Time',
                'raw.timestamp_end as End Time', 'raw.duration as Duration', 'raw.taskid as Task ID', 'raw.verification_result as Verification Result', 'raw.utd as UTD',
                'raw.type as Type', 'raw.authority as Authority', 'raw.notes as Notes', 'status.name as Status', 'raw.prod_count as Prod Count'
            )
            ->leftJoin('user', 'user.main_user_id', '=', 'raw.userid')
            ->leftJoin('status', 'status.id', '=', 'raw.status_id')
            ->leftJoin('team', 'team.ID', '=', 'raw.teamid')
            ->where('raw.shift_date', '>=', $start)
            ->where('raw.shift_date', '<=', $end)
            ->get();
    }

    public function getBreaks ($start, $end, $timezone)
    {
        // if ($timezone == 'CST') {
        //    $start = $start . " 00:00:00";
        //    $end = $end . " 23:59:59";
        // } else {
        //    $start = $start . " 00:00:00";
        //    $end = $end . " 23:59:59";

        // }

        return $this->db->table('breaks')
            ->select($this->db->raw('concat(user.lastname, \', \', user.firstname) as Name'), 'breaks.shift_date as Shift', 'aux_type.name as AuxBreak', 'breaks.remarks as Remarks',
                'breaks.start_time as Start', 'breaks.end_time as End', 'breaks.duration as Duration'
            )
            ->leftJoin('user', 'user.id', '=', 'breaks.user_id')
            ->leftJoin('aux_type', 'aux_type.id', '=', 'breaks.aux_type_id')
            ->where('breaks.shift_date', '>=', $start)
            ->where('breaks.shift_date', '<=', $end)
            ->get();
    }
}