<?php

namespace Modules\Provider\Services;

class LobService extends BaseService
{
    public function __construct ()
    {
        parent::__construct();
    }

    public function getLobList ()
    {
        return $this->db->table('lob')->get();
    }

    public function deleteLobUser ($id)
    {
        $dataId = explode(",", $id);
        array_push($dataId, 0);
        $res = $this->db->table('user_lob')
            ->whereIn('user_id', $dataId)
            ->delete();
    }

    public function insertLobUser ($id, $lob)
    {
        $dataId  = explode(",", $id);
        $dataLob = explode(",", $lob);

        $insert = [];
        foreach ($dataId as $id) {
            foreach ($dataLob as $lob) {
                $insert[] = ([
                    'user_id' => $id,
                    'lob_id'  => $lob
                ]);
            }
        }

        $res = $this->db->table('user_lob')
            ->insert($insert);
    }

    public function getSettings ($lobId)
    {
        return $this->db->table('lob_setting')
            ->where('lob_id', $lobId)
            ->first();
    }
}