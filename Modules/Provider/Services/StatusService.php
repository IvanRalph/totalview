<?php

namespace Modules\Provider\Services;

class StatusService extends BaseService
{
    const COMPLETED_STATUS_ID = '4';
    const HOLD_STATUS_ID = '6';
    const ESCALATED_STATUS_ID = '5';
    const ONGOING_STATUS_ID
        = [
            1,
            3,
            7
        ];
    const EDIT_COMPLETED_STATUS_ID = 8;
    const EDIT_ACTION_NAME = 'Edit';
    const CONTINUE_ACTION_NAME = 'Continue';
    const EDIT_ACTION_ID = 7;
    const CONTINUE_ACTION_ID = 3;
    const HOLD_ACTION_ID = 6;
    const SAVE_EDIT_ACTION_ID = 8;

    public function getAll ()
    {
        return $this->db->table('status')
            ->get();
    }

    public function getStatusIdByActionId ($actionId)
    {
        $action = $this->db->table('action')
            ->select('status_id')
            ->where('id', $actionId)
            ->first();

        return $action->status_id;
    }
}