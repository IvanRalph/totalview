<?php

namespace Modules\Provider\Services;

class TeamService extends BaseService
{
    public function getLeader ($teamId)
    {
        $teamInfo = $this->db->table('team')
            ->where('id', $teamId)
            ->first();

        $teamLeadersId = explode(',', $teamInfo->lead);

        return $this->db->table('user')
            ->whereIn('main_user_id', $teamLeadersId)
            ->get();
    }

    public function getTeamByUser ($userId)
    {
        return $this->db->table('user')
            ->join('team', 'team.ID', '=', 'user.team')
            ->select('team.description as team')
            ->where('user.main_user_id', $userId)
            ->first();
    }

    public function getTeamList ()
    {
        return $this->db->table('team')
            ->orderByRaw('CASE WHEN lead = ' . session('user_from_account')->id . ' THEN 1 ELSE 2 END, lead')
            ->get();
    }
}