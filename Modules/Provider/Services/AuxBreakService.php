<?php

namespace Modules\Provider\Services;

class AuxBreakService extends BaseService
{
    public function getAuxTypes ()
    {
        return $this->db->table('aux_type')->get();
    }

    public function create ($userId, $shiftDate, $data)
    {
        $insertData = array_merge(
            ['user_id'    => $userId,
             'shift_date' => $shiftDate
            ],
            $data
        );

        return $this->db->table('aux')
            ->insertGetId($insertData);
    }

    public function update ($auxId, $data)
    {
        return $this->db->table('aux')
            ->where('id', $auxId)
            ->update($data);
    }

    public function getCurrent ($userId, $shiftDate)
    {
        return $this->db->table('aux')
            ->where('user_id', $userId)
            ->where('shift_date', $shiftDate)
            ->whereNull('end_time')
            ->first();
    }

    public function getLastBreak ($userId)
    {
        return $this->db->table('aux')
            ->where('user_id', $userId)
            ->orderBy('id', 'desc')
            ->first();
    }

    public function getAuxTypeNameById ($auxTypeId)
    {
        $auxType = $this->db->table('aux_type')
            ->where('id', $auxTypeId)
            ->first();

        return $auxType->name;
    }
}