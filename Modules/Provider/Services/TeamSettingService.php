<?php

namespace Modules\Provider\Services;

use DB;

class TeamSettingService extends BaseService
{
    public function getUserList ()
    {
        $role               = session()->get('user_from_account')->role;
        $idUser             = session()->get('user_from_account')->main_user_id;
        $current_shift_date = $this->getLatestShiftDate()->shift_date;
        if ($role == '5') {
            $ifTeam
                = "(CASE
              WHEN FIND_IN_SET('$idUser', team.lead)  THEN '1'
                  else 0
            END) ";
        } else {
            $ifTeam = '1';
        }

        $res = $this->db->table('user')
            ->leftJoin('shift_schedule', 'user.main_user_id', '=', 'shift_schedule.user_id')
            ->leftJoin('user_lob', 'user.main_user_id', '=', 'user_lob.user_id')
            ->leftJoin('lob', 'lob.id', '=', 'user_lob.lob_id')
            ->leftJoin('team', 'team.id', '=', 'user.team')
            ->leftJoin('role', 'role.id', '=', 'user.role')
            //->leftJoin("tv_main.lob as main_lob", "user_lob.lob_id", '=', "main_lob.id")
            ->select('user.main_user_id as id', \DB::raw("CONCAT(user.firstname,' ',user.lastname) as full_name"),

                // \DB::raw("GROUP_CONCAT(main_lob.name) as lob"),

                'lob.name as lob',
                'team.description', 'user.shift_date', 'user.shift_end',
                'team.lead as teamLeadID',
                'shift_schedule.start_shift', 'shift_schedule.end_shift', 'shift_schedule.leave', 'user.status',
                'team.id as teamID',
                'user_lob.lob_id',
                \DB::raw("GROUP_CONCAT(user_lob.lob_id ) as lob_id"),
                'role.id as roleID', 'user.ramp',
                \DB::raw("$ifTeam as blocked")
            )
            ->where('shift_schedule.shift_date', '=', $current_shift_date)
            ->where('user.active', '=', '1')
            ->groupBy('user_lob.user_id')
            ->get();

        return $res;
    }

    public function getTeamList ()
    {
        $res = $this->db->table('team')
            ->leftJoin('user', function ($join) {
                $join->where(DB::raw('FIND_IN_SET(user.id, team.lead)'), '>', 0);
            })
            ->select('team.ID', 'team.lead', 'user.username', 'team.end_time', 'team.description as team',
                \DB::raw("GROUP_CONCAT(CONCAT(' ',user.firstname,' ', user.lastname, ' ' ) ORDER BY user.id ) as fullname"))
            ->groupBy('team.ID')
            ->get();

        return $res;
    }

    public function getLatestShiftDate ()
    {
        return $shiftDate = $this->db->table('shift_schedule')
            ->select('shift_date')
            ->orderBy('shift_date', 'desc')
            ->first();
    }

    public function getTeamListForTable ()
    {
        $res = $this->db->table('team')
            ->leftJoin('user', function ($join) {
                $join->where(DB::raw('FIND_IN_SET(user.id, team.lead)'), '>', 0);
            })
            ->select('team.ID', 'team.lead', 'user.username', 'team.end_time',
                'team.description as team', \DB::raw("GROUP_CONCAT(CONCAT(' ',user.firstname,' ', user.lastname, ' ' ) ORDER BY user.id ) as fullname"))
            ->groupBy('team.ID')
            ->get();

        return $res;
    }

    public function getTeamLeader ()
    {
        $res = $this->db->table('user')
            ->select(\DB::raw("CONCAT(user.firstname,' ',user.lastname) as full_name"), 'user.id')
            ->whereIn('role', [
                4,
                5
            ])
            ->get();

        return $res;
    }

    public function updateTeamList ($editID, $editTeamName, $checkbox, $editKickout)
    {
        $res = $this->db->table('team')
            ->where('ID', $editID)
            ->update([
                'description' => $editTeamName,
                'lead'        => $checkbox,
                'end_time'    => $editKickout
            ]);
    }

    public function updateTeamUserScheduleShift ($user_id, $user_start_shift, $user_end_shift, $onLeave, $onShift, $teamList, $slctRole, $ramp)
    {
        $current_shift_date = $this->getLatestShiftDate()->shift_date;

        $res = $this->db->table('shift_schedule')
            ->join('user', 'shift_schedule.user_id', '=', 'user.main_user_id')
            ->where('shift_schedule.user_id', $user_id)
            ->where('shift_schedule.shift_date', $current_shift_date)
            ->update([
                'shift_schedule.start_shift' => $user_start_shift,
                'shift_schedule.end_shift'   => $user_end_shift,
                'shift_schedule.leave'       => $onLeave,
                'user.shift_end'             => $onShift,
                'user.role'                  => $slctRole,
                'user.team'                  => $teamList,
                'user.ramp'                  => $ramp
            ]);

        $mainRole = 1;

        if ($slctRole != 5) {
            $mainRole = 2;
        }

        $this->db->table('tv_main.user')
            ->where('id', $user_id)
            ->update([
                'role_id' => $mainRole,
            ]);
    }

    public function updateTeamSchedule ($id, $start_shift, $end_shift, $on_leave, $ramp)
    {
        $myArrayId = explode(',', $id);

        $res = $this->db->table('shift_schedule')
            ->whereIn('user_id', $myArrayId)
            ->join('user', 'user.main_user_id', '=', 'shift_schedule.user_id')
            ->update([
                'start_shift' => $start_shift,
                'end_shift'   => $end_shift,
                'leave'       => $on_leave,
                'user.ramp'   => $ramp
            ]);
    }

    public function updateTeamBulk ($id, $team)
    {
        $myArrayId = explode(',', $id);

        $res = $this->db->table('user')
            ->whereIn('id', $myArrayId)
            ->update(['team' => $team]);
    }
}