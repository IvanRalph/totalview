<?php

namespace Modules\Main\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Main\Services\Core\LobInterface;

class BaseReportsWebController extends Controller
{
    /** @var LobInterface */
    protected $lobService;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index ()
    {
        $data['getLobList'] = $this->lobService->getLobList();

        return view(session('selected_account')->route . '::reports.index', $data);
    }
}




