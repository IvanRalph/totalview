<?php

namespace Modules\Main\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Main\Services\Core\LobInterface;
use Modules\Main\Services\Core\RoleInterface;
use Modules\Main\Services\Core\TeamSettingInterface;
use Modules\Main\Services\Core\UserInterface;

class BaseTeamSettingsWebController extends Controller
{
    /** @var TeamSettingInterface */
    protected $teamSettingService;

    /** @var LobInterface */
    protected $lobService;

    /** @var RoleInterface */
    protected $roleService;

    /** @var UserInterface */
    protected $userService;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index ()
    {
        $data['data']['getTeamLeader']  = $this->teamSettingService->getTeamLeader();
        $data['data']['getTeamList']    = $this->teamSettingService->getTeamList();
        $data['data']['getLobList']     = $this->lobService->getLobList();
        $data['data']['getRoleList']    = $this->roleService->getRoleList();
        $data['data']['getNurseStatus'] = $this->userService->getNurseStatus();

        return view(session('selected_account')->route . '::team-settings.index', $data);
        
    }
}