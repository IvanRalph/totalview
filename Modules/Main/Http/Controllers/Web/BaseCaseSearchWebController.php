<?php

namespace Modules\Main\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Main\Services\Core\FieldInterface;
use Modules\Main\Services\Core\LobInterface;

class BaseCaseSearchWebController extends Controller
{
    /**
     * @var LobInterface
     */
    protected $lobService;

    /**
     * @var FieldInterface
     */
    protected $fieldService;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index ()
    {
        $lobs = $this->lobService->getLobList();

        return view(session('selected_account')->route . '::case-search.index', ['lobs' => $lobs]);
    }
}