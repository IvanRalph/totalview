<?php

namespace Modules\Main\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Main\Services\Core\DashboardInterface;
use Modules\Main\Services\Core\LobInterface;
use Modules\Main\Services\Core\TeamInterface;
use Modules\Main\Services\Core\UserInterface;

class BaseDashboardWebController extends Controller
{
    /** @var DashboardInterface */
    protected $dashboardService;

    /** @var LobInterface */
    protected $lobService;

    /** @var UserInterface */
    protected $userService;

    /** @var TeamInterface */
    protected $teamService;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index ()
    {
        $data['data']['getLobList']   = $this->lobService->getLobList();
        $data['data']['getShiftDate'] = $this->userService->getShiftDateForDashboard();
        $data['data']['getTeamList']  = $this->teamService->getTeamList();

        return view(session('selected_account')->route . '::dashboard.index', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fullscreen ()
    {
        return view(session('selected_account')->route . '::dashboard.partials.dashboard-fullscreen');
    }
}