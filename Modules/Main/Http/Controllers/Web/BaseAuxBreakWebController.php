<?php

namespace Modules\Main\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Main\Services\Core\AuxBreakInterface;

class BaseAuxBreakWebController extends Controller
{
    /** @var AuxBreakInterface */
    protected $auxBreakService;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index ()
    {
        $auxTypes = $this->auxBreakService->getAuxType();

        return view(session('selected_account')->route . '::aux-break.index', ['aux_types' => $auxTypes]);
    }
}