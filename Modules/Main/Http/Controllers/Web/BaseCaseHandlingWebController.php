<?php

namespace Modules\Main\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Main\Services\Core\CaseInterface;
use Modules\Main\Services\Core\FieldInterface;
use Modules\Main\Services\Core\ListValueInterface;
use Modules\Main\Services\Core\LobInterface;
use Modules\Main\Services\Core\UserAuditTrailInterface;
use Modules\Main\Services\Core\UserInterface;

class BaseCaseHandlingWebController extends Controller
{
    /**
     * @var FieldInterface
     */
    protected $fieldService;

    /**
     * @var ListValueInterface
     */
    protected $listValueService;

    /**
     * @var CaseInterface
     */
    protected $caseService;

    /**
     * @var UserInterface
     */
    protected $userService;

    /**
     * @var UserAuditTrailInterface
     */
    protected $userAuditTrailService;

    /**
     * @var LobInterface
     */
    protected $lobService;

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index (Request $request)
    {
        $fields          = [];
        $listValues      = [];
        $trackerStats    = [];
        $lobSettings     = [];
        $mainUser        = $request->get('userData');
        $userLobs        = session('user_lobs_info', []);
        $userFromAccount = $this->userService->getByMainUserId($mainUser['id']);

        foreach ($userLobs as $lob) {
            $lobSettings[$lob->id]  = $this->lobService->getSettings($lob->id);
            $fields[$lob->id]       = $this->fieldService->getByLobId($lob->id);
            $listValues[$lob->id]   = $this->listValueService->getByLobId($lob->id);
            $trackerStats[$lob->id] = $this->caseService->getUserTrackerStats(
                $lob->id,
                $userFromAccount->main_user_id,
                $userFromAccount->shift_date
            );
        }

        return view(
            session('selected_account')->route . '::case-handling.index',
            [
                'userLobs'     => $userLobs,
                'lobSettings'  => $lobSettings,
                'fields'       => $fields,
                'listValues'   => $listValues,
                'trackerStats' => $trackerStats,
                'userInfo'     => $userFromAccount,
                'role'         => $userFromAccount->role,
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function startShift (Request $request)
    {
        $mainUser        = $request->get('userData');
        $userFromAccount = $this->userService->getByMainUserId($mainUser['id']);
        $selectedAccount = session('selected_account');

        $post          = [
            'main_user_id' => $mainUser['id'],
            'data'         => ['shift_start' => date('Y-m-d H:i:s')]
        ];
        $updateSuccess = $this->userService->update($post);

        if ($updateSuccess) {
            $post = [
                'main_user_id' => $mainUser['id'],
                'data'         => ['login' => date('Y-m-d H:i:s')]
            ];

            $shiftId = $this->userService->updateShiftSchedule($post);
            $this->userAuditTrailService->record(
                $mainUser['id'],
                $userFromAccount->shift_date,
                $this->userAuditTrailService::ACTION_START_SHIFT,
                '',
                null,
                $shiftId
            );
        }

        return redirect($selectedAccount->route . '?token=' . $request->get('token'));
    }

    /**
     * @param Request $request
     */
    public function endShift (Request $request)
    {
        $mainUser        = $request->get('userData');
        $userFromAccount = $this->userService->getByMainUserId($mainUser['id']);

        $post          = [
            'main_user_id' => $mainUser['id'],
            'data'         => ['shift_start' => date('Y-m-d H:i:s')]
        ];
        $updateSuccess = $this->userService->update($post);

        if ($updateSuccess) {
            $post = [
                'main_user_id' => $mainUser['id'],
                'data'         => ['logout' => date('Y-m-d H:i:s')]
            ];

            $shiftId = $this->userService->updateShiftSchedule($post);
            $this->userAuditTrailService->record(
                $mainUser['id'],
                $userFromAccount->shift_date,
                $this->userAuditTrailService::ACTION_END_SHIFT,
                '',
                null,
                $shiftId
            );
        }

        return;
        // return redirect()->route('accounts', ['token' => $request->get('token')]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirmEndShift ()
    {
        return view(session('selected_account')->route . '::end-shift.index');
    }
}