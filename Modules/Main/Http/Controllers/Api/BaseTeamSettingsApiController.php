<?php

namespace Modules\Main\Http\Controllers\Api;

use http\Env\Response;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Examworks\Entities\ListValue;
use Modules\Main\Services\Core\LobInterface;
use Modules\Main\Services\Core\TeamSettingInterface;

class BaseTeamSettingsApiController extends Controller
{
    /** @var TeamSettingInterface */
    protected $teamSettingService;

    /** @var LobInterface */
    protected $lobService;

    /**
     * @return mixed
     */
    public function getUserList ()
    {
        $userList = $this->teamSettingService->getUserList();

        return $userList;
    }

    /**
     * @return mixed
     */
    public function getTeamList ()
    {
        $teamList = $this->teamSettingService->getTeamList();

        return $teamList;
    }

    /**
     * @return mixed
     */
    public function getTeamListForTable ()
    {
        $teamList = $this->teamSettingService->getTeamListForTable();

        return $teamList;
    }

    /**
     * @return mixed
     */
    public function getLobDropdownList ($lobId)
    {
        $dropdownList = $this->teamSettingService->getLobDropdownList($lobId);

        return $dropdownList;
    }

    /**
     * @return mixed
     */
    public function getDropdownItems ($fieldId)
    {
        $teamList = $this->teamSettingService->getDropdownItems($fieldId);

        return $teamList;
    }

    /**
     * @param $teamLeaderID
     * @param $checkbox
     * @return mixed
     */
    public function updateTeamList ($teamLeaderID, $checkbox)
    {
        $teamLeaderArray = explode(",", $teamLeaderID);

        $editID       = $teamLeaderArray[0];
        $editTeamName = $teamLeaderArray[1];
        $editKickout  = $teamLeaderArray[2];
        $post         = [
            'edit_id'   => $editID,
            'team_name' => $editTeamName,
            'checkbox'  => $checkbox,
            'kick_out'  => $editKickout
        ];

        $updateTeamList = $this->teamSettingService->updateTeamList($post);

        return $updateTeamList;
    }

    /**
     * @param $teamLeaderID
     * @param $checkbox
     * @return mixed
     */
    public function addTeamList ($teamLeaderID, $checkbox)
    {
        $teamLeaderArray = explode(",", $teamLeaderID);

        $addTeamName = $teamLeaderArray[0];
        $addKickout  = $teamLeaderArray[1];

        $post = [
            'team_name' => $addTeamName,
            'checkbox'  => $checkbox,
            'kick_out'  => $addKickout
        ];

        $addTeamList = $this->teamSettingService->addTeamList($post);

        return $addTeamList;
    }

    /**
     * @param $teamListInfo
     * @return mixed
     */
    public function updateTeamUser ($teamListInfo)
    {
        $teamListInfoArray = explode(",", $teamListInfo);

        $user_id          = $teamListInfoArray[0]; // user id
        $user_start_shift = $teamListInfoArray[1]; // user_start_shift
        $user_end_shift   = $teamListInfoArray[2]; // user_end_shift
        $onLeave          = $teamListInfoArray[3]; // onLeave
        $onShift          = ($teamListInfoArray[4] == 1) ? null : date('Y-m-d H:i:s'); // onShift
        $teamList         = $teamListInfoArray[5]; // teamList
        $slctRole         = $teamListInfoArray[6]; //select role
        $ramp             = $teamListInfoArray[7];
        $nurseStatus      = $teamListInfoArray[8];

        $post = [
            'user_id'      => $user_id,
            'start_shift'  => $user_start_shift,
            'end_shift'    => $user_end_shift,
            'on_leave'     => $onLeave,
            'on_shift'     => $onShift,
            'team_list'    => $teamList,
            'slct_role'    => $slctRole,
            'ramp'         => $ramp,
            'nurse_status' => $nurseStatus
        ];

        $updateTeamUserScheduleShift = $this->teamSettingService->updateTeamUserScheduleShift($post);

        return $updateTeamUserScheduleShift;
    }

    /**
     * @param $id
     * @param $lob
     * @return array
     */
    public function updateUserLob ($id, $lob)
    {


        $deleteLobUser = $this->lobService->deleteLobUser($id);
        $insertLobUser = $this->lobService->insertLobUser([
            'id'     => $id,
            'lob_id' => $lob
        ]);

        return array(
            $deleteLobUser,
            $insertLobUser
        );
    }

    public function updateValuelistItem (Request $request)
    {
        $valuelistData = $request->get('data');

        $post = [
            'itemId'     => $valuelistData['itemId'],
            'itemName'   => $valuelistData['itemName'],
            'itemStatus' => $valuelistData['itemStatus']

        ];

        $updateValuelistUser = $this->teamSettingService->updateValuelistItem($post);

        return $updateValuelistUser;
    }

    public function addValuelistItem (Request $request,$lobId,$fieldid)
    {


        $valuelistItemData   = $request->post('data');

        $insertValuelist = $this->teamSettingService->insertValuelistItem($valuelistItemData,$lobId,$fieldid);
        return json_encode($insertValuelist);

    }
    
    public function updateTeamSchedule ($id, $formData, Request $request)
    {
        $myArrayId       = explode(',', $id);
        $myArrayFormData = explode(',', $formData);

        $start_shift = $myArrayFormData[0];
        $end_shift   = $myArrayFormData[1];
        $on_leave    = $myArrayFormData[2];
        $ramp        = $myArrayFormData[3];

        $post = [
            'id'          => $id,
            'start_shift' => $start_shift,
            'end_shift'   => $end_shift,
            'on_leave'    => $on_leave,
            'ramp'        => $ramp
        ];

        $updateTeamUser = $this->teamSettingService->updateTeamSchedule($post);

        return $updateTeamUser;
    }

    public function updateTeamBulk ($id, $team)
    {
        $updateTeamUser = $this->teamSettingService->updateTeamBulk([
            'id'   => $id,
            'team' => $team
        ]);

        return $updateTeamUser;
    }

    public function updateValuelistItemBulk (Request $request)
    {
        $itemId = explode(',', $request->get('data')['id']);
        $data = [];
        $data['1'] = [];
        $data['0'] = [];

        foreach ($itemId as $key => $value) {
            if ($this->teamSettingService->getStatus($value) == 1) {
                array_push($data['1'], $value);
            }else{
                array_push($data['0'], $value);
            }
        }

        $post = [
            'data' => $data
        ];

        return $this->teamSettingService->updateValueListBulk($post);
    }
}
