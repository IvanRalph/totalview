<?php

namespace Modules\Main\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Main\Services\Core\CaseActivityInterface;
use Modules\Main\Services\Core\CaseInterface;
use Modules\Main\Services\Core\FieldInterface;
use Modules\Main\Services\Core\ListValueInterface;
use Modules\Main\Services\Core\LobInterface;
use Modules\Main\Services\Core\StatusInterface;
use Modules\Main\Services\Core\UserAuditTrailInterface;
use Modules\Vanderbilt\Services\StatusService;
use Modules\Vanderbilt\Services\UserAuditTrailService;

class BaseCaseHandlingApiController extends Controller
{
    /**
     * @var CaseInterface
     */
    protected $caseService;

    /**
     * @var FieldInterface
     */
    protected $fieldService;

    /**
     * @var StatusInterface
     */
    protected $statusService;

    /**
     * @var CaseActivityInterface
     */
    protected $caseActivityLogService;

    /**
     * @var UserAuditTrailInterface
     */
    protected $userAuditTrailService;

    /**
     * @var LobInterface
     */
    protected $lobService;

    /** @var ListValueInterface */
    protected $listValueService;

    /**
     * @param Request $request
     * @param         $lobId
     * @param         $caseId
     * @return \Illuminate\Http\JsonResponse
     */
    public function get (Request $request, $lobId, $caseId)
    {
        $data        = $request->get('data');
        $case        = $this->caseService->getById([
            'lob_id'  => $lobId,
            'case_id' => $caseId
        ]);
        $fields      = $this->fieldService->getByLobId($lobId);
        $actionId    = $request->get('action', 0);
        $currentTime = date('Y-m-d H:i:s');
        if ($case && $actionId) {
            $statusId          = $this->statusService->getStatusIdByActionId($actionId);
            $data['action_id'] = $actionId;

            $post = [
                'lob_id'       => $lobId,
                'case_id'      => $caseId,
                'status_id'    => $statusId,
                'current_time' => $currentTime
            ];

            $this->caseService->updateStatus($post);

            $this->caseActivityLogService->log([
                'lob_id'       => $lobId,
                'case_id'      => $caseId,
                'user_id'      => $case->user_id,
                'shift_date'   => $case->shift_date,
                'action_id'    => $actionId,
                'status_id'    => $statusId,
                'current_time' => $currentTime
            ]);

            $referenceField   = $this->fieldService->getReference($lobId);
            $auditTrailAction = UserAuditTrailService::ACTION_EDIT_CASE;

            if ($actionId == StatusService::CONTINUE_ACTION_ID) {
                $auditTrailAction = UserAuditTrailService::ACTION_CONTINUE_CASE;
            }

            $this->userAuditTrailService->record(
                $case->user_id,
                $case->shift_date,
                $auditTrailAction,
                $case->{$referenceField->html_name},
                $caseId
            );
        }

        return response()->json([
            'case_info' => $case,
            'fields'    => $fields
        ]);
    }

    /**
     * @param Request $request
     * @param         $lobId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCurrent (Request $request, $lobId)
    {
        $mainUser    = $request->get('userData');
        $currentCase = $this->caseService->getCurrentCase($lobId, $mainUser['id']);

        return response()->json(['current_case_info' => $currentCase]);
    }

    /**
     * @param Request $request
     * @param         $lobId
     * @return \Illuminate\Http\JsonResponse
     */
    public function create (Request $request, $lobId)
    {
        $data        = $request->get('data');
        $mainUser    = $request->get('userData');
        $accountUser = session('user_from_account');

        $fields = $this->fieldService->getByLobId($lobId);

        $params = [
            'lob_id'     => $lobId,
            'user_id'    => $mainUser['id'],
            'shift_date' => $accountUser->shift_date,
            'data'       => $data,
            'fields'     => $fields
        ];

        $caseId = $this->caseService->create($params);

        if ($caseId) {
            $referenceField = $this->fieldService->getReference($lobId);

            $this->userAuditTrailService->record(
                $mainUser['id'],
                $accountUser->shift_date,
                UserAuditTrailService::ACTION_START_CASE,
                $data[$referenceField->html_name],
                $caseId
            );
        }

        return response()->json(['case_id' => $caseId]);
    }

    /**
     * @param Request $request
     * @param         $lobId
     * @param         $caseId
     * @return \Illuminate\Http\JsonResponse
     */
    public function update (Request $request, $lobId, $caseId)
    {
        $data        = $request->get('data');
        $mainUser    = $request->get('userData');
        $accountUser = session('user_from_account');

        $validationErrors = $this->validate($lobId, $data, $mainUser['id']);
        if ($validationErrors) {
            return response()->json(['validation_errors' => $validationErrors]);
        } else if (!$validationErrors) {
            $fields = $this->fieldService->getByLobId($lobId);
            $post   = [
                'lob_id'  => $lobId,
                'case_id' => $caseId,
                'data'    => $data,
                'fields'  => $fields
            ];

            $isSuccess = $this->caseService->update($post);

            if ($isSuccess) {
                $referenceField   = $this->fieldService->getReference($lobId);
                $auditTrailAction = UserAuditTrailService::ACTION_END_CASE;

                if ($data['action_id'] == StatusService::HOLD_ACTION_ID) {
                    $auditTrailAction = UserAuditTrailService::ACTION_HOLD_CASE;
                } else if ($data['action_id'] == StatusService::SAVE_EDIT_ACTION_ID) {
                    $auditTrailAction = UserAuditTrailService::ACTION_SAVE_EDIT_CASE;
                }

                $this->userAuditTrailService->record(
                    $mainUser['id'],
                    $accountUser->shift_date,
                    $auditTrailAction,
                    $data[$referenceField->html_name],
                    $caseId
                );
            }

            return response()->json(['is_success' => $isSuccess]);
        }
    }

    /**
     * @param $caseId
     * @param $lobId
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCase ($caseId, $lobId)
    {
        $post = [
            'case_id' => $caseId,
            'lob_id'  => $lobId
        ];

        $isSuccess = $this->caseService->deleteCase($post);

        return response()->json(['is_success' => $isSuccess]);
    }

    /**
     * @param Request $request
     * @param         $lobId
     * @return \Illuminate\Http\JsonResponse
     */
    public function trackerStats (Request $request, $lobId)
    {
        $mainUser    = $request->get('userData');
        $accountUser = session('user_from_account');

        $trackerStatsData = $this->caseService->getUserTrackerStats($lobId, $mainUser['id'], $accountUser->shift_date);

        return response()->json($trackerStatsData);
    }

    /**
     * @param Request $request
     * @param         $lobId
     * @return \Illuminate\Http\JsonResponse
     */
    public function caseTable (Request $request, $lobId)
    {
        $columns     = [
            [
                'data' => 'action',
                'name' => 'Action'
            ]
        ];
        $mainUser    = $request->get('userData');
        $accountUser = session('user_from_account');

        $fields    = $this->fieldService->getByLobId($lobId);
        $userCases = $this->caseService->getUserCaseTable($lobId, $mainUser['id'], $accountUser->shift_date);
        $statuses  = json_decode(json_encode($this->statusService->getAll()->toArray()), true);
        $statusIds = array_column($statuses, 'id');

        foreach ($userCases as $key => $case) {
            $statusKey = array_search($case->status_id, $statusIds);
            $status    = $statuses[$statusKey];

            $case->action = $this->generateActionButton($lobId, $case, $statuses[$statusKey]);
            $case->status = generateStatusDisplay($status);

            $userCases[$key] = $case;
        }


        foreach ($fields as $field) {
            if (!$field->display) {
                continue;
            }

            $columns[] = [
                'data' => $field->html_name,
                'name' => $field->name
            ];
        }

        $columns[] = [
            'data' => 'duration',
            'name' => 'Duration'
        ];
        $columns[] = [
            'data' => 'status',
            'name' => 'Status'
        ];

        return response()->json([
            'data'    => $userCases,
            'columns' => $columns
        ]);
    }

    /**
     * @param $lobId
     * @param $parentId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getChildValues ($lobId, $parentId)
    {
        $post = [
            'lob_id'    => $lobId,
            'parent_id' => $parentId
        ];

        $childValues = $this->listValueService->getByParentId($post);

        return response()->json(['child_values' => $childValues]);
    }

    /**
     * @param $lobId
     * @param $data
     * @param $userId
     * @return mixed
     */
    private function validate ($lobId, $data, $userId)
    {
        $fields  = $this->fieldService->getByLobId($lobId);
        $rules   = [];
        $message = [];
        $data['action_id'] = $data['action_id'] == 4 ? 'Complete' : ($data['action_id'] == 8 ? 'Save' : 'Hold');

        foreach ($fields as $field) {
            if ($field->validation != null) {
                $rules[$field->html_name] = $field->validation;
            }
            $message[$field->html_name] = $field->name;
        }

        $message['action_id'] = 'Action';

        $validator = Validator::make($data, $rules);

        $validator->setAttributeNames($message);

        if ($validator->fails()) {
            return $validator->errors();
        }
    }

    /**
     * @param $lobId
     * @param $case
     * @param $status
     * @return string
     */
    private function generateActionButton ($lobId, $case, $status)
    {
        if (!$status['next_action_name']) {
            return '';
        }

        $classIdentifier = strtolower($status['next_action_name']);

        if ($status['next_action_name'] == StatusService::EDIT_ACTION_NAME) {
            $actionId = StatusService::EDIT_ACTION_ID;
        } else if ($status['next_action_name'] == StatusService::CONTINUE_ACTION_NAME) {
            $actionId = StatusService::CONTINUE_ACTION_ID;
        }

        return '<button data-action-id="' . $actionId . '" data-case-id="' . $case->id . '" data-lob-id="' . $lobId . '" class="btn btn-' . $classIdentifier . ' btn-xs btn-block ' . $status['next_action_name'] . '">' . $status['next_action_name'] . '</button>';
    }
}