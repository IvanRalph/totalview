<?php

namespace Modules\Main\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Main\Services\Core\CaseInterface;
use Modules\Main\Services\Core\TeamInterface;
use Modules\Main\Services\Core\TeamSettingInterface;
use Modules\Main\Services\Core\UserAuditTrailInterface;
use Modules\Main\Services\Core\UserInterface;

class BaseDashboardApiController extends Controller
{
    /**
     * @var CaseInterface
     */
    protected $caseService;

    /**
     * @var UserInterface
     */
    protected $userService;

    /**
     * @var UserAuditTrailInterface
     */
    protected $userAuditTrailService;

    /**
     * @var TeamInterface
     */
    protected $teamService;

    /**
     * @var TeamSettingInterface
     */
    protected $teamSettingService;

    /**
     * @param $lobId
     * @param $teamId
     * @return \Illuminate\Http\JsonResponse
     */
    public function userList ($lobId, $teamId)
    {
        $users = $this->caseService->dashboardQueryBuilder($lobId, $teamId);

        return response()->json(['data' => $users]);
    }

    /**
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function userActivity ($userId)
    {
        $userActivityForDisplay = [];

        $user           = $this->userService->getByMainUserId($userId);
        $userActivities = $this->userAuditTrailService->getByUserId([
            'user_id'    => $userId,
            'shift_date' => $user->shift_date
        ]);

        foreach ($userActivities as $activity) {
            $userActivityForDisplay[] = $this->userAuditTrailService->display($activity);
        }

        return response()->json(['activities' => $userActivityForDisplay]);
    }
}
