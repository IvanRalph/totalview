<?php

namespace Modules\Main\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Main\Services\Core\AuxBreakInterface;
use Modules\Main\Services\Core\CaseInterface;
use Modules\Main\Services\Core\UserAuditTrailInterface;
use Modules\Main\Services\Core\UserInterface;

class BaseAuxBreakApiController extends Controller
{
    /** @var AuxBreakInterface */
    protected $auxBreakService;

    /** @var UserInterface */
    protected $userService;

    /** @var UserAuditTrailInterface */
    protected $userAuditTrailService;

    /** @var CaseInterface */
    protected $caseService;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function start (Request $request)
    {
        $data            = $request->get('data');
        $mainUser        = $request->get('userData');
        $userFromAccount = $this->userService->getByMainUserId($mainUser['id']);

        $data['user_id']    = $mainUser['id'];
        $data['shift_date'] = $userFromAccount->shift_date;

        $auxId = $this->auxBreakService->create($data);

        if ($auxId) {
            $this->userAuditTrailService->record(
                $mainUser['id'],
                $userFromAccount->shift_date,
                $this->userAuditTrailService::ACTION_START_BREAK,
                $this->auxBreakService->getAuxTypeNameById($data['aux_type_id']),
                null,
                null,
                $auxId
            );
        }

        return response()->json(['aux_id' => $auxId]);
    }

    /**
     * @param Request $request
     * @param         $auxId
     * @return \Illuminate\Http\JsonResponse
     */
    public function stop (Request $request, $auxId)
    {
        $mainUser        = $request->get('userData');
        $data            = $request->get('data');
        $userFromAccount = $this->userService->getByMainUserId($mainUser['id']);

        $data['id'] = $auxId;
        $break      = $this->auxBreakService->update($data);
        if ($break) {
            $this->userAuditTrailService->record(
                $mainUser['id'],
                $userFromAccount->shift_date,
                $this->userAuditTrailService::ACTION_START_BREAK,
                $this->auxBreakService->getAuxTypeNameById($data['aux_type_id']),
                null,
                null,
                $break->id

            );
        }

        $breakDuration = $this->getBreaksDuration($mainUser['id']);

        $durationStringFormat = "+" . $breakDuration->h . " hour +" . $breakDuration->i . " minutes +" . $breakDuration->s . " seconds";

        $this->caseService->updateBreakAt([
            'user_id'        => $mainUser['id'],
            'break_duration' => $durationStringFormat
        ]);

        session()->forget('aux_break');

        return response()->json(['update_success' => $break]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCurrent (Request $request)
    {
        $mainUser        = $request->get('userData');
        $userFromAccount = $this->userService->getByMainUserId($mainUser['id']);
        $currentAuxBreak = $this->auxBreakService->getCurrent([
            'user_id'    => $mainUser['id'],
            'shift_date' => $userFromAccount->shift_date
        ]);

        return response()->json(['current_aux_break' => $currentAuxBreak]);
    }

    /**
     * @param $userId
     * @return \DateInterval|false
     */
    public function getBreaksDuration ($userId)
    {
        $lastbreak = $this->auxBreakService->getLastBreak($userId);
        $start     = $lastbreak->start_time;
        $end       = $lastbreak->end_time;

        return date_diff(date_create($start), date_create($end));
    }
}