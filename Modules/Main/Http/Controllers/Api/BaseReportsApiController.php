<?php

namespace Modules\Main\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Modules\Main\Services\Core\CaseInterface;
use Modules\Main\Services\Core\FieldInterface;
use Modules\Main\Services\Core\LobInterface;
use Modules\Main\Services\Core\ReportInterface;

class BaseReportsApiController extends Controller
{
    /** @var LobInterface */
    protected $lobService;

    /** @var ReportInterface */
    protected $reportService;

    /** @var FieldInterface */
    protected $fieldService;

    /** @var CaseInterface */
    protected $caseService;

    /**
     * @return mixed
     */
    public function getLobList ()
    {
        $lobList = $this->lobService->getLobList();

        return $lobList;
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getRawReports (Request $request)
    {
        $rawNumber = $request->get('rawNumber');
        $start     = $request->get('start');
        $end       = $request->get('end');
        $timezone  = $request->get('timezone');
        $data      = $this->reportService->getRaw([
            'rawNumber' => $rawNumber,
            'start' => $start,
            'end'   => $end
        ]);

        return json_encode($data);
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getHoldReports (Request $request)
    {
        $rawNumber = $request->get('rawNumber');
        $start     = $request->get('start');
        $end       = $request->get('end');
        $timezone  = $request->get('timezone');
        $data      = $this->reportService->getHold($rawNumber, $start, $end, $timezone);

        return json_encode($data);
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getConsolidatedReports (Request $request)
    {
        $rawNumber = $request->get('rawNumber');
        $start     = $request->get('start');
        $end       = $request->get('end');
        $timezone  = $request->get('timezone');
        $data      = $this->reportService->getConsolidated([
            'start' => $start,
            'end'   => $end
        ]);

        return json_encode($data);
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getHourlyReports (Request $request)
    {
        $lobId    = $request->get('hourlyNumber');
        $dateFrom = $request->get('start');
        $dateTo   = $request->get('end');
        $timezone = $request->get('timezone');
        $newArray = array();
        $post     = [
            'lob_id'   => $lobId,
            'dateFrom' => $dateFrom,
            'dateTo'   => $dateTo,
            'timezone' => $timezone
        ];

        $hourlyData = $this->reportService->getHourly($post);

        foreach ($hourlyData as $value) {
            array_push($newArray, array_values((array)$value));
        }

        return json_encode($newArray);
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getBreaksReports (Request $request)
    {
        $dateFrom = $request->get('start');
        $dateTo   = $request->get('end');
        $timezone = $request->get('timezone');
        $data     = $this->reportService->getBreaks([
            'start' => $dateFrom,
            'end'   => $dateTo
        ]);

        return json_encode($data);
    }
}