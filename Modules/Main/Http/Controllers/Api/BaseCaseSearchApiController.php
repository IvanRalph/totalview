<?php

namespace Modules\Main\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Main\Services\Core\CaseInterface;
use Modules\Main\Services\Core\FieldInterface;
use Modules\Main\Services\Core\StatusInterface;

class BaseCaseSearchApiController extends Controller
{
    /**
     * @var CaseInterface
     */
    protected $caseService;

    /**
     * @var FieldInterface
     */
    protected $fieldService;

    /**
     * @var StatusInterface
     */
    protected $statusService;

    /**
     * @param Request $request
     * @param         $lobId
     * @return \Illuminate\Http\JsonResponse
     */
    public function get (Request $request, $lobId)
    {
        $mainUser = $request->get('userData');
        $data     = $request->get('data');
        $columns  = [
            [
                'data' => 'shift_date',
                'name' => 'Shift Date'
            ]
        ];

        $filter = [
            [
                'column'     => 'shift_date',
                'comparison' => '>=',
                'value'      => $data['start_date']
            ],
            [
                'column'     => 'shift_date',
                'comparison' => '<=',
                'value'      => $data['end_date']
            ],
            [
                'column'     => 'status_id',
                'comparison' => '>',
                'value'      => '1'
            ]
        ];

        $fields = $this->fieldService->getByLobId($lobId);

        $post = [
            'lob_id'  => $lobId,
            'user_id' => $mainUser['id'],
            'filters' => $filter
        ];

        $userCases = $this->caseService->getByUser($post);
        $statuses  = $this->statusService->getAll()->toArray();
        $statusIds = array_column($statuses, 'id');

        foreach ($userCases as $key => $case) {
            $statusKey    = array_search($case->status_id, $statusIds);
            $status       = $statuses[$statusKey];
            $case->status = generateStatusDisplay($status);

            $userCases[$key] = $case;
        }

        foreach ($fields as $field) {
            $columns[] = [
                'data' => $field->html_name,
                'name' => $field->name
            ];
        }

        $columns[] = [
            'data' => 'duration',
            'name' => 'Duration'
        ];
        $columns[] = [
            'data' => 'status',
            'name' => 'Status'
        ];
        $columns[] = [
            'data' => 'created_at',
            'name' => 'Created'
        ];
        $columns[] = [
            'data' => 'updated_at',
            'name' => 'Updated'
        ];

        return response()->json([
            'data'    => $userCases,
            'columns' => $columns
        ]);
    }
}