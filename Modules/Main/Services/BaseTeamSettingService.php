<?php

namespace Modules\Main\Services;

use DB;
use Carbon\Carbon;
use Modules\Main\Services\Core\TeamSettingInterface;

class BaseTeamSettingService implements TeamSettingInterface
{
    protected $databaseService;

    protected $db;

    public function __construct (DatabaseService $databaseService)
    {
        $this->databaseService = $databaseService;
    }

    /**
     * @return mixed
     */
    public function getUserList ()
    {
        $role   = session()->get('user_from_account')->role;
        $idUser = session()->get('user_from_account')->main_user_id;
        if ($role == '5') {
            $ifTeam
                = "(CASE
            WHEN FIND_IN_SET('$idUser', team.lead)  THEN '1'
            else 0
            END) ";
        } else {
            $ifTeam = '1';
        }

        $params = [
            'modelName' => 'User',
            'leftJoin'  => [
                'shift_schedule' => [
                    'leftField'  => 'user.main_user_id',
                    'operator'   => '=',
                    'rightField' => 'shift_schedule.user_id'
                ],
                'user_status'    => [
                    'leftField'  => 'shift_schedule.user_statusid',
                    'operator'   => '=',
                    'rightField' => 'user_status.id'
                ],
                'user_lob'       => [
                    'leftField'  => 'user.main_user_id',
                    'operator'   => '=',
                    'rightField' => 'user_lob.user_id'
                ],
                'lob'            => [
                    'leftField'  => 'lob.id',
                    'operator'   => '=',
                    'rightField' => 'user_lob.lob_id'
                ],
                'team'           => [
                    'leftField'  => 'team.id',
                    'operator'   => '=',
                    'rightField' => 'user.team'
                ],
                'role'           => [
                    'leftField'  => 'role.id',
                    'operator'   => '=',
                    'rightField' => 'user.role'
                ]
            ],
            'select'    => "user.main_user_id as id, CONCAT(user.firstname,' ',user.lastname) as full_name, 
            lob.name as lob,
            team.description, user.shift_date, user.shift_end,
            team.lead as teamLeadID, 
            shift_schedule.start_shift, shift_schedule.end_shift, shift_schedule.leave, user.status, 
            team.id as teamID,
            user_lob.lob_id,
            GROUP_CONCAT(user_lob.lob_id) as lob_id,
            role.id as roleID, user.ramp,
            $ifTeam as blocked,
            user_status.description AS nurse_status",
            'filter'    => [
                'user.active' => '1'
            ],
            'filterRaw' => 'shift_schedule.id = (SELECT id FROM shift_schedule ss WHERE user_id = user.main_user_id ORDER BY shift_date DESC LIMIT 1)',
            'group'     => [
                'user_lob.user_id'
            ]
        ];

        return $this->databaseService->fetchAll($params);
    }

    /**
     * @return mixed
     */
    public function getLatestShiftDate ()
    {
        $params = [
            'modelName' => 'Schedule',
            'select'    => 'shift_date',
            'order'     => [
                'field' => 'shift_date',
                'type'  => 'desc'
            ]
        ];

        return $this->databaseService->fetch($params);
    }

    /**
     * @return mixed
     */
    public function getTeamLeader ()
    {
        $params = [
            'modelName' => 'User',
            'select'    => "CONCAT(user.firstname,' ',user.lastname) as full_name, user.id",
            'filter'    => [
                'role' => [
                    4,
                    5
                ]
            ]
        ];

        return $this->databaseService->fetchAll($params);
    }

    /**
     * @return mixed
     */
    public function getDropdownItems ($fieldId)
    {
        $params = [
            'modelName' => 'ListValue',
            'select'    => "id,value as `name`,status",
            'filter'    => [
                'field_id' => [
                    $fieldId
                ]
            ],
            'order'     => [
                'field' => 'id',
                'type'  => 'asc'
            ]

        ];

        return $this->databaseService->fetchAll($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function updateTeamList (array $post)
    {
        $params = [
            'modelName' => 'Team',
            'filter'    => [
                'ID' => $post['edit_id']
            ],
            'data'      => [
                'description' => $post['team_name'],
                'lead'        => $post['checkbox'],
                'end_time'    => $post['kick_out']
            ]
        ];

        return $this->databaseService->update($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function addTeamList (array $post)
    {
        $params = [
            'modelName' => 'Team',
            'data'      => [
                'description' => $post['team_name'],
                'lead'        => $post['checkbox'],
                'end_time'    => $post['kick_out'],
                'shift_date'  => Carbon::create()->format('Y-m-d')
            ]
        ];

        return $this->databaseService->create($params);
    }

    /**
     * @param array $post
     */
    public function updateTeamSchedule (array $post)
    {
        $myArrayId = explode(',', $post['id']);

        $params = [
            'modelName' => 'Schedule',
            'join'      => [
                'user' => [
                    'leftField'  => 'user.main_user_id',
                    'operator'   => '=',
                    'rightField' => 'shift_schedule.user_id'
                ]
            ],
            'data'      => [
                'start_shift' => $post['start_shift'],
                'end_shift'   => $post['end_shift'],
                'leave'       => $post['leave'],
                'user.ramp'   => $post['ramp']
            ],
            'filter'    => [
                'user_id' => $myArrayId
            ]
        ];

        $this->databaseService->update($params);
    }

    /**
     * @param array $post
     */
    public function updateValuelistItem (array $post)
    {
        $params = [
            'modelName' => 'ListValue',
            'data'      => [
                'value'  => $post['itemName'],
                'status' => $post['itemStatus'],
            ],
            'filter'    => [
                'id' => $post['itemId']
            ]
        ];

        $this->databaseService->update($params);
    }

    /**
     * @param array $post
     */
    public function updateTeamBulk (array $post)
    {
        $myArrayId = explode(',', $post['id']);

        $params = [
            'modelName' => 'User',
            'filter'    => [
                'id' => $myArrayId
            ],
            'data'      => ['team' => $post['team']]
        ];

        $this->databaseService->update($params);
    }

    public function getStatus ($id)
    {
        $params = [
            'modelName' => 'ListValue',
            'filter'    => [
                'id' => $id
            ]
        ];

        return $this->databaseService->fetch($params)->status;
    }

    public function updateValueListBulk (array $post)
    {
        foreach ($post['data'] as $status => $list) {
            $params = [
                'modelName' => 'ListValue',
                'filter'    => [
                    'id' => $list
                ],
                'data'      => [
                    'status' => ($status) ? 0 : 1
                ]
            ];

            $this->databaseService->update($params);
        }

        return response()->json(['status' => 'success'], 200);
    }

    public function updateTeamUserScheduleShift (array $post)
    {
        $params = [
            'modelName' => 'User',
            'filter'    => [
                'main_user_id' => $post['user_id']
            ]
        ];

        $current_shift_date = $this->databaseService->fetch($params)->shift_date;

        $params = [
            'modelName' => 'Schedule',
            'leftJoin'  => [
                'user' => [
                    'leftField'  => 'shift_schedule.user_id',
                    'operator'   => '=',
                    'rightField' => 'user.main_user_id'
                ]
            ],
            'filter'    => [
                'user_id'                   => $post['user_id'],
                'shift_schedule.shift_date' => $current_shift_date
            ],
            'data'      => [
                'shift_schedule.start_shift'   => $post['start_shift'],
                'shift_schedule.end_shift'     => $post['end_shift'],
                'shift_schedule.leave'         => $post['on_leave'],
                'user.shift_end'               => $post['on_shift'],
                'user.role'                    => $post['slct_role'],
                'user.team'                    => $post['team_list'],
                'user.ramp'                    => $post['ramp'],
                'shift_schedule.user_statusid' => $post['nurse_status']
            ]
        ];

        $res = $this->databaseService->update($params);

        $mainRole = 1;

        if ($post['slct_role'] != 5) {
            $mainRole = 2;
        }

        $this->db->table('tv_main.user')
            ->where('id', $post['user_id'])
            ->update([
                'role_id' => $mainRole,
            ]);

        return $res;
    }

    public function updateTeamKickout (array $post)
    {
        $params = [
            'modelName' => 'Team',
            'filter'    => [
                'ID' => $post['team_id']
            ],
            'data'      => [
                'end_time' => $post['end_time'],
                'isKicked' => array_key_exists('is_kicked', $post) ? $post['is_kicked'] : 0
            ]
        ];

        $this->databaseService->update($params);
    }
}