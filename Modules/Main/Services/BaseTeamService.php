<?php

namespace Modules\Main\Services;

use Modules\Main\Services\Core\TeamInterface;

class BaseTeamService implements TeamInterface
{
    protected $databaseService;

    public function __construct (DatabaseService $databaseService)
    {
        $this->databaseService = $databaseService;
    }

    /**
     * @param $teamId
     * @return mixed
     */
    public function getLeader ($teamId)
    {
        $params = [
            'modelName' => 'Team',
            'filter'    => [
                'id' => $teamId
            ]
        ];

        $teamInfo = $this->databaseService->fetch($params);

        $teamLeadersId = explode(',', $teamInfo->lead);

        $params = [
            'modelName' => 'User',
            'filter'    => [
                'main_user_id' => $teamLeadersId
            ]
        ];

        return $this->databaseService->fetchAll($params);
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getTeamByUser ($userId)
    {
        $params = [
            'modelName' => 'User',
            'join'      => [
                'team' => [
                    'leftField'  => 'team.ID',
                    'operator'   => '=',
                    'rightField' => 'user.team'
                ]
            ],
            'filter'    => [
                'user.main_user_id' => $userId
            ],
            'select'    => 'team.description as team'
        ];

        return $this->databaseService->fetch($params);
    }

    /**
     * @return mixed
     */
    public function getTeamList ()
    {
        $params = [
            'modelName' => 'Team',
            'orderRaw'  => 'CASE WHEN lead = ' . session('user_from_account')->id . ' THEN 1 ELSE 2 END, lead'
        ];

        return $this->databaseService->fetchAll($params);
    }

    public function getKickOut ($teamId)
    {
        $params = [
            'modelName' => 'Team',
            'filter'=>[
                'ID'=>$teamId
            ],
            'select'=>'end_time'
        ];

        return $this->databaseService->fetch($params);
    }
}