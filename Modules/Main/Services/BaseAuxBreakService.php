<?php

namespace Modules\Main\Services;

use Illuminate\Database\Eloquent\Collection;
use Modules\Main\Services\Core\AuxBreakInterface;

Class BaseAuxBreakService implements AuxBreakInterface
{
    /**
     * @var
     */
    protected $databaseService;

    /**
     * BaseAuxBreakService constructor.
     * @param \Modules\Main\Services\DatabaseService $databaseService
     */
    public function __construct (DatabaseService $databaseService)
    {
        $this->databaseService = $databaseService;
    }

    /**
     * @return mixed
     */
    public function getAuxType ()
    {
        $params = [
            'modelName' => 'AuxType'
        ];

        return $this->databaseService->fetchAll($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function create (array $post)
    {
        $params = [
            'modelName' => 'Aux',
            'data'      => $post,
        ];

        return $this->databaseService->create($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function update (array $post)
    {
        if (!array_key_exists('id', $post)) {
            abort(500, 'undefined array key id');
        }

        $params = [
            'modelName' => 'Aux',
            'data'      => $post,
            'filter'    => [
                'id' => $post['id'],

            ],
        ];

        return $this->databaseService->update($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function getCurrent (array $post)
    {
        $params = [
            'modelName' => 'Aux',
            'filter'    => [
                'user_id'    => $post['user_id'],
                'shift_date' => $post['shift_date'],
                'end_time'   => null
            ],

        ];

        return $this->databaseService->fetch($params);
    }

    /**
     * @param string $post
     * @return mixed
     */
    public function getLastBreak (string $post)
    {
        $params = [
            'modelName' => 'Aux',
            'filter'    => [
                'user_id' => $post,
            ],
            'order'     => [
                'field' => [
                    'id'
                ],
                'type'  => 'desc'
            ]
        ];

        return $this->databaseService->fetch($params);
    }

    /**
     * @param string $post
     * @return null
     */
    public function getAuxTypeNameById (string $post)
    {
        $params = [
            'modelName' => 'AuxType',
            'filter'    => [
                'id' => $post,
            ],

        ];

        $auxType = $this->databaseService->fetch($params);

        return $auxType ? $auxType->name : null;
    }
}