<?php

namespace Modules\Main\Services;

use Modules\Main\Services\Core\UserAuditTrailInterface;

class BaseUserAuditTrailService implements UserAuditTrailInterface
{
    protected $databaseService;

    public function __construct (DatabaseService $databaseService)
    {
        $this->databaseService = $databaseService;
    }

    /**
     * @param        $userId
     * @param        $shiftDate
     * @param        $action
     * @param string $reference
     * @param null   $caseId
     * @param null   $shiftId
     * @param null   $breakId
     * @return mixed
     */
    public function record ($userId, $shiftDate, $action, $reference = '', $caseId = null, $shiftId = null, $breakId = null)
    {
        $params = [
            'modelName' => 'UserAudit',
            'data'      => [
                'user_id'     => $userId,
                'shift_date'  => $shiftDate,
                'action'      => $action,
                'reference'   => $reference ? $reference : '',
                'case_id'     => $caseId ? $caseId : null,
                'schedule_id' => $shiftId ? $shiftId : null,
                'break_id'    => $breakId ? $breakId : null,
            ]
        ];

        return $this->databaseService->create($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function getByUserId (array $post)
    {
        $params = [
            'modelName' => 'UserAudit',
            'filter'    => [
                'user_id'    => $post['user_id'],
                'shift_date' => $post['shift_date']
            ],
            'order'     => [
                'field' => 'id',
                'type'  => 'desc'
            ]
        ];

        return $this->databaseService->fetchAll($params);
    }

    /**
     * @param $trail
     * @return array
     */
    public function display ($trail)
    {
        switch ($trail->action) {
            case self::ACTION_START_SHIFT:
            case self::ACTION_END_SHIFT:
                return $this->shiftDisplay($trail);

            case self::ACTION_START_BREAK:
            case self::ACTION_END_BREAK:
                return $this->breakDisplay($trail);

            case self::ACTION_START_CASE:
            case self::ACTION_END_CASE:
            case self::ACTION_HOLD_CASE:
            case self::ACTION_EDIT_CASE:
            case self::ACTION_SAVE_EDIT_CASE:
            case self::ACTION_CONTINUE_CASE:
                return $this->caseDisplay($trail);
        }
    }

    /**
     * @param $trail
     * @return array
     */
    private function shiftDisplay ($trail)
    {
        $description = 'Started shift';

        if ($trail->action == self::ACTION_END_SHIFT) {
            $description = 'Ended shift';
        }

        return [
            'id'          => $trail->id,
            'icon'        => 'fa-clock-o',
            'icon_bg'     => 'navy-bg',
            'type'        => 'Shift',
            'description' => $description,
            'timestamp'   => $trail->created_at
        ];
    }

    /**
     * @param $trail
     * @return array
     */
    private function breakDisplay ($trail)
    {
        $description = 'Started ';

        if ($trail->action == self::ACTION_END_BREAK) {
            $description = 'Ended ';
        }

        return [
            'id'          => $trail->id,
            'icon'        => 'fa-coffee',
            'icon_bg'     => 'lazur-bg',
            'type'        => 'Aux Break',
            'description' => $description . strtolower($trail->reference),
            'timestamp'   => $trail->created_at
        ];
    }

    /**
     * @param $trail
     * @return array
     */
    private function caseDisplay ($trail)
    {
        $caseAction = 'Started';
        $iconBg     = 'blue-bg';

        switch ($trail->action) {
            case self::ACTION_HOLD_CASE:
                $caseAction = 'Held';
                $iconBg     = 'red-bg';
                break;
            case self::ACTION_END_CASE:
                $caseAction = 'Ended';
                break;
            case self::ACTION_CONTINUE_CASE:
                $caseAction = 'Continued';
                break;
            case self::ACTION_EDIT_CASE:
                $caseAction = 'Started edit for';
                break;
            case self::ACTION_SAVE_EDIT_CASE:
                $caseAction = 'Edit completed for';
        }

        return [
            'id'          => $trail->id,
            'icon'        => 'fa-file-text-o',
            'icon_bg'     => $iconBg,
            'type'        => 'Case',
            'description' => $caseAction . ' case ' . $trail->reference,
            'timestamp'   => $trail->created_at
        ];
    }
}