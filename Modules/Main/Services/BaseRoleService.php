<?php

namespace Modules\Main\Services;

use Modules\Main\Services\Core\RoleInterface;

class BaseRoleService implements RoleInterface
{
    protected $databaseService;

    public function __construct (DatabaseService $databaseService)
    {
        $this->databaseService = $databaseService;
    }

    /**
     * @return mixed
     */
    public function getRoleList ()
    {
        $params = [
            'modelName' => 'Role',
            'filter'    => [
                'id' => [
                    5,
                    6
                ]
            ]
        ];

        return $this->databaseService->fetchAll($params);
    }
}