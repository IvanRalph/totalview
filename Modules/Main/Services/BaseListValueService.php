<?php

namespace Modules\Main\Services;

class BaseListValueService
{
    protected $databaseService;

    public function __construct (DatabaseService $databaseService)
    {
        $this->databaseService = $databaseService;
    }

    /**
     * @param $lobId
     * @return mixed
     */
    public function getByLobId ($lobId)
    {
        $params = [
            'modelName' => 'ListValue',
            'filter'    => [
                'lob_id' => $lobId
            ]
        ];

        return $this->databaseService->fetchAll($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function getByParentId (array $post)
    {
        $params = [
            'modelName' => 'ListValue',
            'filter'    => [
                'lob_id'         => $post['lob_id'],
                'parent_list_id' => $post['parent_id']
            ]
        ];

        return $this->databaseService->fetchAll($params);
    }
}