<?php

namespace Modules\Main\Services;

use Modules\Main\Services\Core\UserInterface;

class BaseUserService implements UserInterface
{
    protected $databaseService;

    protected $db;

    const STATUS_ONLINE = '1';
    const STATUS_OFFLINE = '0';

    public function __construct (DatabaseService $databaseService)
    {
        $this->databaseService = $databaseService;
    }

    /**
     * @param $mainUserId
     * @return mixed
     */
    public function getByMainUserId ($mainUserId)
    {
        $params = [
            'modelName' => 'User',
            'filter'    => [
                'main_user_id' => $mainUserId
            ]
        ];

        return $this->databaseService->fetch($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function update (array $post)
    {
        $params = [
            'modelName' => 'User',
            'filter'    => [
                'main_user_id' => $post['main_user_id']
            ],
            'data'      => $post['data']
        ];

        return $this->databaseService->update($params);
    }

    /**
     * @param array $post
     * @return null
     */
    public function updateShiftSchedule (array $post)
    {
        $shiftDate = $this->getShiftSchedule($post['main_user_id'])->shift_date;
        $params    = [
            'modelName' => 'Schedule',
            'filter'    => [
                'user_id'    => $post['main_user_id'],
                'shift_date' => $shiftDate
            ],
            'data'      => $post['data']
        ];

        $this->databaseService->update($params);

        $shiftSched = $this->databaseService->fetch($params);

        return $shiftSched ? $shiftSched->ID : null;
    }

    /**
     * @param array $post
     */
    public function updateUserList (array $post)
    {
        $params = [
            'modelName' => 'Team',
            'filter'    => [
                'ID' => $post['id']
            ],
            'data'      => [
                'description' => $post['team_name'],
                'createdby'   => $post['team_leader']
            ]
        ];

        $this->databaseService->update($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function countAll (array $post)
    {
        $params = [
            'modelName' => 'UserLob',
            'join'      => [
                'user' => [
                    'leftField'  => 'user_lob.user_id',
                    'operator'   => '=',
                    'rightField' => 'user.main_user_id'
                ]
            ],
            'filter'    => [
                'user_lob.lob_id' => $post['lob_id'],
                'user.team'       => $post['team_id'],
                'user.role'       => [
                    6,
                    7,
                    11
                ]
            ],
        ];

        return $this->databaseService->count($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function countAvailable (array $post)
    {
        $params = [
            'modelName' => 'UserLob',
            'join'      => [
                'user' => [
                    'leftField'  => 'user_lob.user_id',
                    'operator'   => '=',
                    'rightField' => 'user.main_user_id'
                ]
            ],
            'filter'    => [
                'user_lob.lob_id' => $post['lob_id'],
                'user.team'       => $post['team_id'],
                'user.status'     => self::STATUS_ONLINE,
                'user.role'       => [
                    6,
                    7,
                    11
                ]
            ],
        ];

        return $this->databaseService->count($params);
    }

    /**
     * @param $lobId
     * @return mixed
     */
    public function getAllByLobId ($lobId)
    {
        $params = [
            'modelName' => 'UserLob',
            'join'      => [
                'user' => [
                    'leftField'  => 'user_lob.user_id',
                    'operator'   => '=',
                    'rightField' => 'user.main_user_id'
                ]
            ],
            'filter'    => [
                'user_lob.lob_id' => $lobId
            ]
        ];

        return $this->databaseService->fetchAll($params);
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getShiftSchedule ($userId)
    {
        $params = [
            'modelName' => 'Schedule',
            'filter'    => [
                'user_id' => $userId
            ],
            'order'     => [
                'field' => 'shift_date',
                'type'  => 'desc'
            ]
        ];

        return $this->databaseService->fetch($params);
    }

    /**
     * @return mixed
     */
    public function getShiftDateForDashboard ()
    {
        $params = [
            'modelName' => 'User',
            'distinct'  => null,
            'order'     => [
                'field' => 'shift_date',
                'type'  => 'desc'
            ],
            'select'    => 'shift_date'
        ];

        return $this->databaseService->fetchAll($params);
    }
}