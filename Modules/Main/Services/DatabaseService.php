<?php

namespace Modules\Main\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Modules\Vanderbilt\Entities\Action;
use Modules\Vanderbilt\Entities\AuxBreak;
use Modules\Vanderbilt\Entities\AuxType;
use Modules\Vanderbilt\Entities\BreakRaw;
use Modules\Vanderbilt\Entities\Case8;
use Modules\Vanderbilt\Entities\CaseActivity;
use Modules\Vanderbilt\Entities\DailyProd;
use Modules\Vanderbilt\Entities\Field;
use Modules\Vanderbilt\Entities\ListValue;
use Modules\Vanderbilt\Entities\Lob;
use Modules\Vanderbilt\Entities\LobSetting;
use Modules\Vanderbilt\Entities\Raw;
use Modules\Vanderbilt\Entities\Role;
use Modules\Vanderbilt\Entities\Schedule;
use Modules\Vanderbilt\Entities\Status;
use Modules\Vanderbilt\Entities\Team;
use Modules\Vanderbilt\Entities\User;
use Modules\Vanderbilt\Entities\UserAudit;
use Modules\Vanderbilt\Entities\UserLob;

class DatabaseService
{
    protected $model;

    /**
     * @return mixed
     */
    public function initConnection ()
    {
        return session('selected_account')->db_name;
    }

    /**
     * @param $modelName
     * @return mixed
     */
    public function initModel ($modelName)
    {
        $conn = $this->initConnection();

        $models = [
            'tv_vanderbilt' => [
                'AuxType'      => AuxType::class,
                'Aux'          => AuxBreak::class,
                'Case8'        => Case8::class,
                'User'         => User::class,
                'CaseActivity' => CaseActivity::class,
                'Field'        => Field::class,
                'ListValue'    => ListValue::class,
                'Lob'          => Lob::class,
                'UserLob'      => UserLob::class,
                'LobSetting'   => LobSetting::class,
                'Raw'          => Raw::class,
                'BreakRaw'     => BreakRaw::class,
                'DailyProd'    => DailyProd::class,
                'Role'         => Role::class,
                'Status'       => Status::class,
                'Action'       => Action::class,
                'Team'         => Team::class,
                'UserAudit'    => UserAudit::class,
                'Schedule'     => Schedule::class,
            ],
            'tv_base2'      => [
                'AuxType'      => \Modules\Base2\Entities\AuxType::class,
                'Aux'          => \Modules\Base2\Entities\AuxBreak::class,
                'Case99'       => \Modules\Base2\Entities\Case99::class,
                'User'         => \Modules\Base2\Entities\User::class,
                'CaseActivity' => \Modules\Base2\Entities\CaseActivity::class,
                'Field'        => \Modules\Base2\Entities\Field::class,
                'ListValue'    => \Modules\Base2\Entities\ListValue::class,
                'Lob'          => \Modules\Base2\Entities\Lob::class,
                'UserLob'      => \Modules\Base2\Entities\UserLob::class,
                'LobSetting'   => \Modules\Base2\Entities\LobSetting::class,
                'Raw'          => \Modules\Base2\Entities\Raw::class,
                'BreakRaw'     => \Modules\Base2\Entities\BreakRaw::class,
                'DailyProd'    => \Modules\Base2\Entities\DailyProd::class,
                'Role'         => \Modules\Base2\Entities\Role::class,
                'Status'       => \Modules\Base2\Entities\Status::class,
                'Action'       => \Modules\Base2\Entities\Action::class,
                'Team'         => \Modules\Base2\Entities\Team::class,
                'UserAudit'    => \Modules\Base2\Entities\UserAudit::class,
                'Schedule'     => \Modules\Base2\Entities\Schedule::class,
            ],
            'tv_examworks'  => [
                'AuxType'      => \Modules\Examworks\Entities\AuxType::class,
                'Aux'          => \Modules\Examworks\Entities\AuxBreak::class,
                'Case9'        => \Modules\Examworks\Entities\Case9::class,
                'Case10'       => \Modules\Examworks\Entities\Case10::class,
                'User'         => \Modules\Examworks\Entities\User::class,
                'CaseActivity' => \Modules\Examworks\Entities\CaseActivity::class,
                'Field'        => \Modules\Examworks\Entities\Field::class,
                'ListValue'    => \Modules\Examworks\Entities\ListValue::class,
                'Lob'          => \Modules\Examworks\Entities\Lob::class,
                'UserLob'      => \Modules\Examworks\Entities\UserLob::class,
                'LobSetting'   => \Modules\Examworks\Entities\LobSetting::class,
                'Raw'          => \Modules\Examworks\Entities\Raw::class,
                'BreakRaw'     => \Modules\Examworks\Entities\BreakRaw::class,
                'DailyProd'    => \Modules\Examworks\Entities\DailyProd::class,
                'Role'         => \Modules\Examworks\Entities\Role::class,
                'Status'       => \Modules\Examworks\Entities\Status::class,
                'Action'       => \Modules\Examworks\Entities\Action::class,
                'Team'         => \Modules\Examworks\Entities\Team::class,
                'UserAudit'    => \Modules\Examworks\Entities\UserAudit::class,
                'Schedule'     => \Modules\Examworks\Entities\Schedule::class,
            ],
            'tv_cotiviti'   => [
                'AuxType'      => \Modules\Cotiviti\Entities\AuxType::class,
                'Aux'          => \Modules\Cotiviti\Entities\AuxBreak::class,
                'Case7'        => \Modules\Cotiviti\Entities\Case7::class,
                'User'         => \Modules\Cotiviti\Entities\User::class,
                'CaseActivity' => \Modules\Cotiviti\Entities\CaseActivity::class,
                'Field'        => \Modules\Cotiviti\Entities\Field::class,
                'ListValue'    => \Modules\Cotiviti\Entities\ListValue::class,
                'Lob'          => \Modules\Cotiviti\Entities\Lob::class,
                'UserLob'      => \Modules\Cotiviti\Entities\UserLob::class,
                'LobSetting'   => \Modules\Cotiviti\Entities\LobSetting::class,
                'Raw'          => \Modules\Cotiviti\Entities\Raw::class,
                'BreakRaw'     => \Modules\Cotiviti\Entities\BreakRaw::class,
                'DailyProd'    => \Modules\Cotiviti\Entities\DailyProd::class,
                'Role'         => \Modules\Cotiviti\Entities\Role::class,
                'Status'       => \Modules\Cotiviti\Entities\Status::class,
                'Action'       => \Modules\Cotiviti\Entities\Action::class,
                'Team'         => \Modules\Cotiviti\Entities\Team::class,
                'UserAudit'    => \Modules\Cotiviti\Entities\UserAudit::class,
                'Schedule'     => \Modules\Cotiviti\Entities\Schedule::class,
            ],
        ];

        return $models[$conn][$modelName];
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function fetch (array $params)
    {
        if (array_key_exists('modelName', $params)) {
            $model   = $this->initModel($params['modelName']);
            $counter = 0;

            if (array_key_exists('select', $params)) {
                if ($counter == 0) {
                    $model = $model::selectRaw($params['select']);
                    $counter++;
                } else {
                    $model->selectRaw($params['select']);
                }
            }

            if (array_key_exists('leftJoin', $params)) {
                foreach ($params['leftJoin'] as $table => $leftJoin) {
                    if ($counter == 0) {
                        $model = $model::leftJoin($table, $leftJoin['leftField'], $leftJoin['operator'], $leftJoin['rightField']);
                        $counter++;
                    } else {
                        $model->leftJoin($table, $leftJoin['leftField'], $leftJoin['operator'], $leftJoin['rightField']);
                    }
                }
            }

            if (array_key_exists('join', $params)) {
                foreach ($params['join'] as $table => $join) {
                    if ($counter == 0) {
                        $model = $model::join($table, $join['leftField'], $join['operator'], $join['rightField']);
                        $counter++;
                    } else {
                        $model->join($table, $join['leftField'], $join['operator'], $join['rightField']);
                    }
                }
            }

            if (array_key_exists('filter', $params)) {
                foreach ($params['filter'] as $field => $value) {
                    if (is_array($value)) {
                        if (array_key_exists('notIn', $value)) {
                            if ($counter == 0) {
                                $model = $model::whereNotIn($field, $value['notIn']);
                                $counter++;
                            } else {
                                $model->whereNotIn($field, $value['notIn']);
                            }
                        } else if (array_key_exists('operator', $value)) {
                            if ($counter == 0) {
                                $model = $model::where($field, $value['operator'], $value['data']);
                                $counter++;
                            } else {
                                $model->where($field, $value['operator'], $value['data']);
                            }
                        } else {
                            if ($counter == 0) {
                                $model = $model::whereIn($field, $value);
                                $counter++;
                            } else {
                                $model->whereIn($field, $value);
                            }
                        }
                    } else {
                        if ($counter == 0) {
                            $model = $model::where($field, $value);
                            $counter++;
                        } else {
                            $model->where($field, $value);
                        }
                    }
                }
            }

            if (array_key_exists('filterRaw', $params)) {
                if ($counter == 0) {
                    $model = $model::whereRaw($params['filterRaw']);
                } else {
                    $model->whereRaw($params['filterRaw']);
                }
            }

            if (array_key_exists('between', $params)) {
                foreach ($params['between'] as $field => $value) {
                    if ($counter == 0) {
                        $model = $model::whereBetween($field, $value);
                        $counter++;
                    } else {
                        $model->whereBetween($field, $value);
                    }
                }
            }

            if (array_key_exists('distinct', $params)) {
                if ($counter == 0) {
                    $model = $model::distinct($params['distinct']);
                    $counter++;
                } else {
                    $model->distinct($params['distinct']);
                }
            }

            if (array_key_exists('order', $params)) {
                $fields = $params['order']['field'];
                if (is_array($fields)) {
                    foreach ($fields as $field) {
                        if ($counter == 0) {
                            $model = $model::orderBy($field, $params['order']['type']);
                        } else {
                            $model->orderBy($field, $params['order']['type']);
                        }
                    }
                } else {
                    if ($counter == 0) {
                        $model = $model::orderBy($fields, $params['order']['type']);
                    } else {
                        $model->orderBy($fields, $params['order']['type']);
                    }
                }
            }

            if (array_key_exists('orderRaw', $params)) {
                if ($counter == 0) {
                    $model = $model::orderByRaw($params['orderRaw']);
                    $counter++;
                } else {
                    $model->orderByRaw($params['orderRaw']);
                }
            }

            if (array_key_exists('group', $params)) {
                foreach ($params['group'] as $group) {
                    if ($counter == 0) {
                        $model = $model::groupBy($group);
                        $counter++;
                    } else {
                        $model->groupBy($group);
                    }
                }
            }

            if (is_object($model)) {
                return $model->first();
            } else {
                return $model::first();
            }
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function fetchAll (array $params)
    {
        if (array_key_exists('modelName', $params)) {
            $model   = $this->initModel($params['modelName']);
            $counter = 0;

            if (array_key_exists('select', $params)) {
                if ($counter == 0) {
                    $model = $model::selectRaw($params['select']);
                    $counter++;
                } else {
                    $model->selectRaw($params['select']);
                }
            }

            if (array_key_exists('leftJoin', $params)) {
                foreach ($params['leftJoin'] as $table => $leftJoin) {
                    if ($counter == 0) {
                        $model = $model::leftJoin($table, $leftJoin['leftField'], $leftJoin['operator'], $leftJoin['rightField']);
                        $counter++;
                    } else {
                        $model->leftJoin($table, $leftJoin['leftField'], $leftJoin['operator'], $leftJoin['rightField']);
                    }
                }
            }

            if (array_key_exists('join', $params)) {
                foreach ($params['join'] as $table => $join) {
                    if ($counter == 0) {
                        $model = $model::join($table, $join['leftField'], $join['operator'], $join['rightField']);
                        $counter++;
                    } else {
                        $model->join($table, $join['leftField'], $join['operator'], $join['rightField']);
                    }
                }
            }

            if (array_key_exists('filter', $params)) {
                foreach ($params['filter'] as $field => $value) {
                    if (is_array($value)) {
                        if (array_key_exists('notIn', $value)) {
                            if ($counter == 0) {
                                $model = $model::whereNotIn($field, $value['notIn']);
                                $counter++;
                            } else {
                                $model->whereNotIn($field, $value['notIn']);
                            }
                        } else if (array_key_exists('operator', $value)) {
                            if ($counter == 0) {
                                $model = $model::where($field, $value['operator'], $value['data']);
                                $counter++;
                            } else {
                                $model->where($field, $value['operator'], $value['data']);
                            }
                        } else {
                            if ($counter == 0) {
                                $model = $model::whereIn($field, $value);
                                $counter++;
                            } else {
                                $model->whereIn($field, $value);
                            }
                        }
                    } else {
                        if ($counter == 0) {
                            $model = $model::where($field, $value);
                            $counter++;
                        } else {
                            $model->where($field, $value);
                        }
                    }
                }
            }

            if (array_key_exists('filterRaw', $params)) {
                if ($counter == 0) {
                    $model = $model::whereRaw($params['filterRaw']);
                } else {
                    $model->whereRaw($params['filterRaw']);
                }
            }

            if (array_key_exists('between', $params)) {
                foreach ($params['between'] as $field => $value) {
                    if ($counter == 0) {
                        $model = $model::whereBetween($field, $value);
                        $counter++;
                    } else {
                        $model->whereBetween($field, $value);
                    }
                }
            }

            if (array_key_exists('distinct', $params)) {
                if ($counter == 0) {
                    $model = $model::distinct($params['distinct']);
                    $counter++;
                } else {
                    $model->distinct($params['distinct']);
                }
            }

            if (array_key_exists('order', $params)) {
                $fields = $params['order']['field'];
                if (is_array($fields)) {
                    foreach ($fields as $field) {
                        if ($counter == 0) {
                            $model = $model::orderBy($field, $params['order']['type']);
                        } else {
                            $model->orderBy($field, $params['order']['type']);
                        }
                    }
                } else {
                    if ($counter == 0) {
                        $model = $model::orderBy($fields, $params['order']['type']);
                    } else {
                        $model->orderBy($fields, $params['order']['type']);
                    }
                }
            }

            if (array_key_exists('orderRaw', $params)) {
                if ($counter == 0) {
                    $model = $model::orderByRaw($params['orderRaw']);
                    $counter++;
                } else {
                    $model->orderByRaw($params['orderRaw']);
                }
            }

            if (array_key_exists('group', $params)) {
                foreach ($params['group'] as $group) {
                    if ($counter == 0) {
                        $model = $model::groupBy($group);
                        $counter++;
                    } else {
                        $model->groupBy($group);
                    }
                }
            }

            if (is_object($model)) {
                return $model->get();
            } else {
                return $model::get();
            }
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function createMany (array $params)
    {
        try {
            DB::connection($this->initConnection())->beginTransaction();
            if (array_key_exists('modelName', $params)) {
                $model = $this->initModel($params['modelName']);
                $id    = $model::insert($params['data']);
                DB::connection($this->initConnection())->commit();

                return $id;
            }
        } catch (\Exception $exception) {
            Log::error('Error Database ('. strtoupper($this->initConnection()) .') Service createMany(): ' . $exception->getMessage());
            DB::connection($this->initConnection())->rollback();
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function create (array $params)
    {
        try {
            DB::connection($this->initConnection())->beginTransaction();
            if (array_key_exists('modelName', $params)) {
                $model = $this->initModel($params['modelName']);
                $id    = $model::insertGetId($params['data']);
                DB::connection($this->initConnection())->commit();

                return $id;
            }
        } catch (\Exception $exception) {
            Log::error('Error Database ('. strtoupper($this->initConnection()) .') Service create(): ' . $exception->getMessage());
            DB::connection($this->initConnection())->rollback();
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function update (array $params)
    {
        try {
            DB::connection($this->initConnection())->beginTransaction();
            if (array_key_exists('modelName', $params)) {
                $model   = $this->initModel($params['modelName']);
                $counter = 0;

                if (array_key_exists('select', $params)) {
                    if ($counter == 0) {
                        $model = $model::selectRaw($params['select']);
                    } else {
                        $model->selectRaw($params['select']);
                    }
                }

                if (array_key_exists('leftJoin', $params)) {
                    foreach ($params['leftJoin'] as $table => $leftJoin) {
                        if ($counter == 0) {
                            $model = $model::leftJoin($table, $leftJoin['leftField'], $leftJoin['operator'], $leftJoin['rightField']);
                            $counter++;
                        } else {
                            $model->leftJoin($table, $leftJoin['leftField'], $leftJoin['operator'], $leftJoin['rightField']);
                        }
                    }
                }

                if (array_key_exists('join', $params)) {
                    foreach ($params['join'] as $table => $join) {
                        if ($counter == 0) {
                            $model = $model::join($table, $join['leftField'], $join['operator'], $join['rightField']);
                            $counter++;
                        } else {
                            $model->join($table, $join['leftField'], $join['operator'], $join['rightField']);
                        }
                    }
                }

                if (array_key_exists('filter', $params)) {
                    foreach ($params['filter'] as $field => $value) {
                        if (is_array($value)) {
                            if (array_key_exists('notIn', $value)) {
                                if ($counter == 0) {
                                    $model = $model::whereNotIn($field, $value['notIn']);
                                    $counter++;
                                } else {
                                    $model->whereNotIn($field, $value['notIn']);
                                }
                            } else if (array_key_exists('operator', $value)) {
                                if ($counter == 0) {
                                    $model = $model::where($field, $value['operator'], $value['data']);
                                    $counter++;
                                } else {
                                    $model->where($field, $value['operator'], $value['data']);
                                }
                            } else {
                                if ($counter == 0) {
                                    $model = $model::whereIn($field, $value);
                                    $counter++;
                                } else {
                                    $model->whereIn($field, $value);
                                }
                            }
                        } else {
                            if ($counter == 0) {
                                $model = $model::where($field, $value);
                                $counter++;
                            } else {
                                $model->where($field, $value);
                            }
                        }
                    }
                }

                if (array_key_exists('filterRaw', $params)) {
                    if ($counter == 0) {
                        $model = $model::whereRaw($params['filterRaw']);
                    } else {
                        $model->whereRaw($params['filterRaw']);
                    }
                }

                if (array_key_exists('between', $params)) {
                    foreach ($params['between'] as $field => $value) {
                        if ($counter == 0) {
                            $model = $model::whereBetween($field, $value);
                            $counter++;
                        } else {
                            $model->whereBetween($field, $value);
                        }
                    }
                }

                if (array_key_exists('distinct', $params)) {
                    if ($counter == 0) {
                        $model = $model::distinct($params['distinct']);
                        $counter++;
                    } else {
                        $model->distinct($params['distinct']);
                    }
                }

                if (array_key_exists('order', $params)) {
                    $fields = $params['order']['field'];
                    if (is_array($fields)) {
                        foreach ($fields as $field) {
                            if ($counter == 0) {
                                $model = $model::orderBy($field, $params['order']['type']);
                            } else {
                                $model->orderBy($field, $params['order']['type']);
                            }
                        }
                    } else {
                        if ($counter == 0) {
                            $model = $model::orderBy($fields, $params['order']['type']);
                        } else {
                            $model->orderBy($fields, $params['order']['type']);
                        }
                    }
                }

                if (array_key_exists('orderRaw', $params)) {
                    if ($counter == 0) {
                        $model = $model::orderByRaw($params['orderRaw']);
                        $counter++;
                    } else {
                        $model->orderByRaw($params['orderRaw']);
                    }
                }

                if (array_key_exists('group', $params)) {
                    foreach ($params['group'] as $group) {
                        if ($counter == 0) {
                            $model = $model::groupBy($group);
                            $counter++;
                        } else {
                            $model->groupBy($group);
                        }
                    }
                }

                $model->update($params['data']);
                DB::connection($this->initConnection())->commit();
                if (is_object($model)) {
                    return $model->first();
                } else {
                    return $model::first();
                }
            }
        } catch (\Exception $exception) {
            Log::error('Error Database ('. strtoupper($this->initConnection()) .') Service update(): ' . $exception->getMessage());
            DB::connection($this->initConnection())->rollback();
        }
    }

    public function delete (array $params)
    {
        try {
            DB::connection($this->initConnection())->beginTransaction();
            if (array_key_exists('modelName', $params)) {
                $model   = $this->initModel($params['modelName']);
                $counter = 0;

                if (array_key_exists('filter', $params)) {
                    foreach ($params['filter'] as $field => $value) {
                        if (is_array($value)) {
                            if (array_key_exists('operator', $value)) {
                                if ($counter == 0) {
                                    $model = $model::where($field, $value['operator'], $value['data']);
                                    $counter++;
                                } else {
                                    $model->where($field, $value['operator'], $value['data']);
                                }
                            } else {
                                if ($counter == 0) {
                                    $model = $model::whereIn($field, $value);
                                    $counter++;
                                } else {
                                    $model->whereIn($field, $value);
                                }
                            }
                        } else {
                            if ($counter == 0) {
                                $model = $model::where($field, $value);
                                $counter++;
                            } else {
                                $model->where($field, $value);
                            }
                        }
                    }
                }

                if (array_key_exists('between', $params)) {
                    foreach ($params['between'] as $field => $value) {
                        if ($counter == 0) {
                            $model = $model::whereBetween($field, $value);
                            $counter++;
                        } else {
                            $model->whereBetween($field, $value);
                        }
                    }
                }

                if (array_key_exists('join', $params)) {
                    if ($counter == 0) {
                        $model = $model::join($params['join'][0], $params['join'][1], $params['join'][2], $params['join'][3]);
                        $counter++;
                    } else {
                        $model->join($params['join'][0], $params['join'][1], $params['join'][2], $params['join'][3]);
                    }
                }

                if (is_object($model)) {
                    $model->delete();
                } else {
                    $model::delete();
                }

                DB::connection($this->initConnection())->commit();
            }
        } catch (\Exception $exception) {
            Log::error('Error Database ('. strtoupper($this->initConnection()) .')  Service delete(): ' . $exception->getMessage());
            DB::connection($this->initConnection())->rollback();
        }
    }

    public function count (array $params)
    {
        if (array_key_exists('modelName', $params)) {
            $model   = $this->initModel($params['modelName']);
            $counter = 0;

            if (array_key_exists('select', $params)) {
                if ($counter == 0) {
                    $model = $model::selectRaw($params['select']);
                } else {
                    $model->selectRaw($params['select']);
                }
            }

            if (array_key_exists('leftJoin', $params)) {
                foreach ($params['leftJoin'] as $table => $leftJoin) {
                    if ($counter == 0) {
                        $model = $model::leftJoin($table, $leftJoin['leftField'], $leftJoin['operator'], $leftJoin['rightField']);
                        $counter++;
                    } else {
                        $model->leftJoin($table, $leftJoin['leftField'], $leftJoin['operator'], $leftJoin['rightField']);
                    }
                }
            }

            if (array_key_exists('join', $params)) {
                foreach ($params['join'] as $table => $join) {
                    if ($counter == 0) {
                        $model = $model::join($table, $join['leftField'], $join['operator'], $join['rightField']);
                        $counter++;
                    } else {
                        $model->join($table, $join['leftField'], $join['operator'], $join['rightField']);
                    }
                }
            }

            if (array_key_exists('filter', $params)) {
                foreach ($params['filter'] as $field => $value) {
                    if (is_array($value)) {
                        if (array_key_exists('notIn', $value)) {
                            if ($counter == 0) {
                                $model = $model::whereNotIn($field, $value['notIn']);
                                $counter++;
                            } else {
                                $model->whereNotIn($field, $value['notIn']);
                            }
                        } else if (array_key_exists('operator', $value)) {
                            if ($counter == 0) {
                                $model = $model::where($field, $value['operator'], $value['data']);
                                $counter++;
                            } else {
                                $model->where($field, $value['operator'], $value['data']);
                            }
                        } else {
                            if ($counter == 0) {
                                $model = $model::whereIn($field, $value);
                                $counter++;
                            } else {
                                $model->whereIn($field, $value);
                            }
                        }
                    } else {
                        if ($counter == 0) {
                            $model = $model::where($field, $value);
                            $counter++;
                        } else {
                            $model->where($field, $value);
                        }
                    }
                }
            }

            if (array_key_exists('filterRaw', $params)) {
                if ($counter == 0) {
                    $model = $model::whereRaw($params['filterRaw']);
                } else {
                    $model->whereRaw($params['filterRaw']);
                }
            }

            if (array_key_exists('between', $params)) {
                foreach ($params['between'] as $field => $value) {
                    if ($counter == 0) {
                        $model = $model::whereBetween($field, $value);
                        $counter++;
                    } else {
                        $model->whereBetween($field, $value);
                    }
                }
            }

            if (array_key_exists('distinct', $params)) {
                if ($counter == 0) {
                    $model = $model::distinct($params['distinct']);
                    $counter++;
                } else {
                    $model->distinct($params['distinct']);
                }
            }

            if (array_key_exists('order', $params)) {
                $fields = $params['order']['field'];
                if (is_array($fields)) {
                    foreach ($fields as $field) {
                        if ($counter == 0) {
                            $model = $model::orderBy($field, $params['order']['type']);
                        } else {
                            $model->orderBy($field, $params['order']['type']);
                        }
                    }
                } else {
                    if ($counter == 0) {
                        $model = $model::orderBy($fields, $params['order']['type']);
                    } else {
                        $model->orderBy($fields, $params['order']['type']);
                    }
                }
            }

            if (array_key_exists('orderRaw', $params)) {
                if ($counter == 0) {
                    $model = $model::orderByRaw($params['orderRaw']);
                    $counter++;
                } else {
                    $model->orderByRaw($params['orderRaw']);
                }
            }

            if (array_key_exists('group', $params)) {
                foreach ($params['group'] as $group) {
                    if ($counter == 0) {
                        $model = $model::groupBy($group);
                        $counter++;
                    } else {
                        $model->groupBy($group);
                    }
                }
            }

            if (is_object($model)) {
                return $model->count();
            } else {
                return $model::count();
            }
        }
    }

    public function sum (array $params)
    {
        try {
            if (array_key_exists('modelName', $params)) {
                $model   = $this->initModel($params['modelName']);
                $counter = 0;

                if (array_key_exists('leftJoin', $params)) {
                    foreach ($params['leftJoin'] as $table => $leftJoin) {
                        if ($counter == 0) {
                            $model = $model::leftJoin($table, $leftJoin['leftField'], $leftJoin['operator'], $leftJoin['rightField']);
                            $counter++;
                        } else {
                            $model->leftJoin($table, $leftJoin['leftField'], $leftJoin['operator'], $leftJoin['rightField']);
                        }
                    }
                }

                if (array_key_exists('join', $params)) {
                    foreach ($params['join'] as $table => $join) {
                        if ($counter == 0) {
                            $model = $model::join($table, $join['leftField'], $join['operator'], $join['rightField']);
                            $counter++;
                        } else {
                            $model->join($table, $join['leftField'], $join['operator'], $join['rightField']);
                        }
                    }
                }

                if (array_key_exists('filter', $params)) {
                    foreach ($params['filter'] as $field => $value) {
                        if (is_array($value)) {
                            if (array_key_exists('notIn', $value)) {
                                if ($counter == 0) {
                                    $model = $model::whereNotIn($field, $value['notIn']);
                                    $counter++;
                                } else {
                                    $model->whereNotIn($field, $value['notIn']);
                                }
                            } else if (array_key_exists('operator', $value)) {
                                if ($counter == 0) {
                                    $model = $model::where($field, $value['operator'], $value['data']);
                                    $counter++;
                                } else {
                                    $model->where($field, $value['operator'], $value['data']);
                                }
                            } else {
                                if ($counter == 0) {
                                    $model = $model::whereIn($field, $value);
                                    $counter++;
                                } else {
                                    $model->whereIn($field, $value);
                                }
                            }
                        } else {
                            if ($counter == 0) {
                                $model = $model::where($field, $value);
                                $counter++;
                            } else {
                                $model->where($field, $value);
                            }
                        }
                    }
                }

                if (array_key_exists('filterRaw', $params)) {
                    if ($counter == 0) {
                        $model = $model::whereRaw($params['filterRaw']);
                    } else {
                        $model->whereRaw($params['filterRaw']);
                    }
                }

                if (array_key_exists('between', $params)) {
                    foreach ($params['between'] as $field => $value) {
                        if ($counter == 0) {
                            $model = $model::whereBetween($field, $value);
                            $counter++;
                        } else {
                            $model->whereBetween($field, $value);
                        }
                    }
                }

                if (is_object($model)) {
                    return $model->sum($params['sum']);
                } else {
                    return $model::sum($params['sum']);
                }
            }
        } catch (\Exception $exception) {
            Log::error('Error Database ('. strtoupper($this->initConnection()) .')  Service Sum(): ' . $exception->getMessage());

            return 0;
        }
    }
}