<?php
/**
 * Created by PhpStorm.
 * User: OMLanuza
 * Date: 1/17/2018
 * Time: 4:11 PM
 */

namespace Modules\Main\Services;

class BaseReportService extends BaseService
{
    protected $databaseService;

    protected $statusService;

    protected $db;

    public function __construct (DatabaseService $databaseService)
    {
        $this->databaseService = $databaseService;
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function getRaw (array $post)
    {
        $params = [
            'modelName' => 'Raw',
            'leftJoin'  => [
                'user'   => [
                    'leftField'  => 'user.main_user_id',
                    'operator'   => '=',
                    'rightField' => 'raw.user_id'
                ],
                'status' => [
                    'leftField'  => 'status.id',
                    'operator'   => '=',
                    'rightField' => 'raw.status_id'
                ],
                'team'   => [
                    'leftField'  => 'team.ID',
                    'operator'   => '=',
                    'rightField' => 'raw.team'
                ]
            ],
            'between'   => [
                'raw.shift_date' => [
                    $post['start'],
                    $post['end']
                ]
            ],
            'select'    => isset($post['rows']) ? $post['rows'] : '*'
        ];

        return $this->databaseService->fetchAll($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function getBreaks (array $post)
    {
        $params = [
            'modelName' => 'BreakRaw',
            'leftJoin'  => [
                'user'     => [
                    'leftField'  => 'user.main_user_id',
                    'operator'   => '=',
                    'rightField' => 'breaks.user_id'
                ],
                'aux_type' => [
                    'leftField'  => 'aux_type.id',
                    'operator'   => '=',
                    'rightField' => 'breaks.aux_type_id'
                ]
            ],
            'between'   => [
                'breaks.shift_date' => [
                    $post['start'],
                    $post['end']
                ]
            ],
            'select'    => "concat(user.lastname, ', ', user.firstname) as Name, breaks.shift_date as Shift, aux_type.name as AuxBreak, breaks.remarks as Remarks,
         breaks.start_time as Start, breaks.end_time as End, breaks.duration as Duration"
        ];

        return $this->databaseService->fetchAll($params);
    }

    /**
     * @param $rawNumber
     * @param $start
     * @param $end
     * @param $timezone
     * @return mixed
     */
    public function getHold ($rawNumber, $start, $end, $timezone)
    {
        $params = [
            'modelName' => 'Raw',
            'leftJoin'  => [
                'user'   => [
                    'leftField'  => 'user.main_user_id',
                    'operator'   => '=',
                    'rightField' => 'raw.user_id'
                ],
                'status' => [
                    'leftField'  => 'status.id',
                    'operator'   => '=',
                    'rightField' => 'raw.status_id'
                ],
                'team'   => [
                    'leftField'  => 'team.ID',
                    'operator'   => '=',
                    'rightField' => 'raw.team'
                ]
            ],
            'filter'    => [
                'raw.status_id' => 6
            ],
            'group'     => [
                'case_id'
            ],
            'select'    => "max(raw.shift_date) as `Shift Date`, concat(user.lastname, ', ', user.firstname) as Name, team.description as Team, max(raw.start_time) as StartTime, max(raw.end_time) as EndTime,raw.duration as Duration,raw.fin as FIN,raw.hosp_serv as `Hospital Service`,raw.mrn as MRN,raw.admit_date as `Admit Date`, raw.discharge_date as `Discharge Date`, raw.price as Price, raw.observation as Observation, raw.hold_reason as `Hold Reason`, raw.accountstat as `Account Status`, raw.comment as Comments, status.name as Status"
        ];

        return $this->databaseService->fetchAll($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function getConsolidated (array $post)
    {
        $params = [
            'modelName' => 'BreakRaw',
            'filter'    => [
                'start_time' => [
                    'operator' => '>=',
                    'data'     => $post['start'],
                ],
                'end_time'   => [
                    'operator' => '<=',
                    'data'     => $post['end']
                ]
            ]
        ];

        return $this->databaseService->fetchAll($params);
    }
}