<?php

namespace Modules\Main\Services;

use Modules\Main\Services\Core\DashboardInterface;

class BaseDashboardService implements DashboardInterface
{
    protected $databaseService;

    public function __construct (DatabaseService $databaseService)
    {
        $this->databaseService = $databaseService;
    }

    /**
     * @param $post
     * @return mixed
     */
    public function getCases ($post)
    {
        $params = [
            'modelName' => 'Case' . $post['rawNumber'],
            'leftJoin'  => [
                'user' => [
                    'leftField'  => 'user.id',
                    'operator'   => '=',
                    'rightField' => 'Case' . $post['rawNumber'] . '.user_id'
                ]
            ],
            'between'   => [
                'shift_date' => [
                    $post['start'],
                    $post['end']
                ]
            ]
        ];

        return $this->databaseService->fetchAll($params);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getDashboardUserList ($id)
    {
        $params = [
            'modelName' => 'Case' . $id,
            'leftJoin'  => [
                'user' => [
                    'leftField'  => 'Case' . $id . '.user_id',
                    'operator'   => '=',
                    'rightField' => 'attendance.userid'
                ]
            ],
            'distinct'  => null,
            'select'    => 'user.id, user.firstname, user.lastname, attendance.shift_date, user.status'
        ];
        return $this->databaseService->fetchAll($params);
    }
}