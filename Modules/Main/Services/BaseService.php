<?php

namespace Modules\Main\Services;

class BaseService
{
    protected $db;

    public function __construct ()
    {
        $this->db = \DB::connection(config('vanderbilt.db_connection'));
    }
}