<?php

namespace Modules\Main\Services;

use Modules\Main\Services\Core\CaseActivityInterface;

class BaseCaseActivityLogService implements CaseActivityInterface
{
    protected $databaseService;

    public function __construct (DatabaseService $databaseService)
    {
        $this->databaseService = $databaseService;
    }

    /**
     * @param $data
     */
    public function log ($data)
    {
        $params = [
            'modelName' => 'CaseActivity',
            'data'      => $data
        ];

        $this->databaseService->create($params);
    }
}