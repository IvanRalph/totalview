<?php

namespace Modules\Main\Services;

use Modules\Main\Services\Core\StatusInterface;

class BaseStatusService implements StatusInterface
{
    protected $databaseService;

    const COMPLETED_STATUS_ID = '4';
    const HOLD_STATUS_ID = '6';
    const ESCALATED_STATUS_ID = '5';
    const ONGOING_STATUS_ID
        = [
            1,
            3,
            7
        ];
    const EDIT_COMPLETED_STATUS_ID = 8;
    const EDIT_ACTION_NAME = 'Edit';
    const CONTINUE_ACTION_NAME = 'Continue';
    const EDIT_ACTION_ID = 7;
    const CONTINUE_ACTION_ID = 3;
    const START_ACTION_ID = 1;
    const HOLD_ACTION_ID = 6;
    const SAVE_EDIT_ACTION_ID = 8;

    public function __construct (DatabaseService $databaseService)
    {
        $this->databaseService = $databaseService;
    }

    /**
     * @return mixed
     */
    public function getAll ()
    {
        $params = [
            'modelName' => 'Status'
        ];

        return $this->databaseService->fetchAll($params);
    }

    /**
     * @param $actionId
     * @return mixed
     */
    public function getStatusIdByActionId ($actionId)
    {
        $params = [
            'modelName' => 'Action',
            'select'    => 'status_id',
            'filter'    => [
                'id' => $actionId
            ]
        ];

        return $this->databaseService->fetch($params)->status_id;
    }
}