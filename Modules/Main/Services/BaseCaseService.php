<?php

namespace Modules\Main\Services;

use Modules\Main\Services\Core\CaseActivityInterface;
use Modules\Main\Services\Core\CaseInterface;
use Modules\Main\Services\Core\FieldInterface;
use Modules\Main\Services\Core\StatusInterface;

abstract class BaseCaseService implements CaseInterface
{
    const TABLE_NAME = 'case';
    const TABLE_NAME_SUB = 'submeasure';
    const LAST_CASE_HOUR_INTERVAL = 5;

    /**
     * @var CaseActivityInterface
     */
    protected $caseActivityLogService;

    /**
     * @var StatusInterface
     */
    protected $statusService;

    /**
     * @var FieldInterface
     */
    protected $fieldService;

    /**
     * @var DatabaseService
     */
    protected $databaseService;

    protected $db;

    /**
     * BaseCaseService constructor.
     * @param DatabaseService $databaseService
     */
    public function __construct (DatabaseService $databaseService)
    {
        $this->databaseService = $databaseService;
    }

    /**
     * @param $lobId
     * @param $userId
     * @return mixed
     */
    abstract function getCurrentCase ($lobId, $userId);


  /**
    * @param $userId
     * @param $shift_date
     * @return mixed
     */
    public function getCurrentCaseLob($userId, $shiftDate)
    {

       $params = [
            'modelName' => 'CaseActivity',
            'filter'    => [
                'shift_date' => $shiftDate,
                'user_id'    => $userId
              
              /*  'action_id'  => [
                    BaseStatusService::START_ACTION_ID,
                    BaseStatusService::CONTINUE_ACTION_ID
                ]*/
            ],
            'order'     => [
                'field' => 'id',
                'type'  => 'desc'
            ],
        ];
        
         return $this->databaseService->fetch($params);
    }


    /**
     * @param array $post
     * @return mixed
     */
    public function getUserCompleted (array $post)
    {
        $params = [
            'modelName' => 'Case' . $post['lob_id'],
            'filter'    => [
                'user_id'    => $post['user_id'],
                'shift_date' => $post['shift_date'],
                'status_id'  => [
                    BaseStatusService::COMPLETED_STATUS_ID,
                    BaseStatusService::EDIT_COMPLETED_STATUS_ID
                ]
            ]
        ];

        return $this->databaseService->fetchAll($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function getUserPending (array $post)
    {
        $params = [
            'modelName' => 'Case' . $post['lob_id'],
            'filter'    => [
                'user_id'    => $post['user_id'],
                'shift_date' => $post['shift_date'],
                'status_id'  => [BaseStatusService::HOLD_STATUS_ID]
            ]
        ];

        return $this->databaseService->fetchAll($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function getUserBreakDuration (array $post)
    {
        $params = [
            'modelName' => 'Aux',
            'filter'    => [
                'user_id'    => $post['user_id'],
                'shift_date' => $post['shift_date'],
            ],
            'group'     => [
                'user_id'
            ],
            'select'    => 'coalesce(sec_to_time(sum(time_to_sec(timediff(end_time,start_time)))),\'00:00:00\') as 
            duration'
        ];

        return $this->databaseService->fetch($params);
    }

    /**
     * @param string $post
     * @return mixed
     */
    public function getUserTarget (string $post)
    {
        $params = [
            'modelName' => 'User',
            'filter'    => [
                'main_user_id' => $post,
            ],
            'select'    => 'ramp'
        ];

        return $this->databaseService->fetch($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function getById (array $post)
    {
        $params = [
            'modelName' => 'Case' . $post['lob_id'],
            'filter'    => [
                'id' => $post['case_id'],
            ],
        ];

        return $this->databaseService->fetch($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function create (array $post)
    {
        $actionId         = 1;
        $customFieldsData = $this->establishCustomFieldsData($post['fields'], $post['data']);
        $statusId         = $this->statusService->getStatusIdByActionId($actionId);
        $mainFieldsData = [

            'shift_date'    => $post['shift_date'],
            'user_id'       => $post['user_id'],
            'status_id'     => $statusId,
            'duration'      => $post['data']['duration'],
            'created_at'    => $post['data']['created_at'],
            'updated_at'    => '',
            'break_at'      => NULL

        ];

        $params = [
            'modelName' => 'Case' . $post['lob_id'],
            'data'      => array_merge($mainFieldsData, $customFieldsData)
        ];

        $caseId = $this->databaseService->create($params);

        $params = [
            'lob_id'     => $post['lob_id'],
            'case_id'    => $caseId,
            'user_id'    => $post['user_id'],
            'shift_date' => $post['shift_date'],
            'action_id'  => $actionId,
            'status_id'  => $statusId,
            'created_at' => $post['data']['created_at']
        ];

        $this->caseActivityLogService->log($params);

        return $caseId;
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function update (array $post)
    {
        $statusId         = $this->statusService->getStatusIdByActionId($post['data']['action_id']);
        $customFieldsData = $this->establishCustomFieldsData($post['fields'], $post['data'], true);
        $mainFieldsData   = [
            'status_id'  => $statusId,
            'duration'   => $post['data']['duration'],
            'updated_at' => $post['data']['updated_at']
        ];

        $params = [
            'modelName' => 'Case' . $post['lob_id'],
            'data'      => array_merge($customFieldsData, $mainFieldsData),
            'filter'    => [
                'id' => $post['case_id']
            ]
        ];

        $caseInfo = $this->databaseService->fetch($params);

        $updateSuccess = $this->databaseService->update($params);

        $params = [
            'lob_id'     => $post['lob_id'],
            'case_id'    => $post['case_id'],
            'user_id'    => $caseInfo->user_id,
            'shift_date' => $caseInfo->shift_date,
            'action_id'  => $post['data']['action_id'],
            'status_id'  => $statusId,
            'created_at' => $post['data']['updated_at']
        ];

        $this->caseActivityLogService->log($params);

        return $updateSuccess;
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function deleteCase (array $post)
    {
        $params = [
            'modelName' => 'Case' . $post['lob_id'],
            'join'      => [
                'case_activity_log',
                'case_activity_log.case_id',
                '=',
                'Case' . $post['lob_id'] . '.id'
            ],
            'filter'    => [
                'Case' . $post['lob_id'] . '.id' => $post['case_id']
            ]
        ];

        return $this->databaseService->delete($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function updateStatus (array $post)
    {
        $mainFieldsData = [
            'status_id'  => $post['status_id'],
            'updated_at' => $post['current_time']
        ];

        $params = [
            'modelName' => 'Case' . $post['lob_id'],
            'filter'    => [
                'id' => $post['case_id']
            ],
            'data'      => $mainFieldsData
        ];

        return $this->databaseService->update($params);
    }

    /**
     * @param $post
     * @return mixed
     */
    public function updateBreakAt ($post)
    {
        $lobId = $this->getBreakLobId($post['user_id'])->lob_id;

        $currentCase = $this->getCurrentCase($lobId, $post['user_id']);

        if ($currentCase) {
            $caseId = $currentCase->id;

            $startTime = $currentCase->created_at;

            if ($currentCase->break_at != null) {
                $startTime = $currentCase->break_at;
            }

            $computedTime = date('Y-m-d H:i:s', strtotime($post['break_duration'], strtotime($startTime)));

            $FieldsData = [
                'break_at' => $computedTime
            ];

            $params = [
                'modelName' => 'Case' . $lobId,
                'filter'    => [
                    'id' => $caseId
                ],
                'data'      => $FieldsData
            ];

            return $this->databaseService->update($params);
        }

        return null;
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getBreakLobId ($userId)
    {
        $params = [
            'modelName' => 'CaseActivity',
            'filter'    => [
                'user_id' => $userId
            ],
            'order'     => [
                'field' => 'created_at',
                'type'  => 'desc'
            ],
            'select'    => 'lob_id'
        ];

        return $this->databaseService->fetch($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function getCases (array $post)
    {
        $timezone = "";

        if ($timezone == '1') {
            $hour = '14';
        } else {
            $hour = '0';
        }
        $selectLob = $this->fieldService->getByLobId($post['rawNumber']);
        $fields    = [];
        foreach ($selectLob as $key) {
            $fields[] = ([
                'html_name' => $key->html_name,
                'name'      => $key->name,
            ]);
        }

        for ($i = 0; $i <= count($selectLob) - 1; $i++) {
            $field[] = $fields[$i]['html_name'] . ' as ' . '`' . $fields[$i]['name'] . '`';
        }

        $table    = 'case' . $post['rawNumber'];
        $column[] = $table . "." . 'shift_date AS `Shift Date`';
        $column[] = 'duration AS Duration';
        $column[] = \DB::raw("CONCAT(user.firstname,' ',user.lastname) AS Name");
        $column[] = 'status.name AS Status';
        $column[] = 'duration AS Duration';
        $date[]   = \DB::raw("DATE_FORMAT(created_at, DATE_SUB(created_at, INTERVAL '$hour' HOUR)) as Created");
        $date[]   = \DB::raw("DATE_FORMAT(updated_at, DATE_SUB(updated_at, INTERVAL '$hour' HOUR)) as Updated");
        $columns  = array_merge($column, $field, $date);

        $params = [
            'modelName' => 'Case' . $post['rawNumber'],
            'leftJoin'  => [
                'user'   => [
                    'leftField'  => 'user.main_user_id',
                    'operator'   => '=',
                    'rightField' => $table . '.user_id'
                ],
                'status' => [
                    'leftField'  => $table . '.status_id',
                    'operator'   => '=',
                    'rightField' => 'status.id'
                ]
            ],
            'distinct'  => null,
            'select'    => implode(',', $columns)
        ];

        return $this->databaseService->fetchAll($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function countByStatusId (array $post)
    {
        $params = [
            'modelName' => 'Case' . $post['lob_id'],
            'filter'    => [
                'shift_date' => session('user_from_account')->shift_date,
            ]
        ];

        if (is_array($post['status_id'])) {
            $params['filter']['status_id'] = [
                'status_id' => $post['status_id']
            ];
        } else {
            $params['filter']['status_id'] = $post['status_id'];
        }

        return $this->databaseService->count($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function getByUser (array $post)
    {
        $params = [
            'modelName' => 'Case' . $post['lob_id'],
            'filter'    => []
        ];

        foreach ($post['filters'] as $filter) {
            $params['filter'][$filter['column']] = [
                'operator' => $filter['comparison'],
                'data'     => $filter['value']
            ];
        }

        return $this->databaseService->fetchAll($params);
    }

    /**
     * @param      $fields
     * @param      $data
     * @param bool $isUpdate
     * @return array
     */
    public function establishCustomFieldsData ($fields, $data, $isUpdate = false)
    {
        $customFieldsData = [];
        foreach ($fields as $field) {
            if (isset($data[$field->html_name])) {
                $customFieldsData[$field->html_name] = $data[$field->html_name];
            } else if (!$isUpdate) {
                $customFieldsData[$field->html_name] = $field->default_value;
            }
        }

        return $customFieldsData;
    }

    /**
     * @param $lobId
     * @param $teamId
     * @return mixed
     */
    public function dashboardQueryBuilder ($lobId, $teamId)
    {
        $shiftDate = $this->db->table('user')->where('user.id', session('user_from_account')->id)->first();

        if ($teamId == 'all') {
            $teamId = "(SELECT ID FROM team)";
        }

        $result = $this->db->select('Select
            CONCAT(user.lastname, ", ", user.firstname) AS name,
            team.description AS team,
            user.shift_date AS shift_date,
            COALESCE(CONCAT(shift_schedule.start_shift, " - ", shift_schedule.end_shift), "00:00:00 - 00:00:00") AS shift_schedule,
            SUM(IF(case' . $lobId . '.status_id IN (4,8), 1, 0)) AS total_completed_cases,
            SUM(IF(case' . $lobId . '.status_id = 6, 1, 0)) AS total_pending_cases,
            user.ramp AS cph_target,
            (SELECT count(*) FROM case' . $lobId . ' WHERE user_id = user.main_user_id AND shift_date = "' . $shiftDate->shift_date . '" AND status_id NOT IN (1,3,7)) AS total_handled_cases,
            SEC_TO_TIME(SUM(IF(case' . $lobId . '.status_id IN (4,8), TIME_TO_SEC(case' . $lobId . '.duration), 0))) AS total_duration,
            COALESCE(SEC_TO_TIME(CEILING(SUM(IF(case' . $lobId . '.status_id IN (4,8), TIME_TO_SEC(case' . $lobId . '.duration), 0)) / SUM(IF(case' . $lobId . '.status_id IN (4,8), 1, 0)))), "00:00:00") AS aht, user.main_user_id AS main_user_id, user.status AS status
            FROM user
            left join case' . $lobId . '
            on user.main_user_id = case' . $lobId . '.user_id
            AND case' . $lobId . '.shift_date = "' . $shiftDate->shift_date . '"
            INNER JOIN team
            ON user.team = team.id
            LEFT JOIN shift_schedule
            ON user.main_user_id = shift_schedule.user_id
            AND shift_schedule.shift_date = user.shift_date
            WHERE user.team IN (' . $teamId . ')
            AND user.role IN (5,11)
            AND user.active = 2
            GROUP BY user.main_user_id
            ORDER BY user.status DESC');

        return $result;
    }
}