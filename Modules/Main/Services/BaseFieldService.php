<?php

namespace Modules\Main\Services;

use Modules\Main\Services\Core\FieldInterface;

class BaseFieldService implements FieldInterface
{
    protected $databaseService;

    public function __construct (DatabaseService $databaseService)
    {
        $this->databaseService = $databaseService;
    }

    private $fields = [];

    /**
     * @param $lobId
     * @return mixed
     */
    public function getByLobId ($lobId)
    {
        if (isset($this->fields[$lobId])) {
            return $this->fields[$lobId];
        }

        $params = [
            'modelName' => 'Field',
            'filter'    => [
                'lob_id' => $lobId
            ],
            'order'     => [
                'field' => 'sequence',
                'type'  => 'asc'
            ]
        ];

        $this->fields[$lobId] = $this->databaseService->fetchAll($params);

        return $this->fields[$lobId];
    }

    /**
     * @param $lobId
     * @return bool
     */
    public function getReference ($lobId)
    {
        $fields = $this->getByLobId($lobId);

        foreach ($fields as $field) {
            if ($field->is_reference) {
                return $field;
            }
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getServiceLine ()
    {
        $params = [
            'modelName' => 'ListValue',
            'filter'    => [
                'field_id' => 9
            ],
            'select'    => 'value'
        ];

        return $this->databaseService->fetchAll($params);
    }
}