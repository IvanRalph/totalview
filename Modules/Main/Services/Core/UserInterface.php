<?php
/**
 * Created by PhpStorm.
 * User: igvitto
 * Date: 8/14/2018
 * Time: 4:19 PM
 */

namespace Modules\Main\Services\Core;

interface UserInterface
{
    public function getByMainUserId ($mainUserId);

    public function update (array $post);

    public function updateShiftSchedule (array $post);

    public function updateUserList (array $post);

    public function countAll (array $post);

    public function countAvailable (array $post);

    public function getAllByLobId ($lobId);

    public function getShiftSchedule ($userId);

    public function getShiftDateForDashboard ();
}