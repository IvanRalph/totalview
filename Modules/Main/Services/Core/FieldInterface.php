<?php
/**
 * Created by PhpStorm.
 * User: igvitto
 * Date: 8/13/2018
 * Time: 6:36 PM
 */

namespace Modules\Main\Services\Core;

interface FieldInterface
{
    public function getByLobId ($lobId);

    public function getReference ($lobId);

    public function getServiceLine ();
}