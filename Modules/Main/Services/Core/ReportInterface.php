<?php
/**
 * Created by PhpStorm.
 * User: igvitto
 * Date: 8/13/2018
 * Time: 7:58 PM
 */

namespace Modules\Main\Services\Core;

interface ReportInterface
{
    public function getRaw (array $post);

    public function getBreaks (array $post);

    public function getHold ($rawNumber, $start, $end, $timezone);

    public function getConsolidated (array $post);
}