<?php
/**
 * Created by PhpStorm.
 * User: igvitto
 * Date: 8/3/2018
 * Time: 9:12 PM
 */

namespace Modules\Main\Services\Core;

interface AuxBreakInterface
{
    public function getAuxType ();

    public function create (array $post);

    public function update (array $params);

    public function getCurrent (array $params);

    public function getLastBreak (string $params);

    public function getAuxTypeNameById (string $params);
}