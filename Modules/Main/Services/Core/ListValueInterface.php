<?php
/**
 * Created by PhpStorm.
 * User: igvitto
 * Date: 8/13/2018
 * Time: 7:19 PM
 */

namespace Modules\Main\Services\Core;

interface ListValueInterface
{
    public function getByLobId ($lobId);

    public function getByParentId (array $post);
}