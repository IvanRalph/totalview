<?php
/**
 * Created by PhpStorm.
 * User: igvitto
 * Date: 8/13/2018
 * Time: 9:14 PM
 */

namespace Modules\Main\Services\Core;

interface RoleInterface
{
    public function getRoleList ();
}