<?php
/**
 * Created by PhpStorm.
 * User: igvitto
 * Date: 8/13/2018
 * Time: 6:21 PM
 */

namespace Modules\Main\Services\Core;

interface DashboardInterface
{
    public function getCases ($post);

    public function getDashboardUserList ($id);
}