<?php
/**
 * Created by PhpStorm.
 * User: igvitto
 * Date: 8/14/2018
 * Time: 8:23 PM
 */

namespace Modules\Main\Services\Core;

interface LobInterface
{
    public function getLobList ();

    public function deleteLobUser ($id);

    public function insertLobUser (array $post);

    public function getSettings ($lobId);
}