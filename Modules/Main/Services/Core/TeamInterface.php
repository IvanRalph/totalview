<?php
/**
 * Created by PhpStorm.
 * User: igvitto
 * Date: 8/13/2018
 * Time: 9:59 PM
 */

namespace Modules\Main\Services\Core;

interface TeamInterface
{
    public function getLeader ($teamId);

    public function getTeamByUser ($userId);

    public function getTeamList ();
}