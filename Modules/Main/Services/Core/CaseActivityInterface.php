<?php
/**
 * Created by PhpStorm.
 * User: igvitto
 * Date: 8/13/2018
 * Time: 6:04 PM
 */

namespace Modules\Main\Services\Core;

interface CaseActivityInterface
{
    public function log ($data);
}