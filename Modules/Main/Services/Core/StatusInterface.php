<?php
/**
 * Created by PhpStorm.
 * User: igvitto
 * Date: 8/13/2018
 * Time: 9:18 PM
 */

namespace Modules\Main\Services\Core;

interface StatusInterface
{
    public function getAll ();

    public function getStatusIdByActionId ($actionId);
}