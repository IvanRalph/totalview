<?php
/**
 * Created by PhpStorm.
 * User: igvitto
 * Date: 8/14/2018
 * Time: 4:00 PM
 */

namespace Modules\Main\Services\Core;

interface UserAuditTrailInterface
{
    const TABLE_NAME = 'user_audit_trail';
    const ACTION_START_BREAK = 'start_break';
    const ACTION_END_BREAK = 'end_break';
    const ACTION_START_CASE = 'start_case';
    const ACTION_END_CASE = 'end_case';
    const ACTION_HOLD_CASE = 'hold_case';
    const ACTION_EDIT_CASE = 'edit_case';
    const ACTION_CONTINUE_CASE = 'continue_case';
    const ACTION_SAVE_EDIT_CASE = 'save_edit_case';
    const ACTION_START_SHIFT = 'start_shift';
    const ACTION_END_SHIFT = 'end_shift';

    public function record ($userId, $shiftDate, $action, $reference = '', $caseId = null, $shiftId = null, $breakId = null);

    public function getByUserId (array $post);

    public function display ($trail);
}