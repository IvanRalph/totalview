<?php
/**
 * Created by PhpStorm.
 * User: igvitto
 * Date: 8/9/2018
 * Time: 6:18 PM
 */

namespace Modules\Main\Services\Core;

interface CaseInterface
{
    public function getUserCompleted (array $post);

    public function getUserPending (array $post);

    public function getUserBreakDuration (array $post);

    public function getUserTarget (string $post);

    public function getById (array $post);

    public function create (array $post);

    public function update (array $post);

    public function deleteCase (array $post);

    public function updateStatus (array $post);

    public function updateBreakAt ($post);

    public function getBreakLobId ($userId);

    public function getCases (array $post);

    public function countByStatusId (array $post);

    public function getByUser (array $post);

    public function establishCustomFieldsData ($fields, $data, $isUpdate = false);

    public function dashboardQueryBuilder ($lobId, $teamId);
}