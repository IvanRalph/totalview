<?php
/**
 * Created by PhpStorm.
 * User: igvitto
 * Date: 8/13/2018
 * Time: 10:16 PM
 */

namespace Modules\Main\Services\Core;

interface TeamSettingInterface
{
    public function getUserList ();

    public function getLatestShiftDate ();

    public function getTeamLeader ();

    public function updateTeamList (array $post);

    public function addTeamList (array $post);

    public function updateTeamSchedule (array $post);

    public function updateTeamBulk (array $post);

    public function getStatus ($id);

    public function updateValueListBulk (array $post);

    public function updateTeamUserScheduleShift (array $post);

    public function updateTeamKickout(array $post);
}