<?php

namespace Modules\Main\Services;

use Modules\Main\Services\Core\LobInterface;

class BaseLobService implements LobInterface
{
    protected $databaseService;

    public function __construct (DatabaseService $databaseService)
    {
        $this->databaseService = $databaseService;
    }

    /**
     * @return mixed
     */
    public function getLobList ()
    {
        $params = [
            'modelName' => 'Lob'
        ];

        return $this->databaseService->fetchAll($params);
    }

    /**
     * @param $id
     */
    public function deleteLobUser ($id)
    {
      

        $dataId = explode(",", $id);
        array_push($dataId, 0);

        $params = [
            'modelName' => 'UserLob',
            'filter'    => [
                'user_id' => $dataId
            ]
        ];

        $this->databaseService->delete($params);
    }

    /**
     * @param array $post
     * @return mixed
     */
    public function insertLobUser (array $post)
    {

        $dataId  = explode(",", $post['id']);
        $dataLob = explode(",", $post['lob_id']);
        $insert = [];
        foreach ($dataId as $id) {
            foreach ($dataLob as $lob) {
                $insert[] = ([
                    'user_id' => $id,
                    'lob_id'  => $lob
                ]);
            }
        }

        $params = [
            'modelName' => 'UserLob',
            'data'      => $insert
        ];

        return $this->databaseService->createMany($params);
    }

    /**
     * @param $lobId
     * @return mixed
     */
    public function getSettings ($lobId)
    {
        $params = [
            'modelName' => 'LobSetting',
            'filter'    => [
                'lob_id' => $lobId
            ]
        ];

        return $this->databaseService->fetch($params);
    }
}