<?php

namespace Modules\Base\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Modules\Base\Services\LobService;
use Modules\Base\Services\CaseService;
use Modules\Base\Services\FieldService;

class ReportsApiController extends Controller
{
    private $lobService;

    public function __construct (LobService $lobService, CaseService $caseService, FieldService $fieldService)
    {
        $this->lobService   = $lobService;
        $this->caseService  = $caseService;
        $this->fieldService = $fieldService;
    }

    public function getLobList ()
    {
        $userList = $this->lobService->getLobList();
    }

    public function getRawReports (Request $request)
    {
        $rawNumber = $request->get('rawNumber');
        $start     = $request->get('start');
        $end       = $request->get('end');
        $timezone  = $request->get('timezone');

        $data = $this->caseService->getCases($rawNumber, $start, $end, $timezone);

        if (count($data) == 0) {
            $data       = ['returns' => 'false'];
            $statusCode = 500;

            return json_encode($data, $statusCode);
        } else {
            $statusCode = 200;

            return json_encode($data, $statusCode);
        }
    }
}