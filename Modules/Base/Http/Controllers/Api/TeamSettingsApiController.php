<?php

namespace Modules\Base\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Base\Services\TeamSettingService;
use Modules\Base\Services\LobService;

class TeamSettingsApiController extends Controller
{
    private $teamSettingService;

    public function __construct (TeamSettingService $teamSettingService, LobService $lobService)
    {
        $this->teamSettingService = $teamSettingService;
        $this->lobService         = $lobService;
    }

    public function getUserList ()
    {
        $userList = $this->teamSettingService->getUserList();

        return $userList;
    }

    public function getTeamList ()
    {
        $teamList = $this->teamSettingService->getTeamList();

        return json_encode($teamList);
    }

    public function updateTeamList ($teamLeaderID, $checkbox)
    {
        $teamLeaderArray = explode(",", $teamLeaderID);
        $editID          = $teamLeaderArray[0];
        $editTeamName    = $teamLeaderArray[1];

        $updateTeamList = $this->teamSettingService->updateTeamList($editID, $editTeamName, $checkbox);

        return $updateTeamList;
    }

    public function updateTeamUser ($teamListInfo)
    {
        $teamListInfoArray = explode(",", $teamListInfo);

        $user_id             = $teamListInfoArray[0]; // user id
        $user_start_shift    = $teamListInfoArray[1]; // user_start_shift
        $user_end_shift      = $teamListInfoArray[2]; // user_end_shift
        $onLeave             = $teamListInfoArray[3]; // onLeave
        $teamListPlaceholder = $teamListInfoArray[4]; // teamListPlaceholder
        $slctRole            = $teamListInfoArray[5]; //select role
        $ramp                = $teamListInfoArray[6];

        $updateTeamUser = $this->teamSettingService->updateTeamUser($user_id, $user_start_shift, $user_end_shift, $onLeave, $teamListPlaceholder, $slctRole, $ramp);

        return $updateTeamUser;
    }

    public function updateUserLob ($id, $lob)
    {
        $deleteLobUser = $this->lobService->deleteLobUser($id);
        $insertLobUser = $this->lobService->insertLobUser($id, $lob);

        return array(
            $deleteLobUser,
            $insertLobUser
        );
    }

    public function updateTeamSchedule ($id, $formData, Request $request)
    {
        $myArrayId       = explode(',', $id);
        $myArrayFormData = explode(',', $formData);

        $start_shift = $myArrayFormData[0];
        $end_shift   = $myArrayFormData[1];
        $on_leave    = $myArrayFormData[2];
        $ramp        = $myArrayFormData[3];

        $updateTeamUser = $this->teamSettingService->updateTeamSchedule($id, $start_shift, $end_shift, $on_leave, $ramp);

        return $updateTeamUser;
    }

    public function updateTeamBulk ($id, $team)
    {
        $updateTeamUser = $this->teamSettingService->updateTeamBulk($id, $team);

        return $updateTeamUser;
    }
}
