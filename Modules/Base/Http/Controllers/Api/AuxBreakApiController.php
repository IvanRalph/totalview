<?php

namespace Modules\Base\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Base\Services\AuxBreakService;
use Modules\Base\Services\UserAuditTrailService;
use Modules\Base\Services\UserService;

class AuxBreakApiController extends Controller
{
    /**
     * @var AuxBreakService
     */
    private $auxBreakService;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var UserAuditTrailService
     */
    private $userAuditTrailService;

    public function __construct (AuxBreakService $auxBreakService, UserService $userService, UserAuditTrailService $userAuditTrailService)
    {
        $this->auxBreakService       = $auxBreakService;
        $this->userService           = $userService;
        $this->userAuditTrailService = $userAuditTrailService;
    }

    public function start (Request $request)
    {
        $data            = $request->get('data');
        $mainUser        = $request->get('userData');
        $userFromAccount = $this->userService->getByMainUserId($mainUser['id']);

        $auxId = $this->auxBreakService->create(
            $mainUser['id'],
            $userFromAccount->shift_date,
            $data
        );

        if ($auxId) {
            $this->userAuditTrailService->record(
                $mainUser['id'],
                $userFromAccount->shift_date,
                UserAuditTrailService::ACTION_START_BREAK,
                $this->auxBreakService->getAuxTypeNameById($data['aux_type_id'])
            );
        }

        return response()->json(['aux_id' => $auxId]);
    }

    public function stop (Request $request, $auxId)
    {
        $mainUser        = $request->get('userData');
        $data            = $request->get('data');
        $userFromAccount = $this->userService->getByMainUserId($mainUser['id']);
        $updateSuccess   = $this->auxBreakService->update($auxId, $data);

        if ($updateSuccess) {
            $this->userAuditTrailService->record(
                $mainUser['id'],
                $userFromAccount->shift_date,
                UserAuditTrailService::ACTION_END_BREAK,
                $this->auxBreakService->getAuxTypeNameById($data['aux_type_id'])
            );
        }

        return response()->json(['update_success' => $updateSuccess]);
    }

    public function getCurrent (Request $request)
    {
        $mainUser        = $request->get('userData');
        $userFromAccount = $this->userService->getByMainUserId($mainUser['id']);
        $currentAuxBreak = $this->auxBreakService->getCurrent($mainUser['id'], $userFromAccount->shift_date);

        return response()->json(['current_aux_break' => $currentAuxBreak]);
    }
}