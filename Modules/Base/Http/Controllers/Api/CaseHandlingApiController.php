<?php

namespace Modules\Base\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Base\Services\CaseActivityLogService;
use Modules\Base\Services\CaseService;
use Modules\Base\Services\FieldService;
use Modules\Base\Services\LobService;
use Modules\Base\Services\StatusService;
use Modules\Base\Services\UserAuditTrailService;

class CaseHandlingApiController extends Controller
{
    /**
     * @var CaseService
     */
    private $caseService;

    /**
     * @var FieldService
     */
    private $fieldService;

    /**
     * @var StatusService
     */
    private $statusService;

    /**
     * @var CaseActivityLogService
     */
    private $caseActivityLogService;

    /**
     * @var UserAuditTrailService
     */
    private $userAuditTrailService;

    /**
     * @var LobService
     */
    private $lobService;

    public function __construct (
        CaseService $caseService,
        FieldService $fieldService,
        StatusService $statusService,
        CaseActivityLogService $caseActivityLogService,
        UserAuditTrailService $userAuditTrailService,
        LobService $lobService
    )
    {
        $this->caseService            = $caseService;
        $this->fieldService           = $fieldService;
        $this->statusService          = $statusService;
        $this->caseActivityLogService = $caseActivityLogService;
        $this->userAuditTrailService  = $userAuditTrailService;
        $this->lobService             = $lobService;
    }

    public function get (Request $request, $lobId, $caseId)
    {
        $data        = $request->get('data');
        $case        = $this->caseService->getById($lobId, $caseId);
        $fields      = $this->fieldService->getByLobId($lobId);
        $actionId    = $request->get('action', 0);
        $currentTime = date('Y-m-d H:i:s');
        if ($case && $actionId) {
            $statusId          = $this->statusService->getStatusIdByActionId($actionId);
            $data['action_id'] = $actionId;
            $this->caseService->updateStatus($lobId, $caseId, $statusId, $currentTime);
            $this->caseActivityLogService->log(
                $lobId,
                $caseId,
                $case->user_id,
                $case->shift_date,
                $actionId,
                $statusId,
                $currentTime
            );

            $referenceField   = $this->fieldService->getReference($lobId);
            $auditTrailAction = UserAuditTrailService::ACTION_EDIT_CASE;

            if ($actionId == StatusService::CONTINUE_ACTION_ID) {
                $auditTrailAction = UserAuditTrailService::ACTION_CONTINUE_CASE;
            }

            $this->userAuditTrailService->record(
                $case->user_id,
                $case->shift_date,
                $auditTrailAction,
                $case->{$referenceField->html_name}
            );
        }

        return response()->json([
            'case_info' => $case,
            'fields'    => $fields
        ]);
    }

    public function getCurrent (Request $request, $lobId)
    {
        $mainUser    = $request->get('userData');
        $currentCase = $this->caseService->getCurrentCase($lobId, $mainUser['id']);

        return response()->json(['current_case_info' => $currentCase]);
    }

    public function create (Request $request, $lobId)
    {
        $data        = $request->get('data');
        $mainUser    = $request->get('userData');
        $accountUser = session('user_from_account');

        $validationErrors = $this->validate($lobId, $data);
        if ($validationErrors) {
            return response()->json(['validation_errors' => $validationErrors]);
        }

        $fields = $this->fieldService->getByLobId($lobId);
        $caseId = $this->caseService->create($lobId, $mainUser['id'], $accountUser->shift_date, $data, $fields);

        if ($caseId) {
            $referenceField = $this->fieldService->getReference($lobId);

            $this->userAuditTrailService->record(
                $mainUser['id'],
                $accountUser->shift_date,
                UserAuditTrailService::ACTION_START_CASE,
                $data[$referenceField->html_name]
            );
        }

        return response()->json(['case_id' => $caseId]);
    }

    public function update (Request $request, $lobId, $caseId)
    {
        $data        = $request->get('data');
        $mainUser    = $request->get('userData');
        $accountUser = session('user_from_account');
        $fields      = $this->fieldService->getByLobId($lobId);
        $isSuccess   = $this->caseService->update($lobId, $caseId, $data, $fields);

        if ($isSuccess) {
            $referenceField   = $this->fieldService->getReference($lobId);
            $auditTrailAction = UserAuditTrailService::ACTION_END_CASE;

            if ($data['action_id'] == StatusService::HOLD_ACTION_ID) {
                $auditTrailAction = UserAuditTrailService::ACTION_HOLD_CASE;
            } else if ($data['action_id'] == StatusService::SAVE_EDIT_ACTION_ID) {
                $auditTrailAction = UserAuditTrailService::ACTION_SAVE_EDIT_CASE;
            }

            $this->userAuditTrailService->record(
                $mainUser['id'],
                $accountUser->shift_date,
                $auditTrailAction,
                $data[$referenceField->html_name]
            );
        }

        return response()->json(['is_success' => $isSuccess]);
    }

    public function trackerStats (Request $request, $lobId)
    {
        $mainUser    = $request->get('userData');
        $accountUser = session('user_from_account');

        $trackerStatsData = $this->caseService->getUserTrackerStats($lobId, $mainUser['id'], $accountUser->shift_date);

        return response()->json($trackerStatsData);
    }

    public function caseTable (Request $request, $lobId)
    {
        $columns     = [
            [
                'data' => 'action',
                'name' => 'Action'
            ]
        ];
        $mainUser    = $request->get('userData');
        $accountUser = session('user_from_account');

        $fields    = $this->fieldService->getByLobId($lobId);
        $userCases = $this->caseService->getUserCaseTable($lobId, $mainUser['id'], $accountUser->shift_date);
        $statuses  = $this->statusService->getAll()->toArray();
        $statusIds = array_column($statuses, 'id');

        foreach ($userCases as $key => $case) {
            $statusKey    = array_search($case->status_id, $statusIds);
            $status       = $statuses[$statusKey];
            $case->action = $this->generateActionButton($lobId, $case, $status);
            $case->status = generateStatusDisplay($status);

            $userCases[$key] = $case;
        }

        foreach ($fields as $field) {
            if (!$field->display) {
                continue;
            }

            $columns[] = [
                'data' => $field->html_name,
                'name' => $field->name
            ];
        }

        $columns[] = [
            'data' => 'duration',
            'name' => 'Duration'
        ];
        $columns[] = [
            'data' => 'status',
            'name' => 'Status'
        ];

        return response()->json([
            'data'    => $userCases,
            'columns' => $columns
        ]);
    }

    private function validate ($lobId, $data)
    {
        $validationErrors = [];
        $fields           = $this->fieldService->getByLobId($lobId);
        $lobSettings      = $this->lobService->getSettings($lobId);

        foreach ($fields as $field) {
            if ($field->required && !$data[$field->html_name]) {
                $validationErrors[] = '<b>' . $field->name . '</b>' . ' is required.';
                continue;
            }

            if ($field->unique) {
                $caseInfo = $this->caseService->getByConditions(
                    $lobId,
                    [
                        [
                            'column'     => $field->html_name,
                            'comparison' => '=',
                            'value'      => $data[$field->html_name]
                        ]
                    ],
                    true
                );

                if ($caseInfo) {
                    $validationErrors[] = '<b>' . $field->name . '</b>' . ' already exist.';
                    continue;
                }
            }
        }

        // return immediately if there are errors from initial validation
        if ($validationErrors) {
            return $validationErrors;
        }

        // unique fields combination validation
        $conditions                 = [];
        $uniqueFieldNamesForDisplay = [];
        $uniqueFieldIds             = explode(',', $lobSettings->unique_field_ids);
        $fieldIds                   = array_column($fields->toArray(), 'id');

        foreach ($uniqueFieldIds as $uniqueField) {
            $uniqueField                  = trim($uniqueField);
            $fieldKey                     = array_search($uniqueField, $fieldIds);
            $field                        = $fields[$fieldKey];
            $uniqueFieldNamesForDisplay[] = '<b>' . $field->name . '</b>';
            $conditionValue               = isset($data[$field->html_name]) ? $data[$field->html_name] : '';

            $conditions[] = [
                'column'     => $field->html_name,
                'comparison' => '=',
                'value'      => $conditionValue
            ];
        }

        if ($this->caseService->getByConditions($lobId, $conditions, true)) {
            $validationErrors[] = implode(", ", $uniqueFieldNamesForDisplay) . " combination already exist.";
        }

        return $validationErrors;
    }

    private function generateActionButton ($lobId, $case, $status)
    {
        if (!$status->next_action_name) {
            return '';
        }

        $classIdentifier = strtolower($status->next_action_name);

        if ($status->next_action_name == StatusService::EDIT_ACTION_NAME) {
            $actionId = StatusService::EDIT_ACTION_ID;
        } else if ($status->next_action_name == StatusService::CONTINUE_ACTION_NAME) {
            $actionId = StatusService::CONTINUE_ACTION_ID;
        }

        return '<button data-action-id="' . $actionId . '" data-case-id="' . $case->id . '" data-lob-id="' . $lobId . '" class="btn btn-' . $classIdentifier . ' btn-xs btn-block ' . $status->next_action_class . '">' . $status->next_action_name . '</button>';
    }
}