<?php

namespace Modules\Base\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Base\Services\CaseService;
use Modules\Base\Services\FieldService;
use Modules\Base\Services\ListValueService;
use Modules\Base\Services\LobService;
use Modules\Base\Services\UserAuditTrailService;
use Modules\Base\Services\UserService;

class CaseHandlingWebController extends Controller
{
    /**
     * @var FieldService
     */
    private $fieldService;

    /**
     * @var ListValueService
     */
    private $listValueService;

    /**
     * @var CaseService
     */
    private $caseService;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var UserAuditTrailService
     */
    private $userAuditTrailService;

    /**
     * @var LobService
     */
    private $lobService;

    /**
     * CaseHandlingWebController constructor.
     * @param FieldService          $fieldService
     * @param ListValueService      $listValueService
     * @param CaseService           $caseService
     * @param UserService           $userService
     * @param UserAuditTrailService $userAuditTrailService
     * @param LobService            $lobService
     */
    public function __construct (
        FieldService $fieldService,
        ListValueService $listValueService,
        CaseService $caseService,
        UserService $userService,
        UserAuditTrailService $userAuditTrailService,
        LobService $lobService
    )
    {
        $this->fieldService          = $fieldService;
        $this->listValueService      = $listValueService;
        $this->caseService           = $caseService;
        $this->userService           = $userService;
        $this->userAuditTrailService = $userAuditTrailService;
        $this->lobService            = $lobService;
    }

    public function index (Request $request)
    {
        $fields          = [];
        $listValues      = [];
        $trackerStats    = [];
        $lobSettings     = [];
        $mainUser        = $request->get('userData');
        $userLobs        = session('user_lobs_info', []);
        $userFromAccount = $this->userService->getByMainUserId($mainUser['id']);

        foreach ($userLobs as $lob) {
            $lobSettings[$lob->id]  = $this->lobService->getSettings($lob->id);
            $fields[$lob->id]       = $this->fieldService->getByLobId($lob->id);
            $listValues[$lob->id]   = $this->listValueService->getByLobId($lob->id);
            $trackerStats[$lob->id] = $this->caseService->getUserTrackerStats(
                $lob->id,
                $userFromAccount->main_user_id,
                $userFromAccount->shift_date
            );
        }

        return view(
            'base::case-handling.index',
            [
                'userLobs'     => $userLobs,
                'lobSettings'  => $lobSettings,
                'fields'       => $fields,
                'listValues'   => $listValues,
                'trackerStats' => $trackerStats,
                'userInfo'     => $userFromAccount
            ]
        );
    }

    public function startShift (Request $request)
    {
        $mainUser        = $request->get('userData');
        $userFromAccount = $this->userService->getByMainUserId($mainUser['id']);
        $selectedAccount = session('selected_account');
        $updateSuccess   = $this->userService->update($mainUser['id'], ['shift_start' => date('Y-m-d H:i:s')]);

        if ($updateSuccess) {
            $this->userAuditTrailService->record(
                $mainUser['id'],
                $userFromAccount->shift_date,
                UserAuditTrailService::ACTION_START_SHIFT
            );
        }

        return redirect($selectedAccount->route . '?token=' . $request->get('token'));
    }

    public function endShift (Request $request)
    {
        $mainUser        = $request->get('userData');
        $userFromAccount = $this->userService->getByMainUserId($mainUser['id']);
        $updateSuccess   = $this->userService->update($mainUser['id'], ['shift_end' => date('Y-m-d H:i:s')]);

        if ($updateSuccess) {
            $this->userAuditTrailService->record(
                $mainUser['id'],
                $userFromAccount->shift_date,
                UserAuditTrailService::ACTION_END_SHIFT
            );
        }

        return redirect()->route('accounts', ['token' => $request->get('token')]);
    }
}