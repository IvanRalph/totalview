<?php

namespace Modules\Base\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Base\Services\TeamSettingService;
use Modules\Base\Services\LobService;
use Modules\Base\Services\RoleService;

class TeamSettingsWebController extends Controller
{
    private $teamSettingService;

    private $lobService;

    public function __construct (TeamSettingService $teamSettingService, LobService $lobService, RoleService $roleService)
    {
        $this->teamSettingService = $teamSettingService;
        $this->lobService         = $lobService;
        $this->roleService        = $roleService;
    }

    public function index ()
    {
        $data['data']['getTeamLeader'] = $this->teamSettingService->getTeamLeader();
        $data['data']['getTeamList']   = $this->teamSettingService->getTeamList();
        $data['data']['getLobList']    = $this->lobService->getLobList();
        $data['data']['getRoleList']   = $this->roleService->getRoleList();

        return view('base::team-settings.index', $data);
    }
}