<?php

namespace Modules\Base\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\Base\Services\DashboardService;
use Modules\Base\Services\LobService;

class DashboardWebController extends Controller
{
    private $dashboardService;

    public function __construct (DashboardService $dashboardService, LobService $lobService)
    {
        $this->dashboardService = $dashboardService;
        $this->lobService       = $lobService;
    }

    public function index ()
    {
        $data['data']['getLobList'] = $this->lobService->getLobList();

        return view('base::dashboard.index', $data);
    }
}