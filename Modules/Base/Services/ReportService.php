<?php
/**
 * Created by PhpStorm.
 * User: OMLanuza
 * Date: 1/17/2018
 * Time: 4:07 PM
 */

namespace Modules\Base\Services;

class ReportService
{
    public function getHourly ($lobId, $dateFrom, $dateTo)
    {
        return $this->db->table('case_activity_log')
            ->select('hour(case_activity_log.created_at) as hour', 'concat(user.lastname, \', \', user.firstname) as Name', 'user.id', 'count(case_activity_log.id) as count')
            ->leftJoin('user', 'user.id', '=', 'case_activity_log.user_id')
            ->where('lob_id', $lobId)
            ->whereBetween('created_at', [
                $dateFrom,
                $dateTo
            ])
            ->groupBy('user.id', 'hour(case_activity_log.created_at)')
            ->get();
    }
}