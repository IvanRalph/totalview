<?php

namespace Modules\Base\Services;

class CaseActivityLogService extends BaseService
{
    public function log ($lobId, $caseId, $userId, $shiftDate, $actionId, $statusId, $createdAt)
    {
        $data = [
            'lob_id'     => $lobId,
            'case_id'    => $caseId,
            'user_id'    => $userId,
            'shift_date' => $shiftDate,
            'action_id'  => $actionId,
            'status_id'  => $statusId,
            'created_at' => $createdAt
        ];

        $this->db->table('case_activity_log')->insert($data);
    }
}