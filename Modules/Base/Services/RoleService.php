<?php

namespace Modules\Base\Services;

class RoleService extends BaseService
{
    public function getRoleList ()
    {
        $res = $this->db->table('role')
            ->get();

        return $res;
    }
}