<?php

namespace Modules\Base\Services;

use DB;

class TeamSettingService
{
    public function __construct ()
    {
        $this->db = \DB::connection('tv_base');
    }

    public function getUserList ()
    {
        $res = $this->db->table('user')
            ->leftJoin('shift_schedule', 'user.main_user_id', '=', 'shift_schedule.user_id')
            ->leftJoin('user_lob', 'user.main_user_id', '=', 'user_lob.user_id')
            ->leftJoin('lob', 'lob.ID', '=', 'user_lob.lob_id')
            ->leftJoin('team', 'team.id', '=', 'user.team')
            ->leftJoin('role', 'role.ID', '=', 'user.role')
            ->leftJoin("tv_main.lob as main_lob", "user_lob.lob_id", '=', "main_lob.id")
            ->select('user.main_user_id as id', \DB::raw("CONCAT(user.firstname,' ',user.lastname) as full_name"),
                \DB::raw("GROUP_CONCAT(main_lob.name) as lob"),
                'team.description', 'shift_schedule.shift_date',
                'shift_schedule.start_shift', 'shift_schedule.end_shift', 'shift_schedule.leave', 'user.status',
                'team.id as teamID',
                \DB::raw("GROUP_CONCAT(user_lob.lob_id) as lob_id"),
                'role.ID as roleID', 'user.ramp')
            ->where('shift_schedule.shift_date', '=', \DB::raw("(SELECT max(shift_schedule.shift_date) FROM shift_schedule)"))
            ->groupBy('user_lob.user_id')
            // ->toSql();
            ->get();

        return $res;
    }

    public function getTeamList ()
    {
        $res = $this->db->table('team')
            ->leftJoin('user', function ($join) {
                $join->where(DB::raw('FIND_IN_SET(user.id, team.lead)'), '>', 0);
            })
            ->select('team.ID', 'team.lead', 'user.username', 'team.description', \DB::raw("GROUP_CONCAT(user.username ORDER BY user.id) as lob"))
            ->groupBy('team.ID')
            ->get();

        return $res;
    }

    public function getTeamLeader ()
    {
        $res = $this->db->table('user')
            ->where('role', '=', 4)
            ->get();

        return $res;
    }

    public function updateTeamList ($editID, $editTeamName, $checkbox)
    {
        $res = $this->db->table('team')
            ->where('ID', $editID)
            ->update([
                'description' => $editTeamName,
                'lead'        => $checkbox
            ]);
    }

    public function updateTeamUser ($user_id, $user_start_shift, $user_end_shift, $onLeave, $teamListPlaceholder, $slctRole, $ramp)
    {
        $res = $this->db->table('user')
            ->where('user.main_user_id', $user_id)
            ->join('shift_schedule', 'user.main_user_id', '=', 'shift_schedule.user_id')
            ->join('user_lob', 'user.main_user_id', '=', 'user_lob.user_id')
            ->join('lob', 'lob.ID', '=', 'user_lob.lob_id')
            ->join('team', 'team.id', '=', 'user.team')
            ->join('role', 'role.ID', '=', 'user.role')
            ->update([
                'shift_schedule.start_shift' => $user_start_shift,
                'shift_schedule.end_shift'   => $user_end_shift,
                'shift_schedule.leave'       => $onLeave,
                'user.team'                  => $teamListPlaceholder,
                'user.role'                  => $slctRole,
                'user.ramp'                  => $ramp,
            ]);
    }

    public function updateTeamSchedule ($id, $start_shift, $end_shift, $on_leave, $ramp)
    {
        $myArrayId = explode(',', $id);

        $res = $this->db->table('shift_schedule')
            ->whereIn('user_id', $myArrayId)
            ->join('user', 'user.main_user_id', '=', 'shift_schedule.user_id')
            ->update([
                'start_shift' => $start_shift,
                'end_shift'   => $end_shift,
                'leave'       => $on_leave,
                'user.ramp'   => $ramp
            ]);
    }

    public function updateTeamBulk ($id, $team)
    {
        $myArrayId = explode(',', $id);

        $res = $this->db->table('user')
            ->whereIn('id', $myArrayId)
            ->update(['team' => $team]);
    }
}