<?php

namespace Modules\Base\Services;

class ListValueService extends BaseService
{
    public function getByLobId ($lobId)
    {
        return $this->db->table('list_value')
            ->where('lob_id', $lobId)
            ->get();
    }
}