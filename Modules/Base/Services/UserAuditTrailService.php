<?php

namespace Modules\Base\Services;

class UserAuditTrailService extends BaseService
{
    const TABLE_NAME = 'user_audit_trail';
    const ACTION_START_BREAK = 'start_break';
    const ACTION_END_BREAK = 'end_break';
    const ACTION_START_CASE = 'start_case';
    const ACTION_END_CASE = 'end_case';
    const ACTION_HOLD_CASE = 'hold_case';
    const ACTION_EDIT_CASE = 'edit_case';
    const ACTION_CONTINUE_CASE = 'continue_case';
    const ACTION_SAVE_EDIT_CASE = 'save_edit_case';
    const ACTION_START_SHIFT = 'start_shift';
    const ACTION_END_SHIFT = 'end_shift';

    public function record ($userId, $shiftDate, $action, $reference = '')
    {
        return $this->db->table(self::TABLE_NAME)
            ->insertGetId([
                'user_id'    => $userId,
                'shift_date' => $shiftDate,
                'action'     => $action,
                'reference'  => $reference
            ]);
    }

    public function getByUserId ($userId, $shiftDate)
    {
        return $this->db->table(self::TABLE_NAME)
            ->where('user_id', $userId)
            ->where('shift_date', $shiftDate)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function display ($trail)
    {
        switch ($trail->action) {
            case self::ACTION_START_SHIFT:
            case self::ACTION_END_SHIFT:
                return $this->shiftDisplay($trail);

            case self::ACTION_START_BREAK:
            case self::ACTION_END_BREAK:
                return $this->breakDisplay($trail);

            case self::ACTION_START_CASE:
            case self::ACTION_END_CASE:
            case self::ACTION_HOLD_CASE:
            case self::ACTION_EDIT_CASE:
            case self::ACTION_SAVE_EDIT_CASE:
            case self::ACTION_CONTINUE_CASE:
                return $this->caseDisplay($trail);
        }
    }

    private function shiftDisplay ($trail)
    {
        $description = 'Started shift';

        if ($trail->action == self::ACTION_END_SHIFT) {
            $description = 'Ended shift';
        }

        return [
            'id'          => $trail->id,
            'icon'        => 'fa-clock-o',
            'icon_bg'     => 'navy-bg',
            'type'        => 'Shift',
            'description' => $description,
            'timestamp'   => $trail->created_at
        ];
    }

    private function breakDisplay ($trail)
    {
        $description = 'Started ';

        if ($trail->action == self::ACTION_END_BREAK) {
            $description = 'Ended ';
        }

        return [
            'id'          => $trail->id,
            'icon'        => 'fa-coffee',
            'icon_bg'     => 'lazur-bg',
            'type'        => 'Aux Break',
            'description' => $description . strtolower($trail->reference),
            'timestamp'   => $trail->created_at
        ];
    }

    private function caseDisplay ($trail)
    {
        $caseAction = 'Started';
        $iconBg     = 'blue-bg';

        switch ($trail->action) {
            case self::ACTION_HOLD_CASE:
                $caseAction = 'Held';
                $iconBg     = 'red-bg';
                break;
            case self::ACTION_END_CASE:
                $caseAction = 'Ended';
                break;
            case self::ACTION_CONTINUE_CASE:
                $caseAction = 'Continued';
                break;
            case self::ACTION_EDIT_CASE:
                $caseAction = 'Started edit for';
                break;
            case self::ACTION_SAVE_EDIT_CASE:
                $caseAction = 'Edit completed for';
        }

        return [
            'id'          => $trail->id,
            'icon'        => 'fa-file-text-o',
            'icon_bg'     => $iconBg,
            'type'        => 'Case',
            'description' => $caseAction . ' case ' . $trail->reference,
            'timestamp'   => $trail->created_at
        ];
    }
}