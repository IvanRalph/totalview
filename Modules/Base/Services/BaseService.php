<?php

namespace Modules\Base\Services;

class BaseService
{
    protected $db;

    public function __construct ()
    {
        $this->db = \DB::connection(config('base.db_connection'));
    }
}