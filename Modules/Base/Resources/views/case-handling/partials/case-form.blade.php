<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Case Form</h5>
        <div class="ibox-tools">
            <button id="btn-aux-break" class="btn btn-primary"
                    href="/{{ session('selected_account')->route }}/aux-break?token={{ app('request')->get('token') }}">
                <i class="fa fa-clock-o"></i> Aux
            </button>
        </div>
    </div>
    <div class="ibox-content">
        <div class="tabs-container">
            <div class="tabs-left">
                <ul id="lob-tab" class="nav nav-tabs">
                    @foreach($userLobs as $key => $lob)
                        @if($key == 0)
                            <?php $active = "active" ?>
                        @else
                            <?php $active = "" ?>
                        @endif

                        <li data-lob-id="{{ $lob->id }}" class="lob-selector {{ $active }}"><a data-toggle="tab"
                                                                                               href="#tab{{ $lob->id }}">{{ $lob->name }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content gray-bg">
                    @foreach($userLobs as $key => $lob)
                        @if($key == 0)
                            <?php $active = "active" ?>
                        @else
                            <?php $active = "" ?>
                        @endif

                        <div id="tab{{ $lob->id }}" class="tab-pane {{ $active }}">
                            <div class="panel-body">
                                <div class="col-lg-6">
                                    <div class="ibox float-e-margins border-left-right border-bottom">
                                        <div class="ibox-title">
                                            <h5>Completed Cases</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <h1 id="completed-case-count-{{ $lob->id }}"
                                                class="no-margins text-info">{{ $trackerStats[$lob->id]['total_completed_case'] }}</h1>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="ibox float-e-margins border-left-right border-bottom">
                                        <div class="ibox-title">
                                            <h5>AHT</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <h1 id="aht-{{ $lob->id }}"
                                                class="no-margins text-info">{{ $trackerStats[$lob->id]['aht'] }}</h1>
                                        </div>
                                    </div>
                                </div>
                                <div id="validation-error-panel-{{ $lob->id }}" class="col-lg-12"
                                     style="display: none;">
                                    <div class="ibox float-e-margins border-left-right border-bottom">
                                        <div class="ibox-title">
                                            <h5><span class="fa fa-exclamation-triangle"></span> Validation Errors </h5>
                                            <div class="ibox-tools">
                                                <a id="close-validation-error-panel" data-lob-id="{{ $lob->id }}">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            <ul id="validation-error-list-{{ $lob->id }}">
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <form id="frm-{{ $lob->id }}" class="form-horizontal">
                                        @foreach($fields[$lob->id] as $field)
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">{{ $field->name }}</label>
                                                @if($field->html_name == 'measure_syntax')
                                                    <div class="col-sm-4">
                                                        {!! create_html_field($field, $listValues[$lob->id]) !!}
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <select id="syntax-outcome" class="form-control">
                                                            <option>Choose Outcome</option>
                                                        </select>
                                                    </div>
                                                @else
                                                    <div class="col-sm-9">
                                                        {!! create_html_field($field, $listValues[$lob->id]) !!}
                                                    </div>
                                                @endif
                                            </div>
                                        @endforeach
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="pull-right">
                                                    <button type="button" actionid="1" lobid="{{ $lob->id }}"
                                                            id="btn-start-{{ $lob->id }}"
                                                            class="btn btn-info btn-start-case">START
                                                    </button>
                                                    <button type="button" actionid="4" lobid="{{ $lob->id }}"
                                                            id="btn-stop-{{ $lob->id }}"
                                                            class="btn btn-primary btn-stop-case" disabled="disabled">
                                                        STOP
                                                    </button>
                                                    <button type="button" actionid="6" lobid="{{ $lob->id }}"
                                                            id="btn-stop-{{ $lob->id }}"
                                                            class="btn btn-warning btn-hold-case" disabled="disabled">
                                                        HOLD
                                                    </button>
                                                    @if ($lobSettings[$lob->id]->allow_escalate)
                                                        <button type="button" actionid="5" lobid="{{ $lob->id }}"
                                                                id="btn-stop-{{ $lob->id }}"
                                                                class="btn btn-danger btn-escalate-case"
                                                                disabled="disabled">ESCALATE
                                                        </button>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" id="case-id" name="case_id" value="">
                                        <input type="hidden" id="duration" name="duration" value="">
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
