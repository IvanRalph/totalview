@extends('base::layouts.master')

@section('content')
    <div class="wrapper wrapper-content">
        <div class="col-lg-8">
            <div class="row">
                @include('base::dashboard.partials.metrics')
            </div>
            <div class="row">
                @include('base::dashboard.partials.nurses-productivity-table', $data)
            </div>
        </div>
        <div class="col-lg-4">
            <div class="row">
                @include('base::dashboard.partials.nurse-audit-trail')
            </div>
        </div>
    </div>
@stop

@section('module-scripts')
    {!! Html::script('js/module/base/constant.js') !!}
    {!! Html::script('js/module/base/dashboard.js') !!}
@stop
