@extends('base::layouts.master')

@section('content')

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="tabs-container">

                    <div class="tab-content">
                        <div class="tab-pane active">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h2>Reports</h2>
                                </div>
                                <div class="ibox-content">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a data-toggle="tab" href="#rawReports">
                                                <i class="fa fa-group"></i>Raw Reports
                                            </a>
                                        </li>
                                        <li class="" style="display: none">
                                            <a data-toggle="tab" href="#productivityReports">
                                                <i class="fa fa-user"></i>Productivity Reports
                                            </a>
                                        </li>
                                    </ul>
                                    <p></p>

                                    <div class="tab-content">
                                        <div id="rawReports" class="tab-pane active">
                                            <div class="full-height-scroll">
                                                <div class="row clearfix">
                                                    <div class="col-md-3 col-xs-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel-body">
                                                                <form id="frmRawReports" name="frmRawReports"
                                                                      class="form" method="get">
                                                                    <div class="form-group">
                                                                        <label class="control-label">LOB:</label>
                                                                        <select id="slct-lob-id" class="form-control"
                                                                                name="rawNumber" required="required">
                                                                            @foreach($getLobList as $lobList)
                                                                                <option value="{{ $lobList->id }}">{{ $lobList->name }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>


                                                                    <div class="form-group">
                                                                        <label class="control-label">Select Time
                                                                                                     Zone:</label>
                                                                        <select id="slct-lob-id" class="form-control"
                                                                                name="timezone">
                                                                            <option value="0">Philippine Time</option>
                                                                            <option value="1">Central Standard Time
                                                                                              (North America)
                                                                            </option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label">Shift Date
                                                                                                     Range</label>
                                                                        <div class="input-daterange input-group"
                                                                             id="datepickerForProductivity">
                                                                            <input type="text"
                                                                                   class="input-sm form-control"
                                                                                   required="required" name="start"
                                                                                   value=""
                                                                                   placeholder="YYYY-MM-DD">
                                                                            <span class="input-group-addon">to</span>
                                                                            <input type="text"
                                                                                   class="input-sm form-control"
                                                                                   required="required" name="end"
                                                                                   value=""
                                                                                   placeholder="YYYY-MM-DD">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group clearfix">
                                                                        <button type="submit" id="submitBtn"
                                                                                class="btn btn-md btn-primary col-md-12 col-xs-12">
                                                                            Generate
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-9 col-xs-12">
                                                        <div class="table-responsive">
                                                            <table id="reports-table"
                                                                   class="table table-striped table-bordered table-hover"
                                                                   cellspacing="0">
                                                                <thead>
                                                                <tr role="row">
                                                                    <th>Shift Date</th>
                                                                    <th>User ID</th>
                                                                    <th>Status</th>
                                                                    <th>Duration</th>
                                                                    <th>Attachment</th>
                                                                    <th>Ref Num</th>
                                                                    <th>Text</th>
                                                                    <th>Dropdown</th>
                                                                    <th>Created</th>
                                                                    <th>Updated</th>
                                                                </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="productivityReports" class="tab-pane" style="display: none">
                                            <div class="full-height-scroll">
                                                <div class="row clearfix">
                                                    <div class="col-md-3 col-xs-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel-body">
                                                                <form id="frmProdReportsSummary"
                                                                      name="frmProdReportsSummary" class="form"
                                                                      method="get">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Date Range</label>
                                                                        <div class="input-daterange input-group"
                                                                             id="datepickerForProductivity">
                                                                            <input type="text"
                                                                                   class="input-sm form-control"
                                                                                   required="required" name="start"
                                                                                   value=""
                                                                                   placeholder="YYYY-MM-DD">
                                                                            <span class="input-group-addon">to</span>
                                                                            <input type="text"
                                                                                   class="input-sm form-control"
                                                                                   required="required" name="end"
                                                                                   value=""
                                                                                   placeholder="YYYY-MM-DD">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group clearfix">
                                                                        <button type="submit" id="submitBtn"
                                                                                class="btn btn-md btn-primary col-md-12 col-xs-12">
                                                                            Generate
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-9 col-xs-12">
                                                        <h1>wala pa daw</h1>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('module-scripts')
    {!! Html::script('js/module/base/constant.js') !!}
    {!! Html::script('js/module/base/reports.js') !!}
@stop