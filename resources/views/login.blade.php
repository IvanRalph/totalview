<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png/ico" href="/images/favicon.ico">
    <title>Total View</title>
    {!! Html::style('css/lib/all.css') !!}
</head>

<body class="skin-md pace-done">

    <div id="wrapper">
        <div class="middle-box text-center loginscreen animated fadeInDown">
            <div class="m-l m-r">
                <div class="m-t-lg">
                    <img alt="TotalView" class="img login-total-view-logo" src="/images/totalview_logo.png">
                </div>
                <h3>Welcome to TotalView</h3>
                <p class="login-sub-header"> Please use your NT credentials to Login.</p>
                <form class="m-l m-r" method="POST" action="{{ url('auth') }}">
                    @if(Session::has('login_error'))
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <b>{{ Session::get('login_error') }}</b>
                        </div>
                    @endif
                    <div class="form-group">
                        <input id="username" name="username" value="" autocomplete="off" class="form-control col-md-" placeholder="Username" type="text">
                    </div>
                    <div class="form-group">
                        <input id="password" name="password" value="" autocomplete="off" class="form-control" placeholder="Password" type="password">
                    </div>
                    <div class="form-group">
                        <button id="login-button" class="btn btn-primary block full-width m-b" type="submit">Login</button>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>
                <p>
                    <small class="login-footer">SWHealth TotalView All Rights Reserved &copy; 2017</small>
                </p>
                <br>
            </div>
        </div>

        @include('layouts.footer')
    </div>

{!! Html::script('js/lib/all.js') !!}
</body>
</html>