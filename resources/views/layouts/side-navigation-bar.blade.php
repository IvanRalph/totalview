<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse" style="">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="profile-element">
                    <span>
                        <img alt="image" class="img total-view-logo" src="/images/totalview_logo.png"/>
                     </span>
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold bindFullName"></strong>
                            </span>
                            <span class="text-muted text-xs block"></span>
                        </span>
                    </a>
                </div>
                <div class="logo-element">
                    <img alt="image" class="mini-sidelogo"
                         src="/images/tv_icon.png"/>
                </div>
            </li>
            @section('side-navigation-menu')@show
        </ul>
    </div>
</nav>
