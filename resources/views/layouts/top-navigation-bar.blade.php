<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li class="right-sidebar-toggle">
                <a href="/accounts{{ '?token='.app('request')->get('token') }}">
                    <i class="fa fa-exchange"></i> Switch Account
                </a>
            </li>
        </ul>
    </nav>
</div>