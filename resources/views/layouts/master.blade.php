<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png/ico" href="/images/favicon.ico">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Total View</title>

    {!! Html::style('css/lib/all.css') !!}
    {!! Html::style('css/lib/select2.min.css') !!}
    {!! Html::style('css/lib/sweetalert.min.css') !!}

    @section('module-styles')@show
</head>

<body>
<div id="wrapper">
    {!! csrf_field() !!}
    @include('layouts.side-navigation-bar')

    <div id="page-wrapper" class="gray-bg">
        @include('layouts.top-navigation-bar')

        @section('content')@show

        @include('layouts.footer')
    </div>
</div>


{!! Html::script('js/lib/all.js') !!}
{!! Html::script('js/lib/moment-timezone-with-data.js') !!}
{!! Html::script('js/lib/sweetalert.min.js')!!}

{!! Html::script('js/lib/select2.full.min.js') !!}
{!! Html::script('js/lib/jquery.bootstrap-touchspin.js') !!}

{!! Html::script('js/local-storage-handler.js') !!}
{!! Html::script('js/spinners.js') !!}
{!! Html::script('js/side-navigation.js') !!}
{!! Html::script('js/http-request.js') !!}
{!! Html::script('js/routes.js') !!}
{!! Html::script('js/response-handler.js') !!}
{!! Html::script('js/schema-builder.js') !!}
{!! Html::script('js/commons.js') !!}



<!-- {!! Html::script('js/auth.js') !!} -->

@section('module-scripts')@show
{!! Html::script('js/token-expired.js') !!}

</body>
</html>