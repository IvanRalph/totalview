<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png/ico" href="/images/favicon.ico">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Total View</title>
    {!! Html::style('css/lib/all.css') !!}
</head>

<body class="skin-md">
    <div id="wrapper gray-bg">
        {!! csrf_field() !!}

        <nav class="navbar white-bg navbar-static-top" role="navigation" style="margin-bottom: 0">
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="{{ url('logout?token='.app('request')->get('token')) }}">
                        <i class="fa fa-sign-out"></i> Log Out
                    </a>
                </li>
            </ul>
        </nav>

        <div class="row" style="margin-top: 20px;">
            <div class="col-lg-4 col-lg-offset-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3 class="text-center">Choose Account</h3>
                    </div>
                    <div class="ibox-content">
                        @foreach($userData['account'] as $account)
                            <a class="btn btn-block btn-default" href="{{ url('select-account?token='.app('request')->get('token'), ['account_id' => $account['id']]) }}">
                                <h2 class="inline">{{ $account['name'] }}</h2>
                            </a>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>
    @include('layouts.footer')
</body>
</html>